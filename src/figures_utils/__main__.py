#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch the creation of figures saying if exons \
regulated by a splicing factor are more often co-localized than randomly \
expected
"""

from . exons_interactions import launch_figures_creation
from . exons_interactions import get_info_from_database
import sqlite3
from .config_figures import Config


def launcher():
    """
     Launch the creation of figures saying if exons \
    regulated by a splicing factor are more often co-localized than randomly \
    expected
    """
    list_sf = get_info_from_database(sqlite3.connect(Config.db_file),
                                     """SELECT sf_name
                                        FROM cin_project_splicing_lore""")
    list_sf = list(set(list_sf))

    for sf in list_sf:
        launch_figures_creation(sf)


launcher()
