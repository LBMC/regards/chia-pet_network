#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Produce figures with:
- the percentage of exons down regulated (by the studied SF) in interaction
- the percentage of exons up regulated (by the studied SF) in interaction
- the percentage of exons control in interaction (in regards with the number of
exons down and up regulated by the studied SF)
"""

import sqlite3
from .config_figures import Config
import seaborn as sns
import random
import pandas as pd
import doctest
from typing import List, Tuple, Dict
import statistics
import matplotlib.pyplot as plt
from datetime import datetime
from numpy import mean


# ########################GET DATA FROM THE DATABASE#########################
def get_info_from_database(cnx: sqlite3.Connection, query: str) -> list:
    """
    Get data from the database, for example:
    - the ID of all fasterdb exons, e.g: ['9995_6', '9995_7',
    '9996_1', '9996_10']
    - the ID of all ChIA-PET experiments, e.g: ['GSM1327093',
    'GSM1327094', 'GSM1436259']

    :param cnx: connexion to the ChIA-PET database
    :param query: the SQL query that allows us to get data from the database
    :return list_data: the list with the data obtained, e.g: ['9995_6',
    '9995_7', '9996_1', '9996_10']

    >>> get_info_from_database(sqlite3.connect(Config.db_file),
    ... 'SELECT id_sample FROM cin_projects LIMIT 2')
    ['GSM1018961_GSM1018962', 'GSM1018963_GSM1018964']
    """
    cursor = cnx.cursor()
    cursor.execute(query)
    res = cursor.fetchall()
    list_data = []
    for data in res:
        list_data += list(data)
    return list_data


# ####################GET EXONS DOWN OR UP REGULATED BY SFs####################
def get_projects_links_to_a_splicing_factor(cnx: sqlite3.Connection,
                                            sf_name: str) -> list:
    """
    Get the ID of every projects corresponding to a particular splicing factor.

    :param cnx: connexion to the ChIA-PET database
    :param sf_name: the splicing factor name, e.g: PTBP1
    :return id_project: a list of id_project (table cin_project_splicing_lore),
    corresponding to a particular splicing factor. E.g: [7, 30, 96, 135]
    """
    cursor = cnx.cursor()
    query = """SELECT id
               FROM cin_project_splicing_lore
               WHERE sf_name = ?"""
    cursor.execute(query, (sf_name,))
    res = cursor.fetchall()

    id_project = [val[0] for val in res if val[0] not in [139, 13, 164]]
    return id_project


def get_ase_events(cnx: sqlite3.Connection, id_project: str) -> List:
    """
    Get every exons regulated (down or up) according to a particular project.

    :param cnx: connexion to the ChIA-PET database
    :param id_project: a project ID of the table cin_project_splicing_lore
    :return nres: each sublist corresponds to an exon (exon_regulation +
    gene_id + exon_position on gene), e.g: ['down', 18673, '18673_17']
    """
    cursor = cnx.cursor()
    query = """SELECT delta_psi, gene_id, exon_id
               FROM cin_ase_event
               WHERE id_project = ?
               AND (delta_psi >= 0.1 OR delta_psi <= -0.1)
               AND pvalue_glm_cor <= 0.05"""
    cursor.execute(query, (id_project,))
    res = cursor.fetchall()

    if len(res) == 0:
        query = """SELECT delta_psi, gene_id, exon_id
                       FROM cin_ase_event
                       WHERE id_project = ?
                       AND (delta_psi >= 0.1 OR delta_psi <= -0.1)
                       AND pvalue <= 0.05"""
        cursor.execute(query, (id_project,))
        res = cursor.fetchall()

    nres = []
    for exon in res:
        nexon = list(exon[1:3])
        if exon[0] < 0:
            nexon = ["down"] + nexon
        else:
            nexon = ["up"] + nexon
        nres.append(nexon)
    return nres


def washing_events(exon_list: List) -> List:
    """
    Remove redundant exons or remove exons showing different regulation.

    :param exon_list: each sublist corresponds to an exon (exon_regulation +
    gene_id + exon_position on gene), e.g: ['down', 18673, '18673_17']
    Every exon regulated by a splicing factor in different projects.
    :return new_exon_list: each sublist corresponds to an exon (exon_regulation
    + gene_id + exon_position on gene), e.g: ['down', '18962', '18962', '14'].
    Every exon regulated by a splicing factor in different projects without
    redundancy.
    """
    replace_dic = {"up": "down", "down": "up"}
    dic = {}
    prefix_list = []
    for exon in exon_list:
        exon_prefix = "%s_%s" % (exon[1], exon[2])
        exon_name = "%s_%s" % (exon[0], exon_prefix)
        if exon_name not in dic:
            if exon_prefix not in prefix_list:
                dic[exon_name] = 1
                prefix_list.append(exon_prefix)
            else:
                reverse_name = exon_name.replace(exon[0], replace_dic[exon[0]])
                if reverse_name in dic:
                    del(dic[reverse_name])
                # Else : the exon was deleted before because of a different
                # regulation
        else:
            dic[exon_name] += 1

    # Creation of the new list of exons
    new_exon_list = []
    for key in dic:
        my_exon = key.split("_")
        new_exon_list.append(my_exon)
    return new_exon_list


def get_every_events_4_a_sl(cnx: sqlite3.Connection, sf_name: str,
                            regulation: str) -> Tuple:
    """
    Get every splicing events for a given splicing factor.

    :param cnx: connexion to the ChIA-PET database
    :param sf_name: the splicing factor name, e.g: PTBP1
    :param regulation: up or down
    :return sf_reg: a dictionary with a list of regulated exons depending on a
    splicing factor and its regulation, e.g: {'PTBP1_up': ['345_38', '681_2',
    '781_4', '1090_16', '1291_12']}
    :return number_exons: a str which is the concatenation of the sf_name, the
    regulation and the number of exons regulated by this SF according to the
    type of regulation, e.g: HNRNPC_down_2482.
    """
    exons_list = []
    id_projects = get_projects_links_to_a_splicing_factor(cnx, sf_name)
    for id_project in id_projects:
        ase_event = get_ase_events(cnx, id_project)
        exons_list += ase_event

    washed_exon_list = washing_events(exons_list)
    if regulation in ["up", "down"]:
        reg_exon_list = [
            exon[2] + "_" + exon[3]
            for exon in washed_exon_list
            if exon[0] == regulation
        ]
    else:
        reg_exon_list = [
            exon[2] + "_" + exon[3]
            for exon in washed_exon_list
        ]

    sf_reg = {sf_name + "_" + regulation: reg_exon_list}
    number_exons = sf_name + "_" + regulation + "_" + str(len(reg_exon_list))
    return sf_reg, number_exons


# ##########GET ALL EXONS IN INTERACTION (for all ChIA-PET datasets)###########
def get_co_localisation_by_project(cnx: sqlite3.Connection, id_project: str) \
        -> List[Tuple[str, str]]:
    """
    Allows to retrieve in the database the pairs of interacting exons,
    according to a ChIA-PET dataset.

    :param cnx: connexion to the ChIA-PET database
    :param id_project: the ID of the ENCODE or GEO project that will allow us
    to obtain the list of exons in interactions, e.g: GSM1517080.
    :return res: the exons co-localized 2 by 2, e.g: [('9992_3', '9992_4'),
    ('9992_5', '9992_8')]
    """
    cursor = cnx.cursor()
    query = """SELECT exon1, exon2
               FROM cin_exon_interaction
               WHERE id_project = ?"""
    cursor.execute(query, (id_project,))
    res = cursor.fetchall()
    cursor.close()
    return res


def get_colocalized_exons(interaction: List[Tuple[str, str]], id_project: str
                          ) -> Dict[str, List]:
    """
    Obtain a dictionary, with for each exon the list of exons with which it
    interacts, e.g: '9843_10.GSM1234': ['9843_2', '9843_3', '9843_4'],
    according to a ChIA-PET dataset.

    :param interaction: the exons co-localized 2 by 2, e.g: [('9992_3',
    '9992_4'), ('9992_5', '9992_8')], see get_co_localisation_by_project().
    :param id_project: the ID of the ENCODE or GEO project that will allow us
    to obtain the list of exons in interactions, e.g: GSM1517080.
    :return exons_interactions: a dictionary, with for each exon the list of
    exons with which it interacts, e.g: '9843_10.GSM1234': ['9843_2', '9843_3',
    '9843_4'], according to a ChIA-PET dataset.

    >>> interaction = [('1_1', '2_1'), ('1_1', '3_1'), ('3_1', '2_1')]
    >>> res = {'1_1.test': ['2_1', '3_1'], '2_1.test': ['1_1', '3_1'],
    ... '3_1.test': ['1_1', '2_1']}
    >>> get_colocalized_exons(interaction, id_project = 'test') == res
    True
    """
    exons_interactions = {}
    for key, value in interaction:
        for k, v in [(key, value), (value, key)]:
            k = k + "." + id_project
            if k not in exons_interactions:
                exons_interactions[k] = [v]
            else:
                exons_interactions[k].append(v)
    return exons_interactions


def get_exons_in_interaction(cnx: sqlite3.Connection, id_project: str) -> Dict:
    """
    Allows:
    - to launch the get_co_localisation_by_project() function, so to retrieve
    in the database the pairs of interacting exons, according to a ChIA-PET
    dataset.
    - to launch the get_colocalized_exons() function, so to obtain a
    dictionary, with for each exon the list of exons with which it interacts,
    e.g: '9843_10.GSM1234': ['9843_2', '9843_3', '9843_4'], according to a
    ChIA-PET dataset.

    :param cnx: connexion to the ChIA-PET database
    :param id_project: the ID of the ENCODE or GEO project that will allow us
    to obtain the list of exons in interactions, e.g: GSM1517080.
    :return exons_interactions: a dictionary, with for each exon the list of
    exons with which it interacts, e.g: '9843_10.GSM1234': ['9843_2', '9843_3',
    '9843_4'], according to a ChIA-PET dataset.
    """
    interaction = get_co_localisation_by_project(cnx, id_project)
    return get_colocalized_exons(interaction, id_project)


def combine_project_dictionary(projects_dictionary: Dict[str, List]):
    """
    See get_every_exons_in_interaction() - part 2.

    :param projects_dictionary: a total dictionnary, in which we have for each
    dataset of ChIA-PET, a dictionary, with for each exon the list of exons
    with which it interacts, e.g: '9843_10.GSM1234': ['9843_2', '9843_3',
    '9843_4'].
    :return final_exons_interactions: a dictionary with for each exon the
    complete list of exons with which it interacts, for all the datasets that
    we have, e.g: '4448_11': ['4448_20'], '4857_5': ['4878_1', '494_6'].

    >>> res = {'1_1.test': ['2_1', '3_1'], '2_1.test': ['1_1', '3_1'],
    ... '3_1.test': ['1_1', '2_1'], '1_1.test2': ['2_1', '3_1', '4_1'],
    ... '2_1.test2': ['1_1', '6_1'], '3_1.test2': ['1_1', '2_1'],
    ... '51_1.test3': ['17_1']}
    >>> expected = {'1_1': ['2_1', '3_1', '2_1', '3_1', '4_1'],
    ... '2_1': ['1_1', '3_1', '1_1', '6_1'],
    ... '3_1': ['1_1', '2_1', '1_1', '2_1'],
    ... '51_1': ['17_1']}
    >>> combine_project_dictionary(res) == expected
    True
    """
    final_exons_interactions = {}
    for key, value in projects_dictionary.items():
        key = key.split(".")[0]
        if key not in final_exons_interactions:
            final_exons_interactions[key] = value
        else:
            final_exons_interactions[key] += value
    return final_exons_interactions


def get_every_exons_in_interaction() -> Dict[str, List]:
    """
    Allows to:
    - obtain for each dataset of ChIA-PET, a dictionary, with for each exon
    the list of exons with which it interacts, e.g: '9843_10.GSM1234':
    ['9843_2', '9843_3', '9843_4']. All dictionaries are stored in one total
    dictionary: dict_interaction, see get_exons_in_interaction().
    - to launch the combine_project_dictionary() function: from the total
    dictionary, we obtain for an exon the complete list of exons with which it
    interacts, not only for one ChIA-PET dataset, but for all the datasets that
    we have, e.g: '4448_11': ['4448_20'], '4857_5': ['4878_1', '494_6'].

    :return: a dictionary with for each exon the complete list of exons with
    which it interacts, for all the datasets that we have, e.g: '4448_11':
    ['4448_20'], '4857_5': ['4878_1', '494_6'].
    """
    dict_interaction = {}
    id_chia_pets = get_info_from_database(sqlite3.connect(Config.db_file),
                                          """SELECT id_sample FROM
                                          cin_projects""")
    for id_chia_pet in id_chia_pets:
        inter = get_exons_in_interaction(sqlite3.connect(Config.db_file),
                                         id_chia_pet)
        dict_interaction.update(inter)
    return combine_project_dictionary(dict_interaction)


# ################FIND THE PERCENTAGE OF EXONS IN INTERACTION##################
def find_exons_in_interaction(final_exons_interactions: Dict,
                              list_exons: List) -> int:
    """
    - For each exon of the list to study, it allows to obtain the complete list
    of exons with which it interacts, according to all the datasets that we
    have.
    - Then for each exon studied, we look in the complete list of exons with
    which it interacts, if at least one of the exons is also a exon studied.
    And so we count how many exons of the list to study are in interaction.

    :param final_exons_interactions: a dictionary with for each exon the
    complete list of exons with which it interacts, for all the datasets that
    we have, e.g: '4448_11': ['4448_20'], '4857_5': ['4878_1', '494_6'], see
    get_every_exons_in_interaction().
    :param list_exons: the list of exons that we want to study in order to
    determine how many of them are in interaction.
    :return count: the number of exons studied in interaction with at least one
    other exon studied. So, the number of exons studied in interaction.

    >>> inter_dic = {'1_1': ['2_1', '3_1'],
    ... '3_1': ['2_1', '1_1'],
    ... '5_1': ['1_1', '6_1']}
    >>> list_exons_ctrl = ['1_1', '3_1', '5_1']
    >>> find_exons_in_interaction(inter_dic, list_exons_ctrl)
    3
    >>> list_exons_ctrl = ['6_1', '1_1', '3_1']
    >>> find_exons_in_interaction(inter_dic, list_exons_ctrl)
    2
    >>> inter_dic = {'1_1': ['2_1', '3_1'],
    ... '3_1': ['7_1', '19_1'],
    ... '5_1': ['17_1', '6_1']}
    >>> find_exons_in_interaction(inter_dic, list_exons_ctrl)
    1
    """
    count = 0
    for exon in list_exons:
        tmp = [e for e in list_exons if e != exon]
        try:
            for iexon in final_exons_interactions[exon]:
                if iexon in tmp:
                    count += 1
                    break
        except KeyError:
            continue
    return count


# ############FIND THE PERCENTAGE OF CONTROL EXONS IN INTERACTION#############
def interactions_ctrl(final_exons_interactions: Dict[str, List], sf_reg: Dict,
                      number_exons_ctrl: str) -> tuple:
    """
    Allows to:
    - Generate a list of exons control of size identical to that of the
    SF studied according to its regulation (down or up). This list does not
    contain exons regulated by the SF and the regulation studied.
    - This exons control list is randomly generated 200 times. For each time,
    we determine the percentage of exons control in interaction with at least
    one other exon control. Then we average the 200 percentages obtained.

    :param final_exons_interactions: a dictionary with for each exon the
    complete list of exons with which it interacts, for all the datasets that
    we have, e.g: '4448_11': ['4448_20'], '4857_5': ['4878_1', '494_6'], see
    get_every_exons_in_interaction().
    :param sf_reg: a dictionary with a list of regulated exons depending on a
    splicing factor and its regulation, e.g: {'PTBP1_up': ['345_38', '681_2',
    '781_4', '1090_16', '1291_12']}, see get_every_events_4_a_sl().
    :param number_exons_ctrl: a str which is the concatenation of the sf_name,
    the regulation and the number of exons regulated by this SF according to
    the type of regulation, e.g: HNRNPC_down_2482, see
    get_every_events_4_a_sl()
    :return rounded_average_percent_ctrl: is the percentage of exons control in
    interaction with at least one other exon control. This percentage is
    calculated for the 200 exons control list, and then we average it.
    :return list_percentage_ctrl: is a list with the 200 percentages, used to
    calculate average_percent_ctrl.
    """
    list_exons = get_info_from_database(sqlite3.connect(Config.db_file),
                                        """SELECT id FROM cin_exon""")

    number_exons_ctrl = int(number_exons_ctrl.split("_")[-1])

    for key, value in sf_reg.items():
        for elmt in value:
            list_exons.remove(elmt)

    list_percentage_ctrl = []
    for _ in range(Config.draw_number):
        print(_)
        list_exons_ctrl = random.choices(list_exons, k=number_exons_ctrl)
        nb_exons_ctrl_int = find_exons_in_interaction(final_exons_interactions,
                                                      list_exons_ctrl)
        percentage_ctrl = (nb_exons_ctrl_int / number_exons_ctrl) * 100
        rounded_percentage_ctrl = round(percentage_ctrl, 2)
        list_percentage_ctrl.append(rounded_percentage_ctrl)

    average_percent_ctrl = sum(list_percentage_ctrl) / \
        len(list_percentage_ctrl)
    rounded_average_percent_ctrl = round(average_percent_ctrl, 2)
    return rounded_average_percent_ctrl, list_percentage_ctrl


# ###############FIND THE PERCENTAGE OF SF EXONS IN INTERACTION################
def interactions_sf(final_exons_interactions: Dict, sf_reg: Dict,
                    number_exons_sf: str) -> float:
    """
    - For each exon (down and in a second time up) regulated by the SF of
    interest, it allows to obtain the complete list of exons with which it
    interacts, according to all the datasets that we have, see
    find_exons_in_interaction().
    - Then for each exon (down and in a second time up) regulated by the SF of 
    interest, we look in the complete list of exons with which it interacts, 
    if at least one of the exons is also regulated by the same SF, in the same 
    way, see find_exons_in_interaction().
    - Finally, we determine the percentage of exons (down and in a second time
    up) regulated by the SF of interest in interaction with at least one other
    exon regulated by the same SF, in the same way.

    :param final_exons_interactions: a dictionary with for each exon the
    complete list of exons with which it interacts, for all the datasets that
    we have, e.g: '4448_11': ['4448_20'], '4857_5': ['4878_1', '494_6'], see
    get_every_exons_in_interaction().
    :param sf_reg: a dictionary with a list of regulated exons depending on a
    splicing factor and its regulation, e.g: {'PTBP1_up': ['345_38', '681_2',
    '781_4', '1090_16', '1291_12']}, see get_every_events_4_a_sl().
    :param number_exons_sf: a str which is the concatenation of the sf_name,
    the regulation and the number of exons regulated by this SF according to
    the type of regulation, e.g: HNRNPC_down_2482, see
    get_every_events_4_a_sl()
    :return rounded_percentage_sf: the percentage of exons (down and in a 
    second time up) regulated by the SF of interest in interaction with at 
    least one other exon regulated by the same SF, in the same way.

    >>> inter_dic = {'1_1': ['2_1', '3_1'],
    ... '3_1': ['2_1', '1_1'],
    ... '5_1': ['1_1', '6_1']}
    >>> list_exons_ctrl = {'SRSF1_down': ['1_1', '3_1', '5_1']}
    >>> interactions_sf(inter_dic, list_exons_ctrl, 'SRSF1_down_3')
    100.0
    >>> inter_dic = {'1_1': ['2_1', '3_1'],
    ... '3_1': ['2_1', '1_1'],
    ... '5_1': ['96_7', '6_1']}
    >>> list_exons_ctrl = {'SRSF1_down': ['1_1', '3_1', '5_1']}
    >>> interactions_sf(inter_dic, list_exons_ctrl, 'SRSF1_down_3')
    66.67
    >>> inter_dic = {'1_1': ['2_1', '3_1'],
    ... '3_1': ['2_1', '14_18'],
    ... '5_1': ['96_7', '6_1']}
    >>> list_exons_ctrl = {'SRSF1_down': ['1_1', '3_1', '5_1']}
    >>> interactions_sf(inter_dic, list_exons_ctrl, 'SRSF1_down_3')
    33.33
    """
    nb_exons_sf_int = find_exons_in_interaction(final_exons_interactions,
                                                sf_reg[list(sf_reg.keys())[0]])
    number_exons_sf = int(number_exons_sf.split("_")[-1])
    percentage_sf = (nb_exons_sf_int / number_exons_sf) * 100
    rounded_percentage_sf = round(percentage_sf, 2)
    return rounded_percentage_sf


def launch_figures_creation(sf: str):
    """
    Main function to create figure, with:
    - the percentage of exons (down and up) regulated by the SF of interest in
    interaction with at least one other exon regulated by the same SF, in the
    same way.
    - the percentage of exons control in interaction with at least one other
    exon control. This percentage is calculated for the 200 exons control list,
    and then we average it. This manipulation is carried out twice, depending
    on the number of down and up regulated exons.
    """
    print(sf)
    print(datetime.now())
    final_exons_interactions = get_every_exons_in_interaction()

    # SF_DOWN
    sf_reg_do, number_exons_do = get_every_events_4_a_sl(sqlite3.connect(
        Config.db_file), sf, "down")
    rounded_percent_sf_do = interactions_sf(final_exons_interactions,
                                            sf_reg_do,
                                            number_exons_do)
    # CTRL_DOWN
    average_percent_ctrl_do, list_percent_ctrl_do = interactions_ctrl(
        final_exons_interactions, sf_reg_do, number_exons_do)

    # SF_UP
    sf_reg_up, number_exons_up = get_every_events_4_a_sl(sqlite3.connect(
        Config.db_file), sf, "up")
    rounded_percent_sf_up = interactions_sf(final_exons_interactions,
                                            sf_reg_up,
                                            number_exons_up)
    # CTRL_UP
    average_percent_ctrl_up, list_percent_ctrl_up = interactions_ctrl(
        final_exons_interactions, sf_reg_up, number_exons_up)

    # CREATE FIGURE
    data = {"Exons groups": [f"{sf} down"] + ["Control down"] * len(
        list_percent_ctrl_do) + [f"{sf} up"] + ["Control up"] * len(
        list_percent_ctrl_up),
            "Interacting exons (%)":
            [rounded_percent_sf_do] + list_percent_ctrl_do +
            [rounded_percent_sf_up] + list_percent_ctrl_up}
    df = pd.DataFrame(data)

    sns.barplot(data=df, x="Exons groups", y="Interacting exons (%)",
                palette=["blue", "grey", "red", "silver"],
                estimator=mean, ci="sd")
    plt.title(f"{sf} - All ChIA-PET datasets")

    Config.figures_all_chia_pet_datasets.mkdir(exist_ok=True, parents=True)
    plt.savefig(Config.figures_all_chia_pet_datasets /
                f"{sf}_all_chia_pet_datasets.png")
    plt.clf()
    plt.close()
    print(datetime.now())


if __name__ == "__main__":
    doctest.testmod()
