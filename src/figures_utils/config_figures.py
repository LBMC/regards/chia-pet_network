#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: Configuration class
"""


from pathlib import Path
from typing import List
from itertools import filterfalse
from doctest import testmod
import re
import sqlite3


def get_p(x: str):
    """
    return False if x contains 'Keep' True else

    >>> get_p('Keep: GMX32, GSM78')
    False
    >>> get_p('Delete: Bou, foo, bar')
    True
    """
    if "Keep" in x:
        return False
    else:
        return True


def get_good_project(cell: str = "ALL") -> List[str]:
    """
    Return only good ChIA-PET projects base on TAD co-localisation results

    :parm cell: The list of cell to filter
    :return: The list of project to keep

    >>> res = get_good_project()
    >>> type(res)
    <class 'list'>
    >>> len(res) == 23
    True
    >>> "GSM1018963_GSM1018964" in res
    True
    >>> res[0:2]
    ['GSM1018963_GSM1018964', 'GSM1018961_GSM1018962']
    >>> pat = re.compile(r"[GSM0-9_]+")
    >>> sum([re.findall(pat, c)[0] == c for c in res]) == len(res)
    True
    >>> get_good_project("K562")
    ['GSM832464', 'GSM832465', 'GSM970213']
    """

    with Config.file_datasets_filtering.open('r') as f:
        line = next(filterfalse(get_p, f)).replace("Keep: ", "").\
            replace('\n', '')
    if cell == "ALL":
        return line.split(', ')
    line = "'" + line.replace(", ", "', '") + "'"
    query = f"""SELECT id_sample
                FROM cin_projects
                WHERE id_sample IN ({line})
                AND cell_line = '{cell}'"""
    cnx = sqlite3.connect(Config.db_file)
    c = cnx.cursor()
    c.execute(query)
    return [r[0] for r in c.fetchall()]


class Config:
    """
    A class containing every parameters used in the submodule figures_utils
    """
    results = Path(__file__).parents[2] / "results"
    figures_all_chia_pet_datasets = results / "figures_all_chia_pet_datasets"
    figures_all_chia_pet_datasets_2 = results / "figures_all_chia_pet_" \
                                                "datasets_after_filtering"
    figures_by_chia_pet_dataset = results / "figures_by_chia_pet_dataset"
    db_file = results / 'chia_pet_database.db'
    draw_number = 1000
    file_datasets_filtering = results / "projects_filtering.txt"
    venn_tf = results / "Venn_common_tf_projects"


if __name__ == "__main__":
    testmod()