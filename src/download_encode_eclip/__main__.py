#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launcher that will download all eclip experiment file.
"""

from .get_encode_clips import download_eclip
from .config import ConfigEncodeClip


def get_eclips():
    download_eclip(ConfigEncodeClip.output)


get_eclips()
