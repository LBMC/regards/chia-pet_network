#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Merge clip dataset by factor at replicate, cell or factor level.
"""


from .config import ConfigEncodeClip
from pathlib import Path
from typing import List, Tuple, Dict
import subprocess as sp
import lazyparser as lp
from .create_gene_bed import create_gene_bed


def load_clip_files(assay_name: List[str] = "eclip") -> List[Path]:
    """
    Recover every experiment of `assay_name`from the encode download files.

    :param assay_name: Name of an experiment
    :return: The list of files corresponding to this assay

    >>> load_clip_files(["lil"])
    []
    >>> len(load_clip_files(["eCLIP"])) == 675
    True
    >>> len(load_clip_files(["eCLIP", "iCLIP"])) == 681
    True
    """
    res = []
    for a in assay_name:
        res += list(Path(ConfigEncodeClip.output).glob(f"*_{a}.*.bed.gz"))
    return res


def get_data_from_bed(mfile: Path) -> Tuple[str, str, str, str]:
    """
    Get the replicate data, the cell data and the sample and the target \
    factor data from an encode file.

    :param mfile: An encode bed file
    :return:  Get the replicate data, the cell data, the sample, and \
     the target factor data from an encode file.

    >>> myfile = "./FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP"
    >>> myfile += "_rep2_eCLIP.narrowPeak.hg19.bed.gz"
    >>> get_data_from_bed(Path(myfile))
    ('FUS', 'HepG2', 'ENCSR464OSH_ENCFF106CGP', 'rep2')
    """
    data = mfile.name.split("_")
    return data[0], data[1], f"{data[3]}_{data[4]}", data[5]


def group_files(mfiles: List[Path], target: str) -> Dict[str, List[Path]]:
    """
    Regroup experiment files on the same target.

    :param mfiles: A list of file
    :param target: The level used to group the files. (factor, replicate, cell)
    :return: A dictionary that links each target to the files of interest

    >>> r = ["FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP.narrowPeak",
    ... "FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106XLP_rep1_eCLIP.narrowPeak",
    ... "FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106XLP_rep1-2_eCLIP.narrowPeak",
    ... "FUS_HepG2_ENCODE_ENCSR464OXH_ENCFF106CVP_rep1_eCLIP.narrowPeak",
    ... "FUS_HepG2_ENCODE_ENCSR464OSQ_ENCFF106XYZ_rep2_iCLIP.narrowPeak",
    ... "FUS_K562_ENCODE_ENCSR464ORS_ENCFF106ATG_rep1_eCLIP.narrowPeak",
    ... "SRSF1_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP.narrowPeak"]
    >>> b = [Path(a + ".hg19.bed.gz") for a in r]
    >>> res = group_files(b, "factor")
    >>> for k in res:
    ...     res[k] = [str(x).replace(".narrowPeak.hg19.bed.gz", "") for
    ... x in res[k]]
    >>> res == {'FUS': ['FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP',
    ... 'FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106XLP_rep1_eCLIP',
    ... 'FUS_HepG2_ENCODE_ENCSR464OXH_ENCFF106CVP_rep1_eCLIP',
    ... 'FUS_HepG2_ENCODE_ENCSR464OSQ_ENCFF106XYZ_rep2_iCLIP',
    ... 'FUS_K562_ENCODE_ENCSR464ORS_ENCFF106ATG_rep1_eCLIP'],
    ... 'SRSF1': ['SRSF1_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP']}
    True
    >>> res = group_files(b, "cell")
    >>> for k in res:
    ...     res[k] = [str(x).replace(".narrowPeak.hg19.bed.gz", "") for
    ... x in res[k]]
    >>> res == {'FUS_HepG2':
    ... ['FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP',
    ...  'FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106XLP_rep1_eCLIP',
    ...  'FUS_HepG2_ENCODE_ENCSR464OXH_ENCFF106CVP_rep1_eCLIP',
    ...  'FUS_HepG2_ENCODE_ENCSR464OSQ_ENCFF106XYZ_rep2_iCLIP'],
    ... 'FUS_K562': ['FUS_K562_ENCODE_ENCSR464ORS_ENCFF106ATG_rep1_eCLIP'],
    ... 'SRSF1_HepG2': [
    ... 'SRSF1_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP']}
    True
    >>> res = group_files(b, "replicate")
    >>> for k in res:
    ...     res[k] = [str(x).replace(".narrowPeak.hg19.bed.gz", "") for
    ... x in res[k]]
    >>> res == {'FUS_HepG2_ENCSR464OSH':
    ... ['FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP',
    ... 'FUS_HepG2_ENCODE_ENCSR464OSH_ENCFF106XLP_rep1_eCLIP'],
    ... 'FUS_HepG2_ENCSR464OXH':
    ... ['FUS_HepG2_ENCODE_ENCSR464OXH_ENCFF106CVP_rep1_eCLIP'],
    ... 'FUS_HepG2_ENCSR464OSQ':
    ... ['FUS_HepG2_ENCODE_ENCSR464OSQ_ENCFF106XYZ_rep2_iCLIP'],
    ... 'FUS_K562_ENCSR464ORS':
    ... ['FUS_K562_ENCODE_ENCSR464ORS_ENCFF106ATG_rep1_eCLIP'],
    ... 'SRSF1_HepG2_ENCSR464OSH':
    ... ['SRSF1_HepG2_ENCODE_ENCSR464OSH_ENCFF106CGP_rep2_eCLIP']}
    True
    """
    d = {}
    for f in mfiles:
        if "rep1-2" not in f.name:
            data = get_data_from_bed(f)
            if target == "factor":
                key = data[0]
            elif target == "cell":
                key = f"{data[0]}_{data[1]}"
            else:
                key = f"{data[0]}_{data[1]}_{data[2].split('_')[0]}"
            if key in d:
                d[key].append(f)
            else:
                d[key] = [f]
    return d


def merge_files(dfiles: Dict[str, List[Path]], target: str, filter_bed: Path
                ) -> None:
    """
    Merge the file together.

    :param dfiles: A dictionary that links each target to the files of interest
    :param target: The level used to group the files. (factor, replicate, cell)
    :param filter_bed: bed used to filter the clip peaks
    :return: The merged bed file
    """
    outfolder = ConfigEncodeClip.merged_output / target
    outfolder.mkdir(parents=True, exist_ok=True)
    for k in dfiles:
        print(f"Working on {k}")
        tmp_out = outfolder / f"{k}_tmp.bed"
        tmp_out2 = outfolder / f"{k}_tmp2.bed"
        res_out = outfolder / f"{k}_merged.bed.gz"
        list_file = " ".join(map(str, dfiles[k]))
        cmd = "zcat %s | awk 'BEGIN{FS=\"\\t\"}{if($5==1000){print $0}}' " \
              "> %s" % (list_file, tmp_out)
        cmd2 = f"bedtools intersect -a {tmp_out} -b {filter_bed} -s " \
               f"| sort -k1,1 -k2,2n > {tmp_out2}"
        cmd3 = f"bedtools merge -i {tmp_out2} -d -1 -s -c 4,5,6 " \
               f"-o collapse,mean,distinct | cut -f1-3,5-7 | " \
               f"sed 's/^chr//g' | gzip -c > {res_out}"
        sp.check_output(cmd, shell=True)
        sp.check_output(cmd2, shell=True)
        sp.check_output(cmd3, shell=True)
        tmp_out.unlink()
        tmp_out2.unlink()


@lp.parse(assay=["eCLIP", "iCLIP", "RIP-chip", "RIP-seq"],
          target=["factor", "replicate", "cell"])
def create_merged_files(assay: List[str] = ("eCLIP", "iCLIP"),
                        target: str = "factor", level: str = "exon"):
    """

    :param assay: The kind of assay of interest(default ['eCLIP', 'iCLIP']
    :param target: The level used to group the files. (factor, replicate, \
    cell) (default 'factor').
    :param level: 'gene' to create a bed file without first and last exons \
    'exon' to create a bed file of extended exon (200nt at each size) \
    without first and last exon (default exon)
    """
    filter_bed = create_gene_bed(level)
    mfiles = load_clip_files(assay)
    dfiles = group_files(mfiles, target)
    merge_files(dfiles, target, filter_bed)


if __name__ == "__main__":
    create_merged_files()
