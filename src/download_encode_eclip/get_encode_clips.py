#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to get every encode eclip.
"""

from .config import ConfigEncodeClip
import pandas as pd
import json
import subprocess as sp
from typing import Dict, Optional, Tuple
from pathlib import Path
from ..logging_conf import logging_def
import logging



def load_encode_eclip_file() -> pd.DataFrame:
    """
    :return: A table containing encode eclip experiment

    >>> d = load_encode_eclip_file().head()
    >>> d[d.columns[0:3]]
                              ID Assay title Target gene symbol
    0  /experiments/ENCSR322HHA/       eCLIP              TIAL1
    1  /experiments/ENCSR023PKW/       eCLIP              EIF3G
    2  /experiments/ENCSR366DGX/       eCLIP              KHSRP
    3  /experiments/ENCSR337XGI/       eCLIP               SAFB
    4  /experiments/ENCSR977OXG/       eCLIP              PRPF4
    >>> d[d.columns[3:5]]
      Biosample summary Project
    0             HepG2  ENCODE
    1              K562  ENCODE
    2             HepG2  ENCODE
    3             HepG2  ENCODE
    4             HepG2  ENCODE
    >>> d[d.columns[5:]]
      Biological replicate  Technical replicate      Organism Genome assembly
    0                  1,2                    1  Homo sapiens     GRCh38,hg19
    1                  2,1                    1  Homo sapiens     GRCh38,hg19
    2                  2,1                    1  Homo sapiens     GRCh38,hg19
    3                  2,1                    1  Homo sapiens     GRCh38,hg19
    4                  1,2                    1  Homo sapiens     GRCh38,hg19
    """
    df =  pd.read_csv(ConfigEncodeClip.eclip_file, sep="\t")
    df.rename({"Biosample term name": "Biosample summary"}, axis=1,
              inplace=True)
    return df


def get_encode_url(encode_id: str) -> str:
    """

    :param encode_id: An partial encode URL for an experiment
    :return: The complete encode URL

    >>> get_encode_url("/experiments/ENCSR464OSH/")
    'https://www.encodeproject.org/experiments/ENCSR464OSH/'
    >>> get_encode_url("lolipop")
    Traceback (most recent call last):
    ...
    ValueError: The encode_id should begin with /experiments/
    """
    if not encode_id.startswith("/experiments/"):
        raise ValueError("The encode_id should begin with /experiments/")
    return f"{ConfigEncodeClip.encode_website}{encode_id}"


def get_json(url: str) -> Dict:
    """
    :param url: An encode URL pointing to an eclip experiment
    :return: The json contained in the url

    >>> d = get_json("https://www.encodeproject.org/experiments/ENCSR069EVH/")
    >>> list(d.keys())[0:4]
    ['assay_term_name', 'biosample_ontology', 'documents', 'references']
    """
    cmd = f"curl {url} --silent | sed -n '10p'"
    res = sp.check_output(cmd, shell=True).decode("UTF-8")
    return json.loads(res)


def define_expected_parameters(assay_name: str) -> Optional[Dict]:
    """
    If the assay name is in eCLIP, iCLIP, RIP-chip and RIP-seq get data \
    about the file to download.

    :param assay_name: The name of the assay.
    :return: A dictionary with data about the kind of files to recover

    >>> define_expected_parameters("eCLIP")
    {'file_format': 'bed', 'file_format_type': 'narrowPeak'}
    >>> define_expected_parameters("iCLIP")
    {'file_format': 'bed', 'file_format_type': 'narrowPeak'}
    >>> define_expected_parameters("RIP-chip")
    {'file_format': 'bed', 'file_format_type': 'broadPeak'}
    >>> define_expected_parameters("RIP-seq")
    {'file_format': 'bed', 'file_format_type': 'broadPeak'}
    >>> define_expected_parameters("lil")
    """
    if assay_name in ["eCLIP", "iCLIP"]:
        return {"file_format": "bed", "file_format_type": "narrowPeak"}
    if assay_name == "RIP-chip":
        return {"file_format": "bed", "file_format_type": "broadPeak"}
    if assay_name == "RIP-seq":
        return {"file_format": "bed", "file_format_type": "broadPeak"}
    return None


def get_bed_eclip_file(dic_file: Dict) -> Optional[Tuple[str, Dict]]:
    """
    From a dictionary containing data on an experiment file, return \
    the url to download the file if it's a bed of narrowpeak on hg19.

    :param dic_file: A file containing data on an experiment file
    :return: The url of the file if the file is a bed file of narrowpeaks \
    on hg19.

    >>> d = get_json("https://www.encodeproject.org/experiments/ENCSR069EVH/")
    >>> df = d["files"][24]
    >>> url, data = get_bed_eclip_file(df)
    >>> url.replace("https://www.encodeproject.org", "")
    '/files/ENCFF777FHS/@@download/ENCFF777FHS.bed.gz'
    >>> data == {'rep': [1, 2], 'title': 'ENCFF777FHS', 'file_format': 'bed',
    ... 'file_format_type': 'narrowPeak'}
    True
    >>> get_bed_eclip_file(d["files"][22])
    """
    for k in ["file_format", "file_format_type", "no_file_available",
              "assembly", "assay_term_name"]:
        if k not in dic_file.keys():
            return None
    tmp = define_expected_parameters(dic_file["assay_term_name"])
    if tmp is None:
        return None
    if dic_file["file_format"] != tmp["file_format"] or \
        dic_file["file_format_type"] != tmp["file_format_type"] or \
        dic_file["no_file_available"] or dic_file["assembly"] != "hg19" or \
        dic_file["status"] != "released":
        return None
    data = {"rep": dic_file["biological_replicates"],
            "title": dic_file["title"], **tmp}
    return f"{ConfigEncodeClip.encode_website}{dic_file['href']}", data


def download_clip_file(url_clip: str, output: Path, data: Dict,
                       row: pd.Series) -> Path:
    """
    Save the file located at url_clip and return the Path of the created file.

    :param url_clip: An url containing a bed file
    :param output: The folder where the result will be created
    :param data: A dictionary containing the number of replicates \
    used to create the file, the sample name, the file_format of interest \
    and the file_format_type.
    :param row: The row of an encode table containing eclip \
    experiments.
    :return: The Path where the file downloaded from clip_url is stored

    >>> u = str(ConfigEncodeClip.encode_website) + \
    "/files/ENCFF777FHS/@@download/ENCFF777FHS.bed.gz"
    >>> data = {'rep': [1, 2], 'title': 'ENCFF777FHS', 'file_format': 'bed',
    ... 'file_format_type': 'narrowPeak'}
    >>> s = pd.Series({"ID": "/experiments/ENCSR069EVH/",
    ... "Assay title": "eCLIP",
    ... "Target gene symbol": "FUS", "Biosample summary": "HepG2",
    ... "Project": "ENCODE", "Biological replicate": "1,2",
    ... "Technical replicate": "1", "Organism": "Homo sapiens",
    ... "Genome assembly": "GRCh38,hg19"})
    >>> outf = download_clip_file(u, Path("/tmp"), data, s)
    >>> outf.is_file()
    True
    >>> outf.unlink()
    """
    bs = row['Biosample summary'].replace(" ", "-")
    formatf = "bed.gz" if data["file_format"] == "bed" else data["file_format"]
    outfile = output / f"{row['Target gene symbol']}_" \
                       f"{bs}_{row['Project']}_" \
                       f"{row['ID'].replace('/experiments/', '')[:-1]}_" \
                       f"{data['title']}_" \
                       f"rep{'-'.join(map(str, data['rep']))}_" \
                       f"{row['Assay title']}" \
                       f".{data['file_format_type'].replace(' ', '')}" \
                       f".hg19.{formatf}"
    cmd = f"curl -L {url_clip} -o {outfile} --silent"
    sp.check_call(cmd, shell=True)
    return outfile


def download_eclip(output: Path) -> None:
    """
    Download every eclip experiment of interest.

    :param output:  File where the eclip experiment will be stored
    """
    output.mkdir(exist_ok=True)
    logging_def(ConfigEncodeClip.output, __file__, "INFO")
    df = load_encode_eclip_file()
    for i in range(df.shape[0]):
        sample_ok = 0
        my_row = df.iloc[i, :]
        exp_name = my_row['ID'].replace('/experiments/', '')[:-1]
        logging.info(f"Working on experiment "
                     f"{exp_name}")
        dic_exp = get_json(get_encode_url(my_row["ID"]))
        if "files" not in dic_exp.keys():
            continue
        for dic_file in dic_exp["files"]:
            res = get_bed_eclip_file(dic_file)
            if res is not None:
                download_clip_file(res[0], output, res[1], my_row)
                sample_ok += 1
                logging.info(f"  --> {dic_file['title']}, done")
        if (sample_ok != 3 and my_row["Assay title"] == "eCLIP") or \
                sample_ok == 0:
            logging.warning(f"experiment {exp_name} has {sample_ok} bed "
                            f"reported !")


if __name__ == "__main__":
    import doctest
    doctest.testmod()