#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Contains variables needed in this submodule
"""

from ..db_utils.config import Config


class ConfigEncodeClip:
    """
    Contains the variable used in this submodule
    """
    data = Config.data
    eclip_file = data / "CLIP_bed" / "experiment_report_2021_1_22_16h_39m.tsv"
    output = Config.results / "Encode_clips"
    output_gene = output / "gene_without_first_last_exons.bed"
    output_exon = output / "exon_without_first_last_exons.bed"
    encode_website = "https://www.encodeproject.org"
    merged_output = Config.results / "Encode_clips_merged"

