#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Make density graphics for one particular nucleotides \
on a given project. The figure should present in x-axis \
the frequency of an exon for a nucleotide N and in y-axis the \
mean frequency of N in every other co-localized exons.
"""

import sqlite3
import pandas as pd
import logging
import numpy as np
import doctest
from .config import ConfigNt
from ..logging_conf import logging_def
from typing import Dict, Tuple, Any, List, Optional
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import linregress
from itertools import product
from random import random
import multiprocessing as mp
import os
from ..find_interaction_cluster.config import get_communities_basefile


class NoInteractionError(Exception):
    pass


def get_select_addition(global_weight: int, get_weight: bool, same_gene: bool):
    """
    Return the column to add if get_weight is not false.

    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param get_weight: Say if we want to recover the weight of the interaction
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :return: The additional column to get
    """
    if get_weight:
        if global_weight == 0 and same_gene:
            return ', weight'
        elif global_weight == 0:
            return ', t1.weight'
        elif global_weight >= 0 and same_gene:
            return ', ROUND(AVG(weight), 2)'
        elif global_weight >= 0:
            return ', ROUND(AVG(t1.weight), 2)'
    return ''


def get_project_colocalisation(cnx: sqlite3.Connection, project: str,
                               weight: int,
                               global_weight: int, same_gene: bool,
                               get_weight: bool = False,
                               exon_bc: Optional[np.array] = None,
                               inter_chr: bool = False, distance: int = 10000,
                               level: str = "exon",
                               cell: str = "ALL") -> np.array:
    """
    Get the interactions in project `project` for gene and exons

    :param cnx: Connection to chia-pet database
    :param project: The project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param get_weight: Say if we want to recover the weight of the interaction
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param exon_bc: exons in big communities
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :param level: the feature that we want to recover
    :param cell: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :return: The table containing the number of interaction by projects
    """
    logging.debug(f'Recovering interaction ({os.getpid()})')
    select_add = get_select_addition(global_weight, get_weight, same_gene)
    inter_str = " AND level = 'inter'" \
        if inter_chr else f"AND (level = 'inter' OR distance >= {distance})"
    inter_str_t = " AND t1.level = 'inter'" \
        if inter_chr \
        else f"AND (t1.level = 'inter' OR t1.distance >= {distance})"
    if level == "gene" and not same_gene:
        logging.warning(f"When level is equal to gene then same_gene must "
                        f"be true. Setting it to True.")
        same_gene = True
    if exon_bc is None:
        filter_exons = filter_exons_t = ''
    else:
        tmp = tuple(exon_bc)
        filter_exons = f' AND {level}1 IN {tmp} AND {level}2 IN {tmp}'
        filter_exons_t = f' AND t1.{level}1 IN {tmp} AND t1.{level}2 IN {tmp}'
    if global_weight == 0:
        if same_gene:
            query = f"SELECT {level}1, {level}2{select_add} " \
                    f"FROM cin_{level}_interaction " \
                    f"WHERE weight >= {weight} " \
                    f"AND id_project = '{project}'" \
                    + filter_exons \
                    + inter_str
        else:
            query = f"""SELECT t1.exon1, t1.exon2{select_add}
                        FROM cin_exon_interaction t1, cin_exon t2, cin_exon t3
                        WHERE t1.weight >= {weight}
                        AND id_project = '{project}' 
                        AND t1.exon1 = t2.id
                        AND t1.exon2 = t3.id
                        AND t2.id_gene != t3.id_gene""" \
                    + filter_exons_t \
                    + inter_str_t
    else:
        good_projects = tuple(ConfigNt.good_projects(cell))
        if same_gene:
            query = f"SELECT {level}1, {level}2{select_add} " \
                    f"FROM cin_{level}_interaction " \
                    f"WHERE weight >= {weight} " \
                    f"AND id_project IN {good_projects} " \
                    f"{filter_exons} " \
                    f"{inter_str} " \
                    f"GROUP BY {level}1, {level}2 " \
                    f"HAVING COUNT(*) >= {global_weight}"
        else:
            query = f"""SELECT t1.exon1, t1.exon2{select_add}
                        FROM cin_exon_interaction t1, cin_exon t2, cin_exon t3
                        WHERE t1.weight >= {weight}
                        AND t1.exon1 = t2.id
                        AND t1.exon2 = t3.id
                        AND t2.id_gene != t3.id_gene
                        AND t1.id_project IN {good_projects}
                        {filter_exons_t}
                        {inter_str_t}
                        GROUP BY exon1, exon2
                        HAVING COUNT(*) >= {global_weight}"""

    c = cnx.cursor()
    c.execute(query)
    result = np.asarray(c.fetchall(), dtype=str)
    if len(result) == 0:
        msg = f'No interaction found in project {project}'
        logging.exception(msg)
        raise NoInteractionError(msg)
    logging.debug(result[0:5])
    return result


def get_frequency_dic(cnx: sqlite3.Connection, ft: str, ft_type: str,
                      level: str = "exon") -> Dict[str, float]:
    """
    Get the frequency of a feature for every exon in \
    the chia-pet database.

    :param cnx: Connection to chia-pet database
    :param ft: The feature of interest
    :param ft_type: The type of feature of interest
    :param level: The kind of feature to analyse (exon or gene)
    :return: The frequency dic
    """
    logging.debug('Calculating frequency table')
    reg = ''
    if level == "gene":
        reg = "AND region='gene'"
    query = f"SELECT id_{level}, frequency " \
            f"FROM cin_{level}_frequency " \
            f"WHERE ft = '{ft}' " \
            f"AND ft_type = '{ft_type}' " \
            f"{reg}"

    c = cnx.cursor()
    c.execute(query)
    result = c.fetchall()
    if len(result) == 0:
        msg = f'No Frequencies found for {level} {ft} ({ft_type})'
        logging.exception(msg)
        raise NoInteractionError(msg)
    if level == "exon":
        dic = {exon: freq for exon, freq in result}
    else:
        dic = {str(exon): freq for exon, freq in result}
    logging.debug({k: dic[k] for k in list(dic.keys())[0:5]})
    return dic


def get_dic_co_regulated_exon(arr_interaction: np.array):
    """
    Get all exon interacting with each other.

    :param arr_interaction: List of co-regulated exons.
    :return: Dictionary containing all regulated exons

    >>> a = np.array([['1_1', '2_1'], ['1_1', '3_1'], ['3_1', '2_1']])
    >>> get_dic_co_regulated_exon(a)
    {'1_1': ['2_1', '3_1'], '2_1': ['1_1', '3_1'], '3_1': ['1_1', '2_1']}
    """
    d = {}
    for exon1, exon2 in arr_interaction:
        for e1, e2 in [(exon1, exon2), (exon2, exon1)]:
            if e1 not in d:
                d[e1] = [e2]
            else:
                d[e1] += [e2]
    return d


def density_mean(arr_interaction: np.array, dic_freq: Dict[str, float],
                 level: str):
    """
    Create the density table, a table showing the frequency of \
    a nucleotide in every exon in a chia-pet project and the mean \
    frequency of every other co-localised exons

    :param arr_interaction: array of interaction between exons.
    :param dic_freq: The frequency dataframe.
    :param level: The kind of feature to analyse (exon or gene)
    :return: The density table
    """
    exons_dic = get_dic_co_regulated_exon(arr_interaction)
    dic = {f'{level}': [], f'freq_{level}': [], f'freq_coloc_{level}': [],
           f'o{level}': []}
    for exon in exons_dic.keys():
        freq_ex = dic_freq[exon]
        oexon = np.unique(exons_dic[exon])
        freq_oexon = np.nanmean(np.asarray([dic_freq[ex] for ex in oexon],
                                           dtype=float))
        if freq_ex is not None and not np.isnan(freq_oexon):
            dic[level].append(exon)
            dic[f'freq_{level}'].append(freq_ex)
            dic[f'freq_coloc_{level}'].append(freq_oexon)
            dic[f'o{level}'].append(", ".join(oexon))
    df = pd.DataFrame(dic)
    logging.debug(df.head())
    return df


def simple_density(arr_interaction: np.array, dic_freq: Dict[str, float],
                   level: str):
    """
    Create the density table, a table showing the frequency of \
    a nucleotide in every exon in a chia-pet project and the \
    frequency of of one co-localised exon.

    :param arr_interaction: array of interaction between exons.
    :param dic_freq: The frequency dataframe.
    :param level: The kind of feature to analyse (exon or gene)
    :return: The density table
    """
    dic = {f'{level}1': [], f'freq_{level}': [],
           f'freq_coloc_{level}': [], f'{level}2': []}
    for exon1, exon2 in arr_interaction:
        freq_ex = dic_freq[exon1]
        freq_ex2 = dic_freq[exon2]
        if freq_ex is not None and freq_ex2 is not None:
            dic[f'{level}1'].append(exon1)
            dic[f'freq_{level}'].append(freq_ex)
            dic[f'freq_coloc_{level}'].append(freq_ex2)
            dic[f'{level}2'].append(exon2)
    df = pd.DataFrame(dic)
    logging.debug(df.head())
    return df


def create_density_table(arr_interaction: np.array, dic_freq: Dict[str, float],
                         compute_mean: bool = False, level: str = "exon"
                         ) -> pd.DataFrame:
    """
    Create the density table, a table showing the frequency of \
    a nucleotide in every exon in a chia-pet project and the mean \
    frequency of every other co-localised exons or the frequency of one \
    co-localised exons.

    :param arr_interaction: array of interaction between exons.
    :param dic_freq: The frequency dataframe.
    :param compute_mean: True to compute the mean frequency of co-localised \
    exons, false to only compute the frequency of one co-localized exons.
    :param level: The kind of feature to analyse (exon or gene)
    :return: The density table
    """
    logging.debug(f'Calculating density table ({os.getpid()})')
    if compute_mean:
        return density_mean(arr_interaction, dic_freq, level)
    else:
        return simple_density(arr_interaction, dic_freq, level)


def create_density_fig(df: pd.DataFrame, project: str, ft_type: str, ft: str,
                       weight: int, global_weight: int,
                       community_size: Optional[int],
                       inter_chr: bool, level: str
                       ) -> Tuple[float, float]:
    """
    Compute a density file showing if the feature frequency of \
    an exons correlates with the frequency in other co-localised exons.

    :param df: The dataframe containing frequency of exons and \
    co-localized exons.
    :param project: The name of the project where the co-localisation \
    where observed
    :param ft_type: The type of feature of interest
    :param ft: The feature of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :param level: The kind of feature to analyse (exon or gene)
    :return: The correlation and the p-value
    """
    logging.debug('Creating the density figure')
    sns.set()
    sns.set_context('talk')
    plt.figure(figsize=(20, 12))
    df2 = df[[f'freq_{level}', f'freq_coloc_{level}']].copy()
    s, i, r, p, stderr = linregress(df[f"freq_{level}"],
                                    df[f'freq_coloc_{level}'])
    sns.kdeplot(df2[f'freq_{level}'], df2[f'freq_coloc_{level}'], shade=True,
                shade_lowest=False,
                cmap="YlOrBr")
    plt.plot(df[f"freq_{level}"], i + s * df[f"freq_{level}"], 'r',
             label=f'r={round(r,4)}, p={round(p, 4)}')
    plt.legend()
    ml = "an exon" if level == "exon" else "a gene"
    plt.xlabel(f"Freq of {ft} in {ml}")
    plt.ylabel(f"Freq of {ft} in co-localized {level}s")
    title = f'Freq of {ft} of {level}s and their co-localized partner in ' \
            f'{project}'
    if inter_chr:
        title += '\n(inter chromosomal interactions)'
    plt.title(title)
    plt.savefig(ConfigNt.get_density_file(weight, global_weight, project,
                                          ft_type, ft, fig=True,
                                          community_size=community_size,
                                          inter_chr=inter_chr))
    plt.close()
    return r, p


def create_density_figure(nt: str, ft_type: str,
                          project: str, weight: int, global_weight: int,
                          same_gene: bool, inflation: float, cell_line: str,
                          compute_mean: bool,
                          community_size: Optional[int],
                          inter_chr: bool = False, level: str = "exon",
                          logging_level: str = "DISABLE"
                          ) -> Tuple[float, float]:
    """
    Create a density figure for the project `project` for the nucleotide \
    `nt`.

    :param project: The chia-pet project of interest
    :param nt: The nucleotide used to build the figure
    :param ft_type: The type of feature of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param compute_mean: True to compute the mean frequency of co-localised \
    exons, false to only compute the frequency of one co-localized exons.
    :param community_size: The size of the community to consider in the \
    analysis.
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :param level: The kind of feature to analyse (exon or gene)
    :param logging_level: The level of information to display
    :return: The correlation and the p-value
    """
    logging_def(ConfigNt.interaction, __file__, logging_level)
    outfile = ConfigNt.get_density_file(weight, global_weight, project,
                                        ft_type, nt, fig=False,
                                        community_size=community_size,
                                        inter_chr=inter_chr)
    if not outfile.is_file():
        exons_bc = recover_exon_in_big_communities(community_size, project,
                                                   weight, global_weight,
                                                   inflation, cell_line, level)
        cnx = sqlite3.connect(ConfigNt.db_file)
        arr_interaction = get_project_colocalisation(cnx, project, weight,
                                                     global_weight, same_gene,
                                                     False, exons_bc,
                                                     inter_chr, level=level)
        dic_freq = get_frequency_dic(cnx, nt, ft_type, level)
        df = create_density_table(arr_interaction, dic_freq, compute_mean,
                                  level)
        df.to_csv(outfile, sep="\t", index=False)
        r, p = create_density_fig(df, project, ft_type, nt, weight,
                                  global_weight, community_size, inter_chr,
                                  level)
    else:
        logging.debug(f'The file {outfile} exist, recovering data '
                      f'({os.getpid()})')
        df = pd.read_csv(outfile, sep="\t")
        s, i, r, p, stderr = linregress(df[f"freq_{level}"],
                                        df[f"freq_coloc_{level}"])
    return r, p


def create_scatterplot(df_cor: pd.DataFrame, ft_type: str, ft: str,
                       weight: int, global_weight: int,
                       community_size: Optional[int],
                       inter_chr: bool):
    """
    Create a scatterplot showing the R-value according to the  \
    number of interaction.

    :param df_cor: The dataframe of correlation.
    :param ft_type: The feature type of interest
    :param ft: The feature of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    """
    sns.set()
    sns.set_context('talk')
    plt.figure(figsize=(20, 12))
    df_cor = df_cor[(df_cor['ft_type'] == ft_type) &
                    (df_cor['ft'] == ft)].copy()
    sns.scatterplot(x='cor', y='nb_interaction', data=df_cor)
    left, right = plt.xlim()
    bottom, top = plt.ylim()
    for i in range(df_cor.shape[0]):
        plt.text(df_cor.cor.values[i] + right * 0.005,
                 df_cor.nb_interaction.values[i] + random() * top / 60,
                 df_cor.project.values[i], fontsize=8)
    plt.xlabel(f"Correlation for {ft} ({ft_type}) in project")
    plt.ylabel("Number of total interaction in projects")
    title = f'Project correlation for {ft} ({ft_type}) ' \
            f'according to the number of interaction in projects'
    if inter_chr:
        title += "\n(inter-chromosmal interactions)"
    plt.title(title)
    plt.savefig(ConfigNt.get_density_recap(weight, global_weight, ft_type,
                                           ft, fig=True,
                                           community_size=community_size,
                                           inter_chr=inter_chr))
    plt.close()


def execute_density_figure_function(di: pd.DataFrame, project: str,
                                    ft_type: str, ft: str, weight: int,
                                    global_weight: int,
                                    same_gene: bool,
                                    inflation: float,
                                    cell_line: str,
                                    compute_mean: bool,
                                    community_size: Optional[int],
                                    inter_chr: bool = False,
                                    level: str = "exon",
                                    ) -> Dict[str, Any]:
    """
    Execute create_density_figure and organized the results in a dictionary.

    :param di: The dataframe of interaction
    :param project: The project of interest
    :param ft_type: The feature type of interest
    :param ft: The feature of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param compute_mean: True to compute the mean frequency of co-localised \
    exons, false to only compute the frequency of one co-localized exons.
    :param community_size: he size of the community to consider in the \
    analysis.
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :param level: The kind of feature to analyse (exon or gene)
    :return:
    """
    logging.info(f'Working on {project}, {ft_type}, {ft} - {os.getpid()}')
    r, p = create_density_figure(ft, ft_type, project, weight,
                                 global_weight, same_gene, inflation,
                                 cell_line, compute_mean,
                                 community_size, inter_chr, level)
    if global_weight == 0:
        return {"project": project, "ft_type": ft_type,
                "ft": ft, "cor": r, "pval": p,
                'nb_interaction': di[di['projects'] == project].iloc[0, 1]}
    else:
        return {"project": project, "ft_type": ft_type,
                "ft": ft, "cor": r, "pval": p}


def combine_dic(list_dic: List[Dict]) -> Dict:
    """
    Combine The dictionaries in list_dic.

    :param list_dic: A list of dictionaries
    :return: The combined dictionary
    """
    dic = {k: [] for k in list_dic[0]}
    for d in list_dic:
        for k in d:
            dic[k].append(d[k])
    return dic


def recover_exon_in_big_communities(community_size: Optional[int],
                                    project: str, weight: int,
                                    global_weight: int, inflation: float,
                                    cell_line: str,
                                    level: str) -> Optional[np.array]:
    """
    Recover the list of exon present in community with a larger size than \
    `community_size`

    :param community_size: The minimum size of community that we want to \
    consider
    :param project: The name of the project of interest
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param weight: The weight of interaction to consider
    :parma inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param level: The kind of feature to analyse (exon or gene)
    :return: The list of exon of interest
    """
    if community_size is None:
        return None
    outfile = ConfigNt.get_community_file(project, weight, global_weight,
                                          True, inflation, cell_line, level,
                                          "_communities.txt")
    if not outfile.is_file():
        msg = f"The file {outfile} was not found ! You must try " \
              f"to launch find_interaction_cluster script with " \
              f"the same parameters !"
        logging.exception(msg)
        raise FileNotFoundError(msg)
    communities = get_communities_basefile(outfile)
    res = []
    for c in communities:
        res += c
    return res


def create_all_frequency_figures(ps: int, weight: int = 1,
                                 global_weight: int = 0, ft_type: str = "nt",
                                 same_gene=True, inflation: float = 1.5,
                                 cell_line: str = "ALL",
                                 compute_mean: bool = True,
                                 community_size: Optional[int] = None,
                                 inter_chr: bool = False, level: str = "exon",
                                 logging_level: str = "DISABLE"):
    """
    Make density figure for every selected projects.

    :param logging_level: The level of data to display.
    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param ft_type: The kind of feature to analyse
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param compute_mean: True to compute the mean frequency of co-localised \
    exons, false to only compute the frequency of one co-localized exons.
    :param community_size: The size of the community to consider in the \
    analysis.
    :param ps: The number of processes to create
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :param level: The kind of feature to analyse (exon or gene)
    """
    logging_def(ConfigNt.interaction, __file__, logging_level)
    if global_weight == 0:
        di = pd.read_csv(ConfigNt.get_interaction_file(weight, same_gene,
                                                       inter_chr), sep="\t")
        projects = ConfigNt.good_projects
    else:
        di = pd.DataFrame()
        projects = [f"Global_projects_{global_weight}"]
    ft_list = ConfigNt.get_features(ft_type)
    param = product(projects, ft_list, [ft_type])
    pool = mp.Pool(processes=ps)
    processes = []
    for project, ft, ft_type in param:
        args = [di, project, ft_type, ft, weight, global_weight, same_gene,
                inflation, cell_line, compute_mean, community_size, inter_chr,
                level]
        processes.append(pool.apply_async(execute_density_figure_function,
                                          args))
    results = [proc.get(timeout=None) for proc in processes]
    dic = combine_dic(results)
    df_corr = pd.DataFrame(dic)
    df_corr.to_csv(ConfigNt.get_density_recap(weight, global_weight, ft_type,
                                              'ALL', fig=False,
                                              community_size=community_size,
                                              inter_chr=inter_chr),
                   sep="\t")
    if global_weight == 0:
        for ft in ft_list:
            create_scatterplot(df_corr, ft_type, ft, weight, global_weight,
                               community_size, inter_chr)


if __name__ == "__main__":
    doctest.testmod()
