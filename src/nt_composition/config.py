#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This file contains the location of every input and result \
file that will be produced by this module.
"""

from ..db_utils.config import Config
from typing import List, Optional
from ..figures_utils.config_figures import get_good_project
from ..find_interaction_cluster.config import ConfigGraph


def get_weight_folder(weight: int, global_weight: int, ft_type: str,
                      community_size: Optional[int], inter_chr: bool):
    """
    Get the weight folder.

    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :parma ft_type: The type fo featrue of interst
    :param community_size: The minimum size of communities we want to consider
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :return: The folder that will contains the interaction with a weight \
    greater or equal to `weigh` in ChIA-PET projects
    """
    inter_str = "_inter-chromosmal" if inter_chr else ""
    if community_size is None:
        c = ''
    else:
        c = f'_community_size_gte_{community_size}'
    if global_weight == 0:
        weight_folder = ConfigNt.density_folder / \
                        f"{ft_type}_project_weight-{weight}{c}{inter_str}"
    else:
        weight_folder = ConfigNt.density_folder / \
                        f"{ft_type}_weight-{weight}_" \
                        f"global_weight-{global_weight}{c}{inter_str}"
    weight_folder.mkdir(parents=True, exist_ok=True)
    return weight_folder


def get_density_file(weight: int, global_weight: int, project: str,
                     ft_type: str, ft: str, fig: bool = False,
                     kind: str = 'density',
                     community_size: Optional[int] = None,
                     inter_chr: bool = False):
    """
    Get the filename that will contain the density data or figure.

    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param project: A project name
    :param ft_type: A feature type
    :param ft: A feature
    :param fig: Say if the result file is a figure or not
    :param kind: The kind of the figure
    :param community_size: The minimum size of communities we want to consider
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :return: The filename that will contain the density data or figure.
    """
    if fig:
        ext = "pdf"
    else:
        ext = "txt"
    res_folder = get_weight_folder(weight, global_weight, ft_type,
                                   community_size, inter_chr) / project
    res_folder.mkdir(exist_ok=True, parents=True)
    return res_folder / f"{project}_{ft_type}_{ft}_{kind}.{ext}"


def get_density_recap(weight: int, global_weight: int,
                      ft_type: str, ft: str, fig: bool = False,
                      community_size: Optional[int] = False,
                      inter_chr: bool = False):
    """
    Get the density correlation recap file.

    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param ft_type:  A feature type
    :param ft:  A feature
    :param fig: Say if the result file is a figure or not
    :param community_size: The minimum size of communities we want to consider
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :return:
    """
    if fig:
        ext = "pdf"
    else:
        ext = "txt"
    outfolder = get_weight_folder(weight, global_weight, ft_type,
                                  community_size, inter_chr)
    outfolder.mkdir(exist_ok=True, parents=True)
    return outfolder / f"{ft_type}_{ft}_density_recap.{ext}"


def get_interaction_file(weight: int, same_gene: bool, inter_chr: bool):
    """
    Return the interaction file : coresponding to the file containing \
    iteraction with a weight greater or eaqual to `weight`

    :param weight: minimum weight of interaction to \
    concider
    :param same_gene: Say if we are considering interaction within the same \
    gene
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else
    :return: The interaction filename
    """
    same_str = "_same_gene_allowed" if same_gene else ""
    inter_str = "inter-chromosmal_" if inter_chr else ""
    return ConfigNt.interaction / \
        f"{inter_str}interaction_number_weight>={weight}_by_project" \
        f"{same_str}.txt"


def get_features(ft_type: str) -> List[str]:
    """
    Get all the feature corresponding to `ft_type`.

    :param ft_type: The feature type of interest
    :return: The list of feature type of interest
    """
    nt_list = ["A", "C", "G", "T"]
    if ft_type == 'nt':
        return nt_list + ["S", "W"]
    elif ft_type == 'dnt':
        return [a + b for a in nt_list for b in nt_list]
    elif ft_type == 'tnt' or ft_type == 'codon':
        return [a + b + c for a in nt_list for b in nt_list for c in nt_list]
    elif ft_type == 'aa':
        return list("RHKDESTNQCGPAVILMFYW")
    else:
        return ['Hydrophobicity_Goldsack', 'Hydrophobicity_Sweet',
                'Hydrophobicity_Kyte_Doolittle', 'Hydrophobicity_Tanford',
                'accessible_resiudues_Janin', 'burried_resiudues_Rose',
                'Disorder_promoting_Campen',
                'Disorder_propensity_Russel_Linding',
                'Disorder_propensity_Deleage_Roux',
                'intrinsically_unstructured_proteins_Tompa',
                'Interface_propensity_Jones', 'Linker_propensity_Bae',
                'Flexibility_Vihinen', 'Stability_Zhou_Zhou',
                'Bulkiness_Zimmerman', 'Molecular_weight_Fasman',
                'Molecular_weight_Dawson', 'Polarity_Radzicka',
                'Isoelectric_Point_Zimmerman', 'Alpha-helix_Deleage',
                'Beta-sheet_Deleage', 'Coil_conformation_Deleage',
                'Positive_charge_Fauchere', 'Negative_charge_Fauchere',
                'Sequence_freq_Jungck']


class ConfigNt:
    """
    A class containing every parameters used in the submodule nt_composition
    """
    cpu = Config.cpu
    data = Config.data
    result = Config.results
    db_file = Config.db_file
    output_folder = result / "nt_composition"
    interaction = output_folder / 'interaction_number'
    get_interaction_file = get_interaction_file
    density_folder = output_folder / 'density_fig'
    get_density_recap = get_density_recap
    selected_project = interaction / "selected_sample.txt"
    get_density_file = get_density_file
    get_features = get_features
    good_projects = get_good_project
    get_community_file = ConfigGraph.get_community_file