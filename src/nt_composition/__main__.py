#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This file contains main function used to analyse the \
nucleotide composition of co-localised exons
"""

from .config import ConfigNt
from .make_nt_correlation import create_all_frequency_figures
from .distance import create_all_distance_figures
from .get_projects_interaction import get_interactions_number
import lazyparser as lp


@lp.flag(same_gene=True, inter_chr=True)
@lp.parse(weight='weight > 0',
          ft_type=['nt', 'dnt', 'tnt', 'codon', 'aa', 'properties'],
          kind=["density", "distance"])
def launcher(weight: int = 1, global_weight: int = 0, ft_type: str = 'nt',
             same_gene: bool = False, compute_mean: bool = True,
             iteration: int = 10000, kind: str = 'density',
             community_size: int = -1, inflation: float = 1.5,
             cell_line: str = "ALL",
             inter_chr: bool = False,
             level: str = "exon",
             logging_level: str = "DISABLE"):
    """
    Launch the creation of density file.

    :param weight: The weight of interaction to consider (default 1)
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all project are merge together and the interaction \
    seen in `global_weight` project are taken into account (default 0)
    :param ft_type: The feature type of interest (default 'nt')
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param compute_mean: True to compute the mean frequency of co-localised \
    exons, false to only compute the frequency of one co-localized exons. \
    This parameters is only used when kind = "density". (default True)
    :param iteration: Number of control co-localisation to make. This \
    parameter is only used when kind = "distance": (Default : 10000)
    :param kind: The type of analysis to make. 'density' to create \
    correlation figure between the feature frequency of an exon and the \
    frequency of the same feature for other co-localized exons. 'distance' \
    to check if the frequencies of co-localized exon are closer than \
    randomly expected. (default : 'density').*
    :param community_size: consider only exons within communities  bigger \
    than community size. if community_size = -1. This option is deactivated.
    :param inflation: The inflation parameter. This parameter is used only \
    if community size is not -1. (default 1.5).
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param inter_chr: True to only get inter-chromosomal interactions \
    False else (default: False)
    :param level: The kind of feature to analyse (exon or gene)
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    get_interactions_number(weight, same_gene, inter_chr, logging_level)
    if community_size == -1:
        community_size = None
    if kind == "density":
        create_all_frequency_figures(ConfigNt.cpu, weight, global_weight,
                                     ft_type, same_gene, inflation, cell_line,
                                     compute_mean, community_size, inter_chr,
                                     level, logging_level)
    else:
        create_all_distance_figures(ConfigNt.cpu, weight, global_weight,
                                    ft_type, same_gene, iteration, inter_chr,
                                    level, logging_level)


launcher()