#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Configuration folder
"""

from ..db_utils.config import Config
from pathlib import Path
from ..figures_utils.config_figures import get_good_project
from typing import List, Dict
import pandas as pd


def get_weight_folder(weight: int, global_weight: int, inflation: float,
                      cell_line: str):
    """
    Get the weight folder.

    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all project are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :return: The folder that will contains the interaction with a weight \
    greater or equal to `weigh` in ChIA-PET projects
    """
    cell_line = "" if cell_line == "ALL" else f"-{cell_line}"
    if global_weight == 0:
        weight_folder = ConfigGraph.community_folder / \
                        f"project_weight-{weight}-{inflation}{cell_line}"
    else:
        weight_folder = ConfigGraph.community_folder / \
                        f"weight-{weight}_" \
                        f"global_weight-{global_weight}-{inflation}{cell_line}"
    weight_folder.mkdir(parents=True, exist_ok=True)
    return weight_folder


def get_community_file(project: str, weight: int, global_weight: int,
                       same_gene: bool, inflation: float,
                       cell_line: str = "ALL", feature: str = 'exon',
                       ext: str = ".txt", sub_fold: str = ''):
    """
    Get the output file of interest.

    :param project: Name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: the kind of feature analyzed
    :param ext: The file extension
    :param sub_fold: if filled, then the data are recovered from a subfolder
    :return: The filename of interest
    """
    folder = get_weight_folder(weight, global_weight, inflation, cell_line)
    if sub_fold != '':
        folder = folder / sub_fold
    folder.mkdir(exist_ok=True, parents=True)
    if global_weight != 0:
        project = f"global-weight-{global_weight}"
    return folder / \
           f"{project}_weight-{weight}_same_gene-{same_gene}_{feature}{ext}"


def get_hipmcl_folder() -> Path:
    """
    Return the name of the hipmlc folder
    :return: The hipmlc folder
    """
    return list(Config.data.glob('*hipmcl*'))[0]


def get_hipmcl_prog() -> Path:
    """
    Get the hipmcl program.

    :return: The file corresponding to hipmcl program
    """
    return get_hipmcl_folder() / "bin" / "hipmcl"


def get_communities_basefile(result: Path, threshold: int = 0
                             ) -> List[List[str]]:
    """
    Get the communities inside the file `result`

    :param result: A file containing the communities found by the hipMCL \
    program.
    :param threshold: The number of exon the community must contains at least \
    to be recovered
    :return: The list of communities find by the hipMCL program.
    """
    communities = []
    with result.open('r') as f:
        for line in f:
            tmp = line.replace('\n', '').strip().split(' ')
            if len(tmp) > threshold:
                communities.append(tmp)
    return communities


def get_communities(result: Path, feature: str, threshold: int = 0,
                    ) -> Dict[str, List[str]]:
    """
    Get the communities inside the file `result`

    :param result: A file containing the communities found by the hipMCL \
    program.
    :param threshold: The number of exon the community must contains at least \
    to be recovered
    :param feature: The kind of feature analysed
    :return: The list of communities find by the hipMCL program.

    >>> get_communities(Config.tests_files / "test_community_file.txt", "gene",
    ... 0)
    {'C1': ['415', '416', '421', '422', '423', '433', '441', '475', \
'481', '502', '511'], 'C2': ['10123', '10209', '8812', '9140', '9166']}
    """
    df = pd.read_csv(result, sep="\t")[["community", "nodes", f"{feature}s"]]
    df = df[df["nodes"] >= threshold].copy()
    df.index = df["community"]
    df.drop(["nodes", "community"], axis=1, inplace=True)
    dic = df.to_dict()[f"{feature}s"]
    return {k: dic[k].split(", ") for k in dic}


class ConfigGraph:
    """
    Class containing all the variables that will be used in this submodule
    """
    data = Config.data
    cpu = 7 # Config.cpu
    db_file = Config.db_file
    results = Config.results
    output_folder = results / 'community_of_co-localized-exons'
    community_folder = output_folder / 'communities'
    community_calibration_folder = output_folder / "community_calibration"
    get_community_file = get_community_file
    hip_zip = data / 'hipMCL.zip'
    get_hip_folder = get_hipmcl_folder
    get_hipmcl_prog = get_hipmcl_prog
    good_projects = get_good_project()
    ppi_threshold = 700
    output_factor = output_folder / "factor_community_enrichment"
    output_heatmap = output_folder / "community_heatmaps"
    output_barplots = output_folder / "community_barplots"