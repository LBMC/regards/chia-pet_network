#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create graph figures containing the communities enriched in exon/genes that are regulated by a splicing factor
"""

import lazyparser as lp
from .graph_functions import create_graph_figure, create_many_graph_figures


@lp.parse(weight=range(1, 11), global_weight=range(11),
          feature=('gene', 'exon'), reg=("reg", "down", "up"))
def launcher(ps: int, sf_name: str, reg: str, weight: int,
             global_weight: int, inflation: float, same_gene: bool = True,
             cell_line: str = "ALL", project: str = 'GSM1018963_GSM1018964',
             feature: str = "gene", threshold: float = 0.1, min_reg: int = 2,
             iteration: int = 0, min_community: int = 3):
    """
    :param ps: The number of processes to create (only used if sf_name \
    equals ALL).
    :param project: A project name of interest. Used only if \
    global_weight is 0 (default GSM1018963_GSM1018964)
    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are \
    calculated by project, else all project are merge together and the \
    interaction seen in `global_weight` project are taken into account \
    :param same_gene: Say if we consider as co-localised, exons within \
    the same gene (True) or not (False). Default True
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made \
     on a specific cell line (ALL to disable this filter). Default ALL
    :param feature: The feature we want to analyse (default gene)
    :param sf_name: The name of the splicing factor of interest. ALL to \
    create the figure for every splicing Factor.
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated) \
    (default 0.1)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account (default 2)
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    """
    if feature == "gene" and not same_gene:
        print("Warning: setting same_gene to True")
    if sf_name != "ALL":
        create_graph_figure(project, weight, global_weight, same_gene,
                            inflation, cell_line, feature, sf_name, reg,
                            threshold, min_reg, iteration, min_community)
    else:
        create_many_graph_figures(ps, project, weight, global_weight,
                                  same_gene, inflation, cell_line,
                                  feature, threshold, min_reg, iteration,
                                  min_community)

launcher()