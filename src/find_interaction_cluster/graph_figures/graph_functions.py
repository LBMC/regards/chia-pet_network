#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""
import sqlite3
import networkx as nx
import pandas as pd
from .config_graph_figure import ConfigGraph, Config
from pathlib import Path
from ..clip_figures.clip_analyser import get_regulation_table, \
    get_community_tables, merge_dataframes
from typing import List, Dict, Tuple
import json
import matplotlib.cm as cm
import numpy as np
from ..community_finder import lighten_color, create_figure
from ..sf_and_communities import get_sfname
from itertools import product
from ..community_figures.fig_functions import get_feature_by_community, \
    perm_with_ctrl
import multiprocessing as mp


class Parameters:
    """
    A class containing all the parameters needed to recover the graph generated
    """
    def __init__(self, project: str, weight: int, global_weight: int,
                 same_gene: bool, inflation: float, cell_line: str,
                 feature: str):
        """
        Creation of an object containing wanted parameters.

        :param project: A project name of interest. Used only if \
        global_weight is 0
        :param weight: The weight of interaction to consider
        :param global_weight: The global weight to consider. if \
        the global weight is equal to 0 then then density figure are \
        calculated by project, else all project are merge together and the \
        interaction seen in `global_weight` project are taken into account \
        :param same_gene: Say if we consider as co-localised, exons within \
        the same gene (True) or not (False)
        :param inflation: The inflation parameter
        :param cell_line: Interactions are only selected from projects made \
         on a specific cell line (ALL to disable this filter)
        :param feature: The feature we want to analyse
        """
        self.project = project
        self.weight = weight
        self.global_weight = global_weight
        self.same_gene = same_gene
        self.inflation = inflation
        self.cell_line = cell_line
        self.feature = feature


def recover_json_graph_of_interest(p: Parameters) -> Tuple[Path, Path]:
    """
    recover json files and the community file.

    :param p: A parameter object
    :return: The wanted file

    >>> mp = Parameters('', 2, 2, True, 1.2, "ALL", "gene")
    >>> res, com = recover_json_graph_of_interest(mp)
    >>> res.name
    'global-weight-2_weight-2_same_gene-True_gene.json'
    >>> str(res.relative_to(ConfigGraph.data.parent).parent)
    'results/community_of_co-localized-exons/communities/\
weight-2_global_weight-2-1.2'
    >>> com.name
    'global-weight-2_weight-2_same_gene-True_gene.txt'
    >>> str(com.relative_to(ConfigGraph.data.parent).parent)
    'results/community_of_co-localized-exons/communities/\
weight-2_global_weight-2-1.2'
    """
    return (ConfigGraph.get_community_file(
                p.project, p.weight, p.global_weight, p.same_gene,
                p.inflation, p.cell_line, p.feature, ".json"),
            ConfigGraph.get_community_file(
                p.project, p.weight, p.global_weight, p.same_gene,
                p.inflation, p.cell_line, p.feature, ".txt")
            )


def check_if_exist(graph_file: Path) -> None:
    """
    Check if graph_file exists.

    :param graph_file: A json file
    """
    if not graph_file.is_file():
        raise FileNotFoundError(
            f"Error, file {graph_file} not found. "
            f"You main want to launch __main__ script "
            f"under tad_to_communities module with the same "
            f"parameter to build it")


def get_regulated_community_perm(df: pd.DataFrame, iteration: int,
                                 feature: str) -> List[str]:
    """
    Get the list of community containing a significant amount of \
    regulated genes/exons thanks to a permutation test.

    :param df: A dataframe containing for each gene, the community \
    to which it belong and if it is regulated by a splicing factor.
    :param iteration: The number of iteration to perform
    :param feature: The kind of feature to use
    :return: The list of colony containing a significant amount of \
    genes/exons regulated by a splicing factor
    """
    dic_com = get_feature_by_community(df, feature)
    df_stat = perm_with_ctrl(df, feature, "regulation", dic_com, iteration)
    return df_stat.loc[(df_stat['p-adj'] <= 0.05) &
                       (df_stat["reg-adj"] == " + ") &
                       (df_stat["community_size"] >= 10),
                       "community"].to_list()


def get_regulated_community_thresholds(df: pd.DataFrame, threshold: float,
                                       min_reg: int = 2) -> List[str]:
    """
    Get the list of community containing a significant amount of \
    regulated genes/exons by using thresholds.

    :param df: A dataframe containing for each gene, the community \
    to which it belong and if it is regulated by a splicing factor.
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :return: The list of community containing a significant amount of \
    genes/exons regulated by a splicing factor

    >>> d = pd.DataFrame({"id_gene": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    ... "community": ["C1"] * 3 + ["C2"] * 3 + ["C3"] * 3,
    ... "community_size": [3] * 9,
    ... "regulation": [1, 0, 0, 1, 1, 0, 0, 0, 0]})
    >>> get_regulated_community_thresholds(d, 0.1, 2)
    ['C2']
    >>> get_regulated_community_thresholds(d, 0.1, 3)
    []
    >>> get_regulated_community_thresholds(d, 0.1, 1)
    ['C1', 'C2']
    >>> get_regulated_community_thresholds(d, 0.5, 1)
    ['C2']
    >>> get_regulated_community_thresholds(d, 0.0, 0)
    ['C1', 'C2', 'C3']
    """
    ndf = df[["community", "community_size", "regulation"]]\
        .groupby(["community", "community_size"])\
        .sum().reset_index()
    ndf["freq"] = ndf["regulation"] / ndf["community_size"]
    return ndf.loc[(ndf["regulation"] >= min_reg) & (ndf["freq"] >= threshold),
                   "community"].to_list()


def get_regulated_community(df: pd.DataFrame, feature: str,
                            threshold: float, min_reg: int = 2,
                            iteration: int = 0):
    """
    Get the list of community containing a significant amount of \
    regulated genes/exons with the wanted method.

    :param df: A dataframe containing for each gene, the community \
    to which it belong and if it is regulated by a splicing factor.
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param iteration: The number of iteration to perform
    :param feature: The kind of feature to use
    :return: The list of community containing a significant amount of \
    genes/exons regulated by a splicing factor
    """
    if iteration >= 20:
        return get_regulated_community_perm(df, iteration, feature)
    return get_regulated_community_thresholds(df, threshold, min_reg)


def select_gene_in_selected_communities(df: pd.DataFrame,
                                        list_communities: List[str],
                                        feature: str
                                        ) -> List[List]:
    """
    Return the list of gene or exons in each selected communities.

    :param df: A dataframe containing for each gene, the community \
    to which it belong and if it is regulated by a splicing factor.
    :param list_communities: A list of selected communities
    :param feature: Gene of exons
    :return: The list of exons/genes in each communities
    >>> d = pd.DataFrame({"id_gene": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    ... "community": ["C1"] * 3 + ["C2"] * 3 + ["C3"] * 3,
    ... "community_size": [3] * 9,
    ... "regulation": [1, 0, 0, 1, 1, 0, 0, 0, 0]})
    >>> select_gene_in_selected_communities(d, ["C1", "C2"], "gene")
    [[1, 2, 3], [4, 5, 6]]
    >>> select_gene_in_selected_communities(d, ["C1", "C3"], "gene")
    [[1, 2, 3], [7, 8, 9]]
    """
    return [df.loc[df["community"] == c, f'id_{feature}'].to_list()
            for c in list_communities]


def load_graphic(graph_file: Path) -> nx.Graph:
    """
    Load interaction graphics.

    :param graph_file: A graphics containing interaction
    :return: The graph loaded in networkx

    >>> mg = load_graphic(Config.tests_files / "small_graph.json")
    >>> mg.nodes
    NodeView(('A', 'B', 'C', 'D'))
    >>> mg.edges
    EdgeView([('A', 'B'), ('B', 'C'), ('C', 'D')])
    """
    with graph_file.open('r') as f:
        d = json.load(f)
        g = nx.readwrite.json_graph.node_link_graph(d)
    return g


def subgraph_creation(graph: nx.Graph, list_ft: List[List]) -> nx.Graph:
    """
    Create a subgraph only containing nodes indicated in `list_ft`.

    :param graph: A graph of interaction
    :param list_ft: A list of list of group of feature in communities
    :return: The subgraph

    >>> g = load_graphic(Config.tests_files / "small_graph.json")
    >>> sg = subgraph_creation(g, [["A", "B"], ["C"]])
    >>> sg.nodes
    NodeView(('A', 'B', 'C'))
    >>> sg.edges
    EdgeView([('A', 'B'), ('B', 'C')])
    """
    nodes = []
    for c in list_ft:
        nodes += c
    nodes = list(map(str, nodes))
    return graph.subgraph(nodes).copy()


def build_community_dic(list_ft: List[List], list_community: List[str],
                        reg_ft: List[str]) -> Dict:
    """
    Create a dictionary containing for each node, it's community and color.

    :param list_ft: A list of exons/genes
    :param list_community: The name community in which exons/genes in \
    list_ft belongs
    :param reg_ft: List of feature regulated by the splicing factor
    :return: The dictionary linking each features to it's community

    >>> dc = build_community_dic([[1, 2], [3, 4]], ["C17", "C42"])
    >>> dc == {
    ... '1': {'num': 'C17', 'col': 'rgba(255, 0, 0, 1)'},
    ... '2': {'num': 'C17', 'col': 'rgba(255, 0, 0, 1)'},
    ... '3': {'num': 'C42', 'col': 'rgba(255, 0, 23, 1)'},
    ... '4': {'num': 'C42', 'col': 'rgba(255, 0, 23, 1)'}}
    True
    """
    colors = cm.hsv(np.linspace(0, 1, len(list_ft)))
    dic_community = {}
    for k, c in enumerate(list_ft):
        for ft in c:
            col_line = "black" if ft in reg_ft else lighten_color(colors[k])
            dic_community[str(ft)] = {'num': list_community[k],
                                      'col': lighten_color(colors[k]),
                                      'line': col_line}
    return dic_community


def get_title(sf_name: str, reg: str, threshold: float, min_reg, feature: str,
              iteration: int) -> str:
    """
    Return a title

    :param sf_name: The name of the splicing factor of interest
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param feature: The kind of feature we want to analyse
    :param iteration: The number of iteration to perform
    :return: The title of the figure

    >>> get_title("SF1", "down", 0.1, 2, "gene", 0)
    'Figure of the communities containing at least 10.0 % of their genes \
(or more than 2 genes) down-regulated by SF1'
    """
    reg = "regulated" if reg == "reg" else f"{reg}-regulated"
    if iteration < 20:
        return f"Figure of the communities containing at least " \
               f"{threshold * 100} % of their {feature}s (or more than " \
               f"{min_reg} {feature}s) {reg} by {sf_name}"
    return f"Figure of the communities enriched in {feature}s" \
           f" {reg} by {sf_name} (permutation test {iteration} iteration)"


def get_outfile(graph_file: Path, sf_name: str, reg: str, threshold: float,
                min_reg, feature: str, iteration: int, num_community: int
                ) -> Path:
    """
    Return The outfile of the figure

    :param graph_file: The file containing the graph in json format
    :param sf_name: The name of the splicing factor of interest
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param feature: The kind of feature we want to analyse
    :param iteration: The number of iteration to perform
    :param num_community: The number of community in the figure
    """
    output_tmp = graph_file.parent / "graph_figures"
    if iteration < 20:
        output_dir = output_tmp / "graph_threshold"
        output_dir.mkdir(exist_ok=True, parents=True)
        return output_dir / \
               f"{sf_name}_{reg}_{threshold}_{min_reg}_{feature}" \
               f"_cluster-{num_community}.html"
    else:
        output_dir = output_tmp / "graph_permutation"
        output_dir.mkdir(exist_ok=True, parents=True)
        return output_dir / \
               f"{sf_name}_{reg}_perm-{iteration}_{feature}_" \
               f"cluster-{num_community}.html"


def create_graph_figure(project: str, weight: int, global_weight: int,
                        same_gene: bool, inflation: float, cell_line: str,
                        feature: str, sf_name: str, reg: str, threshold: float,
                        min_reg: int = 2, iteration: int = 0,
                        min_community: int = 3):
    """
    :param project: A project name of interest. Used only if \
    global_weight is 0
    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are \
    calculated by project, else all project are merge together and the \
    interaction seen in `global_weight` project are taken into account \
    :param same_gene: Say if we consider as co-localised, exons within \
    the same gene (True) or not (False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made \
     on a specific cell line (ALL to disable this filter)
    :param feature: The feature we want to analyse
    :param sf_name: The name of the splicing factor of interest
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    """
    print(f"Working on {sf_name}, {reg}")
    p = Parameters(project, weight, global_weight, same_gene, inflation,
                   cell_line, feature)
    graph_file, comm_file = recover_json_graph_of_interest(p)
    check_if_exist(graph_file)
    reg_table = get_regulation_table(sqlite3.connect(Config.db_file),
                                     sf_name, reg, feature)
    df_com_file = pd.read_csv(comm_file, sep="\t")
    df_com = get_community_tables(df_com_file, feature)
    full_com = merge_dataframes(reg_table, df_com, feature)
    reg_ft = full_com.loc[full_com["regulation"] == 1,
                          f"id_{feature}"].to_list()
    full_com.loc[full_com["community_size"] < 10.0,
                 ["community", "community_size"]] = [np.nan, np.nan]
    list_communities = get_regulated_community(full_com, feature, threshold,
                                               min_reg, iteration)
    num_communities = len(list_communities)
    if num_communities >= min_community:
        list_ft = select_gene_in_selected_communities(full_com,
                                                      list_communities,
                                                      feature)
        graph = load_graphic(graph_file)
        sub_graph = subgraph_creation(graph, list_ft)
        dic_community = build_community_dic(list_ft, list_communities, reg_ft)
        my_title = get_title(sf_name, reg, threshold, min_reg, feature,
                             iteration)
        outfile = get_outfile(graph_file, sf_name, reg, threshold, min_reg,
                              feature, iteration, num_communities)
        create_figure(sub_graph, outfile, dic_community, my_title)
    else:
        print(f"only {num_communities} cluster(s) found ! "
              f"no figure created")



def create_many_graph_figures(ps: int, project: str, weight: int,
                              global_weight: int, same_gene: bool,
                              inflation: float, cell_line: str,
                              feature: str, threshold: float,
                              min_reg: int = 2, iteration: int = 0,
                              min_community: int = 3):
    """
    :param ps: The number of processes to create
    :param project: A project name of interest. Used only if \
    global_weight is 0
    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are \
    calculated by project, else all project are merge together and the \
    interaction seen in `global_weight` project are taken into account \
    :param same_gene: Say if we consider as co-localised, exons within \
    the same gene (True) or not (False)
    :param inflation: The inflation parameter
    :param feature: The feature we want to analyse
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    """
    sf_list = get_sfname()
    my_prod = list(product(sf_list, ["down", "up"]))
    processes = []
    pool = mp.Pool(processes=min(len(my_prod), ps))
    for sf_name, reg in my_prod:
        args = [project, weight, global_weight, same_gene, inflation,
                cell_line, feature, sf_name, reg, threshold, min_reg,
                iteration, min_community]
        processes.append(pool.apply_async(create_graph_figure, args))
    pool.close()
    [p.get(timeout=None) for p in processes]
    pool.join()



if __name__ == "__main__":
    import doctest
    doctest.testmod()
