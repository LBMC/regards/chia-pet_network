#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create a graph figure showing wich community is regulated \
by which splicing factor
"""
import itertools

import networkx as nx
from typing import List, Dict, Tuple
import pandas as pd
import sqlite3
import numpy as np
from .graph_functions import Parameters, recover_json_graph_of_interest, \
    Config, get_regulation_table, get_community_tables, merge_dataframes, \
    get_regulated_community, subgraph_creation, load_graphic, check_if_exist
from .create_community_node_graph import create_community_sized_graph
from ..sf_and_communities import get_sfname
import matplotlib.cm as cm
from pathlib import Path
import json
from pyvis.network import Network
import lazyparser as lp
from matplotlib.colors import to_hex


def add_node_color(c_graph: nx.Graph, sf_name: str, color: str,
                   communities: List[str]) -> nx.Graph:
    """
    Update the graphic of community to add the color ``color`` to \
    the community containing a enriched amount of exons or genes \
    regulated by a splicing factor.

    :param c_graph: The graphic of community to update
    :param sf_name: The name of the splicing factor regulating the \
    communities given in communities.
    :param color: The color to give to the communities
    :param communities: The list of community to update
    :return: The network update
    >>> g = nx.Graph()
    >>> g.add_nodes_from(list("ABC"))
    >>> g.add_edges_from([("A", "B"), ("A", "C")])
    >>> ng = add_node_color(g, "TRA2_down", "red", ["A", "B"])
    >>> list(ng.nodes[x] for x in list("ABC")) == [{'reg': 'TRA2_down',
    ... 'color': 'red', 'title': 'TRA2_down'}, {'reg': 'TRA2_down',
    ... 'color': 'red', 'title': 'TRA2_down'}, {}]
    True
    >>> ng = add_node_color(ng, "SR1_up", "white", ["A", "C"])
    >>> list(ng.nodes[x] for x in list("ABC")) == [{'reg':
    ... 'TRA2_down, SR1_up', 'color': 'red', 'title': 'TRA2_down<br/>SR1_up',
    ... 'shape': 'triangle'}, {'reg': 'TRA2_down', 'color': 'red',
    ... 'title': 'TRA2_down'}, {'reg': 'SR1_up', 'color': 'white',
    ... 'title': 'SR1_up'}]
    True
    """
    for c in communities:
        if "reg" not in c_graph.nodes[c].keys():
            c_graph.nodes[c]["reg"] = sf_name
            # c_graph.nodes[c]["color"] = color
        else:
            c_graph.nodes[c]["reg"] += f", {sf_name}"
            c_graph.nodes[c]["shape"] = 'triangle'
        if "title" not in c_graph.nodes[c].keys():
            c_graph.nodes[c]["title"] = sf_name
        else:
            c_graph.nodes[c]["title"] += f"<br/>{sf_name}"
    return c_graph


def get_regulated_communities(c_graph: nx.Graph) -> List[str]:
    """
    Get the communities regulated by a splicing factor.

    :param c_graph: The graphic of community to update
    :return: The list of regulated communities
    >>> g = nx.Graph()
    >>> g.add_nodes_from(list("ABC"))
    >>> g.add_edges_from([("A", "B"), ("A", "C")])
    >>> ng = add_node_color(g, "TRA2_down", "red", ["A", "B"])
    >>> get_regulated_communities(ng)
    ['A', 'B']
    """
    return [node for node in c_graph.nodes
            if "reg" in c_graph.nodes[node].keys()]


def select_splicing_factors(sf_list: List[str]) -> List[str]:
    """
    Return sf_list if sf_list doesn't contain ALL else return ALL.

    :param sf_list: A list of splicing factor of interest
    :return: sf_list if sf_list doesn't contain ALL else return ALL.

    >>> select_splicing_factors(list("ABC"))
    ['A', 'B', 'C']
    """
    return sf_list if "ALL" not in sf_list else get_sfname()


def get_title(nb_sf: int, sf_displayed: int, reg: str, threshold: float,
              min_reg, feature: str, iteration: int, min_community: int) -> str:
    """
    Return a title

    :param nb_sf: The number of splicing factor analyzed
    :param sf_displayed: The number of splicing factor displayed in the figure
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param feature: The kind of feature we want to analyse
    :param iteration: The number of iteration to perform
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    :return: The title of the figure

    >>> get_title(5, 2, "down", 0.1, 2, "gene", 0, 2)
    'Figure of the communities containing at least 10.0 % of their genes \
(or more than 2 genes) down-regulated by 5 splicing factors (2 selected) \
in at least 2 clusters'
    """
    reg = "regulated" if reg == "reg" else f"{reg}-regulated"
    if iteration < 20:
        return f"Figure of the communities containing at least " \
               f"{threshold * 100} % of their {feature}s (or more than " \
               f"{min_reg} {feature}s) {reg} by {nb_sf} splicing factors " \
               f"({sf_displayed} selected) in at least {min_community} clusters"
    return f"Figure of the communities enriched in {feature}s" \
           f" {reg} by {nb_sf} splicing factors ({sf_displayed} selected) " \
           f"in at least {min_community} clusters " \
           f"(permutation test {iteration} iteration)"


def get_outfiles(c_graph_file: Path, sf_list: List[str], threshold: float,
                 min_reg: int, min_community: int, min_community_size: int,
                 iteration: int) -> List[Path]:
    """
    Return the figure and json outfile to store and visualize graph data
    :param c_graph_file: A graph containing a community level graph
    :param sf_list: The list of splicing factor of interest
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    :param min_community_size: The minimum size used to consider communities
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :return:
    """
    outfolder = c_graph_file.parent / "graph_figures" / "community_level"
    outfolder.mkdir(parents=True, exist_ok=True)
    if iteration < 20:
        return [outfolder / f"community_graph_{len(sf_list)}_"
                            f"sf_t{threshold}_min-reg-{min_reg}_min-com_"
                            f"{min_community}_min-size-{min_community_size}."
                            f"{ext}" for ext in ["json", "html", "xlsx"]]
    else:
        return [outfolder / f"community_graph_{len(sf_list)}_"
                            f"sf_t{threshold}_iteration-{iteration}_"
                            f"_min-com_{min_community}_" \
                            f"min-size-{min_community_size}."
                            f"{ext}" for ext in ["json", "html", "xlsx"]]


def update_community_graphic(p: Parameters, c_graph: nx.Graph, color: str,
                             sf_name: str, reg: str,
                             threshold: float, min_reg: int = 2,
                             iteration: int = 0, min_community: int = 3,
                             min_community_size: int = 10) -> nx.Graph:
    """
    :param p: A class containing configurations
    :param c_graph: A community level graph
    :param sf_name: The name of the splicing factor of interest
    :param reg: The name of the regulation chosen
    :param color: color of the regulated node
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    :param min_community_size: The minimum size used to consider communities
    """
    print(f"Working on {sf_name}, {reg}")
    graph_file, comm_file = recover_json_graph_of_interest(p)
    check_if_exist(graph_file)
    reg_table = get_regulation_table(sqlite3.connect(Config.db_file),
                                     sf_name, reg, p.feature)
    df_com_file = pd.read_csv(comm_file, sep="\t")
    df_com = get_community_tables(df_com_file, p.feature)
    full_com = merge_dataframes(reg_table, df_com, p.feature)
    full_com.loc[full_com["community_size"] < min_community_size,
                 ["community", "community_size"]] = [np.nan, np.nan]
    list_communities = get_regulated_community(full_com, p.feature, threshold,
                                               min_reg, iteration)
    if len(list_communities) >= min_community:
        c_graph = add_node_color(c_graph, f"{sf_name}_{reg}", color,
                                 list_communities)
    return c_graph


def write_figure(c_graph: nx.Graph, outfile: Path, title: str = ""
                 ) -> None:
    """
    Write the network figure.

    :param c_graph: An html figure
    :param outfile: The file where the graphic will be created
    :param title: The title of the figure
    """
    net = Network(width="100%", height="100%", heading=title)
    net.from_nx(c_graph)
    net.force_atlas_2based()
    # net.toggle_physics(False)
    net.show_buttons(filter_=["nodes", "edges", "physics"])
    net.write_html(str(outfile))


def community_stats(json_graph: Dict, feature: str) -> pd.DataFrame:
    """
    Get a dataframe indicating for each community, the number \
    of splicing factors that regulates id, it's size and the `
    list of splicing factor regulating it.

    :param json_graph: A json dictionary of the community graph
    :return: A dataframe ndicating for each community, the number \
    of splicing factors that regulates id, it's size and the `
    list of splicing factor regulating it.

    >>> j = {"nodes": [
    ... {"id": "C1", "reg": "SF1_down, SF2_down", "node_size": 2},
    ... {"id": "C2", "reg": "SF3_down", "node_size": 2}]}
    >>> community_stats(j , "gene")
      community  nb_gene  nb_sfreg             sf_list
    0        C1        2         2  SF1_down, SF2_down
    1        C2        2         1            SF3_down
    """
    d = {"community": [], f"nb_{feature}": [], "nb_sfreg": [], "sf_list": []}
    for n in json_graph["nodes"]:
        d["community"].append(n["id"])
        sf_list = n["reg"]
        d["sf_list"].append(sf_list)
        d["nb_sfreg"].append(len(sf_list.split(", ")))
        d[f"nb_{feature}"].append(n["node_size"])
    return pd.DataFrame(d)


def get_clusters(json_graph: Dict, sf: str) -> List[str]:
    """
    Get the clusters regulated by a splicing factor.

    :param json_graph: A json graph cluster
    :param sf: A splicing factor of interest and the regulation chosen
    :return: The list of clusters regulated by `sf` in `json_graph`

    >>> j = {"nodes": [
    ... {"id": "C1", "reg": "SF1_down, SF2_down", "node_size": 2},
    ... {"id": "C2", "reg": "SF3_down", "node_size": 2}]}
    >>> get_clusters(j, "SF1_down")
    ['C1']
    """
    return [n["id"] for n in json_graph["nodes"]
            if sf in n["reg"].split(", ")]



def intersection_stats(json_graph: Dict) -> Tuple[pd.DataFrame, List[str]]:
    """
    get the number of community regulated by two splicing factors.

    :param json_graph: A json dictionary of the community graph
    :return: The dataframe indicating the number of communities \
    regulated by 2 factors and The number of sf which regulates \
    displayed communities in the graph

    >>> j = {"nodes": [
    ... {"id": "C1", "reg": "SF1_down, SF2_down", "node_size": 2},
    ... {"id": "C2", "reg": "SF3_down", "node_size": 2}]}
    >>> r = intersection_stats(j)
    >>> r[1]
    ['SF1_down', 'SF2_down', 'SF3_down']
    >>> r[0][list(r[0].columns[0:4])]
        factor1   factor2  nb_common_clusters  nb_cluster_factor1
    0  SF1_down  SF2_down                   1                   1
    1  SF1_down  SF3_down                   0                   1
    2  SF2_down  SF3_down                   0                   1
    >>> r[0][list(r[0].columns[4::])]
       nb_cluster_factor2 common_clusters cluster_factor1 cluster_factor2
    0                   1              C1              C1              C1
    1                   1                              C1              C2
    2                   1                              C1              C2
    """
    sf_list = []
    for n in json_graph["nodes"]:
        sf_list += n["reg"].split(", ")
    sf_list = list(np.unique(sf_list))
    d = {"factor1": [], "factor2": [], "nb_common_clusters": [],
         "nb_cluster_factor1": [], "nb_cluster_factor2": [],
         "common_clusters": [], "cluster_factor1": [], "cluster_factor2": []}
    for sf1, sf2 in itertools.combinations(sf_list, 2):
        d["factor1"].append(sf1)
        d["factor2"].append(sf2)
        c1 = get_clusters(json_graph, sf1)
        c2 = get_clusters(json_graph, sf2)
        cc = [c for c in c1 if c in c2]
        d["nb_common_clusters"].append(len(cc))
        d["nb_cluster_factor1"].append(len(c1))
        d["nb_cluster_factor2"].append(len(c2))
        d["common_clusters"].append(", ".join(cc))
        d["cluster_factor1"].append(", ".join(c1))
        d["cluster_factor2"].append(", ".join(c2))
    return pd.DataFrame(d), sf_list


def create_stat_files(json_graph: Dict, feature: str, outfile: Path
                      ) -> List[str]:
    """
    Create a stat xlsx file.

    :param json_graph: A json dictionary of the community graph
    :param feature: The feature of interest
    :param outfile: The file where the stat file will be created
    :return: The list of sf which regulates displayed communities in the \
     graph
    """
    sheet1 = community_stats(json_graph, feature)
    sheet2, sf_displayed = intersection_stats(json_graph)
    my_data = zip([sheet1, sheet2], ['community', "interaction"])
    with pd.ExcelWriter(str(outfile), engine='xlsxwriter') as writer:
        for df, sheet in my_data:
            df.to_excel(writer, sheet_name=sheet, index=False)
            worksheet = writer.sheets[sheet]
            for idx, col in enumerate(df):
                series = df[col]
                max_len = max((
                    series.astype(str).map(len).max(),  # len of largest item
                    len(str(series.name))  # len of column name/header
                )) + 1  # adding a little extra space
                worksheet.set_column(idx, idx, max_len)
    return sf_displayed


def update_dic(g_json: Dict, sf_displayed: List[str]) -> Dict:
    """

    :param g_json: A json graph file
    :param sf_diplayed: The list of splicing factor to display
    :return: Tu update dic
    """
    colors = cm.hsv(np.linspace(0, 1, max(len(sf_displayed), 5)))
    colors = [to_hex(c) for c in colors]
    d = {sf: col for sf, col in zip(sf_displayed, colors)}
    for i, n in enumerate(g_json["nodes"]):
        g_json["nodes"][i]["color"] = d[n["reg"].split(", ")[0]]
    return g_json


@lp.parse
def create_community_sf_graph(project: str, weight: int, global_weight: int,
                              same_gene: bool, inflation: float,
                              cell_line: str, feature: str, sf_list: List[str],
                              reg: str, threshold: float, min_reg: int = 2,
                              iteration: int = 0, min_community: int = 3,
                              min_community_size: int = 10):
    """
    :param project: A project name of interest. Used only if \
    global_weight is 0
    :param weight: The weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are \
    calculated by project, else all project are merge together and the \
    interaction seen in `global_weight` project are taken into account \
    :param same_gene: Say if we consider as co-localised, exons within \
    the same gene (True) or not (False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made \
     on a specific cell line (ALL to disable this filter)
    :param feature: The feature we want to analyse
    :param sf_list: The list of the splicing factor of interest
    :param reg: The name of the regulation chosen
    :param threshold: Minimum frequency of gene regulated in a colony to \
    select it (but it must also contains at least min_reg gene regulated)
    :param min_reg: The minimum of regulated exon in a community to \
    take it into account
    :param iteration: If this parameter is greater or equal to 20 then a \
    permutation test is made to find the significantly enriched communities. \
    Below 20, significant communities are found
    :param min_community: The minimum number of enriched community \
    required to produce a figure
    :param min_community_size: The minimum size used to consider communities
    """
    c_graph_file = create_community_sized_graph.__wrapped__(
        project, weight, global_weight, same_gene, inflation, cell_line,
        feature, min_community_size)
    c_graph = load_graphic(c_graph_file)
    p = Parameters(project, weight, global_weight, same_gene, inflation,
                   cell_line, feature)
    sf_list = select_splicing_factors(sf_list)
    colors = cm.hsv(np.linspace(0, 1, max(len(sf_list), 5)))
    colors = [to_hex(c) for c in colors]
    np.random.shuffle(colors)
    for sf_name, color in zip(sf_list, colors):
        c_graph = update_community_graphic(p, c_graph, color, sf_name, reg,
                                           threshold, min_reg, iteration,
                                           min_community, min_community_size)
    regulated_com = get_regulated_communities(c_graph)
    c_graph = subgraph_creation(c_graph, [regulated_com])
    outfiles = get_outfiles(c_graph_file, sf_list, threshold, min_reg,
                            min_community, min_community_size, iteration)
    g_json = nx.json_graph.node_link_data(c_graph)
    sf_displayed = create_stat_files(g_json, feature, outfiles[2])
    g_json = update_dic(g_json, sf_displayed)
    c_graph = nx.readwrite.json_graph.node_link_graph(g_json)
    title = get_title(len(sf_list), len(sf_displayed), reg, threshold, min_reg,
                      feature, iteration, min_community)
    json.dump(g_json, outfiles[0].open('w'), indent=2)
    write_figure(c_graph, outfiles[1], title)


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        import doctest
        doctest.testmod()
    else:
        create_community_sf_graph()
