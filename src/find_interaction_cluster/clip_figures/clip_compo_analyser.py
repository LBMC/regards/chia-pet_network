#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to check whether a communities \
of genes that are more frequently bind by a splicing factor \
has a composition biais.
"""

from pathlib import Path
from typing import List, Tuple, Optional, Dict
import pandas as pd
from .config import ConfigClip, Config, ConfigGraph
from math import floor, ceil
from ..nt_and_community import get_ft_id, get_cpnt_frequency, get_features
import sqlite3
import numpy as np
from scipy.stats import mannwhitneyu
from itertools import combinations, product
from statsmodels.stats.multitest import multipletests
import seaborn as sns
import matplotlib.pyplot as plt
from ...logging_conf import logging_def
import logging
import multiprocessing as mp
import subprocess as sp
import lazyparser as lp


def find_final_clip_tables(folder: Path) -> List[Path]:
    """
    From a folder return every files ending with *_bar.txt.

    :param folder: A folder containing final tables used to produce \
    community figures for clip data (figure produced by the function \
    clip_folder_analysis of the script clip_analyse.py).
    :return: The list of files ending with *_bar.txt

    >>> find_final_clip_tables(Path("fziufriubdsibf"))
    Traceback (most recent call last):
    ...
    NotADirectoryError: Folder fziufriubdsibf doesn't exists !
    >>> find_final_clip_tables(Path("src/"))
    []
    """
    if not folder.is_dir():
        raise NotADirectoryError(f"Folder {folder} doesn't exists !")
    return list(folder.glob("*_bar.txt"))


def prepare_df(clip_df: pd.DataFrame, feature: str) -> pd.DataFrame:
    """
    Prepare the table containing data to produce the clip community figure.

    :param clip_df: table containing data to produce the clip \
     community figure.
    :param feature: The feature of interest (gene or exon)
    :return: The filtered table

    >>> d = prepare_df(pd.read_table(ConfigClip.test_clip_file, sep="\\t"),
    ... "gene")
    >>> d.head()
      community  community_size     p-adj reg-adj  peak_density
    8      C852             1.0  0.015356      -       0.000000
    9       C91             2.0  0.027338      -       0.000000
    0       C10             3.0  0.228296      .       0.046749
    1      C165            12.0  0.252854      .       0.271487
    3      C325            18.0  0.228296      .       0.272363
    >>> d["community"].to_list() == ['C852', 'C91', 'C10', 'C165', 'C325',
    ... 'C232', 'C397', 'C952', 'C365', 'C680', 'C916', 'C690']
    True
    """
    df = clip_df.copy()
    if "regulation" in df.columns:
        df.rename({"regulation": "peak_density",
                   "mean_regulation": "mean_peak_density"}, axis=1,
                  inplace=True)
    df = df.loc[df[f"id_{feature}"] != "ctrl",
                ["peak_density", "community", "community_size", "p-adj",
                 "reg-adj", f"id_{feature}"]]
    df = df.groupby(["community", "community_size", "p-adj", "reg-adj"]
                    ).mean().reset_index()
    df = df.sort_values("peak_density", ascending=True)
    return df


def get_feature_id(clip_df: pd.DataFrame, feature: str,
                   list_community: List[str]) -> pd.DataFrame:
    """
    Get the list of gene contained in the list of communities given.

    :param clip_df: table containing data to produce the clip \
     community figure.
    :param feature: The feature of interest (gene or exon)
    :param list_community: A list of community
    :return: The filtered table)

    >>> d = pd.read_table(ConfigClip.test_clip_file, sep="\\t")
    >>> get_feature_id(d, "gene", ['C916', 'C690'])
    ['11489', '11723', '9289', '9734', '9708']
    >>> get_feature_id(d, "gene", ['C680', 'C916',
    ... 'C690'])
    ['16283', '16852', '11489', '11723', '9289', '9734', '9708']
    >>> get_feature_id(d, "gene", ['C91', 'C852'])
    ['13856', '13360', '7831']
    >>> get_feature_id(d, "gene", ['C91', 'C852',
    ... 'C10'])
    ['13856', '13360', '7831', '16763', '16676', '16817']
    """
    df = clip_df.copy()
    df = df[df[f"id_{feature}"] != "ctrl"]
    return df[df["community"].isin(list_community)][f"id_{feature}"].to_list()


def get_extreme_groups(df: pd.DataFrame, group: str,
                       threshold_feature: int,
                       default_groups: int) -> Tuple[List, str]:
    """
    Recover the impoverished or enriched communities for peaks \
    in a given splicing factor if they exists.

    :param df: A dataframe used to build the community figures \
    of clip density.
    :param group: The kind of community to recover (enriched or impoverished)
    :param threshold_feature: The minimum number of feature to \
    recover from enriched/impoverished communities
    :param default_groups: The number of community to get if none \
    (or not enough) community are enriched or impoverished \
    (depending on group parameter)
    :return: The list of enriched or impoverished community of feature \
    and the name of the list

    >>> d = prepare_df(pd.read_table(ConfigClip.test_clip_file, sep="\\t"),
    ... "gene")
    >>> get_extreme_groups(d, "enriched", 5, 3)
    (['C916', 'C690'], 'enriched')
    >>> get_extreme_groups(d, "enriched", 6, 3)
    (['C680', 'C916', 'C690'], 'enriched_nosig')
    >>> get_extreme_groups(d, "impoverished", 3, 3)
    (['C852', 'C91'], 'impoverished')
    >>> get_extreme_groups(d, "impoverished", 4, 3)
    (['C852', 'C91', 'C10'], 'impoverished_nosig')
    """
    outname = group
    my_reg = " + " if group == "enriched" else " - "
    tmp = df[df["reg-adj"] == my_reg]
    if not tmp.empty and sum(tmp["community_size"]) >= threshold_feature:
        return tmp["community"].to_list(), outname
    outname += "_nosig"
    list_com = df["community"].to_list()
    if group == "enriched":
        return list_com[-default_groups:], outname
    else:
        return list_com[:default_groups], outname


def get_middle_group(df: pd.DataFrame, default_groups: int
                     ) -> Tuple[List, str]:
    """
    Recover the impoverished or enriched communities for peaks \
    in a given splicing factor if they exists.

    :param df: A dataframe used to build the community figures \
    of clip density.
    :param default_groups: The number of community to get if none \
    (or not enough) community are enriched or impoverished \
    (depending on group parameter)
    :return: The list of enriched or impoverished community of feature \
    and the name of the list

    >>> d = prepare_df(pd.read_table(ConfigClip.test_clip_file, sep="\\t"),
    ... "gene")
    >>> get_middle_group(d, 3)
    (['C232', 'C397', 'C952'], 'middle')
    >>> get_middle_group(d, 4)
    (['C325', 'C232', 'C397', 'C952'], 'middle')
    """
    outname = "middle"
    list_com = df.loc[df['reg-adj'] == " . ", "community"].to_list()
    index = floor(len(list_com) / 2)
    middle = list_com[index - floor(default_groups / 2):
                      index + ceil(default_groups / 2)]
    middle_com = df[(df["community"].isin(middle)) &
                    (df['reg-adj'] == " . ")]["community"].to_list()
    return middle_com, outname


def add_ctrl_df(df: pd.DataFrame, feature: str) -> pd.DataFrame:
    """
    Create a dataframe containing every genes/exons not in a community.

    :param df: The dataframe used to create a community figure
    :param feature: The feature of interest (exon or gene)
    :return: A table containing 2 columns: id_{feature} and group \
    (containing 'ctrl')

    >>> import time
    >>> s = time.time()
    >>> add_ctrl_df(pd.DataFrame({"id_gene": list(range(1, 19420)) + ["ctrl"]
    ... }), "gene")
       id_gene group
    0    19420  ctrl
    1    19421  ctrl
    2    19422  ctrl
    3    19423  ctrl
    4    19424  ctrl
    5    19425  ctrl
    6    19426  ctrl
    """
    list_id = get_ft_id(sqlite3.connect(Config.db_file), feature)
    com_id = df[df[f"id_{feature}"] != "ctrl"][f"id_{feature}"].to_list()
    if feature == "gene":
        list_id = list(map(int, list_id))
        com_id = list(map(int, com_id))
    ctrl_id = [mid for mid in list_id if mid not in com_id]
    return pd.DataFrame({f"id_{feature}": ctrl_id,
                         "group": ["ctrl"] * len(ctrl_id)})


def get_interest_groups(clip_table_file: Path, feature: str,
                        threshold_feature: int, default_groups: int
                        ) -> Optional[pd.DataFrame]:
    """

    :param clip_table_file: table containing data to produce the clip \
     community figure.
    :param feature: The feature of interest (gene or exon)
    :param threshold_feature: The minimum number of feature to \
    recover from enriched/impoverished communities
    :param default_groups: The number of community to get if none \
    (or not enough) community are enriched or impoverished \
    (depending on group parameter)
    :return: The dataframe indicating for a id of a feature, the groups where \
    if belong.

    >>> get_interest_groups(ConfigClip.test_clip_file, "gene", 3, 3)
       id_gene         group
    0    13856  impoverished
    1    13360  impoverished
    2     7831  impoverished
    3    17226        middle
    4    17418        middle
    5    16985        middle
    6    16897        middle
    7    16536        middle
    8    17214        middle
    9    17271        middle
    10   17360        middle
    11   16022        middle
    12   16660        middle
    13   17080        middle
    14   17038        middle
    15   10558        middle
    16   11520        middle
    17   10569        middle
    18   10641        middle
    19   10333        middle
    20   12509        middle
    21   11687        middle
    22   10886        middle
    23   10784        middle
    24   10997        middle
    25   10907        middle
    26   12115        middle
    27   10922        middle
    28   10586        middle
    29   12030        middle
    30   10627        middle
    31   12525        middle
    32   12172        middle
    33   11346        middle
    34   11949        middle
    35   15390        middle
    36   15348        middle
    37   14966        middle
    38   15272        middle
    39   15287        middle
    40   15135        middle
    41   14992        middle
    42   15006        middle
    43   15199        middle
    44   15285        middle
    45   11489      enriched
    46   11723      enriched
    47    9289      enriched
    48    9734      enriched
    49    9708      enriched
    >>> r = get_interest_groups(ConfigClip.test_clip_file, "gene", 6, 3)
    >>> r is None
    True
    """
    ini_df = pd.read_csv(clip_table_file, sep="\t")
    df = prepare_df(ini_df, feature)
    enrich_com, name_enrich = get_extreme_groups(
        df, "enriched", threshold_feature, default_groups)
    impov_com, name_impov = get_extreme_groups(
        df, "impoverished", threshold_feature, default_groups)
    if "nosig" in name_enrich and "nosig" in name_impov:
        return None
    middle_com, name_middle = get_middle_group(df, default_groups)
    enrich_grp = get_feature_id(ini_df, feature, enrich_com)
    impov_grp = get_feature_id(ini_df, feature, impov_com)
    middle_grp = get_feature_id(ini_df, feature, middle_com)
    return pd.DataFrame({f"id_{feature}": impov_grp + middle_grp + enrich_grp,
                         "group": [name_impov] * len(impov_grp) +
                                  [name_middle] * len(middle_grp) +
                                  [name_enrich] * len(enrich_grp)})


def create_stat_df(df_comp: pd.DataFrame, cpnt: str) -> pd.DataFrame:
    """
    Compute statistical analysis for every group in df_comp.

    :param df_comp: The dataframe containing the frequency of every `cpnt_type`
    :param cpnt: The component studied
    :return: Dataframe of p-values

    >>> d = pd.DataFrame({"id_gene": [1, 2, 3, 4, 5, 6, 7, 8],
    ... "A": [1, 2, 3, 4, 5, 6, 7, 8], "group": ["test"] * 4 + ["ctrl"] * 4})
    >>> create_stat_df(d, "A")
       grp1  grp2     p-val
    0  ctrl  test  0.030383
    >>> d = pd.DataFrame({"id_gene": list(range(1, 13)),
    ... "A": list(range(1, 12)) + [5], "group": ["test"] * 4 + ["ctrl"] * 4 +
    ... ["test2"] * 4})
    >>> create_stat_df(d, "A")
       grp1   grp2     p-val     p-adj
    0  ctrl   test  0.030383  0.045574
    1  ctrl  test2  0.245383  0.245383
    2  test  test2  0.030383  0.045574

    """
    comparison = combinations(np.unique(df_comp["group"]), 2)
    content = []
    for grp1, grp2 in comparison:
        p_val = mannwhitneyu(
            df_comp.loc[df_comp["group"] == grp1, cpnt].values,
            df_comp.loc[df_comp["group"] == grp2, cpnt].values,
            alternative="two-sided")[1]
        content.append([grp1, grp2, p_val])
    df = pd.DataFrame(content, columns=["grp1", "grp2", "p-val"])
    if df.shape[0] > 1:
        df["p-adj"] = multipletests(df['p-val'].values, method="fdr_bh")[1]
    return df


def make_barplot(df_comp: pd.DataFrame, outfile: Path, cpnt: str,
                 cpnt_type: str, clip_name: str) -> None:
    """
    Create a barplot showing the composition for different groups of feature.

    :param df_comp: The dataframe containing the frequency of every `cpnt_type`
    :param outfile: File were the figure will be created
    :param cpnt: The component of interest
    :param cpnt_type: The type of component of interest
    :param clip_name: The name of the clip studied
    """
    sns.set(context="talk")
    dic_col = df_comp[["group", "color"]].drop_duplicates().copy()
    dic_col.index = dic_col["group"]
    dic_col.drop("group", axis=1, inplace=True)
    dic_col = dic_col.to_dict()["color"]
    g = sns.catplot(x="group", y=cpnt, data=df_comp, height=12, aspect=1.7,
                    kind="violin", palette=dic_col)
    g.fig.suptitle(f"Mean frequency of {cpnt}({cpnt_type})"
                   f"among different community groups\n"
                   f"created from the peak_density from clip {clip_name}")
    g.ax.set_ylabel(f'Frequency of {cpnt} ({cpnt_type})')
    g.savefig(outfile)
    plt.clf()
    plt.close()


def update_composition_group(df_comp: pd.DataFrame, display_size: bool
                             ) -> pd.DataFrame:
    """
    Update the group name of the dataframe df_com.

    :param df_comp: The dataframe containing the frequency of every `cpnt_type`
    :param display_size: True to display the size of \
    the community. False to display nothing.
    :return: if display_size is False return df_comp else return df_comp \
    with the group column containing the size of the groups.
    """
    dic = {"ctrl": 1, "enriched": 2, "enriched_nosig": 2, "middle": 3,
           "impoverished": 4, "impoverished_nosig": 4}
    dic_color = {"ctrl": "#FFCD00", "enriched": "#D40C00",
                 "enriched_nosig": "#FF9A00", "middle": "#FF9A00",
                 "impoverished": "#526EFF", "impoverished_nosig": "#00A5F9"}
    df_comp["order"] = df_comp["group"].map(dic)
    df_comp["color"] = df_comp["group"].map(dic_color)
    df_comp = df_comp.sort_values("order")
    if not display_size:
        return df_comp
    d = {
        gr: f"{gr}({df_comp[df_comp['group'] == gr].shape[0]})"
        for gr in df_comp["group"].unique()
    }
    df_comp["group"] = df_comp["group"].map(d)
    return df_comp


def compute_mean_cpnt(group: str, df_comp: pd.DataFrame, cpnt: str) -> float:
    """
    Compute the mean frequency of a nucleotide/di-nucleotide/amino acid \
     (cpnt) of every exons/genes in the communities belonging to `group` \
     group of community.

    :param group: The group of community of interest
    :param df_comp: A dataframe indicating the composition in cpnt \
    of every genes/exons from every group of community
    :param cpnt: The component (nucleotide, etc) of interest
    :return: The mean composition in cpnt of exon genes belonging to \
    groups of communities

    >>> dc = pd.DataFrame({"id_gene": [1, 2, 3], "A": [0.25, 0.5, 1.],
    ... "group": ["ctrl", "test", "test"],
    ... "order": [1, 2, 3], "color": ["red", "blue", "white"]})
    >>> compute_mean_cpnt("test", dc, "A")
    0.75
    """
    return float(np.mean(df_comp.loc[df_comp["group"] == group, cpnt].values))


def get_regulation(mseries: pd.Series) -> str:
    """
    Return the regulation of interest.

    :param mseries: A row of a dataframe containing stats comparing two \
    groups of communities in frequencies of `cpnt`
    :return: The regulation

    >>> ms = pd.Series({'grp1': 'ctrl', 'grp2': 'test', 'factor': 'SF1',
    ... 'communities': 'Comm', 'mean_grp1': 0.25, 'mean_grp2': 0.5,
    ... 'p-adj': 0.045574})
    >>> get_regulation(ms)
    ' - '
    >>> ms = pd.Series({'grp1': 'ctrl', 'grp2': 'test', 'factor': 'SF1',
    ... 'communities': 'Comm', 'mean_grp1': 0.25, 'mean_grp2': 0.5,
    ... 'p-adj': 0.15574})
    >>> get_regulation(ms)
    ' . '
    >>> ms = pd.Series({'grp1': 'ctrl', 'grp2': 'test', 'factor': 'SF1',
    ... 'communities': 'Comm', 'mean_grp1': 0.95, 'mean_grp2': 0.5,
    ... 'p-adj': 0.005574})
    >>> get_regulation(ms)
    ' + '
    """
    if mseries["p-adj"] > 0.05:
        return " . "
    if mseries["mean_grp1"] > mseries["mean_grp2"]:
        return " + "
    return " - "


def get_ft_enrichment_groups(sf_name: str, communities: Optional[str],
                             cpnt: str, df_stat: pd.DataFrame,
                             df_comp: pd.DataFrame) -> pd.DataFrame:
    """

    :param sf_name: The name of the factor of interest in the clip file \
    used to create the dataframe df_comp.
    :param communities: The kind of spatial communities studied
    :param cpnt: The component (nucleotide, etc) of interest
    :param df_stat: A dataframe indicating which kind of communities \
    (enriched, impoverished in peaks) have a composition bias in cpnt
    :param df_comp: A dataframe indicating the composition in cpnt \
    of every genes/exons from every group of community
    :return: A dataframe indicating the enrichment of each group of \
    community for a given component (cpnt: i.e nucleotide, di-nucleotide, aa)

    >>> ds = pd.DataFrame(
    ... {'grp1': {0: 'ctrl', 1: 'ctrl', 2: 'test'},
    ... 'grp2': {0: 'test', 1: 'test2', 2: 'test2'},
    ... 'p-val': {0: 0.030383, 1: 0.245383, 2: 0.030383},
    ... 'p-adj': {0: 0.045574, 1: 0.245383, 2: 0.045574}})
    >>> dc = pd.DataFrame({"id_gene": [1, 2, 3], "A": [0.25, 0.5, 0.75],
    ... "group": ["ctrl", "test", "test2"],
    ... "order": [1, 2, 3], "color": ["red", "blue", "white"]})
    >>> r = get_ft_enrichment_groups("SF1", "Comm", "A", ds, dc)
    >>> r.iloc[:, 0:8]
       grp1   grp2 factor communities cpnt  mean_grp1  mean_grp2     p-adj
    0  ctrl   test    SF1        Comm    A       0.25       0.50  0.045574
    1  ctrl  test2    SF1        Comm    A       0.25       0.75  0.245383
    2  test  test2    SF1        Comm    A       0.50       0.75  0.045574
    >>> r.iloc[:, 8].to_list()
    [' - ', ' . ', ' - ']
    >>> r = get_ft_enrichment_groups("SF1", None, "A", ds, dc)
    >>> r.iloc[:, 0:7]
       grp1   grp2 factor cpnt  mean_grp1  mean_grp2     p-adj
    0  ctrl   test    SF1    A       0.25       0.50  0.045574
    1  ctrl  test2    SF1    A       0.25       0.75  0.245383
    2  test  test2    SF1    A       0.50       0.75  0.045574
    >>> dc = pd.DataFrame({"id_gene": [1, 2, 3], "A": [0.25, 0.5, 0.75],
    ... "group": ["ctrl(1)", "test(215)", "test2(54165)"],
    ... "order": [1, 2, 3], "color": ["red", "blue", "white"]})
    >>> r = get_ft_enrichment_groups("SF1", None, "A", ds, dc)
    >>> r.iloc[:, 0:7]
       grp1   grp2 factor cpnt  mean_grp1  mean_grp2     p-adj
    0  ctrl   test    SF1    A       0.25       0.50  0.045574
    1  ctrl  test2    SF1    A       0.25       0.75  0.245383
    2  test  test2    SF1    A       0.50       0.75  0.045574
    """
    df = df_stat.copy()
    df_comp["group"] = df_comp["group"].str.replace(r"\(\d+\)", "")
    df["mean_grp1"] = df["grp1"].apply(compute_mean_cpnt, df_comp=df_comp,
                                          cpnt=cpnt)
    df["mean_grp2"] = df["grp2"].apply(compute_mean_cpnt, df_comp=df_comp,
                                          cpnt=cpnt)
    df["grp1_vs_grp2"] = df.apply(get_regulation, axis=1)
    df["factor"] = [sf_name] * df.shape[0]
    df["cpnt"] = [cpnt] * df.shape[0]
    cols = ["grp1", "grp2", "factor", "cpnt", "mean_grp1", "mean_grp2", "p-adj",
            "grp1_vs_grp2"]
    if communities is not None:
        df["communities"] = [communities] * df.shape[0]
        cols = cols[0:3] + ["communities"] + cols[3:]
    return df[cols]


def create_figure_4_clip_table(clip_table_file: Path, feature: str,
                               threshold_feature: int, default_groups: int,
                               df_comp: pd.DataFrame, cpnt_type: str,
                               cpnt: str, df_id_ctrl: pd.DataFrame,
                               write_comp_table: bool,
                               display_size: bool, expression_file: str
                               ) -> Optional[pd.DataFrame]:
    """
    Create a component figure for a clip file

    :param clip_table_file: table containing data to produce the clip \
     community figure.
    :param feature: The feature of interest (gene or exon)
    :param threshold_feature: The minimum number of feature to \
    recover from enriched/impoverished communities
    :param default_groups: The number of community to get if none \
    (or not enough) community are enriched or impoverished \
    (depending on group parameter)
    :param df_comp: The dataframe containing the frequency of every `cpnt_type`
    :param cpnt_type: The kind of component of interest
    :param cpnt: The kind of component of interest
    :param df_id_ctrl: A dataframe indicating control exons
    :param write_comp_table: True to save comp table, false else
    :param display_size: True to display the size of the community .
    False to display nothing. (default False)
    :param expression_file: A file containing expression values of genes
    :return: A dataframe the summarize the component enriched in the \
    different community groups.
    """
    logging.info(f"Working on {clip_table_file.name} - {cpnt} ({cpnt_type})")
    df_group = get_interest_groups(clip_table_file, feature,
                                   threshold_feature, default_groups)
    if df_group is None:
        return None
    df_group = df_group.drop_duplicates()
    if feature == "gene":
        df_group["id_gene"] = df_group["id_gene"].astype(int)
        df_comp["id_gene"] = df_comp["id_gene"].astype(int)
    df_group = pd.concat([df_group, df_id_ctrl], ignore_index=True)
    if len(np.unique(df_group[f"id_{feature}"].values)) != df_group.shape[0]:
        print(df_group[df_group.duplicated(subset=f"id_{feature}")])
        raise ValueError("Found duplicates value in df_group, exiting...")
    df_comp = df_comp.merge(df_group, how="left", on=f"id_{feature}")
    df_comp = df_comp[-df_comp["group"].isna()]
    sf_name = clip_table_file.name.rstrip("_bar.txt")
    res_folder = clip_table_file.parent / f"{cpnt_type}_analysis"
    res_folder.mkdir(exist_ok=True)
    outfiles = [res_folder / sf_name /
                f"{cpnt_type}_{cpnt}_{sf_name}_thd"
                f"{threshold_feature}_dft{default_groups}.{ext}"
                for ext in ['txt', 'pdf', 'bar.txt']]
    outfiles[0].parent.mkdir(exist_ok=True)
    df_comp = filter_composition_dataframe(df_comp, feature, expression_file)
    df_stat = create_stat_df(df_comp, cpnt)
    df_stat.to_csv(outfiles[0], sep="\t", index=False)
    df_comp = update_composition_group(df_comp, display_size)
    make_barplot(df_comp, outfiles[1], cpnt, cpnt_type, sf_name)
    if write_comp_table:
        df_comp.to_csv(outfiles[2], sep="\t", index=False)
    return get_ft_enrichment_groups(clip_table_file.name, None, cpnt, df_stat,
                                    df_comp)


def filter_composition_dataframe(df_comp: pd.DataFrame, feature: str,
                                 expression_file: str
                                 ) -> pd.DataFrame:
    """
    Remove genes/exons with weak expression if a file `expression_file` is \
    given. This files contains the expression of genes.

    :param df_comp: A dataframe containing the composition of each \
    feature of interest
    :param feature: The feature of interest
    :param expression_file: A file containing the expression of gene
    :return: The dataframe filtered if `expression_file` is a file, else \
    return the same dataframe given in input
    >>> d = pd.DataFrame({"id_gene": [1, 2, 3], "A": [10, 15, 26],
    ... "group": ['ctrl', 'ctrl', 'ctrl']})
    >>> filter_composition_dataframe(d, "gene",
    ... Config.tests_files / "expression.txt")
       id_gene   A group
    0        1  10  ctrl
    2        3  26  ctrl
    >>> d = pd.DataFrame({"id_exon": ['1_1', '2_1', '3_1'], "A": [10, 15, 26],
    ... "group": ['ctrl', 'ctrl', 'ctrl']})
    >>> filter_composition_dataframe(d, "exon",
    ... Config.tests_files / "expression.txt")
      id_exon   A group
    0     1_1  10  ctrl
    2     3_1  26  ctrl
    >>> filter_composition_dataframe(d, "gene", "")
      id_exon   A group  id_gene
    0     1_1  10  ctrl        1
    1     2_1  15  ctrl        2
    2     3_1  26  ctrl        3
    """
    if not expression_file or not Path(expression_file).is_file():
        return  df_comp
    df = pd.read_csv(expression_file, sep="\t")
    genes = df.loc[df["baseMean"] >= 10., "gene"].to_list()
    if feature == "exon":
       df_comp["id_gene"] = df_comp["id_exon"].str.replace(r"_\d+", ""
                                                           ).astype(int)
    df_comp = df_comp[df_comp["id_gene"].isin(genes)].copy()
    if feature == "exon":
        df_comp.drop("id_gene", axis=1, inplace=True)
    return df_comp


def create_multiple_figures(clip_result_folder: Path, feature: str,
                            threshold_feature: int, default_groups: int,
                            cpnt_type: str, region: str = "gene",
                            ps: int = 1, display_size: bool = False,
                            expression_file: str = "",
                            logging_level: str = "DEBUG") -> None:
    """
    Create multiple composition figures from a result clip folder.

    :param clip_result_folder: Folder containing tables used to \
    create clip community figures
    :param feature: The feature of interest (exon, or gene)
    :param threshold_feature: The minimum number of feature needed \
    inside communities enriched or impoverished in peak_density to consider \
    those community (otherwise `default_groups` default number of community \
    are taken
    :param default_groups: The number of communities to take as default.
    :param cpnt_type: The kind of component analysed
    :param region: Only used when feature is gene. Corresponds to \
    the region studied within genes.
    :param ps: The number of processes to create
    :param display_size: True to display the size of the community,
    False to display nothing. (default False)
    :param expression_file: A file containing expression values of genes
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    list_tables = find_final_clip_tables(clip_result_folder)
    list_ft = get_features(cpnt_type)
    if len(list_ft) == 0:
        logging.warning(f"No clip table found in {clip_result_folder} !\n"
                        f"Exiting...")
        return None
    cnx = sqlite3.connect(Config.db_file)
    df_comp = get_cpnt_frequency(cnx, get_ft_id(cnx, feature), feature, region,
                                 cpnt_type)
    df_ctrl = add_ctrl_df(pd.read_csv(list_tables[0], sep="\t"), feature)
    conditions = list(product(list_tables, list_ft))
    ps = min(len(conditions), ps)
    pool = mp.Pool(processes=ps if ps > 0 else 1)
    processes = []
    write_bar_table = True
    for clip_file, cpnt in conditions:
        args = [clip_file, feature, threshold_feature, default_groups,
                df_comp, cpnt_type, cpnt, df_ctrl, write_bar_table,
                display_size, expression_file]
        processes.append(pool.apply_async(create_figure_4_clip_table, args))
        write_bar_table = False
    result = [p.get(timeout=None) for p in processes]
    pool.close()
    pool.join()
    recap = pd.concat([r for r in result if r is not None], axis=0,
                          ignore_index=True)
    recap.to_csv(clip_result_folder.parent / f"{cpnt_type}_analysis" /
                 f"recap_{cpnt_type}.csv", sep="\t", index=False)


############################################################################
#               Functions for composition analysis on
#      A clip folder produced by clip_launcher_4_many_communities
############################################################################


def find_clip_directories(folder: Path) -> List[Path]:
    """
    From a folder return every folder inside it.

    :param folder: A folder containing other subfolder. This folder \
    should have been produced by `clip_launcher_4_many_communities`.
    :return: The list of files ending with *_bar.txt

    >>> find_clip_directories(Path("fziufriubdsibf"))
    Traceback (most recent call last):
    ...
    NotADirectoryError: Folder fziufriubdsibf doesn't exists !
    """
    if not folder.is_dir():
        raise NotADirectoryError(f"Folder {folder} doesn't exists !")
    return [d for d in list(folder.glob("*")) if d.is_dir()]


def get_ctrl_dic(list_files: List[Path], feature: str) -> Dict:
    """
    Create a dictionary of control exons/genes
    :param list_files: A list of clip density table
    :param feature: gene or exon
    :return: A dictionary containing the controls for each clip files
    """
    return {
        f.name.replace(".tmp_bar.txt", ""): add_ctrl_df(
            pd.read_csv(f, sep="\t"), feature
        )
        for f in list_files
    }


def merge_figures(folder: Path, list_ft: List[str]) -> None:
    """
    Merge the figures together using imageMagick

    :param folder: A folder containing pdf files
    :param list_ft: The list of feature of interest
    """
    fig_name = folder.parent.name
    tmp_cmd = " ".join(f"{folder}/*_{ft}_*dft*.pdf" for ft in list_ft)
    cmd = f"montage -geometry +1+1 -tile 1X3  " \
          f"-compress jpeg -density 100 -label %t -pointsize 50 " \
          f"{tmp_cmd} {folder}/{fig_name}.pdf"
    sp.check_call(cmd, shell=True)


def create_figure_multi_clip_table(clip_table_file: Path, feature: str,
                                   threshold_feature: int, default_groups: int,
                                   df_comp: pd.DataFrame, cpnt_type: str,
                                   cpnt: str, df_id_ctrl: pd.DataFrame,
                                   write_comp_table: bool,
                                   display_size: bool,
                                   expression_file: str
                                   ) -> Optional[Tuple[Path, pd.DataFrame]]:
    """
    Create a component figure for a clip file

    :param clip_table_file: table containing data to produce the clip \
     community figure.
    :param feature: The feature of interest (gene or exon)
    :param threshold_feature: The minimum number of feature to \
    recover from enriched/impoverished communities
    :param default_groups: The number of community to get if none \
    (or not enough) community are enriched or impoverished \
    (depending on group parameter)
    :param df_comp: The dataframe containing the frequency of every `cpnt_type`
    :param cpnt_type: The kind of component of interest
    :param cpnt: The kind of component of interest
    :param df_id_ctrl: A dataframe indicating control exons
    :param write_comp_table: True to save comp table, false else
    :param display_size: True to display the size of the community .
    False to display nothing. (default False)
    :param expression_file: A file containing the expression values of \
    genes
    :return: The folder where the figure is created
    """
    logging.info(f"Working on {clip_table_file.parent.name} - "
                 f"{clip_table_file.name} - {cpnt} ({cpnt_type})")
    df_group = get_interest_groups(clip_table_file, feature,
                                   threshold_feature, default_groups)
    if df_group is None:
        return None
    df_group = df_group.drop_duplicates()
    if feature == "gene":
        df_group["id_gene"] = df_group["id_gene"].astype(int)
        df_comp["id_gene"] = df_comp["id_gene"].astype(int)
    df_group = pd.concat([df_group, df_id_ctrl], ignore_index=True)
    if len(np.unique(df_group[f"id_{feature}"].values)) != df_group.shape[0]:
        print(df_group[df_group.duplicated(subset=f"id_{feature}")])
        raise ValueError("Found duplicates value in df_group, exiting...")
    df_comp = df_comp.merge(df_group, how="left", on=f"id_{feature}")
    df_comp = df_comp[-df_comp["group"].isna()]
    sf_name = clip_table_file.parent.name
    res_folder = clip_table_file.parent / f"{cpnt_type}_analysis"
    res_folder.mkdir(exist_ok=True)
    outfiles = [res_folder /
                f"{clip_table_file.name.split('.')[0]}_{cpnt_type}_{cpnt}_thd" 
                f"{threshold_feature}_dft{default_groups}.{ext}"
                for ext in ['txt', 'pdf', 'bar.txt']]
    outfiles[0].parent.mkdir(exist_ok=True)
    df_comp = filter_composition_dataframe(df_comp, feature, expression_file)
    df_stat = create_stat_df(df_comp, cpnt)
    df_stat.to_csv(outfiles[0], sep="\t", index=False)
    df_comp = update_composition_group(df_comp, display_size)
    make_barplot(df_comp, outfiles[1], cpnt, cpnt_type, sf_name)
    if write_comp_table:
        df_comp.to_csv(outfiles[2], sep="\t", index=False)
    return res_folder, get_ft_enrichment_groups(
        clip_table_file.parent.name.replace("_merged", ""),
        clip_table_file.name.replace(".tmp_bar.txt", ""),
        cpnt, df_stat, df_comp)


def multi_tads_compo_figures(clip_result_folder: Path, feature: str,
                             threshold_feature: int, default_groups: int,
                             cpnt_type: str, region: str = "gene",
                             ps: int = 1, display_size: bool = False,
                             expression_file: str = "",
                             logging_level: str = "DEBUG") -> None:
    """
    Create multiple composition figures from a result clip folder (produced \
    by clip_launcher_4_many_communities).

    :param clip_result_folder: Folder containing tables used to \
    create clip community figures
    :param feature: The feature of interest (exon, or gene)
    :param threshold_feature: The minimum number of feature needed \
    inside communities enriched or impoverished in peak_density to consider \
    those community (otherwise `default_groups` default number of community \
    are taken
    :param default_groups: The number of communities to take as default.
    :param cpnt_type: The kind of component analysed
    :param region: Only used when feature is gene. Corresponds to \
    the region studied within genes.
    :param ps: The number of processes to create
    :param display_size: True to display the size of the community,
    False to display nothing. (default False)
    :param expression_file: A file containing the expression values of \
    genes
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    list_folder = find_clip_directories(clip_result_folder)
    list_tables = [find_final_clip_tables(folder) for folder in list_folder]
    dic_df_ctrl = get_ctrl_dic(list_tables[0], feature)
    list_tables = np.array(list_tables).flatten()
    list_ft = get_features(cpnt_type)
    if len(list_ft) == 0:
        logging.warning(f"No clip table found in {clip_result_folder} !\n"
                        f"Exiting...")
        return None
    cnx = sqlite3.connect(Config.db_file)
    df_comp = get_cpnt_frequency(cnx, get_ft_id(cnx, feature), feature, region,
                                 cpnt_type)
    conditions = list(product(list_tables, list_ft))
    ps = min(len(conditions), ps)
    pool = mp.Pool(processes=ps if ps > 0 else 1)
    processes = []
    write_bar_table = True
    for clip_file, cpnt in conditions:
        df_ctrl = dic_df_ctrl[clip_file.name.replace(".tmp_bar.txt", "")]
        args = [clip_file, feature, threshold_feature, default_groups,
                df_comp, cpnt_type, cpnt, df_ctrl, write_bar_table,
                display_size, expression_file]
        processes.append(pool.apply_async(create_figure_multi_clip_table,
                                          args))
        write_bar_table = False
    results = [p.get(timeout=None) for p in processes]
    pool.close()
    pool.join()
    results =  [r for r in results if r is not None]
    list_folder = np.unique([r[0] for r in results])
    recap_data = [r[1] for r in results]
    summary_df = pd.concat(recap_data, axis=0, ignore_index=True)
    summary_df.to_csv(clip_result_folder / f"recap_{cpnt_type}.csv", sep="\t",
                      index=False)
    for folder in list_folder:
        merge_figures(folder, list_ft)


@lp.parse(feature=["gene", "exon"], threshold_feature=range(1, 1001),
          default_groups=range(1, 100), cpnt_type=["nt", "dnt", "aa"])
def multi_tads_launcher(clip_result_folder: str, feature: str,
                        threshold_feature: int = 100, default_groups: int = 20,
                        cpnt_type: str = "nt", region: str = "gene",
                        ps: int = 1, display_size: bool = False,
                        expression_file: str = "",
                        logging_level: str = "DEBUG") -> None:
    """
    Launch  multi_tads_compo_figures script

    :param clip_result_folder: Folder containing tables used to \
    create clip community figures
    :param feature: The feature of interest (exon, or gene)
    :param threshold_feature: The minimum number of feature needed \
    inside communities enriched or impoverished in peak_density to consider \
    those community (otherwise `default_groups` default number of community \
    are taken. Default 100)
    :param default_groups: The number of communities to take as default. \
    (default 20)
    :param cpnt_type: The kind of component analysed (default nt)
    :param region: Only used when feature is gene. Corresponds to \
    the region studied within genes. (default gene)
    :param ps: The number of processes to create (default 1)
    :param display_size: True to display the size of the community,
    False to display nothing. (default False)
    :param expression_file: A file containing the expression values of \
    genes (default ''). If this file is provided, then the weakly expressed \
    genes are filtered out from the analysis.
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    multi_tads_compo_figures(Path(clip_result_folder), feature,
                             threshold_feature, default_groups,
                             cpnt_type, region, ps, display_size,
                             expression_file, logging_level)


if __name__ == "__main__":
    import sys
    if len(sys.argv) <= 1:
        import doctest
        doctest.testmod()
    else:
        multi_tads_launcher()
