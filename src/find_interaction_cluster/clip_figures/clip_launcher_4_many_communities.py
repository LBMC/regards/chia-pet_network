#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch Clip analysis for many communities
"""


import logging
from typing import Tuple, Dict, List, Union
from pathlib import Path
from .config import ConfigClip
from .clip_analyser import create_table, \
    add_regulation_column, create_community_fig, find_or_create_community, \
    ConfigGraph, logging_def
import multiprocessing as mp
from itertools import product
import lazyparser as lp
import numpy as np
import subprocess as sp
import sqlite3


def select_community_file(project: str, weight: int, global_weight: int,
                          same_gene: bool, inflation: float, cell_line: str,
                          feature: str, clip_file: Union[Path, Tuple],
                          community_file: str = "") -> Tuple[Path, Path]:
    """
    Return the community file and output folder that will be used.

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_file: A bed file containing clip
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :return: The community file used and the output folder used.

    """
    if community_file == "":
        com_file = find_or_create_community(project, weight, global_weight,
                                            same_gene, inflation, cell_line,
                                            feature)
    else:
        com_file = Path(community_file)
        if not com_file.is_file():
            raise FileNotFoundError(f"File {com_file} was not found !")
    if isinstance(clip_file, Path):
        outname = clip_file.name.split('.')[0]
        type_input = "CLIP"
    else:
        outname = f"{clip_file[0]}_{clip_file[1]}"
        type_input = f"SL_{clip_file[1]}"
    output = ConfigClip.output_folder / \
        f"multiTAD_{type_input}_community_figures-{feature}-{cell_line}" / \
        outname
    return com_file, output


def create_figure(p: Dict, clip_file: Union[Path, Tuple],
                  feature_bed: Path,
                  community_file: Tuple[str, str],
                  test_type: str = "permutation",
                  iteration: int = 10000, display_size: bool = False,
                  sl_reg: bool = False) -> Path:
    """
    Create the final figure
    :param p: A dictionary containing parameter used to compute \
    HipMCL communities
    :param clip_file: A bed file containing clip or a tuple containing \
    a splicing factor and a regulation
    :param feature_bed: A bed files containing exons or genes depending on \
    feature parameter.
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param community_file: A Tuple containing a  file containing custom \
    communities. If it equals to '' then weight, global weight and \
    same genes parameter are used to find the community files computed \
     with ChIA-PET data. The second item of the tuple is it's name.
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :return: Folder containing he figures
    """
    logging.info(f"Working on {clip_file} - {community_file[0]} - "
                 f"{community_file[1]}")
    com_file, output = select_community_file(p["project"], p["weight"],
                                             p["global_weight"],
                                             p["same_gene"], p["inflation"],
                                             p["cell_line"],
                                             p["feature"], clip_file,
                                             community_file[0])
    output.mkdir(exist_ok=True, parents=True)
    outfile = output / f"{community_file[1]}.tmp.pdf"
    final_table = create_table(p["feature"], clip_file, feature_bed, com_file)
    my_name = clip_file.name.split("_")[0] if isinstance(clip_file, Path) \
        else clip_file[0]
    if sl_reg:
        final_table = add_regulation_column(final_table,
                                            my_name,
                                            p["feature"])
    my_col = "peak_density" \
        if "peak_density" in final_table.columns else "regulation"
    create_community_fig(final_table, p["feature"], my_col, outfile,
                         test_type, iteration=iteration,
                         display_size=display_size)
    return outfile.parent


def merge_figures(folder: Path) -> None:
    """
    Merge the figures together using imageMagick

    :param folder: A folder containing pdf files
    """
    fig_name = folder.name
    cmd = f"montage -geometry +1+1 -tile 1X4  " \
          f"-compress jpeg -density 100 -label %t -pointsize 50 " \
          f"{folder}/*.tmp.pdf {folder}/{fig_name}.pdf"
    sp.check_call(cmd, shell=True)


def load_clip_folder_param(clip_folder: str) -> List[Union[Path, Tuple]]:
    """
    Load every file in the folder clip_folder if it exist. Else if \
    clip_folder is in [up, down, reg] load \
    the name of every splicing factors used in splicing lore.

    :param clip_folder: Either a folder containing clip or a string \
    corresponding to a regulation. For a clip folder the density of \
    peak of every gene/exon is computed to make the figure for every clip file.
    If a regulation is used then 1 is used to mark an exon/gene regulated in \
    splicing for every factor
    :return: The bed file in the folder clip_folder or The list of the \
    factor analysed in splicing lore.

    >>> [f.name for f in
    ... load_clip_folder_param(ConfigClip.test_clip_bed.parent)]
    ['genes.bed', 'exons.bed', 'clip.bed']
    >>> r = load_clip_folder_param("down")
    >>> len(r)
    52
    >>> r[0:3]
    [('PTBP1', 'down'), ('SRSF1', 'down'), ('MBNL1_2', 'down')]
    """
    if clip_folder in ["up", "down", "reg"]:
        query = "SELECT DISTINCT sf_name FROM cin_project_splicing_lore"
        cnx = sqlite3.connect(ConfigGraph.db_file)
        c = cnx.cursor()
        c.execute(query)
        result = [(sf_name[0], clip_folder) for sf_name in c.fetchall()]
        c.close()
        cnx.close()
        return result
    clip_folder = Path(clip_folder)
    if clip_folder.is_dir():
        return list(clip_folder.glob("*.bed")) + \
        list(clip_folder.glob("*.bed.gz"))
    raise ValueError(f"clip_folder parameter must be an existing folder or "
                     f"be in ['up', 'down', 'reg']")


@lp.parse(test_type=["permutation", "lm"], feature=["gene", "exon"])
def clip_folder_analysis(clip_folder: str, weight: int,
                         global_weight: int, same_gene: bool = True,
                         project: str = "GSM1018963_GSM1018964",
                         inflation: float = 1.5,
                         cell_line: str = "ALL",
                         feature: str = "exon",
                         test_type: str = "permutation",
                         iteration: int = 10000, display_size: bool = False,
                         sl_reg: bool = False) -> None:
    """
    Create the final figure
    :param clip_folder: Either a folder containing clip or a string \
    corresponding to a regulation. For a clip folder the density of \
    peak of every gene/exon is computed to make the figure for every clip file.
    If a regulation is used then 1 is used to mark an exon/gene regulated in \
    splicing for every factor
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_folder: A folder containing clip file
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    """
    logging_def(ConfigGraph.community_folder, __file__, "INFO")
    feature_bed = ConfigClip.bed_gene if feature == "gene" \
        else ConfigClip.bed_exon
    name_or_file = load_clip_folder_param(clip_folder)
    processes = []
    p = {"project": project, "weight": weight, "global_weight": global_weight,
         "same_gene": same_gene, "inflation": inflation,
         "cell_line": cell_line, "feature": feature}

    prod = list(product(name_or_file, zip(ConfigClip.communities,
                        ConfigClip.communities_name)))
    pool = mp.Pool(processes=min(len(prod), ConfigGraph.cpu))
    for mfile,  community_file in prod:
        if community_file[0] == "":
            tmp = \
                f"HIPMCL_g{global_weight}_w{weight}_{inflation}"
            community_file = (community_file[0], tmp)
        args = [p, mfile, feature_bed, community_file, test_type, iteration,
                display_size, sl_reg]
        processes.append(pool.apply_async(create_figure, args))
    list_path = [str(p.get(timeout=None)) for p in processes]
    list_path = np.unique(list_path)
    for my_folder in list_path:
        merge_figures(Path(my_folder))


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        import doctest
        doctest.testmod()
    else:
        clip_folder_analysis()
