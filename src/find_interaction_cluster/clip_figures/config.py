#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This file contains the variables needed to make \
this submodule work
"""


from ..config import ConfigGraph, Config
from ...hic_TAD_caller.config_tad_hic import ConfigHiC as CHic


class ConfigClip:
    """
    Contains required variables for this submodules
    """
    output_folder = ConfigGraph.output_folder / "clip_community_figures"
    clip_folder = Config.data / "CLIP_bed"
    bed_gene = Config.bed_gene
    bed_exon = Config.bed_exon
    test_gene_bed = Config.tests_files / "genes.bed"
    test_clip_bed = Config.tests_files / "clip.bed"
    test_clip_file = Config.tests_files / "SF_test_bar.txt"
    bed = Config.data / "bed"
    # Location of the community files
    communities = [
        bed / "communities_HiC_TADs_Stein-MCF7-WT.txt",
        bed / "communities_K562_Lieberman-raw_TADs.txt",
        bed / "communities_ISO_Costantini.lifted.txt",
        "",
        CHic.hic_tad_caller / "communities_10000_K652_4DN_4DNFIJBJ6QVH_hg19.bed",
        CHic.hic_tad_caller / "communities_10000_K562_External_4DNFITUOMFUQ_hg19.bed",
        CHic.hic_tad_caller / "communities_25000_K652_4DN_4DNFIJBJ6QVH_hg19.bed",
        CHic.hic_tad_caller / "communities_25000_K562_External_4DNFITUOMFUQ_hg19.bed"
    ]
    # Name of the community files
    communities_name = ["TAD_MCF7", "TAD_K562", "Isochores", "",
                        "TAD_HIC_K562_4DN_10kb", "TAD_HIC_K562_External_10kb",
                        "TAD_HIC_K562_4DN_25kb", "TAD_HIC_K562_External_25kb"]