#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create the communities figures launcher from  a community \
folder
"""

import lazyparser as lp
from pathlib import Path
from .clip_analyser import clip_folder_analysis


@lp.parse(test_type=["permutation", "lm"], feature=["gene", "exon"])
def clip_analysis(clip_folder: str, weight: int = -1,
                  global_weight: int = -1, same_gene: bool = True,
                  inflation: float = 1.5,
                  cell_line: str = "ALL",
                  feature: str = "exon",
                  project: str = "GSM1018963_GSM1018964",
                  test_type: str = "permutation", iteration: int = 10000,
                  display_size: bool=False,
                  community_file: str = "", sl_reg: bool = False,
                  ps: int = 1, logging_level: str = "DEBUG") -> None:
    """
    Create the final figure.

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter (default 1.5)
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_folder: A folder containing clip file or a string \
    corresponding to a splicing factor followed by a regulation
    :param test_type: The kind of test to perform for frequency analysis. \
    (default 'permutation') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :param ps: The number of processes to create (default 1)
    :param logging_level: The level of data to display (default 'DISABLE')
    :return: The final dataframe that can be used to create clip figures
    """
    if (weight == -1 or global_weight == -1) and community_file == "":
        raise ValueError(f"Global weight or weight can't be equal to one if "
                         f"community_file is not defined !")
    if "_up" not in clip_folder and "_down" not in clip_folder:
        clip_folder = Path(clip_folder)
        if not clip_folder.is_dir():
            raise NotADirectoryError(
                f"{clip_folder} is not an existing directory")
    else:
        clip_folder = clip_folder.rsplit("_", 1)
    if test_type == "lm":
        raise NotImplementedError("test_type for lm is not implemented !")
    clip_folder_analysis(clip_folder, project, weight, global_weight,
                         same_gene, inflation, cell_line, feature, test_type,
                         iteration, display_size, community_file, sl_reg, ps,
                         logging_level)


clip_analysis()