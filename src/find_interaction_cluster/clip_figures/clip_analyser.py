#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a community figure \
from a bed file
"""


from .config import ConfigClip, ConfigGraph
import subprocess as sp
from pathlib import Path
from io import StringIO
import pandas as pd
from ..community_finder import multiple_community_launcher, get_projects
from ..community_figures.fig_functions import create_community_fig
from ...logging_conf import logging_def
import logging
from typing import Tuple, List, Union
import multiprocessing as mp
from ...figures_utils.exons_interactions import get_every_events_4_a_sl
import sqlite3
from ..sf_and_communities import get_sfname
import numpy as np
from ..sf_and_communities import get_regulated_features


def bedtools_intersect(gene_bed: Path, clip_bed: Path,
                       feature: str = "exon") -> pd.DataFrame:
    """

    :param gene_bed: A bed file containing FasterDB genes
    :param clip_bed: A bed file containing CLIP peaks
    :param feature: The feature we want to analyse (default 'exon')
    :return: The number of peaks inside each genes

    >>> bedtools_intersect(ConfigClip.test_gene_bed, ConfigClip.test_clip_bed,
    ... "gene")
        id_gene  clip_peak  peak_density
    0         1          2      0.054877
    1         2          1      0.029736
    2         3          3      0.076251
    3         4          0      0.000000
    4         5          0      0.000000
    5         6          0      0.000000
    6         7          0      0.000000
    7         8          0      0.000000
    8         9          0      0.000000
    9       415          0      0.000000
    10    10123          0      0.000000

    """
    res = sp.check_output(f"bedtools intersect -a {gene_bed} "
                          f"-b {clip_bed} -c -s", shell=True)
    res = StringIO(str(res, "utf-8"))
    id_col = f"id_{feature}"
    columns = ["chr", "start", "end", id_col, "name", "strand", "clip_peak"]
    df = pd.read_csv(res, sep="\t", names=columns)
    df['peak_density'] = df['clip_peak'] / ((df['end'] - df['start']) / 1000)
    return df[[f'id_{feature}', 'clip_peak', 'peak_density']]


def get_ctrl_exon(cnx: sqlite3.Connection, feature: str = 'exon') -> List[str]:
    """
    Get the list of fasterDB exons/genes.

    :param cnx: Connection to chIA-PET database
    :param feature: The kind of feature to analyse
    :return: The list of fasterDB exons
    """
    cursor = cnx.cursor()
    query = f'SELECT id FROM cin_{feature}'
    cursor.execute(query)
    res = np.asarray(cursor.fetchall()).flatten()
    cursor.close()
    return list(res)


def get_regulation_table(cnx: sqlite3.Connection,
                         sf_name: str, reg: str,
                         feature: str = 'exon',
                         factor_type: str = "splicing") -> pd.DataFrame:
    """
    Get the list of exons or genes containins exons regulated by a splicing \
    factor

    :param cnx: connection to chia-pet database
    :param sf_name: The name of the splicing factor for \
    which we want to get the exons regulated by a splicing factor
    :param reg: The regulation of the exons regulated by sf_name (up, down, \
    reg)
    :param feature: The kind of feature to analyse
    :param factor_type: The kind of factor to analyse
    :return: The dataframe containing every human exons or genes \
    regulated by the splicing factor

    >>> r = get_regulation_table(sqlite3.connect(ConfigGraph.db_file),
    ... "DAZAP1", "down", "exon")
    >>> r.shape
    (224994, 2)
    >>> r.head()
      id_exon  regulation
    0    75_6           1
    1    89_3           1
    2   176_4           1
    3   315_4           1
    4  374_18           1
    >>> r[r['regulation'] == 1].shape
    (555, 2)
    >>> exon_reg = r[r['regulation'] == 1]['id_exon'].to_list()
    >>> regv = list(np.unique([int(e.split("_")[0]) for e in exon_reg]))
    >>> r = get_regulation_table(sqlite3.connect(ConfigGraph.db_file),
    ... "DAZAP1", "down", "gene")
    >>> r.shape
    (19426, 2)
    >>> r.head()
       id_gene  regulation
    0       75           1
    1       89           1
    2      149           1
    3      160           1
    4      166           1
    >>> r[r['regulation'] == 1].shape
    (531, 2)
    >>> regv == r[r['regulation'] == 1]["id_gene"].to_list()
    True
    >>> r = get_regulation_table(sqlite3.connect(ConfigGraph.db_file),
    ... "DAZAP1", "reg", "gene")
    >>> r[r['regulation'] == 1].shape
    (734, 2)
    """
    list_ft = get_regulated_features(cnx, sf_name, reg, feature, factor_type)
    if feature == "gene":
        list_ft = sorted(list(map(int, list_ft)))
    ctrl_ft = get_ctrl_exon(sqlite3.connect(ConfigGraph.db_file), feature)
    ctrl_ft = [c for c in ctrl_ft if c not in list_ft]
    return pd.DataFrame({f"id_{feature}": list_ft + ctrl_ft,
                         "regulation": [1] * len(list_ft) +
                                       [0] * len(ctrl_ft)})


def find_or_create_community(project: str, weight: int, global_weight: int,
                             same_gene: bool, inflation: float, cell_line: str,
                             feature: str) -> Path:
    """
    Find the community file of interest, or if it doesn't exist, create it.

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter.
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :return: The file containing community

    >>> res = find_or_create_community("", 3, 3, True, 1.5, "ALL", "gene")
    >>> res_path = str(res.relative_to(Path(__file__).parents[3]))
    >>> res_path.endswith("global-weight-3_weight-3_same_gene-True_gene.txt")
    True
    >>> f = "results/community_of_co-localized-exons/" + \
    "communities/weight-3_global_weight-3-1.5"
    >>> res_path.startswith(f)
    True
    """
    cfile = ConfigGraph.get_community_file(project, weight, global_weight,
                                           same_gene, inflation, cell_line,
                                           feature, ".txt")
    if not cfile.is_file():
        project = get_projects(global_weight, project)
        multiple_community_launcher(weight, global_weight, project, same_gene,
                                    inflation, cell_line, False,
                                    feature=feature)
    if not cfile.is_file():
        raise FileNotFoundError(f"The file {cfile} could not be created !")
    return cfile


def get_community_tables(df: pd.DataFrame, feature: str) -> pd.DataFrame:
    """
    Create a dataframe indicating for each feature, to which community it \
    belong.

    :param df: A dataframe of community
    :param feature: The feature we want to analyse (default 'exon')
    :return: dataframe indicating for each feature, to which community it \
    belong.

    >>> d = pd.DataFrame({"community": ["C1", "C2"], "nodes": [2, 3],
    ... "edges": [1, 3], "EC": [1, 1], "HCS": ["no", "no"],
    ... "%E vs E in complete G": [36, 40], "cov": [0.96, 0.96],
    ... "modularity": [0.97, 0.97], "genes": ["1, 2", "3, 4, 5"],
    ... "project": ["Global-weight-3", "Global-weight-3"]})
    >>> get_community_tables(d, "gene")
       id_gene community  community_size
    0        1        C1               2
    1        2        C1               2
    2        3        C2               3
    3        4        C2               3
    4        5        C2               3
    """
    dic = {f"id_{feature}": [], "community": [], "community_size": []}
    for i in range(df.shape[0]):
        row = df.iloc[i, :]
        ft = row[f"{feature}s"].split(", ")
        dic[f"id_{feature}"] += row[f"{feature}s"].split(", ")
        dic["community"] += [row["community"]] * len(ft)
        dic["community_size"] += [row["nodes"]] * len(ft)
    df = pd.DataFrame(dic)
    if feature == "gene":
        df["id_gene"] = df["id_gene"].astype(int)
    return df


def merge_dataframes(peak_df: pd.DataFrame, com_df: pd.DataFrame,
                     feature: str) -> pd.DataFrame:
    """
    Merge a dataframe containing the numbers of clip peak per feature \
    with the dataframe containing the community of each feature.

    :param peak_df: Dataframe containing the number of clip peak per \
    feature
    :param com_df: Dataframe containing the community of each figure
    :param feature: The feature we want to analyse (default 'exon')
    :return: The dataframe merged

    >>> d1 = pd.DataFrame({"id_gene": [0, 1, 2, 3, 4, 5, 6, 7],
    ... "peak_density": [0.054877, 0.029736, 0.076251, 0, 0, 0, 0, 0]})
    >>> d2 = pd.DataFrame({"id_gene": [0, 1, 2, 3, 4],
    ... "community": ["C1", "C1", "C2", "C2", "C2"],
    ... "community_size": [2, 2, 3, 3, 3]})
    >>> merge_dataframes(d1, d2, "gene")
       id_gene  peak_density community  community_size
    0        0      0.054877        C1             2.0
    1        1      0.029736        C1             2.0
    2        2      0.076251        C2             3.0
    3        3      0.000000        C2             3.0
    4        4      0.000000        C2             3.0
    5        5      0.000000       NaN             NaN
    6        6      0.000000       NaN             NaN
    7        7      0.000000       NaN             NaN
    """
    return peak_df.merge(com_df, how="left", on=f"id_{feature}")


def create_table(feature: str, clip_file: Union[Path, Tuple],
                 feature_bed: Path, com_file: Path,
                 factor_type: str = "splicing"
                 ) -> pd.DataFrame:
    """
    Create the final table used to create community figure.

    :param feature: The feature we want to analyse (default 'exon')
    :param clip_file: A bed file containing clip peaks or a splicing \
    factor name followed by the wanted regulation
    :param feature_bed: A bed files containing exons or genes depending on \
    feature parameter.
    :param com_file: A file containing communities
    :param factor_type: The kind of factor to analyse
    :return: The final dataframe that can be used to create clip figures

    >>> cf = find_or_create_community("", 3, 3, True, 1.5, "ALL", "gene")
    >>> create_table("gene", ConfigClip.test_clip_bed,
    ... ConfigClip.test_gene_bed, cf)
        id_gene  clip_peak  peak_density community  community_size
    0         1          2      0.054877       NaN             NaN
    1         2          1      0.029736       NaN             NaN
    2         3          3      0.076251       NaN             NaN
    3         4          0      0.000000       NaN             NaN
    4         5          0      0.000000       NaN             NaN
    5         6          0      0.000000       NaN             NaN
    6         7          0      0.000000       NaN             NaN
    7         8          0      0.000000       NaN             NaN
    8         9          0      0.000000       NaN             NaN
    9       415          0      0.000000        C1            11.0
    10    10123          0      0.000000       NaN             NaN

    >>> r = create_table("gene", ("DAZAP1", "up"),
    ... ConfigClip.test_gene_bed, cf)
    >>> r.head()
       id_gene  regulation community  community_size
    0       89           1       NaN             NaN
    1      173           1       NaN             NaN
    2      215           1       NaN             NaN
    3      282           1     C1386            16.0
    4      284           1     C1386            16.0
    >>> r[r["regulation"]==1].shape
    (222, 4)
    """
    threshold = 10 if feature == "gene" else 50
    if isinstance(clip_file, Path):
        df_clip = bedtools_intersect(feature_bed, clip_file, feature)
    else:
        df_clip = get_regulation_table(sqlite3.connect(ConfigGraph.db_file),
                                       clip_file[0], clip_file[1],
                                       feature, factor_type)
    df_tmp = pd.read_csv(com_file, sep="\t")
    df_com = get_community_tables(df_tmp, feature)
    df_com = df_com.loc[df_com["community_size"] >= threshold, :]
    return merge_dataframes(df_clip, df_com, feature)


def select_community_file(project: str, weight: int, global_weight: int,
                          same_gene: bool, inflation: float, cell_line: str,
                          feature: str,
                          community_file: str = "") -> Tuple[Path, Path]:
    """
    Return the community file and output folder that will be used.

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :return: The community file used and the output folder used.

    """
    if community_file == "":
        com_file = find_or_create_community(project, weight, global_weight,
                                            same_gene, inflation, cell_line,
                                            feature)
        output = com_file.parent / f"CLIP_community_figures_{feature}"
    else:
        com_file = Path(community_file)
        if not com_file.is_file():
            raise FileNotFoundError(f"File {com_file} was not found !")
        tmp_name = com_file.name.replace(".txt", "")
        output = ConfigClip.output_folder / \
            f"CLIP_community_figures-{feature}-{tmp_name}"
    return com_file, output


def add_regulation_column(df_table: pd.DataFrame, sf_name: str, feature: str,
                          ) -> pd.DataFrame:
    """
    Add a column community_data on df_table corresponding to 1 \
    if an exon is regulated in a gene (0 elseà if feature is gene or 1 if \
    the exon is regulated.

    :param df_table: A dataframe containing the peak density for each gene \
    or exons.
    :param sf_name: The splicing factor of interest
    :param feature: The kind of feature analysed
    :return: The dataframe updated

    >>> dexon = {"id_exon": ['11553_16', '3222_30', '1001_3'],
    ...          'clip_peak': [0, 1, 0], "peak_density": [0, 0.1, 0],
    ...          'community': ['C1', 'C1', 'C2']}
    >>> add_regulation_column(pd.DataFrame(dexon), 'TRA2A_B', 'exon')
        id_exon  clip_peak  peak_density community  community_data
    0  11553_16          0           0.0        C1               1
    1   3222_30          1           0.1        C1               1
    2    1001_3          0           0.0        C2               0
    >>> dgene = {"id_gene": [11553, 3222, 1001],
    ...          'clip_peak': [0, 1, 0], "peak_density": [0, 0.1, 0],
    ...          'community': ['C1', 'C1', 'C2']}
    >>> add_regulation_column(pd.DataFrame(dgene), 'TRA2A_B', 'gene')
       id_gene  clip_peak  peak_density community  community_data
    0    11553          0           0.0        C1               1
    1     3222          1           0.1        C1               1
    2     1001          0           0.0        C2               0
    >>> add_regulation_column(pd.DataFrame(dgene), 'TRAgdghfh', 'gene')
       id_gene  clip_peak  peak_density community
    0    11553          0           0.0        C1
    1     3222          1           0.1        C1
    2     1001          0           0.0        C2
    """
    if sf_name not in get_sfname():
        return df_table
    up_exon, x = get_every_events_4_a_sl(sqlite3.connect(ConfigGraph.db_file),
                                         sf_name, "up")
    down_exon, x = get_every_events_4_a_sl(
        sqlite3.connect(ConfigGraph.db_file), sf_name, "down")
    exons = up_exon[f"{sf_name}_up"] + down_exon[f"{sf_name}_down"]
    if feature == "exon":
        df_table["community_data"] = [0] * len(df_table)
        df_table.loc[df_table[f"id_{feature}"].isin(exons),
                     "community_data"] = 1
    else:
        df = pd.read_csv(ConfigClip.bed_exon, sep="\t",
                         names=["chr", "start", "end", "id_exon", "sc", "s"])
        df = df[["id_exon"]]
        df["id_gene"] = df["id_exon"].str.replace(r"_\d+", "").astype(int)
        df["community_data"] = [0] * df.shape[0]
        df.loc[df["id_exon"].isin(exons), "community_data"] = 1
        df.drop("id_exon", axis=1, inplace=True)
        df = df.groupby("id_gene").max().reset_index()
        df_table = df_table.merge(df, how="left", on="id_gene")
    return df_table


def create_figure(project: str, weight: int, global_weight: int,
                  same_gene: bool, inflation: float, cell_line: str,
                  feature: str, clip_file: Union[Path, Tuple],
                  feature_bed: Path, test_type: str = "permutation",
                  iteration: int = 10000, display_size: bool = False,
                  community_file: str = "", sl_reg: bool = False) -> None:
    """
    Create the final figure
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_file: A bed file containing clip
    :param feature_bed: A bed files containing exons or genes depending on \
    feature parameter.
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    """
    logging.info(f"Working on {clip_file}")
    com_file, output = select_community_file(project, weight, global_weight,
                                             same_gene, inflation, cell_line,
                                             feature, community_file)
    output.mkdir(exist_ok=True, parents=True)
    my_name = clip_file.name.split('.')[0] if isinstance(clip_file, Path) \
        else "_".join(clip_file)
    outfile = output / f"{my_name}.pdf"
    final_table = create_table(feature, clip_file, feature_bed, com_file)
    my_name = clip_file.name.split("_")[0] if isinstance(clip_file, Path) \
        else "_".join(clip_file)
    if sl_reg:
        final_table = add_regulation_column(final_table,
                                            my_name,
                                            feature)
    my_col = "peak_density" \
        if "peak_density" in final_table.columns else "regulation"
    print(outfile)
    create_community_fig(final_table, feature, my_col, outfile,
                         test_type, iteration=iteration,
                         display_size=display_size)


def clip_folder_analysis(clip_folder: Union[Path, Tuple], project: str,
                         weight: int,
                         global_weight: int, same_gene: bool, inflation: float,
                         cell_line: str,
                         feature: str, test_type: str = "permutation",
                         iteration: int = 10000, display_size: bool=False,
                         community_file: str = "",  sl_reg: bool = False,
                         ps: int = 1, logging_level: str = "DEBUG") -> None:
    """
    Create the final figure
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_folder: A folder containing clip file
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :param ps: The number of processes to create (default 1)
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    feature_bed = ConfigClip.bed_gene if feature == "gene" \
        else ConfigClip.bed_exon
    if isinstance(clip_folder, Path):
        files = list(clip_folder.glob("*.bed")) + \
            list(clip_folder.glob("*.bed.gz"))
    else:
        files = [clip_folder]
    pool = mp.Pool(processes=min(len(files), ps))
    processes = []
    for mfile in files:
        args = [project, weight, global_weight, same_gene, inflation,
                cell_line, feature,
                mfile, feature_bed, test_type, iteration, display_size,
                community_file, sl_reg]
        processes.append(pool.apply_async(create_figure, args))
    [p.get(timeout=None) for p in processes]


if __name__ == "__main__":
    import doctest
    doctest.testmod()