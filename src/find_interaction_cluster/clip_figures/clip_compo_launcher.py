#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch clip_compo_analyser
"""

import lazyparser as lp
from .clip_compo_analyser import create_multiple_figures
from pathlib import Path
import multiprocessing as mp


@lp.parse(ps = range(1, mp.cpu_count() + 1), feature=["gene", "exon"],
          threshold_feature="9 < threshold_feature < 10000",
          default_groups=range(10, 101), cpnt_type=["nt", "dnt", "tnt", "aa"],
          region=["gene", "exon", "intron"])
def launcher(clip_result_folder: str, feature: str,
             threshold_feature: int, default_groups: int,
             cpnt_type: str, region: str = "gene",
             ps: int = 1, display_size: bool=False, expression_file: str = "",
             logging_level: str = "DISABLE") -> None:
    """
    Create multiple composition figures from a result clip folder.

    :param clip_result_folder: Folder containing tables used to \
    create clip community figures
    :param feature: The feature of interest (exon, or gene)
    :param threshold_feature: The minimum number of feature needed \
    inside communities enriched or impoverished in peak_density to consider \
    those community (otherwise `default_groups` default number of community \
    are taken
    :param default_groups: The number of communities to take as default.
    :param cpnt_type: The kind of component analysed
    :param region: Only used when feature is gene. Corresponds to \
    the region studied within genes. (default: 'gene')
    :param ps: The number of processes to create (default 1)
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param expression_file: A file containing the expression values of \
    genes (default ''). If this file is provided, then the weakly expressed \
    genes are filtered out from the analysis.
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    clip_result_folder = Path(clip_result_folder)
    if not clip_result_folder.is_dir():
        raise NotADirectoryError(f"Directory {clip_result_folder} don't exist")
    create_multiple_figures(clip_result_folder, feature, threshold_feature,
                            default_groups, cpnt_type, region, ps,
                            display_size, expression_file, logging_level)


if __name__ == "__main__":
    launcher()
