#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create fake communities based on \
the density of clip peaks and create community figures based on those fake \
communities.
"""


from pathlib import Path
import pandas as pd
from .clip_analyser import find_or_create_community, ConfigClip, \
    bedtools_intersect, logging, add_regulation_column, create_community_fig, \
    logging_def, ConfigGraph
from .clip_compo_analyser import get_cpnt_frequency, get_ft_id, Config
import numpy as np
from doctest import testmod
import math
import multiprocessing as mp
import lazyparser as lp
import sqlite3
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import linregress
from typing import Tuple


def create_table(feature: str, clip_file: Path,
                 feature_bed: Path) -> pd.DataFrame:
    """
    Create the final table used to create community figure.

    :param feature: The feature we want to analyse (default 'exon')
    :param clip_file: A bed file containing clip
    :param feature_bed: A bed files containing exons or genes depending on \
    feature parameter.
    :return: The final dataframe that can be used to create clip figures

    >>> cf = find_or_create_community("", 3, 3, True, 1.5, "ALL", "gene")
    >>> create_table("gene", ConfigClip.test_clip_bed,
    ... ConfigClip.test_gene_bed)
        id_gene  clip_peak  peak_density community  community_size
    0         4          0      0.000000        C1              11
    1         5          0      0.000000        C1              11
    2         6          0      0.000000        C1              11
    3         7          0      0.000000        C1              11
    4         8          0      0.000000        C1              11
    5         9          0      0.000000        C1              11
    6       415          0      0.000000        C1              11
    7     10123          0      0.000000        C1              11
    8         2          1      0.029736        C1              11
    9         1          2      0.054877        C1              11
    10        3          3      0.076251        C1              11
    """
    df_clip = bedtools_intersect(feature_bed, clip_file, feature)
    df_clip = df_clip.sort_values("peak_density", ascending=True).\
        reset_index(drop=True)
    df_clip["community"] = [""] * df_clip.shape[0]
    interval = np.linspace(0, df_clip.shape[0],
                           max(math.ceil(df_clip.shape[0] / 100), 2),
                           dtype=int)
    for i in range(len(interval) - 1):
        df_clip.iloc[interval[i]:interval[i+1], 3] = f"C{i + 1}"
    com, c = np.unique(df_clip["community"].values, return_counts=True)
    dic = {x: y for x, y in zip(com, c)}
    df_clip["community_size"] = df_clip["community"].map(dic)
    return df_clip


def create_compo_figures(peak_df: pd.DataFrame, feature: str):
    """
    Create a dataframe containing the GC frequency of every genes and \
    their peak density for a factor.

    :param peak_df: A dataframe containing the peak frequency in a factor \
    obtained from a clip experiment.
    :param feature: The kind of feature of interest
    :return: The dataframe containing the frequency of GC and their \
    peak density for a factor.

    >>> peak = pd.DataFrame({'id_gene': [1, 2, 3, 4],
    ... 'clip_peak': [2, 1, 3, 0], 'peak_density': [0.055, 0.03, 0.76, 0.0],
    ... 'community': ["C1", "C1", "C1", 'C1'], "community_size": [4, 4, 4, 4]})
    >>> create_compo_figures(peak, 'gene')
       id_gene  peak_density         S
    0        1         0.055  36.78145
    1        2         0.030  35.19284
    2        3         0.760  36.10462
    3        4         0.000  36.91844
    """
    cnx = sqlite3.connect(Config.db_file)
    df_comp = get_cpnt_frequency(cnx, get_ft_id(cnx, feature), feature, "gene",
                                 "nt")
    if feature == "gene":
        df_comp["id_gene"] = df_comp["id_gene"].astype(int)
    df_comp = peak_df[[f"id_{feature}", "peak_density"]]\
        .merge(df_comp, how="inner", on=f"id_{feature}")
    return df_comp[[f"id_{feature}", "peak_density", "S"]]


def create_density_fig(df: pd.DataFrame, feature: str, outfile: Path,
                       factor: str) -> Tuple[float, float]:
    """
    Create correlation figures between peak density and GC content of genes

    :param df: The dataframe containing frequency of exons and \
    co-localized exons.
    :param feature: The kind of feature to analyse (exon or gene)
    :param outfile: The density figure to create
    :param factor: The factor of interest
    :return: The correlation and the p-value
    """
    logging.debug('Creating the density figure')
    sns.set()
    sns.set_context('talk')
    plt.figure(figsize=(20, 12))
    s, i, r, p, stderr = linregress(df["peak_density"],
                                    df["S"])
    sns.scatterplot(df["peak_density"], df["S"])
    plt.plot(df["peak_density"], i + s * df["peak_density"], 'r',
             label=f'r={round(r,4)}, p={round(p, 4)}')
    plt.legend()
    plt.xlabel(f"Density of peaks of {factor}")
    plt.ylabel(f"Freq of S in {feature}s")
    title = f'Correlation between the peak density of {factor} in {feature}s' \
            f' and their GC content'
    plt.title(title)
    plt.savefig(outfile)
    plt.close()
    return r, p


def create_figure(feature: str, clip_file: Path,
                  feature_bed: Path, test_type: str = "permutation",
                  iteration: int = 10000, display_size: bool = False,
                  sl_reg: bool = False) -> Tuple[str, float, float]:
    """
    Create the final figure
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_file: A bed file containing clip
    :param feature_bed: A bed files containing exons or genes depending on \
    feature parameter.
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :return: The correlation and the p-value
    """
    logging.info(f"Working on {clip_file}")
    output = ConfigClip.output_folder / \
        f"CLIP_fake_community_figures-{feature}"
    output.mkdir(exist_ok=True, parents=True)
    outfile = output / f"{clip_file.name.split('.')[0]}.pdf"
    final_table = create_table(feature, clip_file, feature_bed)
    if sl_reg:
        final_table = add_regulation_column(final_table,
                                            clip_file.name.split("_")[0],
                                            feature)
    create_community_fig(final_table, feature, "peak_density", outfile,
                         test_type, iteration=iteration,
                         display_size=display_size)
    df_comp = create_compo_figures(final_table, feature)
    outfile_cor = outfile.parent / "correlation_fig" / outfile.name
    outfile_cor.parent.mkdir(exist_ok=True)
    factor = clip_file.name.split('.')[0].replace("_merged", "")
    r, p = create_density_fig(df_comp, feature, outfile_cor,
                              factor)
    return factor, r, p


def clip_folder_analysis(clip_folder: Path,
                         feature: str, test_type: str = "permutation",
                         iteration: int = 10000, display_size: bool = False,
                         sl_reg: bool = False, ps: int = 1,
                         logging_level: str = "DEBUG") -> None:
    """
    Create the final figure
    :param clip_folder: A folder containing clip files
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_folder: A folder containing clip file
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :param ps: The number of processes to create (default 1)
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    feature_bed = ConfigClip.bed_gene if feature == "gene" \
        else ConfigClip.bed_exon
    files = list(clip_folder.glob("*.bed")) + \
        list(clip_folder.glob("*.bed.gz"))
    pool = mp.Pool(processes=min(len(files), ps))
    processes = []
    for my_file in files:
        args = [feature, my_file, feature_bed, test_type, iteration,
                display_size, sl_reg]
        processes.append(pool.apply_async(create_figure, args))
    result = [p.get(timeout=None) for p in processes]
    df = pd.DataFrame(result, columns=["factor", "r-pearson", 'p-val'])
    outfile_cor = ConfigClip.output_folder / \
        f"CLIP_fake_community_figures-{feature}" / "correlation_fig" / \
        "correlation_table.txt"
    df.to_csv(outfile_cor, sep="\t", index=False)


@lp.parse
def launcher_fake_communities(clip_folder: str,
                              feature: str, test_type: str = "permutation",
                              iteration: int = 10000,
                              display_size: bool = False,
                              sl_reg: bool = False,
                              ps: int = 1, logging_level: str = "DEBUG"
                              ) -> None:
    """
    Create the final figure
    :param clip_folder: A folder containing clip files
    :param feature: The feature we want to analyse (default 'exon')
    :param clip_folder: A folder containing clip file
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param sl_reg: True to display the FaRLine regulation of the \
    same factor, False to not display it.
    :param ps: The number of processes to create (default 1)
    :param logging_level: The level of data to display (default 'DISABLE')
    """
    clip_folder_analysis(Path(clip_folder), feature, test_type, iteration,
                         display_size, sl_reg, ps, logging_level)


if __name__ == "__main__":
    import sys
    if len(sys.argv) <= 1:
        testmod()
    else:
        launcher_fake_communities()
