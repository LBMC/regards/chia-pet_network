#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

import pandas as pd
from pathlib import Path
from typing import Dict, Tuple, List, Optional
from tqdm import tqdm
from rpy2.robjects import r, pandas2ri
from statsmodels.stats.multitest import multipletests
import numpy as np
from ..radomization_test_ppi import get_pvalue
import seaborn as sns
import matplotlib.pyplot as plt


def get_community_table(communities: List[List[str]],
                        size_threshold: int, feature: str) -> pd.DataFrame:
    """
    return the table indicating the name of the exons and the \
    the name of the community.

    :param communities: List of community of exons
    :param size_threshold: The required size a community must \
    have to be considered
    :param feature: The kind of feature analysed
    :return: table of community
    >>> c = [['1_1', '2_5'], ['7_9', '4_19', '3_3']]
    >>> get_community_table(c, 3, 'exon')
      community id_exon  community_size
    0        C2     7_9               3
    1        C2    4_19               3
    2        C2     3_3               3
    >>> c = [['1', '2'], ['7', '49', '3']]
    >>> get_community_table(c, 3, 'gene')
      community id_gene  community_size
    0        C2       7               3
    1        C2      49               3
    2        C2       3               3
    """
    dic = {"community": [], f"id_{feature}": [], "community_size": []}
    for k, comm in enumerate(communities):
        if len(comm) >= size_threshold:
            name = f'C{k + 1}'
            clen = len(comm)
            for exon in comm:
                dic["community"].append(name)
                dic[f'id_{feature}'].append(exon)
                dic["community_size"].append(clen)
    return pd.DataFrame(dic)


def lm_maker_summary(df: pd.DataFrame, outfile: Path, target_col: str,
                     test_type: str) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Make the lm analysis to see if the exon regulated by a splicing factor \
    are equally distributed among the communities.

    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param outfile: A name of a file
    :param target_col: The name of the column containing the data of interest
    :param test_type: The type of test to make (permutation or lm)
    :return: the entry dataframe and the result dataframe post analysis
    """
    pandas2ri.activate()
    if test_type == "lm":
        mod = f"mod <- lm({target_col} ~ log(community_size) + " \
              f"community, data=data)"
    else:
        mod = f"mod <- glm({target_col} ~ log(community_size) + community," \
              f"data=data, family=binomial(link='logit'))"
        df[target_col] = df[target_col].astype(int)
        tmp = df[[target_col, 'community']].groupby('community').mean().\
            reset_index()
        bad_groups = tmp.loc[(tmp[target_col] == 0) | (tmp[target_col] == 1),
                             "community"].to_list()
        if "C-CTRL" in bad_groups:
            print("Control group as a mean value equals to 0, exiting...")
            exit(1)
        df = df[-df["community"].isin(bad_groups)]
    lmf = r(
        """
        require("DHARMa")

        function(data, folder, partial_name) {
            %s
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(folder, "/dignostics_summary", partial_name, ".png"))
            plot(simulationOutput)
            dev.off()
            return(as.data.frame(summary(mod)$coefficients))
        }

        """ % mod)
    folder = outfile.parent / "diagnostics"
    folder.mkdir(parents=True, exist_ok=True)
    partial_name = outfile.name.replace('.pdf', '')
    res_df = lmf(df, str(folder), partial_name).reset_index()
    res_df.rename({'index': 'community'}, inplace=True, axis=1)
    res_df['community'] = res_df['community'].str.replace('community', '')
    res_df.loc[res_df['community'] == "(Intercept)", "community"] = "C-CTRL"
    good_cols = [target_col, "community", "community_size"]
    if "community_data" in df.columns:
        good_cols.append("community_data")
    mean_df = df[good_cols]. \
        groupby(["community", "community_size"]).mean().reset_index()
    return df, res_df.merge(mean_df, how="left", on="community")


def lm_with_ctrl(df: pd.DataFrame,
                 target_col: str, outfile: Path, test_type: str
                 ) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param target_col: The name of the column containing the data of interest
    :param outfile: File that will contains the final figure
    :param test_type: The type of test to make (permutation or lm)
    :return: The dataframe with ctrl exon and \
    The dataframe with the p-value compared to the control \
    list of feature.
    """
    df['community'] = df['community'].fillna("C-CTRL")
    size = df.loc[df["community"] == "C-CTRL", :].shape[0]
    df['community_size'] = df['community_size'].fillna(size)
    df['community_size'] = df['community_size'].astype(int)
    return lm_maker_summary(df, outfile, target_col, test_type)


def expand_results_lm(df: pd.DataFrame, rdf: pd.DataFrame,
                      target_col: str, feature: str,
                      test_type: str) -> pd.DataFrame:
    """
    Merge df and rdf together.

    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param rdf: The dataframe containing the mean frequency for \
    each community and the p-value of their enrichment compared to control \
    exons.
    :param target_col: The name of the column containing the data of interest
    :param feature: The feature of interest
    :param test_type: The kind of test to make
    :return: The merged dataframe: i.e df with the stats columns
    """
    p_col = "Pr(>|t|)" if test_type == "lm" else "Pr(>|z|)"
    good_cols = [f"id_{feature}", target_col, "community",
                 "community_size"]
    if "community_data" in df.columns:
        good_cols.append("community_data")
    df = df[good_cols].copy()
    rdf = rdf[["community", "community_size", p_col, target_col]].copy()
    rdf.rename({target_col: f"mean_{target_col}", p_col: "p-adj"},
               axis=1, inplace=True)
    df = df.merge(rdf, how="left", on=["community", "community_size"])
    df_ctrl = df[df["community"] == "C-CTRL"]
    df = df[df["community"] != "C-CTRL"].copy()
    df.sort_values([f"mean_{target_col}", "community"], ascending=True,
                   inplace=True)
    return pd.concat([df_ctrl, df], axis=0, ignore_index=True)


def get_permutation_mean(df_ctrl: pd.DataFrame,
                         cpnt: str, size: int, iteration: int) -> List[float]:
    """
    Randomly sample `size` `feature` from `df_ctrl` to extract `iteration` \
    of `nt` frequencies from it.

    :param df_ctrl: A dataframe containing the frequency of each nucleotide \
    in each exons/gene in fasterdb.
    :param cpnt: The component (nt, aa, dnt) of interest
    :param size: The size of each sub samples to create
    :param iteration: The number of sub samples to create
    :return: The list of mean frequencies of `nt` in each subsample
    """
    return [
        float(np.mean(df_ctrl[cpnt].sample(int(size), replace=True).values))
        for _ in range(iteration)
    ]


def perm_community_pval(row: pd.Series, df_ctrl: pd.DataFrame,
                        cpnt: str, iteration: int, alpha: float
                        ) -> Tuple[float, float, float, str]:
    """
    Randomly sample `size` `feature` from `df_ctrl` to extract `iteration` \
    of `nt` frequencies from it.

    :param row: A line of a dataframe containing the frequency of \
    each feature inside a community.
    :param df_ctrl: A dataframe containing the frequency of each nucleotide \
    in each exons/gene in fasterdb.
    :param cpnt: The component (nt, aa, dnt) of interest
    :param iteration: The number of sub samples to create
    :param alpha: Type 1 error threshold
    :return: The ctrl mean frequency value of `nt`, its standard error \
    the pvalue and the regulation of the enrichment/impoverishment \
    of the community in `row` compared to control exons.
    """
    list_values = get_permutation_mean(df_ctrl, cpnt, row["community_size"],
                                       iteration)
    pval, reg = get_pvalue(np.array(list_values), row[cpnt], iteration, alpha)
    return float(np.mean(list_values)), float(np.std(list_values)), pval, reg


def perm_pvalues(df: pd.DataFrame, df_ctrl: pd.DataFrame, feature: str,
                 target_col: str, iteration: int,
                 dic_com: Dict, alpha: float) -> pd.DataFrame:
    """
    Randomly sample `size` `feature` from `df_ctrl` to extract `iteration` \
    of `nt` frequencies from it.

    :param df: A dataframe containing the frequency of each nucleotide \
    in each exons belonging to a community.
    :param df_ctrl: A dataframe containing the frequency of each nucleotide \
    in each exons/gene in fasterdb.
    :param feature: The feature of interest (gene, exon)
    :param target_col: The name of the column containing the data of interest
    :param iteration: The number of sub samples to create
    :param dic_com: A dictionary linking each community to the exons \
    it contains.
    :param alpha: Type 1 error threshold
    :return: The dataframe containing p-values and regulation \
    indicating the enrichment of
    """
    list_pval, list_reg, mean_ctrl, std_ctrl = ([] for _ in range(4))
    for i in tqdm(range(df.shape[0]), desc="performing permutations"):
        row = df.iloc[i, :]
        res = perm_community_pval(row,
                                  df_ctrl.loc[
                                    -df_ctrl[f'id_{feature}'
                                             ].isin(dic_com[row['community']]),
                                    :],
                                  target_col, iteration, alpha)
        [x.append(y) for x, y in zip([mean_ctrl, std_ctrl, list_pval,
                                      list_reg], res)]
    adj_pvals = multipletests(list_pval, alpha=alpha,
                              method='fdr_bh',
                              is_sorted=False,
                              returnsorted=False)[1]
    adj_regs = [list_reg[i] if adj_pvals[i] <= alpha else " . "
                for i in range(len(list_reg))]
    df[f'{target_col}_mean_{iteration}_ctrl'] = mean_ctrl
    df[f'{target_col}_std_{iteration}_ctrl'] = std_ctrl
    df[f'p-adj'] = adj_pvals
    df[f'reg-adj'] = adj_regs
    return df


def perm_with_ctrl(df: pd.DataFrame, feature: str,
                   target_col: str, dic_com: Dict,
                   iteration: int, alpha: float) -> pd.DataFrame:
    """

    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param feature: The kind of feature analysed
    :param target_col:  The name of the column containing the data of interest
    :param dic_com: A dictionary linking each community to the exons \
    it contains.
    :param iteration: The number of sub samples to create
    :param alpha: Type 1 error threshold
    :return: The dataframe with the p-value compared to the control \
    list of exons.
    """
    df_tmp = df.loc[-df["community"].isna(), :]
    mean_df = df_tmp[[target_col, "community", "community_size"]]. \
        groupby(["community", "community_size"]).mean().reset_index()
    return perm_pvalues(mean_df, df, feature, target_col,
                        iteration, dic_com, alpha)


def create_perm_ctrl_df(ctrl_df: pd.DataFrame, order_df: pd.DataFrame,
                        cpnt: str, feature: str, iteration: int
                        ) -> pd.DataFrame:
    """

    :param ctrl_df: A dataframe containing the mean ctrl values, \
    the mean control std and the community from which those control \
    have been created
    :param order_df: A dataframe containing the community and their final \
    order.
    :param cpnt: The component (nt, aa, dnt) of interest
    :param feature: The feature of interest
    :param iteration: The number of iteration
    :return: The ctrl_tmp_df in good order
    """
    dsize = ctrl_df.shape[0]
    ctrl_df[f"mean_{cpnt}"] = \
        [np.mean(ctrl_df[f"{cpnt}_mean_{iteration}_ctrl"])] * dsize
    ctrl_df[f"id_{feature}"] = ['ctrl'] * dsize
    ctrl_df["community_size"] = [dsize] * dsize
    ctrl_df = ctrl_df.merge(order_df, how='left', on="community")
    ctrl_df.rename({f"{cpnt}_mean_{iteration}_ctrl": cpnt,
                    f"{cpnt}_std_{iteration}_ctrl": 'ctrl_std'}, axis=1,
                   inplace=True)
    return ctrl_df.sort_values("order", ascending=True)


def expand_results_perm(df: pd.DataFrame, rdf: pd.DataFrame, target_col: str,
                        feature: str, iteration: int) -> pd.DataFrame:
    """
    Merge df and rdf together.

    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param rdf: The dataframe containing the mean frequency for \
    each community and the p-value of their enrichment compared to control \
    exons.
    :param target_col: The name of the column containing the data of interest
    :param feature: The feature of interest
    :param iteration: The number of iteration
    :return: The merged dataframe: i.e df with the stats columns
    """
    good_cols = [f"id_{feature}", target_col, "community", "community_size"]
    if "community_data" in df.columns:
        good_cols.append("community_data")
    df = df.loc[-df["community"].isna(),
                good_cols].copy()
    ctrl_df = rdf[[f"{target_col}_mean_{iteration}_ctrl",
                   f"{target_col}_std_{iteration}_ctrl", "community"]].copy()
    rdf = rdf[["community", "community_size", target_col, "p-adj",
               "reg-adj"]].copy()
    rdf.rename({target_col: f"mean_{target_col}"}, axis=1, inplace=True)
    df = df.merge(rdf, how="left", on=["community", "community_size"])
    df.sort_values(f"mean_{target_col}", ascending=True, inplace=True)
    order_df = df[["community"]].drop_duplicates().copy()
    order_df["order"] = range(order_df.shape[0])
    df_ctrl = create_perm_ctrl_df(ctrl_df, order_df, target_col, feature,
                                  iteration)
    return pd.concat([df_ctrl, df], axis=0, ignore_index=True)


def handle_community_data(g: sns.FacetGrid, df_bar: pd.DataFrame,
                          display_size: bool):
    """

    :param g: A seaborn FacetGrid
    :param df_bar: A dataframe with the enrichment of a \
    nucleotide frequency for every community (without control)
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :return: The seaborn fascetgrid
    """
    com_col = "community_data"
    if com_col in df_bar.columns:
        df_val = df_bar[["community", com_col]].groupby("community").mean()\
            .reset_index()
        df_val = df_bar.drop("community_data", axis=1)\
            .merge(df_val, how="left",
                   on="community")[["community",
                                    "community_data"]].drop_duplicates()
        ax3 = g.ax.twinx()
        ax3.set_ylabel('community_size', color="purple")
        if display_size:
            ax3.spines["right"].set_position(("axes", 1.045))
            ax3.spines["right"].set_visible(True)
        df_val.plot(x="community", y="community_data", kind="scatter", ax=ax3,
                    legend=False, zorder=55,
                    color=(0.8, 0.2, 0.8, 0.4))
        ax3.tick_params(axis='y', labelcolor="purple")
        ax3.grid(False)
    g.set(xticklabels=[])
    return g


def display_size_fig(g: sns.FacetGrid, display_size: bool, target_col: str,
                     df_bar: pd.DataFrame):
    """

    :param g: A seaborn FacetGrid
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :param target_col: The name of the column containing the data of interest
    :param df_bar: A dataframe with the enrichment of a \
    nucleotide frequency for every community (without control)
    :return: The seaborn fascetgrid
    """
    xrange = g.ax.get_xlim()
    if display_size:
        df_size = df_bar[[f"mean_{target_col}", "community",
                          "community_size"]].drop_duplicates()
        ax2 = g.ax.twinx()
        ax2.set_ylabel('community_size', color="green")
        df_size.plot(x="community", y="community_size", kind="scatter", ax=ax2,
                     legend=False, zorder=55,
                     color=(0.2, 0.8, 0.2, 0.4))
        ax2.tick_params(axis='y', labelcolor="green")
        ax2.grid(False)
        sizes = df_size["community_size"].to_list()
        if max(sizes) - min(sizes) > 500:
            ax2.set_yscale("log")
            g.ax.set_xlim(xrange)
    g = handle_community_data(g, df_bar, display_size)
    return g


def make_barplot(df_bar: pd.DataFrame, outfile: Path, test_type: str,
                 target_col: str, feature: str, target_kind: str = "",
                 sd_community: Optional[str] = "sd",
                 display_size: bool = False) -> None:
    """
    Create a barplot showing the frequency of `nt` for every community \
    of exons/gene in `df_bar`.

    :param df_bar: A dataframe with the enrichment of a \
    nucleotide frequency for every community
    :param outfile: File were the figure will be stored
    :param target_kind: An optional name that describe a bit further \
    target_col.
    :param test_type: The kind of test to perform
    :param target_col: The name of the column containing the data of interest
    :param feature: The king of feature of interest
    :param sd_community: sd to display community error bar, None to display \
    nothing
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    """
    sns.set(context="poster")
    g = sns.catplot(x="community", y=target_col, data=df_bar, kind="point",
                    ci=sd_community, aspect=2.5, height=14, errwidth=0.5,
                    capsize=.4, scale=0.5,
                    palette=["red"] + ["darkgray"] * (df_bar.shape[0] - 1))
    g2 = sns.catplot(x="community", y=target_col, data=df_bar, kind="bar",
                     ci="sd", aspect=2.5, height=14, errwidth=0.5, capsize=.4,
                     palette=["red"] + ["darkgray"] * (df_bar.shape[0] - 1))
    g.fig.subplots_adjust(top=0.9)
    target_kind = f" ({target_kind})" if target_kind else ""
    g.fig.suptitle(f"Mean frequency of {target_col}{target_kind}"
                   f" among community of {feature}s\n"
                   f"(stats obtained with a {test_type} test)")
    g = display_size_fig(g, display_size, target_col, df_bar)
    g.ax.set_ylabel(f'Frequency of {target_col}')
    df_bara = df_bar.drop_duplicates(subset="community", keep="first")
    for i, p in enumerate(g2.ax.patches):
        stats = "*" if df_bara.iloc[i, :]["p-adj"] < 0.05 else ""
        com = df_bara.iloc[i, :]["community"]
        csd = 0
        if sd_community == "sd":
            csd = np.std(df_bar.loc[df_bar["community"] == com, target_col])
        g.ax.annotate(stats,
                      (p.get_x() + p.get_width() / 2., p.get_height() + csd),
                      ha='center', va='center', xytext=(0, 10), fontsize=12,
                      textcoords='offset points')
    g.savefig(outfile)
    plt.close()


def make_barplot_perm(df_bar: pd.DataFrame, outfile: Path,
                      target_col: str, feature: str,
                      target_kind: str = "",
                      sd_community: Optional[str] = "sd",
                      display_size: bool = False, alpha: float = 0.05) -> None:
    """
    Create a barplot showing the frequency of `nt` for every community \
    of exons/gene in `df_bar`.

    :param df_bar: A dataframe with the enrichment of a \
    nucleotide frequency for every community
    :param outfile: File were the figure will be stored
    :param target_kind: An optional name that describe a bit further \
    target_col.
    :param target_col: The name of the column containing the data of interest
    :param feature: The king of feature of interest
    :param sd_community: sd to display community error bar, None to display \
    nothing
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :param alpha: Type 1 error threshold
    """
    sns.set(context="poster")
    df_ctrl = df_bar.loc[df_bar[f"id_{feature}"] == 'ctrl', :]
    df_bar = df_bar.loc[df_bar[f"id_{feature}"] != 'ctrl', :].copy()
    g2 = sns.catplot(x="community", y=target_col, data=df_bar, kind="bar",
                     ci="sd", aspect=2.5, height=14, errwidth=0.5, capsize=.4,
                     palette=["darkgray"] * (df_bar.shape[0]))
    g = sns.catplot(x="community", y=target_col, data=df_bar, kind="point",
                    ci=sd_community, aspect=2.5, height=14, errwidth=0.5,
                    capsize=.4, scale=0.5,
                    palette=["darkgray"] * (df_bar.shape[0]))
    xrange = g.ax.get_xlim()
    yrange = g.ax.get_ylim()
    df_ctrl.plot(x="community", y=target_col, kind="scatter", ax=g.ax,
                 yerr="ctrl_std", legend=False, zorder=10,
                 color=(0.8, 0.2, 0.2, 0.4))
    g.ax.set_xlim(xrange)
    if sd_community is None:
        g.ax.set_ylim(yrange)
    g.fig.subplots_adjust(top=0.9)
    target_kind = f" ({target_kind})" if target_kind else ""
    g.fig.suptitle(f"Mean frequency of {target_col}{target_kind}"
                   f" among community of {feature}s\n"
                   f"(stats obtained with a permutation test) (alpha={alpha})")
    g = display_size_fig(g, display_size, target_col, df_bar)
    g.ax.set_ylabel(f'Frequency of {target_col}')
    df_bara = df_bar.drop_duplicates(subset="community", keep="first")
    for i, p in enumerate(g2.ax.patches):
        stats = "*" if df_bara.iloc[i, :]["p-adj"] <= alpha else ""
        com = df_bara.iloc[i, :]["community"]
        csd = 0
        if sd_community == "sd":
            csd = np.std(df_bar.loc[df_bar["community"] == com, target_col])
        g.ax.annotate(stats,
                      (p.get_x() + p.get_width() / 2., p.get_height() + csd),
                      ha='center', va='center', xytext=(0, 10), fontsize=12,
                      textcoords='offset points')
    g.savefig(outfile)
    plt.close()


def barplot_creation(df_bar: pd.DataFrame, outfig: Path,
                     cpnt: str, test_type: str, feature: str,
                     target_kind: str, sd_community: bool,
                     display_size: bool, alpha: float = 0.05) -> None:
    """
    Reformat a dataframe with the enrichment of a nucleotide frequency \
    for every feature for every community and then create a \
    barplot showing those frequencies.

    :param df_bar: A dataframe with the enrichment of a \
    nucleotide frequency for every community and showing the frequency \
    of each feature in each community
    :param outfig: File were the figure will be stored
    :param cpnt: The component (nt, aa, dnt) of interest
    :param test_type: The kind of test make
    :param feature: The king of feature of interest
    :param test_type: The type of test to make (permutation or lm)
    :param target_kind: An optional name that describe a bit further \
    target_col.
    :param sd_community: True to display the errors bars for communities,
    False else.
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :param alpha: Type 1 error threshold
    """
    sd_community = "sd" if sd_community else None
    if test_type != "permutation":
        make_barplot(df_bar, outfig, test_type, cpnt, feature, target_kind,
                     sd_community=sd_community, display_size=display_size)
    else:
        make_barplot_perm(df_bar, outfig, cpnt, feature, target_kind,
                          sd_community, display_size, alpha)


def get_feature_by_community(df: pd.DataFrame, feature: str) -> Dict:
    """
    Create a dictionary containing the exons contained in each community.

    :param df: A dataframe containing the frequency of each nucleotide \
    in each exons belonging to a community.
    :param feature: the feature of interest (exon, gene)
    :return: A dictionary linking each community to the exons it contains

    >>> dataf = pd.DataFrame({"id_gene": ['1', '2', '3', '4', '5'],
    ... 'community': ['C1', 'C1', 'C2', 'C2', np.nan]})
    >>> get_feature_by_community(dataf, 'gene')
    {'C1': ['1', '2'], 'C2': ['3', '4']}
    >>> dataf.rename({"id_gene": "id_exon"}, axis=1, inplace=True)
    >>> get_feature_by_community(dataf, 'exon')
    {'C1': ['1', '2'], 'C2': ['3', '4']}
    """
    dic = {}
    for i in range(df.shape[0]):
        com, id_ft = df.iloc[i, :][['community', f'id_{feature}']]
        if isinstance(com, float):
            com = None if np.isnan(com) else com
        if com is not None:
            if com in dic:
                dic[com].append(id_ft)
            else:
                dic[com] = [id_ft]
    return dic


def create_community_fig(df: pd.DataFrame, feature: str,
                         target_col: str,
                         outfile_ctrl: Path, test_type: str,
                         dic_com: Optional[Dict] = None,
                         target_kind: str = "",
                         iteration: int = 10000,
                         sd_community: bool = True,
                         display_size: bool = False,
                         alpha: float = 0.05) -> None:
    """
    Create a dataframe with a control community, save it as a table and \
    as a barplot figure.

    :param df: A dataframe containing the id of the chosen `feature` \
    (i.e FasterDB id of genes or exons) a column with data for interest (\
    this column must have the name *target_col*) and the community \
    and the size of the community of the feature if it has one (None, else).
    :param feature: The kind of feature analysed
    :param target_col: The name of the column containing the data of interest
    :param outfile_ctrl: file used to stored the table and the figure \
    containing the test communities and the control community
    :param test_type: The type of test to make (permutation or lm)
    :param dic_com: A dictionary linking each community to the exons \
    it contains.
    :param target_kind: An optional name that describe a bit further \
    target_col.
    :param iteration: The number of sub samples to create
    :param sd_community: True to display the errors bars for communities,
    False else.
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :param alpha: Type I error threshold
    """
    df.to_csv(str(outfile_ctrl).replace(".pdf", ".tmp.txt"), sep="\t",
              index=False)
    if dic_com is None:
        dic_com = {} if test_type != 'permutation' \
            else get_feature_by_community(df, feature)
    if test_type != "permutation":
        ndf, rdf = lm_with_ctrl(df, target_col, outfile_ctrl, test_type)
        df_bar = expand_results_lm(ndf, rdf, target_col, feature, test_type)
    else:
        rdf = perm_with_ctrl(df, feature, target_col, dic_com, iteration,
                             alpha)
        df_bar = expand_results_perm(df, rdf, target_col, feature, iteration)
    rdf.to_csv(str(outfile_ctrl).replace(".pdf", ".txt"), sep="\t",
               index=False)
    bar_outfile = str(outfile_ctrl).replace(".pdf", "_bar.txt")
    df_bar.to_csv(bar_outfile, sep="\t", index=False)
    barplot_creation(df_bar, outfile_ctrl, target_col, test_type, feature,
                     target_kind, sd_community, display_size, alpha)
