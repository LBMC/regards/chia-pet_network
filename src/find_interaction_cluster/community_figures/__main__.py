#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create a barplot showing the frequency/value of a particular \
item in every community of genomic features (genes of exons) that are close \
in the nucleus compared to a control list of features.
"""

import lazyparser as lp
import pandas as pd
from pathlib import Path
from .fig_functions import create_community_fig


class FileNameError(Exception):
    pass


class MissingColumnError(Exception):
    pass


def load_and_check_table(table: str, feature: str, target_col: str):
    """
    Load a file containing a dataframe. It must contains the following \
    columns: id_feature, target_col, community and community_size.


    :param table: A file containing a table with the id of the chosen \
    `feature` (i.e FasterDB id of genes or exons), a column with data of \
    interest ( this column must have the name *target_col*) and two columns \
    with the community and the size of the community of the feature if it \
    has one (None, else).
    :param feature: The kind of feature analysed
    :param target_col: The name of the column containing the data of interest
    :return: The loaded dataframe
    """
    if table.endswith(".gz"):
        df = pd.read_csv(table, sep="\t", compression="gzip")
    else:
        df = pd.read_csv(table, sep="\t")
    required_cols = [f"id_{feature}", target_col, "community",
                     "community_size"]
    for rqd in required_cols:
        if rqd not in df.columns:
            raise MissingColumnError(f"The column {rqd} is missing !")
    return df


@lp.parse(table="file", test_type=["lm", "permutation", "logit"],
          iteration="20 < iteration", sd_community = ["y", "n", "Y", "N"])
def create_community_figures(table: str, feature: str, target_col: str,
                             output: str, outfile: str, test_type: str,
                             target_kind: str = "", sd_community: str = "y",
                             iteration: int = 10000,
                             display_size: bool = False,
                             alpha: float = 0.05) -> None:
    """
    Create a dataframe with a control community, save it as a table and \
    as a barplot figure.

    :param table: A file containing a table with the id of the chosen \
    `feature` (i.e FasterDB id of genes or exons), a column with data of \
     interest ( this column must have the name *target_col*) and two columns \
     with the community and the size of the community of the feature if it \
     has one (None, else).
    :param feature: The kind of feature analysed (exon or gene)
    :param target_col: The name of the column containing the data of interest
    :param output: The output folder
    :param outfile: The name of the output figure file (pdf format)
    :param test_type: The type of test to make (permutation, lm or logit)
    :param target_kind: An optional name that describe a bit further \
    target_col.
    :param iteration: The number of sub samples to create. This parameter \
    is only used if test_type = 'permutation' (default 10000).
    :param sd_community: y to display the standard deviation for communities \
    false else.
    :param display_size: True to display the size of the community above \
    each one of them False to display nothing. (default False)
    :param alpha: Type I error threshold
    """
    if alpha != 0.05 and test_type != "permutation":
        raise NotImplementedError(f"You can't change the alpha if you're "
                                  f"not using a permutation test.")
    df = load_and_check_table(table, feature, target_col)
    if not outfile.endswith(".pdf"):
        raise FileNameError("The output figure must be in pdf format !")
    moutfile = Path(output) / outfile
    sd_community = sd_community.lower() == 'y'
    create_community_fig(df, feature, target_col, moutfile, test_type,
                         target_kind=target_kind, iteration=iteration,
                         sd_community=sd_community, display_size=display_size,
                         alpha=alpha)


if __name__ == "__main__":
    create_community_figures()
