#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to check whether communities \
are often regulated by a splicing factor
"""

from typing import List, Tuple, Dict, Callable
import sqlite3
from .config import ConfigGraph, get_communities
from ..figures_utils.exons_interactions import get_every_events_4_a_sl
from ..figures_utils.tf_function import get_every_events_4_a_tf
import pandas as pd
import numpy as np
from itertools import product
from .community_finder import get_projects
import multiprocessing as mp
import logging
from ..logging_conf import logging_def
import doctest
from functools import reduce
from operator import add
import statsmodels.formula.api as api
from statsmodels.stats.multitest import multipletests
from rpy2.robjects import r, pandas2ri
from pathlib import Path


def get_ft_regulated_in_communities(community: List[str],
                                    regulated_exons: List[str]
                                    ) -> Tuple[int, int, float]:
    """

    :param community: strongly co-localized group of exons belonging to \
    the same community.
    :param regulated_exons: List of regulated exons by a splicing factor
    :return: The number of exons regulated by the splicing factor inside \
    the community. The total number of exons within the community and \
    the percentage of regulated exons within the community
    """
    reg_community = len([e for e in community if e in regulated_exons])
    tot_commmunity = len(community)
    prop = reg_community / tot_commmunity * 100
    return reg_community, tot_commmunity, prop


def get_ctrl_exon(cnx: sqlite3.Connection) -> List[str]:
    """
    Get the list of fasterDB exons.

    :param cnx: Connection to chIA-PET database
    :return: The list of fasterDB exons
    """
    cursor = cnx.cursor()
    query = 'SELECT id FROM cin_exon'
    cursor.execute(query)
    res = np.asarray(cursor.fetchall()).flatten()
    cursor.close()
    return list(res)


def get_stat_community_exons(cnx: sqlite3.Connection, dic: Dict[str, List],
                             communities: List[List[str]],
                             regulated_exons: List[str]):
    """
    Get the data for the list of exons belonging to a community, and update \
    `dic`with it.

    :param cnx: connection to ChIA-PET database
    :param dic: Dictionary containing data about a community and it's content \
    corresponding to eoxns regulated by a splicing factor.
    :param communities: strongly co-localized group of exons belonging to \
    the same community.
    :param regulated_exons: List of regulated exons by a splicing factor
    :return: The update dictionary and the number of regulated exon by a \
    splicing factor that belong to a large community and the number of \
    exons belonging to a large community.
    """
    full_com = reduce(add, communities)
    ctrl_exons = get_ctrl_exon(cnx)
    n_reg, n_tot = np.nan, np.nan
    for cur_com, name in [[full_com, 'All-community'], [ctrl_exons, "FASTERDB"]]:
        reg_community, tot_commmunity, prop = \
            get_ft_regulated_in_communities(cur_com, regulated_exons)
        if name == 'All-community':
            n_reg = reg_community
            n_tot = tot_commmunity
        dic["community"].append(name)
        dic['reg_in_community'].append(reg_community)
        dic['community_size'].append(tot_commmunity)
        dic['%reg in community'].append(prop)
        dic['pval'].append(np.nan)
    return dic, n_reg, n_tot


def get_vector(n: int, t: int) -> List[int]:
    """
    Get a vector containing n ones and t - n zeros.

    :param n: The number of ones wanted
    :param t: The size of the array
    :return: The vector of n one's and t zeros

    >>> get_vector(2, 5)
    [1, 1, 0, 0, 0]
    """
    return [1] * n + [0] * (t - n)


def logistic_regression(n1: int, t1: int, n2: int, t2: int) -> float:
    """
    Get the p-value of the logisitcal test performed on the \
    test vector data defined by n1 ones and t1-n1 zeros and the control \
    vector data defined by n2 ones and t2-n2 zeros.

    :param n1: number of ones in test vectors
    :param t1: size of test vector
    :param n2: number of ones in control vector
    :param t2: size of control vector
    :return: The p-value of the logistic test
    """
    if n1 == 0 or n2 == 0 or n1 == t1 or n2 == t2:
        return 1
    df = pd.DataFrame({'y': get_vector(n1, t1) +
                            get_vector(n2, t2),
                       'x': ['test'] * t1 + ['ctrl'] * t2})
    return api.logit('y ~ x', data=df).fit(disp=False).pvalues['x[T.test]']


def expand_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    """
    Expend the dataframe df to be able to make a general mixed model on it.

    :param df: The dataframe to expand
    :return:  The dataframe expanded

    >>> d = pd.DataFrame({"community": ["C0", "C1"],
    ... "reg_in_community": [2, 3],
    ... "community_size": [5, 7],
    ... "%reg in community": [40, 42.85], 'pval': [1, 0.5], 'padj': [1, 1]})
    >>> expand_dataframe(d)
        is_reg community  reg_in_community  community_size
    0        1        C0                 2               5
    1        1        C0                 2               5
    2        0        C0                 2               5
    3        0        C0                 2               5
    4        0        C0                 2               5
    5        1        C1                 3               7
    6        1        C1                 3               7
    7        1        C1                 3               7
    8        0        C1                 3               7
    9        0        C1                 3               7
    10       0        C1                 3               7
    11       0        C1                 3               7

    """
    content = []
    for row in df.values:
        row = list(row)
        values = [1] * row[1] + [0] * (row[2] - row[1])
        for v in values:
            content.append([v] + row)
    df = pd.DataFrame(content, columns=["is_reg"] + list(df.columns))
    df.drop(["%reg in community", "pval", "padj"], axis=1, inplace=True)
    return df


def glmm_maker(expanded_df: pd.DataFrame, outfile: Path) -> float:
    """
    Make the glmm analysis to see if the exon regulated by a splicing factor \
    are equally distributed among the communities.

    :param expanded_df: The expanded dataframe
    :param outfile: A name of a file
    :return: the pvalue of glmm

    >>> d = pd.DataFrame({"community": ["C0", "C1"],
    ... "reg_in_community": [2, 3],
    ... "community_size": [5, 7],
    ... "%reg in community": [40, 42.85], 'pval': [1, 0.5], 'padj': [1, 1]})
    >>> e_df = expand_dataframe(d)
    >>> outfile = ConfigGraph.get_community_file("Test", 1, 1, True, 1.5,
    ... "ALL", "_stat.txt", "sf_community_enrichment")
    >>> glmm_maker(e_df, outfile)
    1.0
    """
    pandas2ri.activate()
    glmm = r(
        """
        require("lme4")
        require("DHARMa")
        
        function(data, folder, partial_name) {
            null_mod <- glm(is_reg ~ log(community_size) , family=binomial, data=data)
            mod <- glmer(is_reg ~ log(community_size) + (1 | community), data=data, family=binomial, nAGQ=0)
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(folder, "/dignostics_", partial_name, ".png"))
            plot(simulationOutput)
            dev.off()
            return(anova(mod, null_mod, test="Chisq"))
        }
        
        """)
    folder = outfile.parent / "diagnostics"
    folder.mkdir(parents=True, exist_ok=True)
    partial_name = outfile.name.replace('.txt', '')
    if expanded_df["is_reg"].sum() == 0:
        return np.nan
    return glmm(expanded_df, str(folder), partial_name)["Pr(>Chisq)"][1]


def glmm_statistics(df: pd.DataFrame, sf_name: str, reg: str,
                    project: str, weight: int, global_weight: int,
                    same_gene: bool, inflation: float, cell_line: str = "ALL",
                    feature: str = "exon", factor : str = "splicing"
                    ) -> pd.Series:
    """
    Create the glmm statistics for a given splicing factor with \
    given communities.

    :param df: dataframe contaning stats data.
    :param sf_name: The splicing factor of interest
    :param reg: The regulation of interest
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param feature: The kind of feature analysed
    :param inflation: the inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param factor: The kind of factor to analyse
    :return: The glmm pvalue among with other informations
    """
    ndf = df.loc[-df['community'].isin(["All-community", "FASTERDB"]),
          :].copy()
    expanded_df = expand_dataframe(ndf)
    subfolder = "sf_community_enrichment" if factor == "splicing" \
        else "tf_community_enrichment"
    outfile = ConfigGraph.get_community_file(project, weight, global_weight,
                                             same_gene, inflation, cell_line,
                                             feature,
                                             f"{sf_name}_{reg}_stat.txt",
                                             subfolder)
    noutfold = outfile.parent / "expanded_df"
    noutfold.mkdir(exist_ok=True, parents=True)
    noutfile = noutfold / outfile.name
    expanded_df.to_csv(noutfile, sep="\t", index=False)
    pval = glmm_maker(expanded_df, outfile)
    if np.isnan(pval):
        logging.warning(f"The factor {sf_name} doesn't have any regulated "
                        f"genes in selected communities")
    return pd.Series({"project": project, "sf": sf_name, "reg": reg,
                      "pval": pval})


def adapt_regulated_list(cnx: sqlite3.Connection,
                         exon_list: List, sf_name: str, reg: str,
                         feature: str = 'exon') -> List:
    """
    Transform a list containing exons into a list containing gene only if \
    feature is equal to 'gene'.

    :param cnx: connection to chia-pet database
    :param exon_list: A list of exons
    :param sf_name: The name of the splicing factor regulating the exons \
    in exon_list
    :param reg: The regulation of the exons regulated by sf_name
    :param feature: The kind of feature to analyse
    :return: if feature is exon then return the original list else,
    return the list containing the regulated gene
    """
    if feature == 'exon':
        return exon_list
    dic = {"down": "up", "up": "down"}
    # regulated_dic, number = get_every_events_4_a_sl(cnx, sf_name, dic[reg])
    # exon_list2 = regulated_dic[f"{sf_name}_{dic[reg]}"]
    genes1 = list(np.unique([exon.split('_')[0] for exon in exon_list]))
    # genes2 = list(np.unique([exon.split('_')[0] for exon in exon_list2]))
    return genes1 #[gene for gene in genes1 if gene not in genes2]


def get_regulated_features(cnx: sqlite3.connect, factor_name: str,
                           reg: str, feature: str,
                           factor_type: str = "splicing"):
    """

    :param cnx: connection to chia-pet database
    :param factor_name: The name of the splicing factor
    :param reg: The regulation of interest
    :param feature: The kind of analysed feature
    :param factor_type: The kind of factor to analyse
    :return: The list of feature regulated by a factor

    >>> r = get_regulated_features(sqlite3.connect(ConfigGraph.db_file),
    ... "DDX59", "down", "gene", "transcription")
    >>> len(r)
    1712
    >>> r[0:5]
    ['1185', '4246', '12598', '17765', '6342']
    >>> r = get_regulated_features(sqlite3.connect(ConfigGraph.db_file),
    ... "DAZAP1", "down", "gene", "splicing")
    >>> len(r)
    531
    >>> r[0:5]
    ['10038', '10053', '10076', '10102', '10127']
    """
    if factor_type == "splicing":
        regulated_dic, number = get_every_events_4_a_sl(cnx, factor_name, reg)
        reg_ft = regulated_dic[factor_name + "_" + reg]
        return adapt_regulated_list(cnx, reg_ft, factor_name, reg, feature)
    else:
        regulated_dic, number = get_every_events_4_a_tf(cnx, factor_name,
                                                        reg)
        return [str(r) for r in regulated_dic[factor_name + "_" + reg]]


def get_stat4communities(sf_name: str, reg: str,
                         project: str, weight: int,
                         global_weight: int, same_gene: bool,
                         inflation: float, cell_line: str = "ALL",
                         feature: str = 'exon', factor: str = "splicing"
                         ) -> Tuple[pd.DataFrame, pd.Series]:
    """
    Get data (proportion of `reg` regulated exons by a splicing factor
    `sf_name` within a community) for every community.

    :param sf_name: The splicing factor of interest
    :param reg: The regulation of interest
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: The kind of analysed feature
    :param factor: The kind of factor to analyse
    """
    logging.debug(f"Working on {sf_name}-{reg}, for {project}, w:{weight}, "
                  f"g:{global_weight}")
    cnx = sqlite3.connect(ConfigGraph.db_file)
    result = ConfigGraph.get_community_file(project, weight, global_weight,
                                            same_gene, inflation, cell_line,
                                            feature, ".txt")
    communities = get_communities(result, feature, 0)
    communities = [communities[k] for k in communities]
    reg_ft = get_regulated_features(cnx, sf_name, reg, feature, factor)
    if len(reg_ft) == 0:
        raise IndexError(f'There no {feature} {reg} regulated specifically '
                         f'by {sf_name}')
    d = {"community": [], "reg_in_community": [], "community_size": [],
         "%reg in community": [], 'pval': []}
    d, n2, t2 = get_stat_community_exons(cnx, d, communities, reg_ft)
    threshold = 49
    if feature == "gene":
        threshold = 9
    for k, community in enumerate(communities):
        if len(community) > threshold:
            reg_community, tot_commmunity, prop = \
                get_ft_regulated_in_communities(community, reg_ft)
            d["community"].append(f"C{k + 1}")
            d['reg_in_community'].append(reg_community)
            d['community_size'].append(tot_commmunity)
            d['%reg in community'].append(prop)
            d['pval'].append(logistic_regression(reg_community, tot_commmunity,
                                                 n2, t2))
    d['padj'] = [np.nan, np.nan] + list(multipletests(d['pval'][2:],
                                                      method='fdr_bh')[1])
    d['sf'] = [sf_name] * len(d["community"])
    d['reg'] = [reg] * len(d["community"])
    d['project'] = [project] * len(d["community"])
    df = pd.DataFrame(d)
    s = glmm_statistics(df, sf_name, reg, project, weight, global_weight,
                        same_gene, inflation, cell_line, feature, factor)
    return df, s


def get_good_factors(cnx: sqlite3.connect, list_factors: List[str],
                     outfile:Path, regulation: str, threshold: int,
                     get_event: Callable):
    """

    :param cnx: Connection to ChIA-PET DB
    :param list_factors: List of factors of interest
    :param outfile: File to store the factors
    :param regulation: Regulation opf interest
    :param threshold: Minimum number of genes/exon regulated of interest
    :param get_event: A function
    :return: The list of selected factors
    """
    list_sf = []
    for sf in list_factors:
        if regulation in ["up", "down"]:
            val = len(get_event(cnx, sf, regulation)
                      [0][f"{sf}_{regulation}"])
        elif regulation == "one":
            val = max(len(get_event(cnx, sf, x)[0][f"{sf}_{x}"])
                      for x in ["up", "down"])
        else:
            val = min(
                len(get_event(cnx, sf, x)[0][f"{sf}_{x}"])
                for x in ["up", "down"]
            )

        if val >= threshold and "1_2" not in sf:
            list_sf.append(sf)
        outfile.write_text(str(list_sf), encoding="UTF-8")
    return list_sf


def get_tfname(regulation: str = "one", threshold: int = 50) -> List[str]:
    """
    Function that return the transcription factor names in the \
    ChIA-PET database that up or down-regulates at least `threshold` exons.

    :param regulation: up, down, all, one. If regulation is up or down, \
    then the function returns only the transcription factor names up or down \
    regulating at least threshold exons. If 'all' is chosen then \
    only transcription factor that up-regulates and down-regulates at least \
    `threshold` gene are returned. If 'one' is chosen, then \
    the transcription factor must have at least `threshold` up-regulated \
    genes OR down-regulated genes.
    :param threshold: The minimum number of  genes that must be regulated \
    by the transcription factor
    :return: The list of transcription factors regulating at least threshold \
    genes

    >>> get_tfname() == ['NKRF', 'FUBP1', 'NUP35', 'MARK2', 'DNAJC2', 'SFPQ',
    ... 'ZC3H8', 'HNRNPK', 'WRN', 'UPF2', 'CNOT8', 'PHF6', 'BCLAF1', 'PARN',
    ... 'ZRANB2', 'FUBP3', 'PA2G4', 'XRCC5', 'MTPAP', 'PAPOLA', 'XRN2',
    ... 'SRPK2', 'AARS', 'ILF2', 'NFX1', 'DDX59']
    True
    """
    outfile = ConfigGraph.results / \
              f"tf_list_reg-{regulation}_t{threshold}.txt"
    if outfile.is_file():
        return outfile.open("r").read().strip(r"[|]").replace("'", "")\
            .split(", ")
    cnx = sqlite3.connect(ConfigGraph.db_file)
    c = cnx.cursor()
    query = "SELECT DISTINCT tf_name from cin_project_tf"
    c.execute(query)
    res = [tf[0] for tf in c.fetchall()]
    return get_good_factors(cnx, res, outfile, regulation, threshold,
                            get_every_events_4_a_tf)


def get_sfname(regulation: str = "one", threshold: int = 50) -> List[str]:
    """
    Function that return the splicing factor names in the ChIA-PET database \
    that up or down-regulates at least `threshold` exons.

    :param regulation: up, down, all, one. If regulation is up or down, \
    then the function returns only the splicing factor names up or down \
    regulating at least threshold exons. If 'all' is chosen then \
    only splicing factor that up-regulates and down-regulates at least \
    `threshold` exons are returned. If 'one' is chosen, then \
    the splicing factor must have at least `threshold` up-regulated exons OR \
    down-regulated exons.
    :param threshold: The minimum number of  exons that must be regulated \
    by the splicing factor
    :return: The list of splicing factor regulating at least threshold \
    exons

    >>> get_sfname() == ['PTBP1', 'SRSF1', 'SF3B1', 'SNRPC',
    ... 'U2AF2', 'DAZAP1', 'HNRNPC', 'RBFOX2', 'RBM10', 'ESRP2',
    ... 'HNRNPA1', 'HNRNPA2B1', 'HNRNPF', 'HNRNPH1', 'HNRNPM', 'HNRNPU',
    ... 'HNRNPL', 'SRSF3', 'HNRNPK', 'MBNL1', 'SRSF2', 'U2AF1', 'RBM25',
    ... 'RBM47', 'TRA2A_B', 'DDX5_DDX17', 'RBM17', 'RBM39', 'SF1', 'SFPQ',
    ... 'KHSRP', 'PRPF8', 'QKI', 'SF3B4', 'RBM15', 'RBM22', 'SF3A3',
    ... 'SNRNP70', 'SRSF7', 'SRSF9', 'TIA1', 'SRSF5', 'SNRNP200', 'PCBP1',
    ... 'PCBP2', 'FUS', 'RBMX', 'SRSF6', 'MBNL2', 'AGO2', 'AGO3', 'AKAP8L',
    ... 'AQR', 'CCAR1', 'CELF1', 'DDX5', 'EFTUD2', 'EWSR1', 'FMR1', 'FUBP1',
    ... 'GEMIN5', 'GPKOW', 'HNRNPLL', 'KHDRBS1', 'MATR3', 'NCBP2', 'PPIG',
    ... 'PPP1R8', 'PRPF4', 'PUF60', 'RAVER1', 'SAFB2', 'SART3', 'SMN1',
    ... 'SMNDC1', 'SUGP2', 'TAF15', 'TARDBP', 'TIAL1']
    True
    """
    outfile = ConfigGraph.results / \
              f"sf_list_reg-{regulation}_t{threshold}.txt"
    if outfile.is_file():
        return outfile.open("r").read().strip(r"[|]").replace("'", "")\
            .split(", ")
    cnx = sqlite3.connect(ConfigGraph.db_file)
    c = cnx.cursor()
    query = "SELECT DISTINCT sf_name from cin_project_splicing_lore"
    c.execute(query)
    res = [sf[0] for sf in c.fetchall()]
    return get_good_factors(cnx, res, outfile, regulation, threshold,
                            get_every_events_4_a_sl)


def get_factors(factor: str, regulation: str = "one",
                threshold: int = 50) -> List[str]:
    """
    Function that return the factors names in the ChIA-PET database \
    that up or down-regulates at least `threshold` exons/genes.

    :param factor: The kind of factor to analyse
    :param regulation: up, down, all, one. If regulation is up or down, \
    then the function returns only the factor names up or down \
    regulating at least threshold exons/genes. If 'all' is chosen then \
    only factor that up-regulates and down-regulates at least \
    `threshold` exons/genes are returned. If 'one' is chosen, then \
    the factors must have at least `threshold` up-regulated exons OR \
    down-regulated exons.
    :param threshold: The minimum number of  exons/genes that must be \
    regulated by the  factor
    :return: The list of factor regulating at least threshold \
    exons/genes
    """
    if factor == "splicing":
        return get_sfname(regulation, threshold)
    return get_tfname(regulation, threshold)


def get_key(project: str, weight: int) -> str:
    """
    Get the combination of project and weight parameters

    :param project: A project name
    :param weight: A weight
    :return: The string combination of the two
    """
    return f"{project}_{weight}"


def multiple_stat_launcher(ps: int,
                           weight: int,
                           global_weight: int,
                           project: str,
                           same_gene: bool, inflation: float,
                           cell_line: str = "ALL",
                           feature: str = 'exon',
                           factor: str = "splicing",
                           logging_level: str = "DISABLE"):
    """
    Launch the statistical analysis for every

    :param ps: The number of processes to use
    :param weight: The weight of interaction to consider
    :param global_weight: The global weighs to consider. if \
    the global weight is equal to 0  then the project `project` is \
    used.
    :param project: The project name, used only if global_weight = 0
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param feature: The feature we want to analyse
    :param inflation: the inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param factor: The kind of factor to analyse
    :param logging_level: Level of information to display
    """
    if factor == "transcription" and feature == "exon":
        raise ValueError("You must set feature to gene when you study "
                         "transcription factors")
    ConfigGraph.community_folder.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    logging.info(f"Checking if communities contains often {feature}s "
                 f"regulated by a {factor} factor")
    sf_list = get_factors(factor)
    project = get_projects(global_weight, project)
    regulations = ['down', 'up', 'both']
    condition = list(product([project], [weight], sf_list, regulations))
    processes = {}
    pool = mp.Pool(processes=min(ps, len(condition)))
    logging.debug("Calculating stats...")
    for project, weight, sf_name, reg in condition:
        ckey = get_key(project, weight)
        args = [sf_name, reg, project, weight, global_weight, same_gene,
                inflation, cell_line, feature, factor]
        if ckey in processes:
            processes[ckey].append(pool.apply_async(get_stat4communities, args))
        else:
            processes[ckey] = [pool.apply_async(get_stat4communities, args)]
    for p, value in processes.items():
        project, weight = p.split("_")
        list_tuples = [proc.get(timeout=None) for proc in value]
        pool.close()
        pool.join()
        list_df = [t[0] for t in list_tuples]
        list_series = [t[1] for t in list_tuples]
        # df = pd.concat(list_df, axis=0, ignore_index=True)
        subfolder = "sf_community_enrichment" if factor == "splicing" else \
            "tf_community_enrichment"
        # outfile = ConfigGraph.get_community_file(project, weight,
        #                                          global_weight,
        #                                          same_gene, inflation,
        #                                          cell_line,
        #                                          feature,
        #                                          "_stat.txt",
        #                                          subfolder)
        # df.to_csv(outfile, sep="\t", index=False)
        glm_df = pd.DataFrame(list_series)
        for reg in regulations:
            cglm_df = glm_df[glm_df["reg"] == reg].copy()
            cglm_df["padj"] = multipletests(cglm_df['pval'].values,
                                           method='fdr_bh')[1]
            outfile = ConfigGraph.get_community_file(project, weight,
                                                     global_weight,
                                                     same_gene, inflation,
                                                     cell_line,
                                                     feature,
                                                     f"_{reg}_glmm_stat.txt",
                                                     subfolder)
            cglm_df.to_csv(outfile, sep="\t", index=False)


if __name__ == "__main__":
    doctest.testmod()