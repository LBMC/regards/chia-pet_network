#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

from typing import Tuple
import pandas as pd
import logging
import  sqlite3
from .config import ConfigGraph
from .clip_figures.config import ConfigClip
import numpy as np
import multiprocessing as mp
from pathlib import Path
from .sf_and_communities import get_communities, get_regulated_features, \
    get_stat_community_exons, get_ft_regulated_in_communities, \
    logistic_regression, multipletests, logging_def, \
    get_factors, product, glmm_maker, expand_dataframe
import lazyparser as lp


def glmm_statistics(df: pd.DataFrame, output_folder: Path,
                    sf_name: str, reg: str, project: str) -> pd.Series:
    """
    Create the glmm statistics for a given splicing factor with \
    given communities.

    :param df: dataframe containing stats data.
    :param output_folder: Folder containing the results
    :param sf_name: The splicing factor of interest
    :param reg: The regulation of interest
    :param project: The name of the project of interest
    :return: The glmm pvalue among with other informations
    """
    ndf = df.loc[-df['community'].isin(["All-community", "FASTERDB"]),
          :].copy()
    expanded_df = expand_dataframe(ndf)
    noutfold = output_folder / "expanded_df"
    noutfold.mkdir(exist_ok=True, parents=True)
    noutfile = noutfold / f"expanded_{sf_name}_{reg}.txt"
    expanded_df.to_csv(noutfile, sep="\t", index=False)
    pval = glmm_maker(expanded_df, noutfold)
    if np.isnan(pval):
        logging.warning(f"The factor {sf_name} doesn't have any regulated "
                        f"genes in selected communities")
    return pd.Series({"project": project, "sf": sf_name, "reg": reg,
                      "pval": pval})


def get_stat4communities(community_file: Path, output_folder: Path,
                         sf_name: str, reg: str,
                         feature: str = 'exon', factor: str = "splicing"
                         ) -> Tuple[pd.DataFrame, pd.Series]:
    """
    Get data (proportion of `reg` regulated exons by a splicing factor
    `sf_name` within a community) for every community.

    :param community_file: The community file used
    :param output_folder: folder where the result should be created
    :param sf_name: The splicing factor of interest
    :param reg: The regulation of interest
    :param project: The name of the project of interest
    :param feature: The kind of analysed feature
    :param factor: The kind of factor to analyse
    """
    project = community_file.name
    logging.debug(f"Working on {sf_name}-{reg}, for {project}")
    cnx = sqlite3.connect(ConfigGraph.db_file)
    communities = get_communities(community_file, feature, 0)
    communities = [communities[k] for k in communities]
    reg_ft = get_regulated_features(cnx, sf_name, reg, feature, factor)
    if len(reg_ft) == 0:
        raise IndexError(f'There no {feature} {reg} regulated specifically '
                         f'by {sf_name}')
    d = {"community": [], "reg_in_community": [], "community_size": [],
         "%reg in community": [], 'pval': []}
    d, n2, t2 = get_stat_community_exons(cnx, d, communities, reg_ft)
    threshold = 49
    if feature == "gene":
        threshold = 9
    for k, community in enumerate(communities):
        if len(community) > threshold:
            reg_community, tot_commmunity, prop = \
                get_ft_regulated_in_communities(community, reg_ft)
            d["community"].append(f"C{k + 1}")
            d['reg_in_community'].append(reg_community)
            d['community_size'].append(tot_commmunity)
            d['%reg in community'].append(prop)
            d['pval'].append(logistic_regression(reg_community, tot_commmunity,
                                                 n2, t2))
    d['padj'] = [np.nan, np.nan] + list(multipletests(d['pval'][2:],
                                                      method='fdr_bh')[1])
    d['sf'] = [sf_name] * len(d["community"])
    d['reg'] = [reg] * len(d["community"])
    d['project'] = [project] * len(d["community"])
    df = pd.DataFrame(d)
    s = glmm_statistics(df, output_folder, sf_name, reg, project)
    return df, s


@lp.parse
def multiple_stat_launcher(ps: int,
                           factor: str = "splicing",
                           logging_level: str = "DISABLE"):
    """
    Launch the statistical analysis for every

    :param ps: The number of processes to use
    :param factor: The kind of factor to analyse
    :param logging_level: Level of information to display
    """
    feature = "gene"
    community_files = [c for c in ConfigClip.communities if c]
    ConfigGraph.community_folder.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    logging.info(f"Checking if communities contains often {feature}s "
                 f"regulated by a {factor} factor")
    sf_list = get_factors(factor)
    regulations = ['down', 'up', 'both']
    condition = list(product(sf_list, regulations))
    logging.debug("Calculating stats...")
    for community_file in community_files:
        processes = []
        pool = mp.Pool(processes=min(ps, len(condition)))
        output_folder = ConfigGraph.output_factor / community_file.name / factor
        output_folder.mkdir(parents=True, exist_ok=True)
        for sf_name, reg in condition:
            args = [community_file, output_folder, sf_name, reg, feature, factor]
            processes.append(pool.apply_async(get_stat4communities, args))

        list_tuples = [proc.get(timeout=None) for proc in processes]
        pool.close()
        pool.join()
        list_series = [t[1] for t in list_tuples]
        glm_df = pd.DataFrame(list_series)
        for reg in regulations:
            cglm_df = glm_df[glm_df["reg"] == reg].copy()
            cglm_df["padj"] = multipletests(cglm_df['pval'].values,
                                           method='fdr_bh')[1]
            outfile = output_folder / f"{reg}_glmm_stat.txt"
            cglm_df.to_csv(outfile, sep="\t", index=False)


if __name__ == "__main__":
    multiple_stat_launcher()