#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Download and install hipMCL
"""

import subprocess as sp
from pathlib import Path
from .config import ConfigGraph
import logging
from ..logging_conf import logging_def


def download_hipmcl(outfile: Path):
    """
    Download the hipmlc script.

    :param outfile: The output folder where hipMCL algorithm will \
    be downloaded.
    :return: The folder containing hipMCL file
    """
    cmd = f"curl https://bitbucket.org/azadcse/hipmcl/get/6fa32bbcb4bc.zip " \
          f"--output {outfile}"
    sp.check_call(cmd, shell=True, stderr=sp.STDOUT)
    cmd2 = f"unzip {outfile} -d {ConfigGraph.data}"
    sp.check_call(cmd2, shell=True, stderr=sp.STDOUT)


def compilation(hip_folder: Path):
    """
    Compile hip_folder.

    :param hip_folder: Folder with hipfile
    """
    cmd = f"ln -s {hip_folder}/makefile-mac {hip_folder}/Makefile && " \
          f"(cd {hip_folder} && make hipmcl)"
    sp.check_call(cmd, shell=True, stderr=sp.STDOUT)


def install_hipmcl(logging_level: str = "DISABLE"):
    """
    INstall hipMCL.
    """
    logging_def(ConfigGraph.results, __file__, logging_level)
    logging.info("Downloading and installing hipmcl. If the installation "
                 "doesn't work, please try to install libopenmpi-dev on your "
                 "computer")
    download_hipmcl(ConfigGraph.hip_zip)
    compilation(ConfigGraph.get_hip_folder())
