#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a recap table from \
the folder produced by the script sf_and_other_communities.py
"""

from pathlib import Path
from typing import List
import lazyparser as lp
import pandas as pd


def load_files(folder: Path) -> List[Path]:
    """
    Return the list of recap table (files with the suffix "glmm_stat.txt".

    :param folder: Folder produced by sf_and_other_communities.py
    :return: The list of tables produced by this script
    """
    return list(folder.glob("**/*glmm_stat.txt"))


def recover_data(glmm_file: Path) -> pd.Series:
    """
    Recover Data of interest from a file.

    :param glmm_file: A file containing the summary produced by  \
    sf_and_other_communities.py script
    :return: A serie with all the data of interest
    """
    df = pd.read_csv(glmm_file, sep="\t")
    return pd.Series({
        "community": glmm_file.parents[1].name,
        "factorKind": glmm_file.parent.name,
        "regulation": glmm_file.name.split("_")[0],
        "nb_factor": len(df["sf"].unique()),
        "nb_sig_pval": df[df["pval"] <= 0.05].shape[0],
        "nb_sig_padj": df[df["padj"] <= 0.05].shape[0],
        "%_factor_sig": (df[df["padj"] <= 0.05].shape[0] /
                          len(df["sf"].unique()))   * 100
    })


def recover_all_data(list_file: List[Path]) -> List[pd.Series]:
    """
    Recover the data for every glmm files produced by \
     f_and_other_communities.py script

    :param list_file: The list of file of interest
    :return: The list of series of interest
    """
    return [recover_data(mfile) for mfile in list_file]


def build_and_save_df(list_series: List[pd.Series], outfile: Path) -> None:
    """
    Build and save the dataframe.

    :param list_series: The list of data of interest
    :param outfile: output file to store the table
    """
    df = pd.DataFrame(list_series)
    df.to_csv(outfile, sep="\t", index=False)


@lp.parse
def recap_builder(input_folder: str, output_file: str) -> None:
    """
    Create a recap table from the folder produced by \
    the script sf_and_other_communities.py

    :param input_folder: Folder produced by the script \
    sf_and_other_communities.py
    :param output_file: Output file.
    """
    list_files = load_files(Path(input_folder))
    list_series = recover_all_data(list_files)
    build_and_save_df(list_series, Path(output_file))


if __name__ == "__main__":
    recap_builder()