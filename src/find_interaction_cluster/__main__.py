#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch the script dedicated to find communities
"""


from .community_finder import multiple_community_launcher
import lazyparser as lp
from .config import ConfigGraph
from typing import List
from .install_hipMCL import install_hipmcl
from .sf_and_communities import multiple_stat_launcher
from .nt_and_community import multiple_nt_lm_launcher
from .create_ppi_files import ppi_stat_launcher
import logging
from ..logging_conf import logging_def
from .colocalisation_n_ppi_analysis import coloc_ppi_stat_main


@lp.parse(weight=range(1, 11), global_weight=range(11),
          feature=('gene', 'exon'), region=('', 'exon', 'intron', 'gene'),
          iteration="iteration > 20", test_type=('lm', 'permutation'),
          component_type=("nt", "aa", "dnt"))
def launcher(weight: int = 1,
             global_weight: int = 0,
             same_gene: bool = True,
             inflation: float = 1.5,
             cell_line: str = 'ALL',
             use_weight: bool = False,
             ps: int = ConfigGraph.cpu,
             html_fig: bool = False, feature: str = 'exon', region: str = '',
             component_type: str = 'nt',
             iteration: int = 1000, test_type: str = "lm",
             display_size: bool = False,
             project: str = "GSM1018963_GSM1018964",
             logging_level: str = "DISABLE"):
    """
    Script used to find communities inside  exon co-localized within a project

    :param weight: The weight of interaction to consider (default 1)
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all project are merge together and the interaction \
    seen in `global_weight` project are taken into account (default 0)
    :param project: The name of the project of interest (default GSM1517081)
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default True)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter). (default ALL)
    :param use_weight: Say if we want to write the weight into the result file.
    :param  html_fig: True to display the html figure (default False).
    :param feature: The feature we want to analyse (default 'exon')
    :param region: The region of a gene to analyse (used only if feature \
    is 'gene') (default '') (can be 'gene', 'exon', 'intron').
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa' (default 'nt').
    :param logging_level: The level of data to display (default 'DISABLE')
    :param iteration: the number of iteration for ppi and frequency analysis
    :param project: The project name of interest \
    (only used is global_weight = 0).
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param ps: The number of processes to use
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    if feature == "gene" and not same_gene:
        logging.warning(f"When level is equal to gene then same_gene must "
                        f"be true. Setting it to True.")
        same_gene = True
    if not ConfigGraph.get_hipmcl_prog().is_file():
        install_hipmcl("INFO")

    multiple_community_launcher(weight, global_weight, project, same_gene,
                                inflation, cell_line, use_weight,
                                html_fig, feature, logging_level)
    multiple_stat_launcher(ps, weight, global_weight, project, same_gene,
                           inflation, cell_line, feature, "splicing",
                           logging_level)
    multiple_stat_launcher(ps, weight, global_weight, project, same_gene,
                           inflation, cell_line, feature, "transcription",
                           logging_level)
    # multiple_nt_lm_launcher(ps, weight, global_weight, project,
    #                         same_gene, inflation, cell_line, feature, region,
    #                         component_type, test_type, iteration, display_size,
    #                         logging_level=logging_level)
    # if feature == "gene":
    #     # ppi_stat_launcher(weight, global_weight, project, same_gene,
    #     #                   inflation, cell_line,
    #     #                   ConfigGraph.ppi_threshold, iteration,
    #     #                   logging_level)
    #     coloc_ppi_stat_main(weight, global_weight, project, same_gene,
    #                         inflation, cell_line, iteration, logging_level)



launcher()