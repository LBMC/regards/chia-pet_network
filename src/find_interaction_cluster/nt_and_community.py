#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to see if the nucleotide \
frequency is not random between communities
"""

import pandas as pd
import logging
import sqlite3
from .config import ConfigGraph, get_communities
from typing import List, Tuple, Dict
import lazyparser as lp
from functools import reduce
from pathlib import Path
from rpy2.robjects import r, pandas2ri
from statsmodels.stats.multitest import multipletests
from .community_finder import get_projects
from ..logging_conf import logging_def
from itertools import product
import multiprocessing as mp
from .community_figures.fig_functions import create_community_fig
from ..nt_composition.config import get_features


def get_cpnt_frequency(cnx: sqlite3.Connection, list_ft: List[str],
                       feature: str, region: str = "",
                       component_type: str = "nt") -> pd.DataFrame:
    """
    Get the frequency of every nucleotides for features in list_ft.

    :param cnx: Connection to chia-pet database
    :param list_ft: The list of exons for which we want to get
    :param feature: the kind of feature analysed
    :param region: The region of gene analysed if feature is gene
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :return: the frequency of nucleotides for the list of exons.

    >>> d = get_cpnt_frequency(sqlite3.connect(ConfigGraph.db_file),
    ... ["1_1", "1_2"], "exon")
    >>> d[["id_exon", 'A', 'C', 'G', 'T']]
    ft id_exon         A         C         G         T
    0      1_1  16.63480  34.60803  32.12237  16.63480
    1      1_2  16.06426  26.10442  39.75904  18.07229
    >>> d = get_cpnt_frequency(sqlite3.connect(ConfigGraph.db_file),
    ... ['1', '2'], "gene")
    >>> d[["id_gene", 'A', 'C', 'G', 'T']]
    ft id_gene         A         C         G         T
    0        1  29.49376  18.34271  18.43874  33.72479
    1        2  31.90401  16.40251  18.79033  32.90315
    >>> d = get_cpnt_frequency(sqlite3.connect(ConfigGraph.db_file),
    ... ['1', '2'], "gene", 'exon', 'aa')
    >>> d[["id_gene", "R", "K", "D", "Q", "E"]]
    ft id_gene        R        K        D        Q        E
    0        1  4.75247  5.19300  5.95391  4.07997  6.96189
    1        2  4.34203  6.23736  6.77708  5.21984  7.01769
    """
    query_region = ""
    if feature == "gene":
        list_ft = [int(ft) for ft in list_ft]
        if region == "":
            region = "gene"
        query_region = f"AND region = '{region}'"
    query = f"""
             SELECT ft, id_{feature}, frequency
             FROM cin_{feature}_frequency
             WHERE id_{feature} IN {tuple(list_ft)}
             AND ft_type = '{component_type}' 
             {query_region}
             """
    df = pd.read_sql_query(query, cnx)
    df = df.pivot_table(index=f"id_{feature}", columns="ft",
                        values="frequency").reset_index()
    df[f"id_{feature}"] = df[f"id_{feature}"].astype(str)
    return df


def get_community_table(communities: Dict[str, List[str]],
                        size_threshold: int, feature: str) -> pd.DataFrame:
    """
    return the table indicating the name of the exons and the \
    the name of the community.

    :param communities: Dictionary of community of exons
    :param size_threshold: The required size a community must \
    have to be considered
    :param feature: The kind of feature analysed
    :return: table of community
    >>> c = {"C1": ['1_1', '2_5'], "C2": ['7_9', '4_19', '3_3']}
    >>> get_community_table(c, 3, 'exon')
      community id_exon  community_size
    0        C2     7_9               3
    1        C2    4_19               3
    2        C2     3_3               3
    >>> c = {"G1": ['1', '2'], "G2": ['7', '49', '3']}
    >>> get_community_table(c, 3, 'gene')
      community id_gene  community_size
    0        G2       7               3
    1        G2      49               3
    2        G2       3               3
    """
    dic = {"community": [], f"id_{feature}": [], "community_size": []}
    for k, comm in communities.items():
        if len(comm) >= size_threshold:
            clen = len(comm)
            for exon in comm:
                dic["community"].append(k)
                dic[f'id_{feature}'].append(exon)
                dic["community_size"].append(clen)
    return pd.DataFrame(dic)


def lm_maker(df: pd.DataFrame, outfile: Path, nt: str) -> float:
    """
    Make the lm analysis to see if the exon regulated by a splicing factor \
    are equally distributed among the communities.

    :param df: The dataframe
    :param outfile: A name of a file
    :param nt: the nucleotide of interest
    :return: the pvalue of lm

    """
    pandas2ri.activate()
    lmm = r(
        """
        require("lme4")
        require("DHARMa")

        function(data, folder, partial_name) {
            null_mod <- lm(%s ~ log(community_size), data=data)
            mod <- lmer(%s ~ log(community_size) + (1 | community), data=data)
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(folder, "/dignostics_", partial_name, ".png"))
            plot(simulationOutput)
            dev.off()
            return(anova(mod, null_mod, test="Chisq"))
        }
        """ % (nt, nt))
    folder = outfile.parent / "diagnostics"
    folder.mkdir(parents=True, exist_ok=True)
    partial_name = outfile.name.replace('.txt', '')
    res = lmm(df, str(folder), partial_name)
    return res["Pr(>Chisq)"][1]


def get_ft_id(cnx: sqlite3.Connection, feature: str = "exon") -> List[str]:
    """
    Return the id of every gene/exons in chia-pet database.

    :param cnx: A connection to chiapet database
    :param feature: The feature of interest
    :return: The list of feature id
    """
    query = f"SELECT DISTINCT id FROM cin_{feature}"
    c = cnx.cursor()
    c.execute(query)
    res = c.fetchall()
    return [str(cid[0]) for cid in res]


def create_ctrl_community(df: pd.DataFrame,
                          feature: str = 'exon', region: str = "",
                          cpnt_type: str = 'nt') -> pd.DataFrame:
    """
    :param df: A dataframe containing the frequency of each feature in a \
    community.
    :param feature: The kind of feature to analyse
    :param region: only use if feature is 'gene'. Used to focus on \
    a given region in genes (can be gene, exon, intron).
    :param cpnt_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :return: A dataframe containing the frequency of every nucleotides \
    of every exon in a large community
    """
    size_threshold = 10 if feature == "gene" else 50
    cnx = sqlite3.connect(ConfigGraph.db_file)
    ft_id = get_ft_id(cnx, feature)
    list_ft = [cid for cid in ft_id
               if cid not in df[f"id_{feature}"].to_list()]
    df_nt = get_cpnt_frequency(cnx, list_ft, feature, region, cpnt_type)
    df_com = get_community_table({"C-CTRL": list_ft}, size_threshold, feature)
    df_nt = df_nt.merge(df_com, how="left", on=f"id_{feature}")
    df = pd.concat([df, df_nt], axis=0, ignore_index=True)
    return df


def get_feature_by_community(df: pd.DataFrame, feature: str) -> Dict:
    """
    Create a dictionary containing the exons contained in each community.

    :param df: A dataframe containing the frequency of each nucleotide \
    in each exons belonging to a community.
    :param feature: the feature of interest (exon, gene)
    :return: A dictionary linking each community to the exons it contains

    >>> dataf = pd.DataFrame({"id_gene": ['1', '2', '3', '4'],
    ... 'community': ['C1', 'C1', 'C2', 'C2']})
    >>> get_feature_by_community(dataf, 'gene')
    {'C1': ['1', '2'], 'C2': ['3', '4']}
    >>> dataf.rename({"id_gene": "id_exon"}, axis=1, inplace=True)
    >>> get_feature_by_community(dataf, 'exon')
    {'C1': ['1', '2'], 'C2': ['3', '4']}
    """
    dic = {}
    for i in range(df.shape[0]):
        com, id_ft = df.iloc[i, :][['community', f'id_{feature}']]

        if com in dic:
            dic[com].append(id_ft)
        else:
            dic[com] = [id_ft]
    return dic


def prepare_dataframe(df: pd.DataFrame, test_type: str, nt: str,
                      iteration: int) -> pd.DataFrame:
    """

    :param df: A dataframe with the enrichment of a \
    nucleotide frequency for every community
    :param test_type: The kind of test performed to
    :param nt: The nucleotide of interest
    :param iteration: The number of iteration permformed to \
    produce the database
    :return: The dataframe ready for barplot visualisation
    """
    if test_type == "lm":
        # removing the size parameter
        df = df[df["community"] != "log(_size)"].copy()
        df.rename({"Pr(>|t|)": "p-adj"}, axis=1, inplace=True)
        df_bar = df[['community', nt, 'p-adj']]
        df_ctrl = df_bar[df_bar["community"] == "C-CTRL"]
        df_bar = df_bar[df_bar["community"] != "C-CTRL"].copy()
        df_bar.sort_values(nt, ascending=True, inplace=True)
    else:
        df_bar = df[["community", nt, f"{nt}_mean_{iteration}_ctrl",
                     "p-adj"]].copy()
        ctrl_val = df_bar[f"{nt}_mean_{iteration}_ctrl"]
        df_bar.drop(f"{nt}_mean_{iteration}_ctrl", axis=1, inplace=True)
        df_bar.sort_values(nt, ascending=True, inplace=True)
        df_ctrl = pd.DataFrame({"community": ["C-CTRL"] * len(ctrl_val),
                                nt: ctrl_val})
    df_bar = pd.concat([df_ctrl, df_bar], axis=0, ignore_index=True)
    return df_bar


def create_outfiles(project: str, weight: int, global_weight: int,
                    same_gene: bool, inflation: float, cell_line: str,
                    feature: str, cpnt_type: str, cpnt: str, test_type: str,
                    community_file: str) -> Tuple[Path, Path]:
    """
    Create a file used to store diagnostics and a file used to store the \
    table containing the test communities and the control community

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param cpnt_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param cpnt: The component (nt, aa, dnt) of interest
    :param feature: The kind of feature analysed
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param test_type: The type of test to make (permutation or lm)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :return: file used to store diagnostics and a file used to store the \
    table containing the test communities and the control community
    """

    outfolder = f"community_enrichment/{cpnt_type}_analysis"
    if community_file != "":
        cn = Path(community_file).name.replace(".txt", "")
        base = ConfigGraph.output_folder / outfolder / f"custom_{cn}"
        base.mkdir(exist_ok=True, parents=True)
        outfile = base / f"{cpnt}-{cpnt_type}_stat_{test_type}.txt"
        outfile_ctrl = base / \
            f"{cpnt}-{cpnt_type}_VS_CTRL_stat_{test_type}.pdf"
        return outfile, outfile_ctrl
    outfile = ConfigGraph.\
        get_community_file(project, weight, global_weight, same_gene,
                           inflation, cell_line, feature,
                           f"{cpnt}-{cpnt_type}_stat_{test_type}.txt",
                           outfolder)
    outfile_ctrl = ConfigGraph.\
        get_community_file(project, weight, global_weight, same_gene,
                           inflation, cell_line, feature,
                           f"{cpnt}-{cpnt_type}_VS_CTRL_stat_{test_type}.pdf",
                           outfolder)
    return outfile, outfile_ctrl


def get_stat_cpnt_communities(df: pd.DataFrame, project: str, weight: int,
                              global_weight: int, same_gene: bool,
                              inflation: float, cell_line: str,
                              cpnt_type: str, cpnt: str,
                              dic_com: Dict, feature: str = "exon",
                              test_type: str = "",
                              iteration: int = 1000,
                              display_size: bool = False,
                              community_file: str = "") -> pd.Series:
    """
    Get data (proportion of `reg` regulated exons by a splicing factor
    `sf_name` within a community) for every community.

    :param df: A dataframe containing the frequency of each nucleotide \
    in each exons belonging to a community.
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param cpnt_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param cpnt: The component (nt, aa, dnt) of interest
    :param dic_com: A dictionary linking each community to the exons \
    it contains.
    :param feature: The kind of feature analysed
    :param test_type: The type of test to make (permutation or lm)
    :param iteration: The number of sub samples to create
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    """
    logging.debug(f"{test_type} for {project}, w:{weight}, "
                  f"g:{global_weight} cpnt: {cpnt}({cpnt_type})")
    outfile, outfile_ctrl = create_outfiles(project, weight, global_weight,
                                            same_gene, inflation, cell_line,
                                            feature, cpnt_type,
                                            cpnt, test_type, community_file)
    res = {"project": project, "cpnt": cpnt,
           'pval': lm_maker(df, outfile, cpnt)}
    create_community_fig(df, feature, cpnt, outfile_ctrl, test_type,
                         dic_com, cpnt_type, iteration,
                         display_size=display_size)
    return pd.Series(res)


def create_dataframe(project: str, weight: int, global_weight: int,
                     same_gene: bool, inflation: float, cell_line: str = "ALL",
                     feature: str = 'exon',
                     region: str = "", component_type: str = 'nt',
                     community_file: str = "",
                     from_communities: bool = True,
                     ) -> pd.DataFrame:
    """
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False)
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: The kind of feature to analyse
    :param from_communities: True if we only select gene/exons
    :param region: the region of interest to extract from gene
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :return: A dataframe containing the frequency of every nucleotides \
    of every exon in a large community
    """
    size_threshold = 10 if feature == "gene" else 50
    cnx = sqlite3.connect(ConfigGraph.db_file)
    if from_communities:
        if community_file == "":
            result = ConfigGraph.get_community_file(project, weight,
                                                    global_weight,
                                                    same_gene, inflation,
                                                    cell_line,
                                                    feature, ".txt")
        else:
            result = Path(community_file)
            if not result.is_file():
                raise FileNotFoundError(f"File {community_file} not found !")
        communities = get_communities(result, feature, 0)
        ncommunities = [communities[c] for c in communities
                        if len(communities[c]) >= size_threshold]
        list_exon = reduce(lambda x, y: x + y, ncommunities)

        df_com = get_community_table(communities, size_threshold, feature)
        df = get_cpnt_frequency(cnx, list_exon, feature, region,
                                component_type)
        df = df.merge(df_com, how="left", on=f"id_{feature}")
    else:
        list_exon = get_ft_id(cnx, feature)
        df = get_cpnt_frequency(cnx, list_exon, feature, region,
                                component_type)
    return df


def create_dataframes(project, weight, global_weight, same_gene, inflation,
                      cell_line, feature,
                      region, test_type, component_type: str,
                      community_file: str
                      ) -> Tuple[pd.DataFrame, Dict]:
    """
    :param weight: The weight of interaction to consider
    :param global_weight: The global weighs to consider. if \
    the global weight is equal to 0  then the project `project` is \
    used.
    :param project: The project name, used only if global_weight = 0
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: The kind of analysed feature
    :param region: the region of interest to extract from gene
    :param test_type: The type of test to make (permutation or lm)
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    """
    df = create_dataframe(project, weight, global_weight, same_gene, inflation,
                          cell_line, feature, region, component_type,
                          community_file)
    df_ctrl = create_dataframe(project, weight, global_weight, same_gene,
                               inflation, cell_line, feature, region,
                               component_type,
                               from_communities=False)
    df_ctrl = df_ctrl.loc[-df_ctrl[f"id_{feature}"].isin(df[f"id_{feature}"]),
                          :].copy()
    dic_com = {} if test_type == 'lm' \
        else get_feature_by_community(df, feature)
    df = pd.concat([df, df_ctrl], axis=0, ignore_index=True)
    return df, dic_com


def multiple_nt_lm_launcher(ps: int,
                            weight: int,
                            global_weight: int,
                            project: str,
                            same_gene: bool,
                            inflation: float,
                            cell_line: str = "ALL",
                            feature: str = 'exon',
                            region: str = '',
                            component_type: str = "nt",
                            test_type: str = "lm",
                            iteration: int = 1000,
                            display_size: bool = False,
                            community_file: str = "",
                            logging_level: str = "DISABLE"):
    """
    Launch the creation of community files for every component type selected.

    :param ps: The number of processes to use
    :param weight: The weight of interaction to consider
    :param global_weight: The global weighs to consider. if \
    the global weight is equal to 0  then the project `project` is \
    used.
    :param project: The project name, used only if global_weight = 0
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: The kind of analysed feature
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param region: the region of interest to extract from gene
    :param test_type: The type of test to make (permutation or lmm)
    :param iteration: The number of iteration to perform
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data.
    :param logging_level: Level of information to display
    """
    ConfigGraph.community_folder.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    if feature == "gene" and region != "exon" and component_type == "aa":
        msg = f"If you looking at amino acid at gene level, then the region " \
              f"must be 'exon' not '{region}'. Setting it to exon. " \
              f"(corresponds to the concatenation of exons for all genes)"
        logging.warning(msg)
        region = 'exon'
    logging.info(f"Checking if communities as an effect on {component_type} "
                 "frequency")
    project = get_projects(global_weight, project)
    cpnt_list = get_features(component_type)
    condition = list(product([project], [weight], cpnt_list))
    processes = []
    pool = mp.Pool(processes=min(ps, len(condition)))
    logging.debug("Creating tables")
    df, dic_com = create_dataframes(project, weight, global_weight,
                                    same_gene, inflation, cell_line,
                                    feature, region,
                                    test_type, component_type, community_file)
    for project, weight, cpnt in condition:
        nfile_table = ConfigGraph.get_community_file(
            project, weight, global_weight, same_gene, inflation, cell_line,
            feature, f"_{component_type}_table.txt", "community_enrichment")
        df.to_csv(nfile_table, sep="\t", index=False)
        args = [df, project, weight, global_weight, same_gene, inflation,
                cell_line, component_type, cpnt, dic_com, feature, test_type,
                iteration, display_size, community_file]
        processes.append(pool.apply_async(get_stat_cpnt_communities, args))
    results = [p.get(timeout=None) for p in processes]
    pool.close()
    pool.join()
    fdf = pd.DataFrame(results)
    fdf["padj"] = multipletests(fdf['pval'].values, method='fdr_bh')[1]
    outfile = ConfigGraph.get_community_file(project, weight,
                                             global_weight,
                                             same_gene, inflation, cell_line,
                                             feature,
                                             f"lmm-{component_type}_stat.txt",
                                             "community_enrichment")
    nfolder = outfile.parent / f"{component_type}_analysis"
    noutfile = nfolder / outfile.name
    fdf.to_csv(noutfile, sep="\t", index=False)


@lp.parse(ps=range(mp.cpu_count()), weight=range(1, 11),
          global_weight=range(11),
          feature=('gene', 'exon'), region=('', 'exon', 'intron', 'gene'),
          iteration="iteration > 20", test_type=('lm', 'permutation'),
          component_type=("nt", "aa", "dnt"))
def launcher_community_file(ps: int = 1,
                            weight: int = -1,
                            global_weight: int = -1,
                            project: str = "GSM1018963_GSM1018964",
                            same_gene: bool = True,
                            inflation: float = 1.5,
                            cell_line: str = "ALL",
                            feature: str = 'exon',
                            region: str = '',
                            component_type: str = "nt",
                            test_type: str = "lm",
                            iteration: int = 1000,
                            display_size: bool = False,
                            community_file: str = "",
                            logging_level: str = "DISABLE"):
    """
    Launch the creation of community files for every component type selected.

    :param ps: The number of processes to use (default 1)
    :param weight: The weight of interaction to consider (default -1)
    :param global_weight: The global weighs to consider. if \
    the global weight is equal to 0  then the project `project` is \
    used. (default -1)
    :param project: The project name, used only if global_weight = 0
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene (default True)
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param feature: The kind of analysed feature (default exon)
    :param component_type: The type of component to analyse; It \
    can be 'nt', 'dnt' or 'aa'.
    :param region: the region of interest to extract from gene (default '')
    :param test_type: The type of test to make (permutation or lm) (default lm)
    :param iteration: The number of iteration to perform (default 1000)
    :param display_size: True to display the size of the community. \
    False to display nothing. (default False)
    :param community_file: A file containing custom communities. If \
    it equals to '' then weight, global weight and same genes parameter are \
    used to find the community files computed with ChIA-PET data. (default '')
    :param logging_level: Level of information to display (default DISABLE)
    """
    multiple_nt_lm_launcher(ps, weight, global_weight, project,
                            same_gene, inflation, cell_line, feature, region,
                            component_type, test_type, iteration, display_size,
                            community_file, logging_level)


if __name__ == "__main__":
    launcher_community_file()
