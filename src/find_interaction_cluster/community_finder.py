#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to detect \
communities of close exons inside co-localised exons.
"""

import networkx as nx
import numpy as np
from networkx.algorithms import community
import sqlite3
from .config import ConfigGraph, get_communities_basefile
from ..nt_composition.make_nt_correlation import get_project_colocalisation
from ..logging_conf import logging_def
import pandas as pd
import logging
import plotly.graph_objects as go
import plotly
from pathlib import Path
from typing import Tuple, Dict
import matplotlib.cm as cm
from matplotlib.colors import to_rgba
import json
import subprocess as sp
from networkx.readwrite import json_graph


def get_nodes(interaction: np.array) -> np.array:
    """
    Return the list of exon that are at least co-localized with another.

    :param interaction: List of interaction
    :return: The unique exons in interaction
    """
    return np.unique(interaction[:, [0, 1]].flatten())


def create_graph(interaction: np.array) -> nx.Graph:
    """
    Create the graph of co-localized exons.

    :param interaction:  Each couples of co-localized exons within a project.

    :return: The Graph
    """
    logging.debug("Graph creation ...")
    nodes = get_nodes(interaction)
    graph = nx.Graph()
    graph.add_nodes_from(nodes)
    for inter in interaction:
        graph.add_edge(inter[0], inter[1], weight=float(inter[2]))
    logging.debug(f'Node : {len(nodes)} - Edges : {len(interaction)}')
    return graph


def lighten_color(color: np.array, lighten: float = 1):
    """

    :param color: A tuple of 3 float
    :param lighten: The value to add to color.
    :return: The color lightened
    """
    color = list(to_rgba(color))
    color = [int(c * 255) for c in color[0:3]] + [lighten]
    color = 'rgba(' + ', '.join(map(str, color)) + ')'
    return color


def write_cytoscape_graph(graph: nx.Graph, dic_community: Dict[str, str],
                          outfile: str):
    """
    Write a dictionary in cyjs format usable by cytoscape.

    :param graph: The network
    :param dic_community: dictionary linking each njode to it's community.
    :param outfile: The name of the graph
    """
    data = nx.cytoscape_data(graph)
    for d in data['elements']['nodes']:
        exon = d['data']['id']
        d['data']['community'] = dic_community[exon]['num']
    res = json.dumps(data)
    with open(outfile, 'w') as f:
        f.write(res)
    g_json = json_graph.node_link_data(graph)
    outfile_json = str(outfile).replace(".cyjs", ".json")
    json.dump(g_json, open(outfile_json, 'w'), indent=2)


def find_communities(graph: nx.Graph, project: str,
                     outfile: Path, result_file: Path,
                     feature: str = 'exon', inflation: float = 1.5,
                     compute_ec_cov: bool = True) -> Tuple[pd.DataFrame, Dict]:
    """
    Find the communities within the graph.

    :param graph: Graph of co-localized exons
    :param project: The name of the project
    :param outfile: A file containing interaction
    :param feature: the kind of analysed feature (exons or gene)
    :param inflation: The inflation parameter
    :param compute_ec_cov: If true, compute edges connectivity and \
    coverage.
    :return: The communities within the graph and the community \
    to wich each exon belong
    """
    warn = 0
    logging.debug("Finding community ...")
    if not result_file.is_file():
        cmd = f"mpirun -np 1 {ConfigGraph.get_hipmcl_prog()} -M {outfile} " \
              f"-I {inflation} -per-process-mem 50 -o {result_file}"
        sp.check_call(cmd, shell=True, stderr=sp.STDOUT)
    communities = get_communities_basefile(result_file)
    dic_community = {}
    cov = np.nan
    if compute_ec_cov:
        cov = round(community.coverage(graph, communities), 2)
    modularity = community.modularity(graph, communities, weight='weight')
    d = {'community': [], 'nodes': [], 'edges': [], 'EC': [], 'HCS': [],
         '%E vs E in complete G': [],
         'cov': [], 'modularity': [], f'{feature}s': []}
    colors = cm.hsv(np.linspace(0, 1, len(communities)))
    logging.debug('Creating community result file')
    for k, c in enumerate(communities):
        subg = nx.subgraph(graph, c)
        nb_nodes = len(c)
        if nb_nodes <= 1:
            warn += 1
        nb_edges = len(subg.edges)
        try:
            complete = round(nb_edges / (nb_nodes * (nb_nodes - 1) / 2) * 100,
                             2)
        except ZeroDivisionError:
            complete = np.nan
        edge_connectivity = np.nan
        if compute_ec_cov:
            edge_connectivity = nx.edge_connectivity(subg)
        is_hc = 'yes' if edge_connectivity > nb_nodes / 2 else 'no'
        for exon in c:
            dic_community[exon] = {'num': f'C{k + 1}',
                                   'col': lighten_color(colors[k])
                                   if is_hc == 'yes' else
                                   (lighten_color(colors[k], 0.1)
                                    if nb_nodes > 2 else 'white')}
        d['community'].append(f'C{k + 1}')
        d['nodes'].append(nb_nodes)
        d['edges'].append(nb_edges)
        d['EC'].append(edge_connectivity)
        d['HCS'].append(is_hc)
        d['%E vs E in complete G'].append(complete)
        d[f'{feature}s'].append(', '.join(sorted(list(c))))
        d['cov'].append(cov)
        d['modularity'].append(round(modularity, 5))
        if nb_nodes <= 9:
            graph.remove_nodes_from(c)
    d['project'] = [project] * len(d['community'])
    df = pd.DataFrame(d)
    if warn > 0:
        logging.warning(f" {warn} communities were composed by one {feature} "
                        f"or less !")
    return df, dic_community


def create_figure(graph: nx.Graph, outfile: Path, dic_community: Dict,
                  title: str):
    """
    Create a figure showing the network.

    :param graph: The network object
    :param outfile: File were the figure will be created
    :param dic_community: dictionary linking each node to it's community.
    :param title: The title of the figure
    """
    logging.debug("Creating figure")
    edge_x = []
    edge_y = []
    middle_x = []
    middle_y = []
    weight = []
    nodes = nx.nx_pydot.graphviz_layout(graph, prog="neato")
    for edge in graph.edges():
        x0, y0 = nodes[edge[0]]
        x1, y1 = nodes[edge[1]]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)
        middle_x.append((x0 + x1) / 2)
        middle_y.append((y0 + y1) / 2)
        weight.append(f"{edge[0]}-{edge[1]} = "
                      f"{graph[edge[0]][edge[1]]['weight']}")

    edge_trace = go.Scatter(
        x=edge_x, y=edge_y,
        line=dict(width=1, color='#888'),
        mode='lines')

    middle_node_trace = go.Scatter(
        x=middle_x,
        y=middle_y,
        text=weight,
        mode='markers',
        hoverinfo='text',
        marker=dict(
            color='#888',
            size=3))

    node_x = []
    node_y = []
    node_text = []
    node_color = []
    line_color = []
    for node in graph.nodes():
        x, y = nodes[node]
        node_text.append(f"{node} - {dic_community[node]['num']}")
        node_x.append(x)
        node_y.append(y)
        node_color.append(dic_community[node]['col'])
        line_color.append(dic_community[node]['line'])
    node_trace = go.Scatter(
        x=node_x, y=node_y,
        mode='markers',
        hoverinfo='text',
        marker=dict(
            color=node_color,
            line=dict(width=3, color=line_color),
            size=20,
            ))

    node_trace.text = node_text
    fig = go.Figure(data=[edge_trace, middle_node_trace, node_trace],
                    layout=go.Layout(
                        title={
                            'text': title,
                            'font': {'size': 20},
                            'y': 0.995,
                            'x': 0.5},
                        hovermode='closest',
                        plot_bgcolor='rgba(0,0,0,0)',
                        margin=dict(b=20, l=5, r=5, t=40),
                        xaxis=dict(showgrid=False, zeroline=False,
                                   showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False,
                                   showticklabels=False),)
                    )
    plotly.offline.plot(fig, filename=str(outfile),
                        auto_open=False, validate=False)


def get_figure_title(project, weight, global_weight, same_gene, feature):
    """
    The title of the graphics.

    :param project: The project of interest
    :param weight: The minimum weight we want to consider an interaction
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param feature: The kind of analysed features
    :return: A figure title
    """
    title = f"Co-localisation between {feature}s having a weight greater " \
            f"than {weight} in "
    if global_weight == 0:
        title += f"the project {project}"
    else:
        title += f" at least {global_weight} chia-pet projects"
    if feature == 'exon':
        if not same_gene:
            title += ' (co-localisation within gene are NOT taken into ' \
                     'account)'
        else:
            title += ' (co-localisation within gene ARE taken into account)'
    return title


def write_interaction_file(arr_interaction: np.array, project: str,
                           weight: int, global_weight: int, same_gene: bool,
                           inflation: float, cell_line: str = "ALL",
                           use_weight: bool = False,
                           feature: str = 'exon'):
    """

    :param arr_interaction:  Each couples of co-localized feature within a \
    project.
    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param use_weight: Say if we want to write the weight into the result file.
    :param feature: Says if we want to work at gene or at exons level
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :return:
    """
    logging.debug('Writing interaction files ...')
    outfile = ConfigGraph.get_community_file(project, weight, global_weight,
                                             same_gene, inflation, cell_line,
                                             feature,
                                             f"_interation.txt")
    result = ConfigGraph.get_community_file(project, weight, global_weight,
                                            same_gene, inflation, cell_line,
                                            feature,
                                            f"_communities.txt")
    with outfile.open('w') as f:
        for exon1, exon2, cweight in arr_interaction:
            if not use_weight:
                cweight = 1
            f.write(f"{exon1}\t{exon2}\t{cweight}\n")
    return outfile, result


def community_finder(weight: int, global_weight: int, project: str = "",
                     same_gene=True, html_fig: bool = False,
                     feature: str = "exon", inflation: float = 1.5,
                     cell_line: str = "ALL", use_weight: bool = True,
                     logging_level: str = "DISABLE"):
    """
    Find communities inside co-localisation between exons found in \
    a ChIA-PET project.

    :param project: The name of the project of interest
    :param weight: The minimum weight of interaction to consider
    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param same_gene: Say if we consider as co-localised, exons within the \
    same gene (True) or not (False) (default False)
    :param logging_level: The level of data to display (default 'DISABLE')
    :param html_fig: True to create an html figure, false else
    :param inflation: The inflation parameter
    :param use_weight: Say if we want to write the weight into the result file
    :param feature: The feature we want to analyse (default 'exon')
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    """
    ConfigGraph.output_folder.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigGraph.output_folder, __file__, logging_level)
    cnx = sqlite3.connect(ConfigGraph.db_file)
    interaction = get_project_colocalisation(cnx, project, weight,
                                             global_weight, same_gene, True,
                                             level=feature, cell=cell_line)
    outfile, result_file = write_interaction_file(interaction, project,
                                                  weight, global_weight,
                                                  same_gene, feature=feature,
                                                  inflation=inflation,
                                                  use_weight=use_weight,
                                                  cell_line=cell_line)
    graph = create_graph(interaction)
    df, dic_community = find_communities(graph, project, outfile, result_file,
                                         feature, inflation)
    logging.debug('Writing results ...')
    outfiles = [ConfigGraph.get_community_file(
        project, weight, global_weight, same_gene, inflation, cell_line,
        feature, ext)
        for ext in [f'.txt', f'.cyjs', f'.html']]
    df.to_csv(outfiles[0], sep="\t", index=False)
    logging.debug("Saving the graph ...")
    write_cytoscape_graph(graph, dic_community, outfiles[1])
    if html_fig:
        fig_title = get_figure_title(project, weight, global_weight, same_gene,
                                     feature)
        create_figure(graph, outfiles[2], dic_community, fig_title)
    logging.debug('Done !')


def get_projects(global_weight: int, project: str) -> str:
    """
    Get projects name.

    :param global_weight: The global weight to consider. if \
    the global weight is equal to 0 then then density figure are calculated \
    by project, else all projet are merge together and the interaction \
    seen in `global_weight` project are taken into account
    :param project: The name of a project
    :return: The project to consider
    """
    if global_weight != 0:
        return f'Global-weight-{global_weight}'
    else:
        return project


def multiple_community_launcher(weight: int,
                                global_weight: int,
                                project: str,
                                same_gene: bool,
                                inflation: float = 1.5,
                                cell_line: str = "ALL",
                                use_weight: bool = False,
                                html_fig: bool = False,
                                feature: str = 'exon',
                                logging_level: str = "DISABLE"):
    """
    :param weight: The weight of interaction to consider
    :param global_weight: The global weighs to consider. if \
    the global weight is equal to 0  then the project `project` is \
    used.
    :param same_gene: Say if we consider as co-localised exon within the \
    same gene
    :param inflation: The inflation parameter
    :param cell_line: Interactions are only selected from projects made on \
    a specific cell line (ALL to disable this filter).
    :param use_weight: Say if we want to write the weight into the result file
    :param html_fig: True to create an html figure, false else
    :param feature: The feature we want to analyse (default 'exon')
    :param project: The project name, used only if global_weight = 0
    :param logging_level: Level of information to display
    """
    ConfigGraph.community_folder.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    project = get_projects(global_weight, project)
    logging.info(f'Finding community for project : {project}, '
                 f'global_weight : {global_weight}, weight: {weight}')
    community_finder(weight, global_weight, project, same_gene, html_fig,
                     feature, inflation, cell_line, use_weight)
