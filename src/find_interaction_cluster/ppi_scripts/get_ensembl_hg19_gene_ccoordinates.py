#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to get only coding gene from \
ensembl..
"""

from .config_ppi import ConfigPPI
import pandas as pd
from pathlib import Path
import numpy as np


def load_genes(ensembl_file: Path) -> pd.DataFrame:
    """
    Load a table containing all the human protein of ensembl database \
    and the name and location of their genes.

    :param ensembl_file: A file containing all the human protein of \
    ensembl database and the name and location of their genes.
    :return: table containing all the human protein of ensembl database \
    and the name and location of their genes.
    """
    df = pd.read_csv(ensembl_file, sep="\t",
                     dtype={"Gene_stable_ID": str,
                            "Protein_stable_ID": str,
                            "Chromosome-scaffold_name": str,
                            "Gene_start": int,
                            "Gene_end": int,
                            "Strand": str,
                            "Gene_name": str})
    df = df.loc[-df['Protein_stable_ID'].isna(), :]
    df = df.loc[df["Chromosome-scaffold_name"].isin(
        list(map(str, range(1, 23))) + ["X", "Y"]), :]
    return df.drop_duplicates()


def create_protein_gene_file():
    """
    Create a file linking each ensembl protein to it's gene and the hg19 \
    coordinate of this gene.
    """

    df_gene = load_genes(ConfigPPI.ensembl_gene)
    ConfigPPI.protein_gene.parent.mkdir(parents=True, exist_ok=True)
    df_gene.to_csv(ConfigPPI.protein_gene, sep="\t", index=False)
