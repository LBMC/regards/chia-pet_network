#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

from .get_ensembl_hg19_gene_ccoordinates import create_protein_gene_file
from .create_gene_interaction_file import get_protein_interaction_table

def launcher_ppi():
    """
    launch every script in this submodules.
    """
    create_protein_gene_file()
    get_protein_interaction_table()


launcher_ppi()