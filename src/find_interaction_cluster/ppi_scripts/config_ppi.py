#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script contains all the variables needed in this \
submodules
"""

from ..config import ConfigGraph


class ConfigPPI:
    """
    contains all the variables used in this submodules
    """
    output_folder = ConfigGraph.output_folder / "ppi_analysis"
    data = ConfigGraph.data
    bed_ensembl = data / "ppi_data" / "liftover_hg38gene2hg19.bed"
    ensembl_gene = data / "ppi_data" / "mart_export_hg37_proteins_genes.txt"
    ppi_string = data / "ppi_data" / "9606.protein.links.v11.0.txt.gz"
    protein_gene = output_folder / "protein_gene.txt"
    gene_ppi_file = output_folder / "ppi_gene_file.txt"
    protein_gene_fasterdb = output_folder / "protein_gene_fasterdb.txt"
    fasterdb_ppi = output_folder / "fasterdb_ppi.txt"