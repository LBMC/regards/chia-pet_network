#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create a file of genes having their proteins interacting with \
each other
"""

from pathlib import Path
from ..config import Config
from .config_ppi import ConfigPPI
import pandas as pd
from typing import Dict, Optional, Tuple


def load_string_ppi_file(ppi_file: Path) -> pd.DataFrame:
    """
    Load the string protein protein interaction file.

    :param ppi_file: A file containing the protein protein interaction.
    :return: The loaded string ppi interaction file
    """
    df = pd.read_csv(ppi_file, sep=" ")
    df['protein1'] = df['protein1'].str.replace("9606.", "",  regex=False)
    df['protein2'] = df['protein2'].str.replace("9606.", "", regex=False)
    return df


def load_protein_gene_links(prot_gene_file: Path) -> pd.DataFrame:
    """
    Load the file linking each gene to it's hg19 coordinates and encoded \
    proteins.

    :param prot_gene_file: A file linking each gene to it's hg19 \
    coordinates and encoded proteins.
    :return: The loaded file
    """
    df = pd.read_csv(prot_gene_file, sep="\t")
    df.rename({"Chromosome-scaffold_name": "chr", "Gene_start": "start",
               "Gene_end": "stop", "Strand": "strand"}, axis=1, inplace=True)
    df['Gene_name'] = df['Gene_name'].str.upper()
    return df


def load_fasterdb_gene(bed_gene: Path) -> Dict:
    """
    Load a bed file containing all fasterDB gene.

    :param bed_gene: A bed file containing all fasterdb gene
    :return: A dictionary linking each fasterDB gene name to its coordinates
    """
    dic = {}
    with bed_gene.open("r") as inbed:
        for line in inbed:
            line = line.replace("\n", "").split("\t")
            gene_name = line[4].upper()
            if gene_name not in dic.keys():
                dic[gene_name] = [[line[0], int(line[1]), int(line[2]),
                                   int(line[3]), gene_name, line[5]]]
            else:
                dic[gene_name].append(
                    [line[0], int(line[1]), int(line[2]),
                     int(line[3]), gene_name, line[5]])
    return dic


def find_fasterdb_id(mseries: pd.Series, dic: Dict) -> Optional[int]:
    """
    Find the fasterDB gene id from a line corresponding to an ensembl gene.

    :param mseries: A line containing the ensembl gene id and gene name of \
    a gene
    :param dic: A dictionary linking each gene name to it's coordinate
    :return: The fasterDB id of the ensembl gene.
    """
    if mseries['Gene_name'].upper() not in dic.keys():
            return None
    genes = dic[mseries['Gene_name'].upper()]
    if len(genes) == 1:
        return genes[0][3]
    elif len(genes) >= 1:
        for gene in genes:
            if (
                mseries['chr'] == gene[0]
                and mseries['start'] == gene[1]
                and mseries['stop'] == gene[2]
                and mseries['strand'] == gene[5]
            ):
                return gene[3]
        return None
    return None


def add_gene_id_column(prot_gene_df: pd.DataFrame, dic: Dict) -> pd.DataFrame:
    """
    Add the fasterDB gene id to ensembl gene then only return lines in \
    the dataframe with a fasterDB gene id.

    :param prot_gene_df: A table containing ensembl gene id.
    :param dic: A dictionary linking gene name to is fasterdb id.
    :return: The table prot_gene_df with a column FasterDB_id
    """
    prot_gene_df['FasterDB_id'] = prot_gene_df.apply(find_fasterdb_id,
                                                     axis=1, dic=dic)
    df = prot_gene_df.loc[-prot_gene_df['FasterDB_id'].isna(), :].copy()
    df['FasterDB_id'] = df['FasterDB_id'].astype(int)
    return df


def add_fasterdb_id_to_ppi_file(ppi_df: pd.DataFrame,
                                df_fasterdb: pd.DataFrame) -> pd.DataFrame:
    """
    Add two columns to the ppi_df: one containing the fasterdb gene_id \
    of the gene that encode the first interacting protein and the second \
    containing the fasterdb gene id of the gene encoding the second \
    interacting protein.

    :param ppi_df: Dataframe of interaction
    :param df_fasterdb: df with the fasterDB gene id of the gene en their \
    encoding protein identified by ensembl protein id.
    :return: The dataframe with the gene id of genes encoding the interacting \
    protein.
    """
    df_fasterdb = df_fasterdb[["Protein_stable_ID", "FasterDB_id"]].copy()
    df_fasterdb.rename({"FasterDB_id": "gene"}, axis=1, inplace=True)
    ppi_df = ppi_df.merge(df_fasterdb, how="left", left_on="protein1",
                          right_on="Protein_stable_ID")
    ppi_df = ppi_df.merge(df_fasterdb, how="left", left_on="protein2",
                          right_on="Protein_stable_ID", suffixes=['1', '2'])
    ppi_df = ppi_df.loc[(-ppi_df['gene1'].isna()) &
                        (-ppi_df['gene2'].isna()), :]
    ppi_df.drop(["Protein_stable_ID1", "Protein_stable_ID2"], axis=1,
                inplace=True)
    ppi_df['gene1'] = ppi_df['gene1'].astype(int)
    ppi_df['gene2'] = ppi_df['gene2'].astype(int)
    return ppi_df


def get_protein_interaction_table():
    """
    link ensemble gene and their encoding protein to a fasterDB gene id.
    """
    df_prot_gene = load_protein_gene_links(ConfigPPI.protein_gene)
    dic_gene = load_fasterdb_gene(Config.bed_gene)
    df_fasterdb_id = add_gene_id_column(df_prot_gene, dic_gene)
    df_fasterdb_id.to_csv(ConfigPPI.protein_gene_fasterdb, sep="\t",
                          index=False)
    ppi_df = load_string_ppi_file(ConfigPPI.ppi_string)
    ppi_fasterdb = add_fasterdb_id_to_ppi_file(ppi_df, df_fasterdb_id)
    ppi_fasterdb.to_csv(ConfigPPI.fasterdb_ppi, sep="\t", index=False)
