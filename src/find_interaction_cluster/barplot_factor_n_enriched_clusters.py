#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Here we display the barplot indicating for each factor the \
number of genes/exons it regulates and the number of community enriched in \
genes/exons for this factor
"""
import pylab as p

from .sf_and_communities import get_regulated_features, get_factors
from .clip_figures.clip_analyser import create_table
from .community_figures.fig_functions import create_community_fig
import pandas as pd
from pathlib import Path
from typing import List
from .config import ConfigGraph, Config, get_community_file
from itertools import product
import seaborn as sns
from .clip_figures.config import ConfigClip
import multiprocessing as mp
import logging
from ..logging_conf import logging_def
import lazyparser as lp


def comptute_row(feature: str, factor_name: str, regulation: str,
                 com_files: List[Path], factor_type: str = "splicing",
                 test_type: str = "permutation",
                 iteration: int = 10000,
                 output_folder: Path = ConfigGraph.output_barplots) -> pd.Series:
    """
    Create A row containing for a factor, its name, the number of \
    feature it regulates with a `regulation`regulation and the \
    number of enriched cluster in regulated feature

    :param feature: The feature we want to analyse (default 'exon')
    :param factor_name: Name of the factor of interest
    :param regulation: The regulation of interest (up, down or both)
    :param com_files: A file containing communities
    :param factor_type: The kind of factor to analyse
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param output_folder: Folder where the result will be created
    :return: A row of the dataframe that is used to build the barplots

    # >>> comptute_row("gene", "SRSF1", "down",
    # ... [Config.tests_files / "test_community_file.txt"], "splicing",
    # ... "permutation", 1000, Path("/tmp"))
    # factor                        SRSF1
    # kind                       splicing
    # reg                            down
    # reg-genes                      1232
    # test_community_file.txt           0
    # dtype: object
    """
    logging.debug(f"Working on {factor_name}-{regulation}")
    dic_val = {"factor": factor_name, "kind": factor_type, "reg": regulation}
    for com_file in com_files:
        table_com = create_table(feature, (factor_name, regulation), Path("."),
                                 com_file, factor_type)
        v = table_com[table_com["regulation"] == 1].shape[0]
        if f"reg-{feature}s" in dic_val:
            if dic_val[f"reg-{feature}s"] != v:
                raise ValueError(
                    f"Error for {factor_type}, {regulation}, "
                    f"{com_file.name}: The number of regulated features is "
                    f"different between communities: "
                    f"{dic_val[f'reg-{feature}s']} != {v}")
        else:
            dic_val[f"reg-{feature}s"] = v
        outfiles = [output_folder / \
                  "community_fig" / \
                  f"{factor_type}_{factor_name}_{regulation}_" \
                  f"{feature}_{com_file.name}.{e}" for e in ["pdf", "txt"]]
        if not outfiles[1].is_file():
            outfiles[0].parent.mkdir(exist_ok=True)
            create_community_fig(table_com, feature, "regulation", outfiles[0],
                                 test_type, iteration=iteration)
        res_df = pd.read_csv(outfiles[1], sep="\t")
        dic_val[com_file.name] = res_df[res_df["reg-adj"] == " + "].shape[0]
    return pd.Series(dic_val)


def compute_full_table(ps: int, feature: str, com_files: List[Path],
                       factor_type: str = "splicing",
                       test_type: str = "permutation",
                       iteration: int = 10000,
                       output_folder: Path = ConfigGraph.output_barplots
                       ) -> pd.DataFrame:
    """
    Create A dataframe containing for each factor, its name, the number of \
    feature it regulates with a `regulation`regulation and the \
    number of enriched cluster in regulated feature

    :param ps: Number of threads to use
    :param feature: The feature we want to analyse (default 'exon')
    :param com_files: A file containing communities
    :param factor_type: The kind of factor to analyse
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param output_folder: Folder where the result will be created
    :return: The final dataframe that will be used to build the barplots
    """
    list_factors = get_factors(factor_type)
    regulation = ["down", "up"]
    my_prod = product(list_factors, regulation)
    ps = 1 if ps < 1 else ps
    pool = mp.Pool(processes=min(ps, mp.cpu_count()))
    processes = []
    for factor, regulation in my_prod:
        args = [feature, factor, regulation, com_files, factor_type,
                test_type, iteration, output_folder]
        processes.append(pool.apply_async(comptute_row, args))
    list_series = [p.get(timeout=None) for p in processes]
    pool.close()
    pool.join()
    return pd.DataFrame(list_series)


def create_barplot(df: pd.DataFrame, feature: str, output: Path,
                   community: Path, factor_kind: str) -> None:
    """
    Create a barplot representing for each factor the number of feature \
    it up and down-regulates and the number of cluster enriched in those \
    feature.

    :param df: A dataframe containing the regulation data for each factor
    :param feature: The kind of feature of interest
    :param output: Folder where the result will be created
    :param community: The community file used to produce the figure
    :return: The barplot representing for each factor the number of feature \
    it up and down-regulates and the number of cluster enriched in those \
    feature.

    >>> cf = Config.tests_files / "test_community_file.txt"
    >>> d = pd.DataFrame({"factor": ["SRSF1", "TRA2A", "HNRNPC", "SFQP",
    ... "SRSF3"] * 2,
    ... "kind": ["splicing"] * 10, "reg": ["up"] * 5 + ["down"] * 5,
    ... "reg-genes": [10, 11, 12, 13, 14, 50, 51, 52, 53, 54],
    ... cf.name: [1, 2, 3, 4, 5, 20, 21, 22, 23, 24]})
    >>> create_barplot(d, "gene", Path("/tmp"), cf, "splicing")
    """
    sns.set(context="talk")
    df.sort_values(["factor", "reg"], ascending=[True, False], inplace=True)
    g = sns.catplot(x="factor", y=f"reg-{feature}s", hue="reg", data=df,
                    kind="bar", aspect=1.7, height=18)
    g.fig.subplots_adjust(top=0.9)
    g.fig.suptitle(f"Barplot representing the number of up- and down-regulated "
                   f"{feature}s by {factor_kind} factors and the number of\n"
                   f"clusters enriched in those {feature}s (clusters taken from "
                   f"{community.name})")
    g.ax.set_ylabel("Number of regulated gene")
    val = df.sort_values(["reg", "factor"], ascending=[False, True])[community.name].to_list()
    for p, v in zip(g.ax.patches, val):
        g.ax.annotate(v,
                       (p.get_x() + p.get_width() / 2., p.get_height()),
                       ha='center', va='center',
                       xytext=(0, 9),
                       textcoords='offset points')
    outfile = output / f"barplot_{factor_kind}_{feature}_{community.name}.pdf"
    g.set_xticklabels(rotation=90)
    g.savefig(outfile)



@lp.parse(ps=range(1, mp.cpu_count() + 1), feature=["gene", "exon"],
          factor_type=["splicing", "transcription"])
def create_barplot_figure(ps: int, feature: str = "gene",
                          factor_type: str = "splicing",
                          test_type: str = "permutation",
                          iteration: int = 10000,
                          logging_level: str = "INFO"):
    """
    Create barplots figures for every community figure

    :param ps: The number of threads to use
    :param feature: The feature we want to analyse (default 'gene')
    :param factor_type: The kind of factor to analyse (default 'splicing')
    :param test_type: The king of test to perform for frequency analysis. \
    (default 'lm') (choose from 'lm', 'permutation')
    :param iteration: The number of iteration to make
    :param logging_level: The level of information to display (default INFO)
    :return: The final dataframe that will be used to build the barplots
    """
    logging_def(ConfigGraph.community_folder, __file__, logging_level)
    ConfigGraph.output_barplots.mkdir(exist_ok=True)
    community_files = [c for c in ConfigClip.communities if c] + \
                      [get_community_file("", 2, 2, True, 1.2, "ALL",
                                          "gene", ".txt")]
    outfile = ConfigGraph.output_barplots / \
              f"table_{factor_type}_{feature}_{iteration}.txt"
    if not outfile.is_file():
        df = compute_full_table(ps, feature, community_files, factor_type,
                                test_type, iteration,
                                ConfigGraph.output_barplots)
        df.to_csv(outfile, sep="\t",index=False)
    else:
        logging.info("Table found ! recovering data")
        df = pd.read_csv(outfile, sep="\t")
    for com in community_files:
        create_barplot(df, feature, ConfigGraph.output_barplots, com,
                       factor_type)


if __name__ == "__main__":
    create_barplot_figure()