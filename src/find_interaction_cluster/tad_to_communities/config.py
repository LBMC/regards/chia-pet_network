#!/usr/bin/env python3

# -*- coding: UTF-8 -*-


"""
Description: Variables for the tad_to_communities directory
"""


from pathlib import Path
from ..config import Config as IConfig


class Config:
    """
    Variables for the tad_to_communities directory
    """
    data = Path(__file__).parents[3] / "data"
    results = Path(__file__).parents[3] / "results"
    tad = data / "figures_files" / "tad_to_communities" / "tad_annotations"
    target_gene = IConfig.bed_gene
    target_exon = IConfig.bed_exon
    intersect_tad_gene = results / "figures" / "tad_to_communities" / \
        "intersect_tad_gene"
    tad_communities = results / "figures" / "tad_to_communities" / \
        "tad_communities"
