#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: launch the tad_to_communities module
"""

from . tad_to_communities import main_tad_to_communities

main_tad_to_communities()
