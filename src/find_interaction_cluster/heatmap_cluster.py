#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create an heatmap showing \
the percentage of regulated exons/genes by a factor within each \
communities
"""
import pandas as pd

from .config import get_communities, ConfigGraph, get_community_file
from .clip_figures.config import ConfigClip
from .sf_and_communities import get_regulated_features, get_factors
from typing import List, Dict, Tuple
import sqlite3
import plotly
import plotly.graph_objects as go
import plotly.figure_factory as ff
from pathlib import Path
import numpy as np
import lazyparser as lp
from itertools import product
sch = __import__('scipy.cluster.hierarchy')


def percentage_regulated_in_clusters(regulated_features: List[str],
                                     cluster_feature: List[str]) -> float:
    """
    Get the percentage of regulated features within a cluster.

    :param regulated_features: Feature regulated by a factor
    :param cluster_feature: Feature co-localised in a cluster
    :return: The percentage of regulated features within a cluster.

    >>> reg_f = ["1", "2", "3"]
    >>> cluster_f = ["1", "5", "6", "7"]
    >>> percentage_regulated_in_clusters(reg_f, cluster_f)
    25.0
    """
    return len([f for f in cluster_feature if f in regulated_features]) / \
        len(cluster_feature) * 100


def get_percentage_4_clusters(cnx: sqlite3.connect,
                              dic_community: Dict[str, List[str]],
                              factor_name: str,
                              reg: str, feature: str,
                              factor_type: str = "splicing") -> pd.Series:
    """
    Get the percentage of regulated features by a factor for every \
    clusters.

    :param cnx: connection to chia-pet database
    :param dic_community: The list of features contained in each community
    :param factor_name: The name of the splicing factor
    :param reg: The regulation of interest
    :param feature: The kind of analysed feature
    :param factor_type: The kind of factor to analyse

    >>> cx = sqlite3.connect(ConfigGraph.db_file)
    >>> dc = {"C1": list(map(str, range(1, 11))),
    ... "C2": ["18077", "17475", "13643", "14114", "5358"]}
    >>> get_percentage_4_clusters(cx, dc, "SRSF1", "both", "gene", "splicing")
    C1      0.0
    C2    100.0
    Name: SRSF1, dtype: float64
    """
    regulated_features = get_regulated_features(cnx, factor_name, reg, feature,
                                                factor_type)
    dic_percentage = {c: percentage_regulated_in_clusters(regulated_features,
                                                          dic_community[c])
                      for c in dic_community}
    return pd.Series(dic_percentage, name=factor_name)


def get_heatmap_table(cnx: sqlite3.connect,
                      dic_community: Dict[str, List[str]],
                      reg: str, feature: str,
                      factor_type: str = "splicing", test: bool = False
                      ) -> pd.DataFrame:
    """
    Get the percentage of regulated features for every factors for every \
    clusters.

    :param cnx: connection to chia-pet database
    :param dic_community: The list of features contained in each community
    :param reg: The regulation of interest
    :param feature: The kind of analysed feature
    :param factor_type: The kind of factor to analyse
    :param test: True to just recover SRSF1 and SRSF2 factors else get every \
    factors

    >>> cx = sqlite3.connect(ConfigGraph.db_file)
    >>> dc = {"C1": list(map(str, range(1, 11))),
    ... "C2": ["18077", "17475", "13643", "14114", "5358"]}
    >>> get_heatmap_table(cx, dc, "both", "gene", "splicing", True)
            C1     C2
    SRSF1  0.0  100.0
    SRSF2  0.0   40.0
    """
    list_factors = ["SRSF1", "SRSF2"] if test else get_factors(factor_type)
    list_series = [get_percentage_4_clusters(cnx, dic_community,
                                             factor_name, reg, feature,
                                             factor_type)
                   for factor_name in list_factors]
    return pd.DataFrame(list_series)


def heatmap_fig_old(df: pd.DataFrame, outfile: Path, contrast: int = 100):
    """
    Create a heatmap.

    :param df: List of percentage of features regulated by a factor in \
    a given spatial cluster
    :param outfile: The name of the figure to produce
    :param contrast: (int) the value of the contrast
    """
    data_array = df.values
    labelsx = list(df.columns)
    labelsy = list(df.index)
    d = {labelsy[i]: {labelsx[j]: [i, j] for j in range(len(labelsx))}
         for i in range(len(labelsy))}
    # Initialize figure by creating upper dendrogram
    data_side = data_array.transpose()
    data_up = ff.create_dendrogram(
        data_side, orientation='bottom', labels=labelsx,
        linkagefun=lambda x: sch.cluster.hierarchy.linkage(x, 'average'))

    for i in range(len(data_up['data'])):
        data_up['data'][i]['yaxis'] = 'y2'

    # Create Side Dendrogram
    figure = ff.create_dendrogram(
        data_array, orientation='right', labels=labelsy,
        linkagefun=lambda x: sch.cluster.hierarchy.linkage(x, 'average'))
    for i in range(len(figure['data'])):
        figure['data'][i]['xaxis'] = 'x2'


    # Create Heatmap
    dendro_leaves = figure['layout']['yaxis']['ticktext']
    figure['layout']['xaxis']['ticktext'] = labelsx
    data_arrange = np.array([[None] * len(data_array[0])] * len(data_array))
    for i in range(len(dendro_leaves)):
        for j in range(len(labelsx)):
            vx = d[dendro_leaves[i]][labelsx[j]][0]
            vy = d[dendro_leaves[i]][labelsx[j]][1]
            data_arrange[i][j] = data_array[vx][vy]
    heatmap = [
        go.Heatmap(
            x=labelsx,
            y=dendro_leaves,
            z=data_arrange,
            colorbar={"x": -0.05},
            colorscale="Portland",
            # zmin=0,
            # zmax=contrast
        )
    ]
    figure['layout']['xaxis']['tickvals'] = \
        data_up['layout']['xaxis']['tickvals']
    heatmap[0]['x'] = data_up['layout']['xaxis']['tickvals']
    heatmap[0]['y'] = figure['layout']['yaxis']['tickvals']
    #
    # # Add Heatmap Data to Figure
    for data in heatmap:
        figure.add_trace(data)

    # Edit Layout
    figure['layout'].update({"autosize": True, "height": 1080, "width": 1920,
                             'showlegend': False, 'hovermode': 'closest',
                             })
    # Edit xaxis
    figure['layout']['xaxis'].update({'domain': [0.15, 0.8],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'showticklabels': True,
                                      'ticks': ""})
    # Edit xaxis2
    figure['layout'].update({'xaxis2': {'domain': [0, .15],
                                        'mirror': False,
                                        'showgrid': False,
                                        'showline': False,
                                        'zeroline': False,
                                        'showticklabels': False,
                                        'ticks': ""}})

    # Edit yaxis
    figure['layout']['yaxis'].update({'domain': [0.11, .85],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'showticklabels': True,
                                      'ticks': "",
                                      "side": "right"})
    # Edit yaxis2
    figure['layout'].update({'yaxis2': {'domain': [.825, 1],
                                        'mirror': False,
                                        'showgrid': False,
                                        'showline': False,
                                        'zeroline': False,
                                        'showticklabels': False,
                                        'ticks': ""}})
    plotly.offline.plot(figure, filename=str(outfile), auto_open=False)


def heatmap_fig(df: pd.DataFrame, outfile: Path, contrast: int = 100):
    """
    Create a heatmap.

    :param df: List of percentage of features regulated by a factor in \
    a given spatial cluster
    :param outfile: The name of the figure to produce
    :param contrast: (int) the value of the contrast
    """
    data_array = df.values
    labelsx = list(df.columns)
    labelsy = list(df.index)
    index_side_dic = {l: i for i, l in enumerate(labelsy)}
    index_up_dic = {l: i for i, l in enumerate(labelsx)}
    data_up = data_array.transpose()

    # Initialize figure by creating upper dendrogram
    fig = ff.create_dendrogram(data_up, orientation='bottom', labels=labelsx,
                               linkagefun=lambda x: sch.cluster.hierarchy.linkage(x, 'complete'))
    for i in range(len(fig['data'])):
        fig['data'][i]['yaxis'] = 'y2'

    # Create Side Dendrogram
    dendro_side = ff.create_dendrogram(data_array, orientation='right',
                                       labels=labelsy,
                                       linkagefun=lambda x: sch.cluster.hierarchy.linkage(x, 'complete'))
    for i in range(len(dendro_side['data'])):
        dendro_side['data'][i]['xaxis'] = 'x2'

    # Add Side Dendrogram Data to Figure
    for data in dendro_side['data']:
        fig.add_trace(data)


    # Create Heatmap
    dendro_side_leaves = dendro_side['layout']['yaxis']['ticktext']
    fig['layout']['yaxis']['ticktext'] = dendro_side['layout']['yaxis']['ticktext']
    index_side = [index_side_dic[l] for l in dendro_side_leaves]
    dendro_up_leaves = fig['layout']['xaxis']['ticktext']
    heat_data = data_array[index_side, :]
    index_up = [index_up_dic[l] for l in dendro_up_leaves]
    heat_data = heat_data[:, index_up]

    heatmap = [
        go.Heatmap(
            x=dendro_up_leaves,
            y=dendro_side_leaves,
            z=heat_data,
            colorbar={"x": -0.05},
            colorscale="Portland",
            # zmin=0,
            # zmax=contrast
        )
    ]
    heatmap[0]['x'] = fig['layout']['xaxis']['tickvals']
    fig['layout']['yaxis']['tickvals'] = dendro_side['layout']['yaxis']['tickvals']
    heatmap[0]['y'] = dendro_side['layout']['yaxis']['tickvals']

    #
    # # Add Heatmap Data to Figure
    for data in heatmap:
        fig.add_trace(data)

    # Edit Layout
    fig['layout'].update({"autosize": True, "height": 1080, "width": 1920,
                             'showlegend': False, 'hovermode': 'closest',
                             })
    # Edit xaxis
    fig['layout']['xaxis'].update({'domain': [0.15, 0.8],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'showticklabels': True,
                                      'ticks': ""})
    # Edit xaxis2
    fig['layout'].update({'xaxis2': {'domain': [0, .15],
                                        'mirror': False,
                                        'showgrid': False,
                                        'showline': False,
                                        'zeroline': False,
                                        'showticklabels': False,
                                        'ticks': ""}})

    # Edit yaxis
    fig['layout']['yaxis'].update({'domain': [0.11, .85],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'showticklabels': True,
                                      'ticks': "",
                                      "side": "right"})
    # Edit yaxis2
    fig['layout'].update({'yaxis2': {'domain': [.825, 1],
                                        'mirror': False,
                                        'showgrid': False,
                                        'showline': False,
                                        'zeroline': False,
                                        'showticklabels': False,
                                        'ticks': ""}})
    plotly.offline.plot(fig, filename=str(outfile), auto_open=False)


def get_defaults(community_files: List[str],
                 regulation: List[str], factor: List[str]
                 ) -> Tuple[List, List, List]:
    """
    Get the default value of the input parameters.

    :param community_files: The list of community files of interest \
    (default ())
    :param regulation: The list of regulations of the feature regulated \
    by a factor (default=())
    :param factor: List of factor type of interest \
    ("splicing" or "transcription") (default ())
    :return: The default values
    """
    if not community_files:
        community_files = [c for c in ConfigClip.communities if c] + \
                   [get_community_file("", 2, 2, True, 1.2, "ALL",
                                       "gene", ".txt")]
    else:
        community_files = [Path(c) for c in community_files]
    if not regulation:
        regulation = ["up", "down", "both"]
    if not factor:
        factor = ["trancription", "splicing"]
    return community_files, regulation, factor


@lp.parse(community_files="file", feature=["gene", "exon"],
          regulation=["up", "down", "both"],
          factor=["transcription", "splicing"])
def heatmap_creator(community_files: List[str] = (),
                    regulations: List[str] = (), factors: List[str] = (),
                    feature: str = "gene") -> None:
    """

    :param community_files: The list of community files of interest \
    (default ())
    :param regulations: The list of regulations of the feature regulated \
    by a factor (default=())
    :param factors: List of factor type of interest \
    ("splicing" or "transcription") (default ())
    :param feature: The kind of feature of interest (default "gene")
    """
    community_files, regulations, factors = get_defaults(community_files,
                                                         regulations, factors)
    threshold = 10 if feature == "gene" else 50
    data = list(product(community_files, regulations, factors))
    cnx = sqlite3.connect(ConfigGraph.db_file)
    ConfigGraph.output_heatmap.mkdir(exist_ok=True)
    for community, reg, factor in data:
        outfiles = [ConfigGraph.output_heatmap /
                    f"{community.name}_{reg}_{factor}{e}"
                    for e in [".txt", ".html"]]
        dic_community = get_communities(Path(community), feature, threshold)
        print(community, reg, factor, len(dic_community.keys()))
        df = get_heatmap_table(cnx, dic_community, reg, feature, factor)
        df.to_csv(outfiles[0], sep="\t", index=True)
        heatmap_fig(df, outfiles[1])


if __name__ == "__main__":
    heatmap_creator()
