#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This file contains the variables needed for this submodule
"""


from ..db_utils.config import Config


class ConfigCell:
    """Contains the variable needed for this submodule"""
    data = Config.data
    clip_folder = data / "CLIP_bed" / "original_bed"
    db_file = Config.db_file
    output = Config.results / "projects_cell_lines.txt"