#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This scripts generates a file containing the number \
of splicing lore, chia-pet db and clip projects we have for each cell lines
"""

from .config import ConfigCell
import doctest
import pandas as pd
import numpy as np
import sqlite3
from ..figures_utils.config_figures import get_good_project


def get_clip_cells() -> pd.DataFrame:
    """
    Get the cells used in each clip that we have.

    :return: a datafrme containing the number of time each cell is seen.

    >>> get_clip_cells().head()
           Cell  Clip_project
    0     Brain             4
    1       CD4             3
    2  CTRL.JCV             6
    3        H9             8
    4    HEK293            57
    """
    list_files = list(ConfigCell.clip_folder.glob("*.bed.gz"))
    list_cells = [f.name.split("_")[1] for f in list_files
                  if f.name.split("_")[1]]
    v, c = np.unique(list_cells, return_counts=True)
    return pd.DataFrame({"Cell": v, "Clip_project": c})


def get_sl_cells(cnx: sqlite3.Connection) -> pd.DataFrame:
    """
    Get the cells used in each splicing lore project.

    :param cnx: Connection to chia-pet db
    :return:  a dataframe indicating the number of project made on each \
    cell line

    >>> get_sl_cells(sqlite3.connect(ConfigCell.db_file)).head()
               Cell  Splicing_lore_project
    0          293T                     18
    1         786-O                      1
    2          A498                      1
    3          A673                      1
    4  EndoC-BetaH1                      1

    """
    cursor = cnx.cursor()
    query = """SELECT cl_name
               FROM cin_project_splicing_lore"""
    cursor.execute(query)
    res = cursor.fetchall()
    v, c = np.unique([v[0] for v in res], return_counts=True)
    cursor.close()
    return pd.DataFrame({"Cell": v, "Splicing_lore_project": c})


def get_chiapet_cells(cnx: sqlite3.Connection) -> pd.DataFrame:
    """
    Get dataframe indicating the number of ChIA-PET projects made \
    on each cell line

    :param cnx: connection to ChIA-PET db
    :return: a dataframe indicating the number of ChIA-PET projects made \
    on each cell line

    >>> get_chiapet_cells(sqlite3.connect(ConfigCell.db_file)).head()
          Cell  ChIA-PET_project
    0     BJAB                 2
    1  GM12878                 3
    2   HUVECs                 2
    3     HeLa                 3
    4     K562                 3

    """
    cursor = cnx.cursor()
    good_project = "('" + "', '".join(get_good_project()) + "')"
    query = f"""SELECT cell_line
                FROM cin_projects
                WHERE id_sample in {good_project}"""
    cursor.execute(query)
    res = cursor.fetchall()
    v, c = np.unique([v[0].replace("MCF-7", "MCF7") for v in res],
                     return_counts=True)
    cursor.close()
    return pd.DataFrame({"Cell": v, "ChIA-PET_project": c})


def create_cell_file():
    """
    Create a file containing the number of ChIA-PET, CLIP or Splicing Lore \
    projects made on each cell type.

    >>> create_cell_file()
    """
    cnx = sqlite3.connect(ConfigCell.db_file)
    df_clip = get_clip_cells()
    df_sl = get_sl_cells(cnx)
    df_cp = get_chiapet_cells(cnx)
    cnx.close()
    df = df_cp.merge(
        df_sl.merge(df_clip, how="outer", on="Cell"), how="outer", on="Cell")
    df = df.fillna(0)
    for col in df.columns:
        if "project" in col:
            df[col] = df[col].astype(int)
    df.to_csv(ConfigCell.output, sep="\t", index=False)


if __name__ == "__main__":
    doctest.testmod()