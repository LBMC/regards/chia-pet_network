#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create the table indicating  the number of ChIA-PET, CLIP or \
Splicing Lore projects made on each cell type.
"""

from .generate_cell_name_file import create_cell_file

create_cell_file()