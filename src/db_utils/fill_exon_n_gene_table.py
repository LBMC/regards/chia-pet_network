#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: Create the exon_ctrl file and the exon file
"""

from pathlib import Path
import pandas as pd
from ..logging_conf import logging_def
from .config import Config
import logging
from .populate_database import populate_df


def get_genomic_region(bed_file: Path) -> pd.DataFrame:
    """
    Create a file containing the control exons

    :param bed_file: A bed file containing intern exons
    :return: the bed file as a dataframe
    """
    df = pd.read_csv(bed_file, sep="\t",
                     names=["chromosome", "start", "stop", "id",
                            "score", "strand"])
    df = df.loc[(df.stop - df.start > 2), :]
    return df


def get_gene_table() -> pd.DataFrame:
    """
    Create the gene table.

    :return: The gene_table.
    """
    logging.debug("Load the bed file of genes")
    df_gene = get_genomic_region(Config.bed_gene)
    df_gene.columns = ["chromosome", "start", "stop", "id", "name", "strand"]
    logging.debug(df_gene.head())
    return df_gene


def get_exon_table(df_gene_name: pd.DataFrame) -> pd.DataFrame:
    """
    Create the exon_table

    :param df_gene_name: A datframe of gene id and gene name
    :return: The exon table
    """
    df_gene_name.columns = ['id_gene', 'gene_name']
    df_gene_name['id_gene'].astype(int)
    logging.debug("Load the bed file of exons")
    df_exon = get_genomic_region(Config.bed_exon)
    df_exon = df_exon.loc[df_exon['stop'] - df_exon['start'] > 2, :]
    logging.debug(df_exon.head())
    df_exon.drop('score', inplace=True, axis=1)
    logging.debug('Creating id_gene column ...')
    df_exon['id_gene'] = df_exon['id'].str.replace(r'_\d+$', '')
    df_exon['id_gene'] = df_exon['id_gene'].astype(int)
    logging.debug(df_exon.head())
    logging.debug("Creation of a column pos")
    df_exon['pos'] = df_exon['id'].str.replace(r'^\d+_', '')
    df_exon['pos'] = df_exon['pos'].astype(int)
    logging.debug(df_exon.head())
    logging.debug('Merging with gene name dataframe')
    df_exon = df_exon.merge(df_gene_name, how="left", on='id_gene')
    df_exon['name'] = df_exon['gene_name'] + '_' + \
        df_exon['pos'].astype('str')
    df_exon.drop('gene_name', inplace=True, axis=1)
    return df_exon


def main_fill_exon_n_gene(logging_level: str = "DISABLE") -> None:
    """
    Create the cin_exon and cin_gene table and insert them into \
    the ChIA-PET dabatbase.

    :param logging_level: The level of information to display
    """
    logging_def(Config.db_file.parent, __file__, logging_level)
    df_gene = get_gene_table()
    df_exon = get_exon_table(df_gene[['id', 'name']])
    logging.debug('Filling cin_gene table')
    populate_df(table='cin_gene', df=df_gene, clean='y')
    logging.debug('Filling cin_exon table')
    populate_df(table='cin_exon', df=df_exon, clean='y')


if __name__ == '__main__':
    main_fill_exon_n_gene('DEBUG')
