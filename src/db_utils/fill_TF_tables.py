#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to build tables used to \
store transcription factors (TF) data. One table will contains \
metadata about every TF projects and the other every gene differentially \
expressed when the TF is removed by shRNA
"""


from .config import Config
import pandas as pd
from typing import Dict, Tuple, List
from pathlib import Path
from ..logging_conf import logging_def
import logging
from .populate_database import populate_df
from statsmodels.stats.multitest import multipletests
import numpy as np
import subprocess as sp
from tqdm import tqdm


def recover_encode_project() -> Tuple[pd.DataFrame, Dict]:
    """
    Create the cin_project_TF table and return it with a dictionary \
    containing the link between EN_id and cin_id

    :return: The encore part cin_project_TF table
    >>> t, d = recover_encode_project()
    >>> t[["id", "project_name", "db_id_project",
    ... "tf_name", "cl_name", "knock_method"]].head()
       id             project_name db_id_project tf_name cl_name knock_method
    0   1    NKRF_ENCSR231PWH_K562   ENCSR231PWH    NKRF    K562        shRNA
    1   2   FUBP1_ENCSR608IXR_K562   ENCSR608IXR   FUBP1    K562        shRNA
    2   3   NUP35_ENCSR953IQF_K562   ENCSR953IQF   NUP35    K562        shRNA
    3   4   MARK2_ENCSR016OIX_K562   ENCSR016OIX   MARK2    K562        shRNA
    4   5  DNAJC2_ENCSR577OVP_K562   ENCSR577OVP  DNAJC2    K562        shRNA
    >>> {k: d[k] for k in list(d.keys())[0:5]}
    {1: 1, 4: 2, 5: 3, 7: 4, 10: 5}
    """
    df = pd.read_csv(Config.tf_metadata, sep=",")
    new_id = list(range(1, df.shape[0] + 1))

    dic_link = {v: new_id[i] for i, v in enumerate(df["EN_ID"].to_list())}
    df = df[["Accession", "Target_of_assay", "Biosample_term_name",
             "Assay title"]]
    df.columns = ["db_id_project", "tf_name", "cl_name", "knock_method"]
    df["source_db"] = ["Encode"] * df.shape[0]
    df["knock_method"] = df["knock_method"].str.replace(" RNA-seq", "")
    df["project_name"] = df["tf_name"] + "_" + df["db_id_project"] + "_" + \
        df["cl_name"]
    df['id'] = new_id
    return df[["id", "project_name", "source_db", "db_id_project",
               "tf_name", "cl_name", "knock_method"]], dic_link


def recover_knock_tf_project(first_id: int) -> Tuple[pd.DataFrame, Dict]:
    """
    Load all the metadata for every project in knockTF.

    :param first_id: The first ID of the project for this table
    :return: The knockTF part cin_project_TF table

    >>> t, d = recover_knock_tf_project(10)
    >>> t[["id", "project_name", "db_id_project",
    ... "tf_name", "cl_name", "knock_method"]].head()
       id          project_name db_id_project tf_name cl_name knock_method
    0  10    ESR1_GSE10061_MCF7      GSE10061    ESR1    MCF7        siRNA
    1  11  HNF1A_GSE103128_HuH7     GSE103128   HNF1A    HuH7        shRNA
    2  12  MLXIP_GSE11242_HA1ER      GSE11242   MLXIP   HA1ER        shRNA
    3  13   CREB1_GSE12056_K562      GSE12056   CREB1    K562        shRNA
    4  14  POU5F1_GSE12320_GBS6      GSE12320  POU5F1    GBS6        siRNA
    >>> {k: d[k] for k in list(d.keys())[0:5]} == {'DataSet_01_01': 10,
    ... 'DataSet_01_02': 11, 'DataSet_01_03': 12, 'DataSet_01_04': 13,
    ... 'DataSet_01_05': 14}
    True
    """
    df = pd.read_csv(Config.ktf_metadata, sep=",")
    df["DatasetID"] = df["DatasetID"].str.replace(r"_0(\d\d)$", "_\\1")
    df["db_id_project"] = df["ProfileID"]
    df["source_db"] = ["KnockTF"] * df.shape[0]
    df["id"] = list(range(first_id, first_id + df.shape[0]))
    df.rename({"TF": "tf_name", "Knock-Method": "knock_method",
               "BiosampleName": "cl_name"}, axis=1, inplace=True)
    df["cl_name"] = df["cl_name"].replace(" ", "-")
    df["project_name"] = df["tf_name"] + "_" + df["db_id_project"] + "_" + \
        df["cl_name"]
    dic_link = {d: i
                for i, d in zip(df["id"].to_list(), df["DatasetID"].to_list())}
    return df[["id", "project_name", "source_db", "db_id_project",
               "tf_name", "cl_name", "knock_method"]], dic_link


def create_cin_project_tf() -> Tuple[pd.DataFrame, Dict]:
    """
    Create the full cin_project_tf.

    :return: The full cin_project_tf table

    >>> t, di = create_cin_project_tf()
    >>> t[["id", "project_name", "db_id_project",
    ... "tf_name", "cl_name", "knock_method"]].head()
       id             project_name db_id_project tf_name cl_name knock_method
    0   1    NKRF_ENCSR231PWH_K562   ENCSR231PWH    NKRF    K562        shRNA
    1   2   FUBP1_ENCSR608IXR_K562   ENCSR608IXR   FUBP1    K562        shRNA
    2   3   NUP35_ENCSR953IQF_K562   ENCSR953IQF   NUP35    K562        shRNA
    3   4   MARK2_ENCSR016OIX_K562   ENCSR016OIX   MARK2    K562        shRNA
    4   5  DNAJC2_ENCSR577OVP_K562   ENCSR577OVP  DNAJC2    K562        shRNA
    >>> t.shape
    (619, 7)
    >>> t["id"].to_list() == list(range(1, 620))
    True
    >>> [di[k] for k in di] == list(range(1, 620))
    True
    """
    df, d = recover_encode_project()
    df2, d2 = recover_knock_tf_project(df.shape[0] + 1)
    df = pd.concat([df, df2], axis=0, ignore_index=True)
    return df, {**d, **d2}


def get_de_files() -> List[Path]:
    """
    Recover the list of files used to create the cin_de_event

    :return: the list of files used to create the cin_de_event

    >>> r = get_de_files()
    >>> len(r)
    49
    >>> [a.name for a in r[0:2]]
    ['condition_ZC3H8_CTRL_sig.csv', 'condition_NUP35_CTRL_sig.csv']
    """
    return list(Config.tf_folder.glob("*/condition*.csv"))


def create_cin_de_table_encode(list_files: List[Path], dic_id: Dict,
                               project_table: pd.DataFrame) -> pd.DataFrame:
    """
    Create the cin_DE_table for the encode part.

    :param list_files: The list of files used to build the cin_DE_table
    :param dic_id: Dictionary linking each cin id to EN id
    :param project_table: The table containing project metadata
    :return: The cin_DE_table

    >>> lf = get_de_files()
    >>> t, di = create_cin_project_tf()
    >>> r = create_cin_de_table_encode(lf, di, t)
    >>> r.head()[["gene_id", "id_project", "baseMean", "log2FoldChange"]]
       gene_id  id_project   baseMean  log2FoldChange
    0     1925          47  58.406946        6.663470
    1    10885          47  12.850435       -5.844456
    2     6393          47  20.921393       -5.524661
    3     9154          47  26.319160        4.897672
    4    11462          47  54.610968       -4.600871
    >>> r.head()[["pvalue", "padj"]]
             pvalue          padj
    0  5.632504e-10  9.098382e-09
    1  1.626786e-04  8.666283e-04
    2  2.508788e-06  2.051380e-05
    3  4.327869e-07  4.241582e-06
    4  1.398879e-15  4.614459e-14
    """
    df_list = []
    good_cols = ["gene_id", "id_project", "baseMean", "log2FoldChange",
                 "pvalue", "padj"]
    for cfile in list_files:
        df = pd.read_csv(cfile, sep=",")
        df.rename({"id_gene": "gene_id"}, axis=1, inplace=True)
        en_id = int(cfile.parent.name.split("_", 1)[0].replace("EN", ""))
        cin_id = dic_id[en_id]
        df["id_project"] = [cin_id] * df.shape[0]
        tf_name = project_table.loc[project_table["id"] == cin_id,
                                    "tf_name"].values[0]
        if tf_name != cfile.name.split("_")[1]:
            raise ValueError(f"The transcription factor {tf_name} is not in "
                             f"cfile {cfile.name} !")
        df = df[good_cols]
        df_list.append(df)
    df_final = pd.concat(df_list, axis=0, ignore_index=True).reset_index()
    df_final.rename({"index": "id"}, axis=1, inplace=True)
    df_final["id"] = df_final["id"] + 1
    return df_final


def create_cin_de_table_knocktf(first_id: int,
                                dic_id: Dict,
                                project_table: pd.DataFrame,
                                knocktf_file: Path = Config.ktf_db
                                ) -> pd.DataFrame:
    """
    Create the cin_DE_table.

    :param first_id: The first id of the table
    :param dic_id: Dictionary linking each cin id to knockTF id
    :param project_table: The table containing project metadata
    :param knocktf_file: The file containing the knocktf database
    :return: The cin_DE_table for knockTF

    >>> t, di = create_cin_project_tf()
    >>> r = create_cin_de_table_knocktf(10, di, t,
    ... Config.tests_files / "test_knockTF_database.gz")
    >>> r.head()[["id", "gene_name", "baseMean", "log2FoldChange",
    ... "pvalue", "padj"]]
       id gene_name     baseMean  log2FoldChange        pvalue      padj
    0  10      KRT4  1422.336650         4.92305  1.879500e-09  0.000007
    1  11     UPK1A   863.001320         3.14851  1.871940e-09  0.000007
    2  12    GABBR2   322.153520         3.08927  4.515910e-08  0.000027
    3  13     GPNMB  1750.062325         3.08417  3.425030e-09  0.000007
    4  14      PSCA   249.102200         2.79644  4.461110e-09  0.000007
    """
    logging.debug("loading knockTF database")
    df = pd.read_csv(knocktf_file, sep="\t", compression="gzip",
                     na_values='-')
    list_padj = []
    logging.debug("correcting p-values")
    for dataset in tqdm(df["Sample_ID"].unique(), desc="Adjusting p-values"):
        tmp = df[df["Sample_ID"] == dataset].fillna(1)
        list_padj += list(multipletests(tmp["P_value"], method='fdr_bh')[1])
    df["padj"] = list_padj
    logging.debug("Adjusting misc columns")
    df.loc[df["P_value"].isna(), "padj"] = np.nan
    df["id_project"] = df["Sample_ID"].map(dic_id)
    df['id'] = list(range(first_id, first_id + df.shape[0]))
    df["baseMean"] = (df["Mean Expr. of Treat"] +
                      df["Mean Expr. of Control"]) / 2
    logging.debug("checking Dataset")
    tmp = project_table[["id", "tf_name"]].rename({"id": "id_project"}, axis=1)
    df = df.merge(tmp, how="left", on="id_project")
    test = df[df["TF"] != df["tf_name"]][["Sample_ID", "TF", "tf_name"]]
    if not test.empty:
        raise ValueError(f"The transcription factor name differ between "
                         f"metadata and project: {tmp.shape}\n{tmp.head()}")
    df.rename({"Gene": "gene_name", "Log2FC": "log2FoldChange",
               "P_value": "pvalue"}, axis=1, inplace=True)
    logging.debug("updating gene names")
    alias2_symbol = load_hgnc()
    df["gene_name"] = df["gene_name"].apply(
        lambda x: alias2_symbol[x] if x in alias2_symbol else x)
    good_cols = ["id", "gene_name", "id_project", "baseMean", "log2FoldChange",
                 "pvalue", "padj"]
    return df[good_cols]


def find_bad_hgnc() -> List[str]:
    """
    Get the current symbol that are also in previous aliases columns.

    :return: Get the current symbol that are also in previous aliases columns.

    >>> v = find_bad_hgnc()
    >>> v[0:5]
    ['A1S9T', 'A2MR', 'A2MRAP', 'AA', 'AAC1']
    >>> len(v)
    4128
    """
    current_symbol = []
    old_symbol = []
    if not Config.hgnc_bad.is_file():
        with Config.hgnc_file.open("r") as hin:
            for line in hin:
                if "Approved symbol" not in line:
                    line = line.replace("\n", "").split("\t")
                    current_symbol.append(line[0])
                    aliases = line[1].split(", ") + line[2].split(", ")
                    old_symbol += [a for a in aliases if len(a) > 0]
        my_result = [c for c in current_symbol if c in old_symbol]
        res = ', '.join(c for c in current_symbol if c in old_symbol)
        Config.hgnc_bad.write_text(res)
    else:
        my_result = Config.hgnc_bad.open('r').read().split(", ")
    return my_result


def load_hgnc() -> Dict[str, str]:
    """
    Load every HGNC inside the HGNC file

    :return: A dictionary linking the old symbol to the new gene symbol

    >>> di = load_hgnc()
    >>> {k: di[k] for k in list(di.keys())[0:3]}
    {'NCRNA00181': 'A1BG-AS1', 'A1BGAS': 'A1BG-AS1', 'A1BG-AS': 'A1BG-AS1'}
    """
    if not Config.hgnc_file.is_file():
        cmd = f"curl -o {Config.hgnc_file} 'https://www.genenames.org/cgi-bin/download/custom?col=gd_app_sym&col=gd_prev_sym&col=gd_aliases&status=Approved&status=Entry%20Withdrawn&hgnc_dbtag=on&order_by=gd_app_sym_sort&format=text&submit=submit'"
        sp.check_call(cmd, shell=True)
    d = {}
    bad_hgnc = find_bad_hgnc()
    with Config.hgnc_file.open("r") as hin:
        for line in hin:
            if "Approved symbol" not in line:
                line = line.replace("\n", "").split("\t")
                try:
                    aliases = line[1].split(", ")
                except IndexError:
                    return line
                aliases = [a for a in aliases if len(a) > 0]
                factors = aliases + [line[0]]
                if len([x for x in factors if x in bad_hgnc]) == 0:
                    for a in aliases:
                        d[a] = line[0]
    return d


def load_gene_dic(gene_bed: Path) -> Dict[str, int]:
    """
    Create a dict linking gene name to fasterdb id

    :return: The dictionary linking fasterDB gene name to fasterdb gene id

    >>> di = load_gene_dic(Config.tests_files / "genes.bed")
    >>> di == {'DSC2': 1,
    ... 'DSC1': 2, 'DSG1': 3, 'DSG4': 4, 'KCTD4': 5, 'TPT1': 6,
    ... 'AC011260.1': 7, 'DCC': 8, 'SLC25A30': 9, 'DEFB132': 415,
    ... 'GLRA4': 10123}
    True
    """
    d = {}
    dic_hgnc = load_hgnc()
    id_to_del = []
    with gene_bed.open("r") as bedin:
        for line in bedin:
            line = line.strip().split("\t")
            fname = line[4]
            if fname in d:
                id_to_del.append(fname)
            else:
                fid = int(line[3])
                d[fname] = fid
    for fname in np.unique(id_to_del):
        del(d[fname])
    list_k = list(d.keys())
    for fname in list_k:
        if fname in dic_hgnc:
            fname2 = dic_hgnc[fname]
            if fname2 not in d.keys():
                d[fname2] = d[fname]
                del(d[fname])
    return d


def get_gene_id(df: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Replace the column gene_name by the column gene_id.

    :param df: A dataframe with a column gene_name
    :return: Convert the gene_name to fasterDB gene id

    >>> d = pd.DataFrame({"id": [1, 2, 3, 4, 5], "gene_name": ["DSC1",
    ... "BLOUP1", "TRA2A", "SRSF1", "SRSF3"], "id_project": [50] * 5,
    ... "baseMean": [1, 2, 3, 4, 5], "log2FoldChange": [0.1, 0.2, 0.3, 0.4,
    ... 0.5], "pvalue": [0.9, 0.5, 0.3, 0.1, 0.05], "padj": [0.9, 0.7, 0.5,
    ... 0.1, 0.08]})
    >>> d, r = get_gene_id(d)
    >>> d
       id  gene_id  id_project  baseMean  log2FoldChange  pvalue  padj
    0   1        2          50         1             0.1    0.90  0.90
    2   3     2635          50         3             0.3    0.30  0.50
    3   4    17995          50         4             0.4    0.10  0.10
    4   5    12420          50         5             0.5    0.05  0.08
    >>> r
       id_project  gene_lost  total_gene
    0          50          1           5
    """
    dic_id = load_gene_dic(Config.bed_gene)
    df["gene_id"] = df["gene_name"].map(dic_id)
    good_cols = ["id", "gene_id", "id_project", "baseMean",
                 "log2FoldChange", "pvalue", "padj"]
    res_na = df[df["gene_id"].isna()][["id_project", "id"]]\
        .groupby("id_project").count().reset_index()\
        .rename({"id": "gene_lost"}, axis=1)
    res_na = res_na.merge(df[["id_project", "id"]].groupby("id_project")
                          .count().reset_index()
                          .rename({"id": "total_gene"}, axis=1), how="left",
                          on="id_project")
    df = df[-df["gene_id"].isna()].copy()
    df["gene_id"] = df["gene_id"].astype(int)
    return df[good_cols], res_na


def create_cin_de_table(list_files: List[Path], dic_id: Dict,
                        project_table: pd.DataFrame,
                        test: bool = False) -> pd.DataFrame:
    """
    Create the cin_DE_table for the encode part.

    :param list_files: The list of files used to build the cin_DE_table
    :param dic_id: Dictionary linking each cin id to EN id
    :param project_table: The table containing project metadata
    :param test: True to run function for tests else False
    :return: The cin_DE_table
    >>> list_f = get_de_files()[0:2]
    >>> cin_project_tf, d_id = create_cin_project_tf()
    >>> cin_de_event = create_cin_de_table(list_f, d_id,
    ... cin_project_tf, True)
    >>> cin_de_event.drop("id", axis=1).tail()
           gene_id  id_project     baseMean  log2FoldChange   pvalue      padj
    11924     6724          50    28.782900        -0.00016  0.99856  0.998843
    11925     5191          50    62.007895        -0.00014  0.99859  0.998843
    11926    10155          50    33.594190         0.00013  0.99850  0.998843
    11927     6938          50   217.127705         0.00002  0.99976  0.999760
    11928    14026          50  2415.975315        -0.00002  0.99976  0.999760
    """
    logging.debug("Building encode de events table")
    df = create_cin_de_table_encode(list_files, dic_id, project_table)
    kd_bd = Config.tests_files / "test_knockTF_database.gz" if test \
        else Config.ktf_db
    logging.debug("Building knocktf de events table")
    df_kdtf = create_cin_de_table_knocktf(df.shape[0] + 1, dic_id,
                                          project_table, kd_bd)
    df_kdtf, gene_lost = get_gene_id(df_kdtf)
    gene_lost.to_csv(Config.gene_lost_knockft, sep="\t", index=False)
    return pd.concat([df, df_kdtf], axis=0, ignore_index=True)


def get_tf_tables() -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Get the cin_project_TF and the cin_DE_event Table

    :return: the cin_project_TF and the cin_DE_event Table

    """
    logging_def(Config.results, __file__, "DEBUG")
    if not Config.tf_output_de.is_file() or \
            not Config.tf_output_metadata.is_file():
        Config.tf_output_de.parent.mkdir(exist_ok=True)
        list_files = get_de_files()
        logging.debug("Building cin_project_tf table")
        cin_project_tf, dic_id = create_cin_project_tf()
        logging.debug("Building cin_de_event")
        cin_de_event = create_cin_de_table(list_files, dic_id,
                                           cin_project_tf)
        cin_project_tf.to_csv(Config.tf_output_metadata, sep="\t", index=False)
        cin_de_event.to_csv(Config.tf_output_de, sep="\t", index=False,
                            compression="gzip")
    else:
        cin_project_tf = pd.read_csv(Config.tf_output_metadata, sep="\t")
        cin_de_event = pd.read_csv(Config.tf_output_de, sep="\t",
                                   compression="gzip")
    good_projects = cin_de_event[-cin_de_event["padj"].isna()]["id_project"]\
        .unique()
    all_projects = cin_de_event["id_project"].unique()
    logging.warning(f"keeping {len(good_projects)} / {len(all_projects)}")
    cin_de_event = cin_de_event[cin_de_event["id_project"].isin(good_projects)]
    cin_project_tf = cin_project_tf[cin_project_tf["id"].isin(good_projects)]
    return cin_project_tf, cin_de_event


def fill_tf_data(logging_level: str = 'DISABLE') -> None:
    """
    Fill the tables cin_de_event and cin_project_tf
    """
    logging_def(Config.results, __file__, logging_level)

    cin_project_tf, cin_de_event = get_tf_tables()
    logging.debug('Filling cin_project_tf')
    populate_df(table='cin_project_tf', df=cin_project_tf, clean='y')
    logging.debug('Filling cin_de_event')
    populate_df(table='cin_de_event', df=cin_de_event, clean='y')


if __name__ == "__main__":
    get_tf_tables()
    # import doctest
    # doctest.testmod()
