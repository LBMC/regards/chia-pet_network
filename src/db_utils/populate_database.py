#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: This file can be used to populate the database of the website.
"""


import sqlite3
from .config import Config
from ..logging_conf import logging_def
from pathlib import Path
from typing import List, Tuple, Union
import lazyparser as lp
import logging
import pandas as pd


class TableNameError(Exception):
    pass


class ColumnsNumberError(Exception):
    pass


class ColumnsNameError(Exception):
    pass


def get_number_columns(table: str, cnx: sqlite3.Connection) -> List[str]:
    """
    Get the number of columns in the table ``table``.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param cnx: Connection to the database
    :return: The columns number
    """
    cursor = cnx.cursor()
    query = f"PRAGMA table_info({table});"
    cursor.execute(query)
    cols_data = cursor.fetchall()
    cursor.close()
    return [col[1] for col in cols_data]


def check_file(table: str, file: Union[Path, pd.DataFrame],
               cnx: sqlite3.Connection) -> List[Tuple]:
    """
    Check is the file has alwas the same nu_mber of columns and if \
    it match the number of columns find in the table ``table``.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param file: A tabulated file containing the data to insert in the table \
    tab or a pandas dataframe.
    :param cnx: Connection to the database
    :return: The row in file.
    """
    column_names = get_number_columns(table, cnx)
    df = pd.read_csv(file, sep="\t") if isinstance(file, Path) else file
    if len(df.columns) != len(column_names):
        msg = "Wrong number of columns"
        logging.exception(msg)
        raise ColumnsNumberError(msg)
    if sorted(df.columns) != sorted(column_names):
        msg = f"some of the columns name in your tabulated " \
              f"file {column_names} " \
              f"and the columns name of the database table {table} : " \
              f"{df.columns} differs"
        logging.exception(msg)
        raise ColumnsNameError(msg)
    df = df[column_names]
    return df.values


def clean_table(table: str, cnx: sqlite3.Connection) -> None:
    """
    Remove every data in the table ``table``.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param cnx: Connection to the database
    """
    cursor = cnx.cursor()
    query = f"DELETE FROM {table}"
    cursor.execute(query,)
    cnx.commit()
    cursor.close()


def insert_data(table: str, content: List[Tuple], cnx: sqlite3.Connection
                ) -> None:
    """
    Insert the data in the database.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param content: The content to inject in ``table``
    :param cnx: Connection to the database
    """
    cursor = cnx.cursor()
    v = ",".join(list("?" * len(content[0])))
    query = f"INSERT INTO {table} VALUES ({v});"
    cursor.executemany(query, content)
    cnx.commit()
    cursor.close()


def get_table_names(cnx: sqlite3.Connection) -> List[str]:
    """
    Get the list of available table names.

    :param cnx: The connection to ChIA-PET database.

    :return: The list of availbale tables
    """
    c = cnx.cursor()
    c.execute("""SELECT name FROM sqlite_master WHERE type = 'table';""")
    res = c.fetchall()
    res = [r[0] for r in res]
    return res


def check_table_name(cnx: sqlite3.Connection, table: str):
    """
    Check if we can use the table name `table`.

    :param cnx: Connection to ChIA-PET database
    :param table: The name of the table to fille
    :return: The same name with the prefix gin if it wasn't here.
    """
    tables = get_table_names(cnx)
    if table not in tables:
        msg = f"The name {table} is not available." \
              f" If the table exist in the database, " \
              f"change the config file to add the table name " \
              f"wanted in 'tables' field"
        logging.exception(msg)
        raise TableNameError(msg)
    return table


def cleaning(cnx: sqlite3.Connection, clean: str, table: str) -> None:
    """
    Clean the table ``table`` if asked.

    :param cnx: Connection to ChIA-PET database
    :param clean: Y to clean the database
    :param table: The name of the table to fill in the database
    """
    if clean.upper() == "Y":
        logging.debug("Cleaning table")
        clean_table(table, cnx)


def check_content_clean_and_insert(table: str, df: Union[Path, pd.DataFrame],
                                   cnx: sqlite3.Connection, clean: str):
    """
    Check content of df, clean database if needed and fill database.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param df: A tabulated file containing the data to insert in the table \
    tab or a pandas dataframe.
    :param cnx: Connection to ChIA-PET database
    :param clean: y to remove the data in the table, n else.
    :return:
    """
    logging.debug("Checking file ...")
    content = check_file(table, df, cnx)
    cleaning(cnx, clean, table)
    logging.debug("Inserting data ...")
    insert_data(table, content, cnx)


def populate_df(table: str, df: pd.DataFrame, clean: str):
    """
    Update the content of the database of the web interface.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param df: A tabulated file containing the data to insert in the table \
    tab.
    :param clean: y to remove the data in the table, n else.
    """
    cnx = sqlite3.connect(Config.db_file)
    table = check_table_name(cnx, table)
    check_content_clean_and_insert(table, df, cnx, clean)


@lp.parse(file="file", clean=["y", "Y", "n", "N"])
def populate(table: str, file: str, clean: str, logging_level: str =
             "DISABLE"):
    """
    Update the content of the database of the web interface.

    :param table: The name of the table in the database in which we want \
    to add data.
    :param file: A tabulated file containing the data to insert in the table \
    tab.
    :param clean: y to remove the data in the table, n else.
    :param logging_level: The level of information to display
    """
    logging_def(Config.db_file.parent, __file__, logging_level)
    mfile = Path(file)
    cnx = sqlite3.connect(Config.db_file)
    table = check_table_name(cnx, table)
    check_content_clean_and_insert(table, mfile, cnx, clean)


if __name__ == "__main__":
    populate()
