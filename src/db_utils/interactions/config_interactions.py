#!/usr/bin/env python3

# -*- coding: UTF-8 -*-


"""
Description: Configuration variables for subfolder interactions.
"""

from ..config import Config
from pathlib import Path


class ConfigInteractions:
    """
    Configuration variable for this subfolder
    """
    exon = Config.data / 'bed' / 'exon.bed'
    gene = Config.data / 'bed' / 'gene.bed'
    chr_sizes_hg19 = Config.data / 'interactions_files' / \
                                   'chr_sizes_hg19_nochr.txt'
    exon_window = 200
    gene_window = 200
    chia_pet_interaction = Config.results / 'interactions'
    exon_output = chia_pet_interaction / 'targets' / \
                                         f'exon_w{exon_window}.bed'
    gene_output = chia_pet_interaction / 'targets' / \
                                         f'gene_w{gene_window}.bed'
    chia_pet = Config.data / 'interactions_files' / 'chia_pet'
    pet_vs_exon_output = chia_pet_interaction / 'intersections_exon'
    pet_vs_gene_output = chia_pet_interaction / 'intersections_gene'
    couple_exon = chia_pet_interaction / 'couple_exon'
    couple_gene = chia_pet_interaction / 'couple_gene'
    results = Path(__file__).parents[3] / "results"
    db_file = results / 'chia_pet_database.db'
