#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script allows to determine which couples of genomic regions
interact, e.g. exons or genes, that for each ChIA-PET dataset.

This script requires the output file produced by the script bedtools.py, which
is the result of the intersection, between the genes or exons BED file with the
duplicate BED6 file of ChIA-PET data, that is in the following format:
#Columns of the exons file and then columns of the ChIA-PET data file, e.g.
18 28681 28882 1_1 0 - 18 28682 28782 1:47797..47799-18:28682..28782,2 2 . 100

#Exons: chr=18  start=28681 end=28882   id=1_1  0=0 strand=-
#ChIA-PET: chr2=18  start2=28682    end2=28782
#chr1:start1..end1-chr2:start2..end2,weight1-2=1:47797..47799-18:28682..28782,2
#weight1-2=2 .=. number of base pairs of overlap between exons and ChIA-PET=100
"""


import pandas as pd
from .config_interactions import ConfigInteractions as Config
import logging
from ...logging_conf import logging_def
from itertools import product
from tqdm import tqdm
from typing import Dict, List
import re
import numpy as np
import os.path
import sys
import sqlite3
from ..populate_database import populate_df


MY_ID = sys.argv[1]

PET = Config.chia_pet / f"{MY_ID}.bed"
FILE_1 = os.path.split(PET)[1].split(".")[0]

if sys.argv[2] == "--exon":
    OPT_GENE = "off"
    OPT_EXON = "on"
    INTER_FILE = Config.pet_vs_exon_output / f"exon_w200_vs_{MY_ID}.bed"
    FILE_2 = ((os.path.split(INTER_FILE)[1].split(".")[0]).split("vs_")[1])
    MY_OUT_FILE = Config.couple_exon / f"{MY_ID}_couple_exon.txt"
elif sys.argv[2] == "--gene":
    OPT_GENE = "on"
    OPT_EXON = "off"
    INTER_FILE = Config.pet_vs_gene_output / f"gene_w200_vs_{MY_ID}.bed"
    FILE_2 = ((os.path.split(INTER_FILE)[1].split(".")[0]).split("vs_")[1])
    MY_OUT_FILE = Config.couple_gene / f"{MY_ID}_couple_gene.txt"


def work_on_pet():
    """
    This works on the duplicate BED6 files of ChIA-PET data. Input format is:
    #chr1 start1 end1 chr1:start1..end1-chr2:start2..end2,weight1-2 weight1-2 .
    #chr2 start2 end2 chr1:start1..end1-chr2:start2..end2,weight1-2 weight1-2 .
    We want the following output format:
    #anchor1                    anchor2                 weight
    #chr1:start1..end1          chr2:start2..end2       weight1-2
    10:100172432..100175026     6:92192672..92194172    2

    :return: Pet in this format: chr1:start1..end1 chr2:start2..end2 weight1-2
    """
    pet = pd.read_csv(PET, sep="\t", header=None)
    pet.drop_duplicates(subset=3, inplace=True)
    pet = pet.iloc[:, 3].str.split(r"-|,", expand=True)
    pet.columns = ["anchor1", "anchor2", "weight"]
    return pet


def del_overlaps(pet: pd.DataFrame):
    """
    This works on the previous dataframe result (pet). Input format is:
    #anchor1                    anchor2                 weight
    #chr1:start1..end1          chr2:start2..end2       weight1-2
    We delete from this dataframe the pet that has overlapping anchors, e.g.
    9:139773532..139778733      9:139778161..139781850  7

    :param pet: In this format: chr1:start1..end1 chr2:start2..end2 weight1-2
    :return: Pet dataframe without pet that have overlapping anchors (partial
    and complete)
    """
    pet[["chr1", "start1", "space1", "end1"]] = pet["anchor1"].str.\
        split(r"[:..]", expand=True)
    pet[["chr2", "start2", "space2", "end2"]] = pet["anchor2"].str.\
        split(r"[:..]", expand=True)
    pet = pet.drop(["anchor1", "anchor2", "space1", "space2"], axis=1)
    pet.loc[pet["chr1"] != pet["chr2"], "delete"] = "no"
    # Removal of a partial overlap
    pet.loc[(pet["chr1"] == pet["chr2"]) & ((pet["start1"].astype(int) >=
                                             pet["start2"].astype(int)) &
                                            (pet["start1"].astype(int) <=
                                             pet["end2"].astype(int)) |
                                            (pet["end1"].astype(int) >=
                                             pet["start2"].astype(int)) &
                                            (pet["end1"].astype(int) <=
                                             pet["end2"].astype(int))),
            "delete"] = "yes"
    # Removal of a complete overlap, e.g. anchor1 is fully included in anchor2
    pet.loc[(pet["chr1"] == pet["chr2"]) & ((pet["start1"] >= pet["start2"]) &
                                            (pet["end2"] >= pet["end1"]) |
                                            (pet["start1"] <= pet["start2"]) &
                                            (pet["end2"] <= pet["end1"])),
            "delete"] = "yes"
    to_del = pet[pet.delete == "yes"].index.tolist()
    pet = pet.drop(to_del)
    pet["anchor1"] = pet["chr1"] + ":" + pet["start1"] + ".." + pet["end1"]
    pet["anchor2"] = pet["chr2"] + ":" + pet["start2"] + ".." + pet["end2"]
    pet.drop(["chr1", "start1", "end1", "chr2", "start2", "end2", "delete"],
             inplace=True, axis=1)
    pet = pet[["anchor1", "anchor2", "weight"]]
    return pet


def work_on_intersection():
    """
    This works on the output file produced by the script bedtools.py. The input
    format is (see the description of this script for more information), e.g.
    18 28681 28882 1_1 0 - 18 28682 28782 1:47797..47799-18:28682..28782,2 2 .
    100
    We return a dictionary which link the id of the pet with the region
    (exon/gene) it contains, e.g. '17:73176122..73178842': ['19423_1',
    '19423_2']

    :return: A dictionary which link the id of the pet with the region
    (exon/gene) it contains.
    """
    dic = {}
    with INTER_FILE.open("r") as infile:
        for line in infile:
            line = line.strip("\n").split("\t")
            id_anchor = f"{line[6]}:{line[7]}..{line[8]}"
            if id_anchor not in dic.keys():
                dic[id_anchor] = [line[3]]
            else:
                if line[3] not in dic[id_anchor]:
                    dic[id_anchor].append(line[3])
    return dic


def interactions(pet: pd.DataFrame, anchor_dic: Dict[str, List[str]]):
    """
    Allows to determine which couples of genomic regions interact, according to
    what weight.
    #id_region_1 id_region_2  id_anchor_1       id_anchor_2       level  weight
    9815_1       9815_7       1:858942..862596  1:874802..878017  intra  4
    It means that exon 9815_1 interacts with exon 9815_7, according to a weight
    of 4 and that the interaction is intrachromosome.

    :param pet: del_overlaps() return = pet dataframe without pet that have
    overlapping anchors.
    :param anchor_dic: work_on_intersection() return = a dictionary which link
    the id of the pet with the region (exon/gene) it contains.
    :return: A dataframe with these columns: id_region_1, id_region_2,
    id_anchor_1, id_anchor_2, level, weight
    """
    pet_dic = pet.to_dict("index")
    pbar = tqdm(pet_dic.keys())
    couples_list = []
    pattern = re.compile(r":\S+")
    for index in pbar:
        anchor1 = pet_dic[index]["anchor1"]
        anchor2 = pet_dic[index]["anchor2"]
        try:
            region1 = anchor_dic[anchor1]
            region2 = anchor_dic[anchor2]
            couples = list(product(region1, region2))
            couples = filtering_1(couples)
            clen = len(couples)
            if clen == 0:
                continue
            couples = np.c_[couples, [anchor1] * clen, [anchor2] * clen,
                            [get_level(anchor1, anchor2, pattern)] * clen]
            couples_df = pd.DataFrame(couples, columns=["id_region_1",
                                                        "id_region_2",
                                                        "id_anchor_1",
                                                        "id_anchor_2",
                                                        "level"])
            couples_list.append(couples_df)
        except KeyError:
            continue
    df_final = pd.concat(couples_list, axis=0, ignore_index=True)
    df_final = df_final.merge(pet, how="left",
                              left_on=["id_anchor_1", "id_anchor_2"],
                              right_on=["anchor1", "anchor2"])
    df_final.drop(["anchor1", "anchor2"], axis=1, inplace=True)
    df_final.rename(columns={2: "weight"}, inplace=True)
    return df_final


def get_level(anchor1: str, anchor2: str, pattern: re.Pattern) -> str:
    """
    Look if anchor_1 and anchor_2 (so also region_1 and region_2) are on the
    same chromosome or not, so if the interaction is intrachromosome or
    interchromosome.

    :param anchor1: The id of an anchor
    :param anchor2: The mate of anchor1
    :param pattern: A regex pattern
    :return:
    """
    if re.sub(pattern, "", anchor1) == re.sub(pattern, "", anchor2):
        return "intra"
    else:
        return "inter"


def filtering_1(region_lists: List) -> List:
    """
    Remove pairs of interacting regions that would be the same, e.g.
    #id_region_1    id_region_2    weight
    9815_1    9815_1   2

    :param region_lists: List of couple of regions
    :return: Region_lists without pairs of interacting regions that would be
    the same
    """
    return [sorted(couple) for couple in region_lists
            if couple[0] != couple[1]]


def filtering_2(df_filter_2: pd.DataFrame):
    """
    Use the result of the "interactions" function to add the weights of the
    same pairs of interactions, e.g.
    #id_region_1   id_region_2   id_anchor_1   id_anchor_2   level   weight
    9815_1    16755_2   10:10019900..10020058   11:5834473..5834631 inter   2
    9815_1    16755_2   10:10019900..10020088   11:5834422..5834625 inter   2
    --> #id_region_1    id_region_2     weight  level
    --> 9815_1          16755_2         4       inter

    :param df_filter_2: Result of the "interactions" function
    :return: df_filter_2 with weights added, when it describes the same pairs
    of interactions.
    """
    df_filter_2.drop_duplicates(inplace=True)
    df_filter_2["id"] = df_filter_2["id_region_1"].astype(str) + "$" + \
        df_filter_2["id_region_2"].astype(str)
    df_filter_2.drop(["id_anchor_1", "id_anchor_2", "id_region_1",
                      "id_region_2"], axis="columns", inplace=True)
    df_filter_2["weight"] = df_filter_2["weight"].astype(int)
    df_filter_2 = df_filter_2[["weight", "id", "level"]].groupby(
        ["id", "level"]).sum().reset_index(drop=False)
    df_filter_2[["id_region_1", "id_region_2"]] = df_filter_2.id.str.\
        split("$", expand=True)
    del df_filter_2["id"]
    if OPT_EXON == "on":
        df_filter_2.columns = ["level", "weight", "exon1", "exon2"]
        df_filter_2 = df_filter_2.reindex(columns=["exon1", "exon2", "weight",
                                                   "level"])
    elif OPT_GENE == "on":
        df_filter_2.columns = ["level", "weight", "gene1", "gene2"]
        df_filter_2 = df_filter_2.reindex(columns=["gene1", "gene2", "weight",
                                                   "level"])
    df_filter_2 = df_filter_2.reset_index().rename(columns={"index": "id"})
    if FILE_1 == FILE_2:
        df_filter_2["id_project"] = FILE_1
        df_filter_2["id2"] = df_filter_2["id"].astype(str) + "_" + \
            df_filter_2["id_project"].astype(str)
        del df_filter_2["id"]
        df_filter_2.rename(columns={"id2": "id"}, inplace=True)
    return df_filter_2


def get_info_from_database(cnx: sqlite3.Connection, query: str) -> \
        pd.DataFrame:
    """
    Get the exons and the genes information from the database, for example:
    - for the exons: 1_1	18	28681865	28682388
    - for the genes: 1	18	28645943	28682388

    :param cnx: connexion to the ChIA-PET database
    :param query: the SQL query that allows us to get data from the database
    :return df_res: the dataframe with the data obtained
    """
    cursor = cnx.cursor()
    cursor.execute(query)
    res = list(cursor.fetchall())
    df_res = pd.DataFrame(res, columns=["ID", "chr", "start", "stop"])
    return df_res


def add_info_distance_between_features(df: pd.DataFrame) -> pd.DataFrame:
    """
    Allows to calculate the distance between the two genes or the two exons
    studied in interaction and to add this information in the result dataframe.
    If the result is NULL it is because we study two exons or genes located in
    the same chromosome.
    If the result is 0, it is because we study two exons or genes which have
    different identifiers, but strictly identical coordinates OR two exons or
    genes which overlap (partially or completely).

    :param df: Result of the "filtering_2" function
    :return df: df with distances added or NULL or (null) see before for more
    details.
    """
    info_exon = get_info_from_database(sqlite3.connect(Config.db_file),
                                       """SELECT id, chromosome, start, stop 
                                       FROM cin_exon""")
    info_gene = get_info_from_database(sqlite3.connect(Config.db_file),
                                       """SELECT id, chromosome, start, stop 
                                       FROM cin_gene""")
    if OPT_EXON == "on":
        df = df.merge(info_exon, left_on="exon1", right_on="ID")
        df = df.rename(columns={'ID': 'ID1', 'chr': 'chr1', 'start': 'start1',
                                'stop': 'stop1'})
        df = df.merge(info_exon, left_on="exon2", right_on="ID")
        df = df.rename(columns={'ID': 'ID2', 'chr': 'chr2', 'start': 'start2',
                                'stop': 'stop2'})
        df.loc[df["start1"] < df["start2"], "distance"] = \
            (df["start2"] - df["stop1"] + 1)
        df.loc[df["stop1"] < df["stop2"], "distance"] = \
            (df["start2"] - df["stop1"] + 1)
        df.loc[df["start1"] > df["start2"], "distance"] = \
            (df["start1"] - df["stop2"] + 1)
        df.loc[df["stop1"] > df["stop2"], "distance"] = \
            (df["start1"] - df["stop2"] + 1)
        df.loc[(df["start1"] == df["start2"]) & (df["stop1"] == df["stop2"]),
               "distance"] = 0
        # Removal of a partial overlap
        df.loc[(df["chr1"] == df["chr2"]) & ((df["start1"] <= df["start2"]) &
                                             (df["start2"] <= df["stop1"]) |
                                             (df["start1"] <= df["stop2"]) &
                                             (df["stop2"] <= df["stop1"])),
               "distance"] = 0
        # Removal of a complete overlap, e.g. exon1 is fully included in exon2
        df.loc[(df["chr1"] == df["chr2"]) & ((df["start1"] >= df["start2"]) &
                                             (df["stop2"] >= df["stop1"]) |
                                             (df["start1"] <= df["start2"]) &
                                             (df["stop2"] <= df["stop1"])),
               "distance"] = 0
        df.loc[df["chr1"] != df["chr2"], "distance"] = "NULL"
        df.drop(["start1", "ID1", "chr1", "stop1", "ID2", "chr2", "start2",
                 "stop2"], axis='columns', inplace=True)
    elif OPT_GENE == "on":
        info_gene["ID"] = info_gene["ID"].astype(str)
        df = df.merge(info_gene, left_on="gene1", right_on="ID")
        df = df.rename(columns={'ID': 'ID1', 'chr': 'chr1', 'start': 'start1',
                                'stop': 'stop1'})
        df = df.merge(info_gene, left_on="gene2", right_on="ID")
        df = df.rename(columns={'ID': 'ID2', 'chr': 'chr2', 'start': 'start2',
                                'stop': 'stop2'})
        df.loc[df["start1"] < df["start2"], "distance"] = \
            (df["start2"] - df["stop1"] + 1)
        df.loc[df["stop1"] < df["stop2"], "distance"] = \
            (df["start2"] - df["stop1"] + 1)
        df.loc[df["start1"] > df["start2"], "distance"] = \
            (df["start1"] - df["stop2"] + 1)
        df.loc[df["start1"] > df["start2"], "distance"] = \
            (df["start1"] - df["stop2"] + 1)
        df.loc[df["stop1"] > df["stop2"], "distance"] = \
            (df["start1"] - df["stop2"] + 1)
        df.loc[(df["start1"] == df["start2"]) & (df["stop1"] == df["stop2"]),
               "distance"] = 0
        # Removal of a partial overlap
        df.loc[(df["chr1"] == df["chr2"]) & ((df["start1"] <= df["start2"]) &
                                             (df["start2"] <= df["stop1"]) |
                                             (df["start1"] <= df["stop2"]) &
                                             (df["stop2"] <= df["stop1"])),
               "distance"] = 0
        # Removal of a complete overlap, e.g. exon1 is fully included in exon2
        df.loc[(df["chr1"] == df["chr2"]) & ((df["start1"] >= df["start2"]) &
                                             (df["stop2"] >= df["stop1"]) |
                                             (df["start1"] <= df["start2"]) &
                                             (df["stop2"] <= df["stop1"])),
               "distance"] = 0
        df.loc[df["chr1"] != df["chr2"], "distance"] = "NULL"
        df.drop(["start1", "ID1", "chr1", "stop1", "ID2", "chr2",
                 "start2", "stop2"], axis='columns', inplace=True)
    return df


def create_interaction_table(logging_level: str = "DISABLE"):
    """
    Create the interaction tables.

    :return: The table of interaction
    """
    logging_def(Config.chia_pet_interaction, __file__, logging_level)
    Config.couple_exon.mkdir(exist_ok=True, parents=True)
    Config.couple_gene.mkdir(exist_ok=True, parents=True)
    logging.debug("Reading of intersection between genomic regions and an "
                  "anchor")
    anchor_dic = work_on_intersection()
    logging.debug("Getting anchor couples (PET) and weight")
    df = work_on_pet()
    logging.debug(df.head())
    logging.debug("Removing anchor couples (PET) overlapping")
    df = del_overlaps(df)
    logging.debug(df.head())
    logging.debug("Linking genomic regions interacting with each other")
    df = interactions(df, anchor_dic)
    logging.debug(df.head())
    logging.debug("Sum weight of identical interaction")
    df = filtering_2(df)
    logging.debug(df.head())
    df = add_info_distance_between_features(df)
    logging.debug(df.head())
    df.to_csv(MY_OUT_FILE, index=False, sep="\t", header=True)
    if OPT_EXON == "on":
        logging.debug("Filling cin_exon_interaction")
        populate_df(table="cin_exon_interaction", df=df, clean="n")
    elif OPT_GENE == "on":
        logging.debug("Filling cin_gene_interaction")
        populate_df(table="cin_gene_interaction", df=df, clean="n")
    return df


if __name__ == "__main__":
    create_interaction_table("DEBUG")
