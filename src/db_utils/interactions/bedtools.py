#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: This script requires that ChIA-PET data must be in the following
format: duplicated BED6, in which one line corresponds to an anchor of a PET
and two lines in succession correspond to the two anchors of the same PET:
#chrA startA endA chrA:startA..endA-chrB:startB..endB,weightA-B weightA-B .
1   712493   714023   1:712493..714023-22:43009914..43011424,2   2   .
#chrB startB endB chrA:startA..endA-chrB:startB..endB,weightA-B weightA-B .
22   43009914   43011424   1:712493..714023-22:43009914..43011424,2   2   .

This script allows to launch bedtools slop commands, in order to obtain two
BED files: one with exons and one with genes, with theirs coordinates set to
a window that we want to study, e.g. -200;+200.

Then this script allows to launch bedtools intersect commands, in order to
obtain one file with the list of exons that are matching with anchors of PETs
and one file with the list of genes that are matching with anchors of PETs.
These two files are generated for each project of ChIA-PET analyzed.
"""

import subprocess
from .config_interactions import ConfigInteractions as Config
from pathlib import Path


def launch_bedtools_slop(target_bed_file: Path, target_window: int,
                         target_output: Path) -> None:
    """
    Launch bedtools slop commands, in order to obtain two BED files: one with \
    exons and one with genes, with theirs coordinates set to a window that we \
    want to study, e.g. -200;+200.

    :param target_bed_file: A bed file which contains the coordinates of the \
    targets that we want to study, e.g. genes.
    :param target_window: The nucleotide window that we add to ours targets.
    :param target_output: The output bed file of our target with its changed \
    coordinates.
    """
    subprocess.check_call(f"bedtools slop -i {target_bed_file} " f"-g "
                          f"{Config.chr_sizes_hg19} " f"-b {target_window} " 
                          f"> {target_output}", shell=True,
                          stderr=subprocess.STDOUT)


def launch_bedtools_intersect(target_output: Path, output_repository: Path):
    """
    Launch bedtools intersect commands, in order to obtain one file with the \
    list of exons that are matching with anchors of PETs and one file with \
    the list of genes that are matching with anchors of PETs. These two files \
    are generated for each project of ChIA-PET analyzed.

    :param target_output: The output bed file of our target with its changed \
    coordinates.
    :param output_repository: The output repository in which the bedtools \
    intersect command result files are stored.
    """
    subprocess.check_call(f"for files in `ls {Config.chia_pet}" f"/*` ; do "
                          f"basename_files=$(basename -s .bed " f"$files) ; "
                          f"" f"basename_output=$(basename -s .bed "
                          f"{target_output}) ; " f"bedtools intersect -wo -a "
                          f"{target_output} " f"-b $files > " 
                          f"{output_repository}/" "${basename_output}_vs_"
                          "${basename_files}.bed ; done", shell=True,
                          stderr=subprocess.STDOUT)


def main_bedtools() -> None:
    """
    Launch bedtools slop and bedtools intersect commands for exons and genes.

    """
    Config.exon_output.parent.mkdir(exist_ok=True, parents=True)
    Config.pet_vs_exon_output.mkdir(exist_ok=True, parents=True)
    Config.pet_vs_gene_output.mkdir(exist_ok=True, parents=True)
    launch_bedtools_slop(Config.exon, Config.exon_window, Config.exon_output)
    launch_bedtools_slop(Config.gene, Config.gene_window, Config.gene_output)
    launch_bedtools_intersect(Config.exon_output,
                              Config.pet_vs_exon_output)
    launch_bedtools_intersect(Config.gene_output,
                              Config.pet_vs_gene_output)


if __name__ == "__main__":
    main_bedtools()
