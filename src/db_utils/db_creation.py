#!/usr/bin/env python3


# -*- coding: utf-8 -*-

"""
Creation of the database using Sqlite3 and SQL commands
"""


import sqlite3
from .config import Config
import logging
from ..logging_conf import logging_def


def create_cin_gene_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_gene.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_gene
            ([name] VARCHAR(30) NOT NULL,
            [id] INT NOT NULL,
            [chromosome] VARCHAR(2) NOT NULL,
            [start] INT NOT NULL,
            [stop] INT NOT NULL,
            [strand] VARCHAR(1) NOT NULL,
            PRIMARY KEY ([id]))''')
    conn.commit()


def create_cin_exon_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_exon.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_exon
                 ([id] VARCHAR(30) NOT NULL,
                 [pos] INT NOT NULL,
                 [name] VARCHAR(45) NOT NULL,
                 [id_gene] INT NOT NULL,
                 [chromosome] VARCHAR(2) NOT NULL,
                 [start] INT NOT NULL,
                 [stop] INT NOT NULL,
                 [strand] VARCHAR(1) NOT NULL,
                 PRIMARY KEY ([id])
                 FOREIGN KEY ([id_gene]) REFERENCES cin_gene([id]))''')
    conn.commit()


def create_cin_projects_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_project.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_projects
                 ([id_sample] VARCHAR(60) NOT NULL,
                 [id_project] VARCHAR(45) NOT NULL,
                 [database] VARCHAR(45) NOT NULL,
                 [name] VARCHAR(60) NULL,
                 [description] TEXT NULL,
                 [antibody] VARCHAR(45) NOT NULL,
                 [cell_line] VARCHAR(45) NOT NULL,
                 [institute] VARCHAR(45) NULL,
                 [citation] VARCHAR(20) NULL,
                 PRIMARY KEY ([id_sample]))''')
    conn.commit()


def create_cin_gene_frequency_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_gene_frequency.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_gene_frequency
                 ([id] INT NOT NULL,
                 [ft] VARCHAR(3) NOT NULL,
                 [ft_type] VARCHAR(3) NOT NULL,
                 [id_gene] INT NOT NULL,
                 [region] VARCHAR(10) NOT NULL,
                 [frequency] FLOAT NULL,
                 PRIMARY KEY ([id]),
                 FOREIGN KEY ([id_gene]) REFERENCES cin_gene([id]))''')
    conn.commit()


def create_cin_exon_frequency_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_exon_frequency.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_exon_frequency
            ([id] INT NOT NULL,
            [ft] VARCHAR(50) NOT NULL,
            [ft_type] VARCHAR(10) NOT NULL,
            [id_exon] VARCHAR(30) NOT NULL,
            [frequency] FLOAT NULL,
            PRIMARY KEY ([id]),
            FOREIGN KEY ([id_exon]) REFERENCES cin_exon([id]))''')
    conn.commit()


def create_cin_exon_interaction_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_exon_interaction.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_exon_interaction
                ([id] INT NOT NULL,
                [weight] INT NOT NULL,
                [exon1] VARCHAR(30) NOT NULL,
                [exon2] VARCHAR(30) NOT NULL,
                [id_project] INT NOT NULL,
                [level] VARCHAR(25) NOT NULL,
                [distance] INT,
                PRIMARY KEY ([id]),
                FOREIGN KEY ([exon1]) REFERENCES cin_exon([id]),
                FOREIGN KEY ([exon2]) REFERENCES cin_exon([id]),
                FOREIGN KEY ([id_project]) 
                REFERENCES cin_projects([id_sample]))''')
    conn.commit()


def create_cin_gene_interaction_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_gene_interaction.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_gene_interaction
                ([id] INT NOT NULL,
                [weight] INT NOT NULL,
                [gene1] INT NOT NULL,
                [gene2] INT NOT NULL,
                [id_project] INT NOT NULL,
                [level] VARCHAR(25) NOT NULL,
                [distance] INT,
                PRIMARY KEY ([id]),
                FOREIGN KEY ([gene1]) REFERENCES cin_gene([id]),
                FOREIGN KEY ([gene2]) REFERENCES cin_gene([id]),
                FOREIGN KEY ([id_project]) 
                REFERENCES cin_projects([id_sample]))''')
    conn.commit()


def create_cin_project_splicing_lore_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_project_splicing_lore

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_project_splicing_lore
                ([id] INT NOT NULL,
                [project_name] VARCHAR(45) NULL,
                [source_db] VARCHAR(45) NOT NULL,
                [db_id_project] VARCHAR(15) NOT NULL,
                [sf_name] VARCHAR(45) NOT NULL,
                [cl_name] VARCHAR(45) NOT NULL,
                PRIMARY KEY ([id]))''')
    conn.commit()


def create_cin_ase_event_table(conn: sqlite3.Connection) -> None:
    """
    Create table ase_event.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_ase_event
                ([id] INT NOT NULL,
                [id_project] INT NOT NULL,
                [gene_id] INT NOT NULL,
                [pos] INT NOT NULL,
                [exon_id] VARCHAR(30) NOT NULL,
                [delta_psi] FLOAT NULL,
                [pvalue] FLOAT NULL,
                [pvalue_glm_cor] FLOAT NULL,
                PRIMARY KEY ([id]),
                FOREIGN KEY ([exon_id]) REFERENCES cin_exon([id]),
                FOREIGN KEY ([gene_id]) REFERENCES cin_gene([id]),
                FOREIGN KEY ([id_project]) 
                REFERENCES cin_project_splicing_lore([id]))''')
    conn.commit()


def create_cin_project_tf_table(conn: sqlite3.Connection) -> None:
    """
    Create table cin_project_splicing_lore

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_project_tf
                ([id] INT NOT NULL,
                [project_name] VARCHAR(45) NULL,
                [source_db] VARCHAR(45) NOT NULL,
                [db_id_project] VARCHAR(15) NOT NULL,
                [tf_name] VARCHAR(45) NOT NULL,
                [cl_name] VARCHAR(45) NOT NULL,
                [knock_method] VARCHAR(20) NOT NULL,
                PRIMARY KEY ([id]))''')
    conn.commit()


def create_cin_de_event_table(conn: sqlite3.Connection) -> None:
    """
    Create table ase_event.

    :param conn: Connection to chia-pet database.
    """
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS cin_de_event
                ([id] INT NOT NULL,
                [id_project] INT NOT NULL,
                [gene_id] INT NOT NULL,
                [baseMean] FLOAT NULL,
                [log2FoldChange] FLOAT NULL,
                [pvalue] FLOAT NULL,
                [padj] FLOAT NULL,
                PRIMARY KEY ([id]),
                FOREIGN KEY ([gene_id]) REFERENCES cin_gene([id]),
                FOREIGN KEY ([id_project]) 
                REFERENCES cin_project_tf([id]))''')
    conn.commit()


def main_create_db(logging_level: str = "DISABLE"):
    """
    Create an empty chia-pet database.
    """
    logging_def(Config.db_file.parent, __file__, logging_level)
    conn = sqlite3.connect(Config.db_file)
    logging.debug('Creation of cin_gene')
    create_cin_gene_table(conn)
    logging.debug('Creation of cin_exon')
    create_cin_exon_table(conn)
    logging.debug('Creation of cin_project_gene')
    create_cin_projects_table(conn)
    logging.debug('Creation of cin_gene_frequency_table')
    create_cin_gene_frequency_table(conn)
    logging.debug('Creation of cin_exon_frequency_table')
    create_cin_exon_frequency_table(conn)
    logging.debug('Creation of cin_exon_interaction_table')
    create_cin_exon_interaction_table(conn)
    logging.debug('Creation of cin_gene_interaction_table')
    create_cin_gene_interaction_table(conn)
    logging.debug('Creation of cin_project_splicing_lore_table')
    create_cin_project_splicing_lore_table(conn)
    logging.debug('Creation of cin_ase_event_table')
    create_cin_ase_event_table(conn)
    logging.debug('Creation of cin_project_tf table')
    create_cin_project_tf_table(conn)
    logging.debug('Creation of cin_de_event table')
    create_cin_de_event_table(conn)


if __name__ == "__main__":
    main_create_db("DEBUG")
