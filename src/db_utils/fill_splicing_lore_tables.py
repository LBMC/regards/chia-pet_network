#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to fill the tables \
cin_ase_event and cin_project_splicing_lore
"""

from .config import Config
import pandas as pd
import logging
from ..logging_conf import logging_def
from .populate_database import populate_df


def load_splicing_projects() -> pd.DataFrame:
    """
    Load the projects of splicing lore database.

    :return: The dataframe of the splicing lore projects
    """
    df = pd.read_csv(Config.splicing_projects, sep="\t")
    logging.debug(df.head())
    return df


def load_ase_event() -> pd.DataFrame:
    """
    Load the alternative splicing events of detected in the splicing lore \
    database.

    :return: The dataframe of splicing lore alternative splicing events
    """
    logging.debug('loading cin_project_splicing_lore')
    df = pd.read_csv(Config.ase_event_file, sep="\t", dtype={"chromosome": 'object'})
    df.drop(["gene_symbol", "chromosome", "start", "stop", "exons_flanquants"],
            inplace=True, axis=1)
    cols = list(df.columns)
    if cols.index('exon_skipped') != -1:
        cols[cols.index('exon_skipped')] = 'pos'
        df.columns = cols
    df['exon_id'] = df['gene_id'].astype(str) + '_' + df['pos'].astype(str)
    logging.debug(df.head())
    return df


def fill_splicing_lore_data(logging_level: str = 'DISABLE') -> None:
    """
    Fill the tables cin_ase_event and cin_project_splicing_lore
    """
    logging_def(Config.results, __file__, logging_level)

    sf_projects = load_splicing_projects()
    ase_events = load_ase_event()
    logging.debug('Filling cin_project_splicing_lore')
    populate_df(table='cin_project_splicing_lore', df=sf_projects, clean='y')
    logging.debug('Filling cin_ase_event')
    populate_df(table='cin_ase_event', df=ase_events, clean='y')


if __name__ == "__main__":
    fill_splicing_lore_data('DEBUG')
