#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Contain configuration variables used for computing \
frequencies of exons, and gene
"""

from typing import List, Dict
from ..config import Config
import pandas as pd
from pathlib import Path


def full_defined(sequence: str) -> bool:
    """
    Says if all the nucleotide are well defined within the sequence

    :param sequence: (string) nucleotide sequence
    :return: (boolean) True if all nucleotide are well defined, False else
    """
    return "N" not in sequence


def coding_full_defined(sequence: str) -> bool:
    """
    Says if sequence is an amino adic sequenc fully defined.

    :param sequence: An amino acid sequence
    :return: True if all amino acid are well defined, False else
    """
    for aa in sequence:
        if aa in ['B', 'X', 'Z', 'J', 'U', 'O']:
            return False
    return True


def compute_nt_list() -> List[str]:
    """
    compute the list of nucleotides, di-nucelotides and tri-nucleotides of \
    interest.

    :return: list of nucleotides, di-nucelotides and tri-nucleotides
    """
    nt = ["A", "C", "G", "T", "S", "W", "R", "Y"]
    nt += [x + y for x in nt[0:4] for y in nt[0:4]]
    nt += [x + y + z for x in nt[0:4] for y in nt[0:4] for z in nt[0:4]]
    return nt


def get_property(property_file: Path) -> Dict[str, Dict[str, float]]:
    """

    :param property_file: a file containing the scale
    :return:  link each amino_acid \
    to it's score for each property scale
    """
    df = pd.read_csv(property_file, sep="\t", index_col=0)
    if not df.empty:
        dic = df.to_dict()
        return dic
    else:
        raise ValueError(f"The file {property_file} is empty !")


class Config_freq:
    """A class containing configurations variables used in this subfolder."""
    property_file = Config.data / 'chosen_scales.csv'
    exon_orf_bed = Config.data / 'bed' / 'exon_orf.bed'
    intron_bed = Config.data / 'bed' / 'intron.bed'
    output = Config.results / 'frequency_table'
    output_exon_file = output / 'exon_freq.txt.gz'
    output_gene_file = output / 'gene_freq.txt.gz'
    iupac_dic = {"S": "[GC]", "W": "[AT]", "K": "[GT]",
                 "M": "[AC]", "R": "[AG]", "Y": "[CT]"}
    nt_list = compute_nt_list()
    aa_list = list("RHKDESTNQCGPAVILMFYW")
    codon_list = [nt for nt in nt_list if len(nt) == 3]
    property_dic = get_property(property_file)
    hg19 = Config.data / "Homo_sapiens.GRCh37.dna.primary_assembly.fa"
