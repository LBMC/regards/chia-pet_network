#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Contains function dedicated to compute the nucleotide \
frequency of a sequence.
"""

from .config_freq import Config_freq, full_defined
from typing import Iterable, Dict, List
import logging
from Bio.Seq import Seq
import numpy as np
import regex as re


def frequencies(sequence: str, nt_list: Iterable[str]) -> Dict[str, float]:
    """
    Compute the frequencies of ``sequence`` for every nt in ``nt_list.

    :param sequence: (str) a nucleotide sequence
    :param nt_list: (a list of nt)
    :return: (dic) contains the frequence of every nucleotide
    """
    if not full_defined(sequence) or len(sequence) < 3:
        return {nt: np.nan for nt in nt_list}
    seql = len(sequence)
    dic = {}
    for n in nt_list:
        if n in Config_freq.iupac_dic.keys():
            pat = re.compile(Config_freq.iupac_dic[n])
        else:
            pat = re.compile(n)
        seqlen = seql - len(n) + 1
        dic[n] = round((len(list(re.findall(pat, sequence, overlapped=True)))
                        / seqlen) * 100, 5)
    return dic


def compute_dic(dic_seq: Dict[str, Seq], coord: List[str],
                nt_list: Iterable[str]) -> Dict[str, float]:
    """
    Get the frequencies of a sequence.

    :param dic_seq: (dictionary of seq object) a genome
    :param coord: (tuple of coordinate)
    :param nt_list: (str) list of nucleotide of interest
    :return: (dic) contains the frequence of every nucleotide
    """
    sequence = dic_seq[coord[0]][int(coord[1]): int(coord[2])]
    if coord[3] == "-":
        sequence = sequence.reverse_complement()
    return frequencies(str(sequence), nt_list)


def get_ft_type(ft: str) -> str:
    """
    From a feature get the feature type of interest.

    :param ft: The name of the feature of interest
    :return: The feature type of interest
    """
    if len(ft) == 1:
        return 'nt'
    elif len(ft) == 2:
        return 'dnt'
    elif len(ft) == 3:
        return 'tnt'
    else:
        msg = f'The len of parameter feature should be 1, 2, 3 not {len(ft)} !'
        logging.exception(msg)
        raise NameError(msg)
