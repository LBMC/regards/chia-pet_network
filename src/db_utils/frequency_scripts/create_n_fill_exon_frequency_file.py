#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create
"""

from Bio import SeqIO
from Bio.Seq import Seq
import logging
import logging.config
from typing import Dict, List, Tuple, Callable
from .config_freq import Config_freq, Config
from pathlib import Path
import pandas as pd
from .frequency_function import compute_dic, get_ft_type
from .orf_frequency_functions import mytranslate, get_codon_freq, \
    get_aa_freq, get_mean_propensity_score, get_orf_ft_type
from ...logging_conf import logging_def
from ..populate_database import populate_df
from tqdm import tqdm
import multiprocessing as mp
import numpy as np


HG19 = {"void": Seq("")}  # Global variable thta will store hg19 genome


def multiprocess_freq(line: str) -> pd.Series:
    """
    Get the frequency for the region given by the line of a bed file.

    :param line: A line of a bed file
    :return: The frequency values
    """
    line = line.replace("\n", "")
    sline: List[str] = line.split("\t")
    coord = [sline[0], sline[1], sline[2], sline[5]]
    dic_freq = compute_dic(HG19, coord, Config_freq.nt_list)
    dic_freq['id'] = sline[3]
    dic_freq['length'] = int(sline[2]) - int(sline[1])
    return pd.Series(dic_freq)


def multiprocess_freq_orf(line: str) -> pd.Series:
    exon: List = line.replace('\n', '').split('\t')
    exon[1] = int(exon[1])
    exon[2] = int(exon[2])
    if (exon[2] - exon[1]) % 3 != 0:
        msg = f"The length of {exon[3]} is not a multiple of 3 !"
        logging.exception(msg)
        raise IndexError(msg)
    nt_seq, aa_seq = mytranslate(HG19, [exon[0], exon[1],
                                 exon[2], exon[5]])
    if len(aa_seq) > 0 and "*" not in aa_seq:
        dic_codon = get_codon_freq(nt_seq, Config_freq.codon_list)
        dic_aa = get_aa_freq(aa_seq, Config_freq.aa_list)
        dic_prop = get_mean_propensity_score(aa_seq,
                                             Config_freq.property_dic)
        dic_freq = dict(dic_codon, **dic_aa, **dic_prop)
    else:
        ft_list = Config_freq.codon_list + Config_freq.aa_list + \
                  list(Config_freq.property_dic.keys())
        dic_freq = {cft: np.nan for cft in ft_list}
    dic_freq['id'] = exon[3]
    dic_freq['length'] = (exon[2] - exon[1]) / 3
    return pd.Series(dic_freq)


def adapt_exon_length(mseries: pd.Series) -> int:
    """
    Resize exon length if needed.

    :param mseries: A dataframe line
    :return: The size
    """
    if mseries['ft_type'] not in ['dnt', 'tnt']:
        return mseries['length']
    if mseries['ft_type'] == "dnt":
        return mseries['length'] - 1
    return mseries['length'] - 2


def get_freq_table(ps: int, bed_file: Path, mutli_fnc: Callable,
                   fnc_ft_type: Callable) -> pd.DataFrame:
    """
    Create a table with new frequency files.

    :param bed_file: a bed file
    :param ps: The number of process to create
    :param mutli_fnc: Function to execute
    :param fnc_ft_type: The function to get ft type name
    """
    id_col = f'id_{bed_file.name.replace(".bed", "").replace("_orf", "")}'
    pool = mp.Pool(processes=ps)
    processes = []
    with bed_file.open("r") as inbed:
        for line in inbed:
            arg = [line]
            processes.append(pool.apply_async(mutli_fnc, arg))
    pbar = tqdm(processes, desc=f'Processing {bed_file.name}')
    my_res = [proc.get(timeout=None) for proc in pbar]
    pool.close()
    pool.join()
    df = pd.DataFrame(my_res)
    df = df.melt(id_vars=["id", 'length'], var_name="ft",
                 value_name='frequency')
    df['ft_type'] = df['ft'].apply(fnc_ft_type)
    df['length'] = df.apply(adapt_exon_length, axis=1)
    df.rename(columns={'id': id_col}, inplace=True)
    return df


def load_hg19(hg19: Path) -> Dict[str, Seq]:
    """
    Load hg19 genome.

    :param hg19: The human genome
    :return: The sequence by chromosome in hg19
    """
    dic = {}
    for record in SeqIO.parse(hg19, 'fasta'):
        dic[record.id] = record.seq
    return dic


def compute_gene_orf_data(df: pd.DataFrame, ft_type_check: str = "aa",
                          ft_check: str = "A") -> pd.DataFrame:
    """
    Compute the frequency of codons, amino_acid and properties within \
    genes (corresponds to the concatenation of exons).

    :param df: Dataframe fo exon
    :param ft_type_check: The feature type used to check if exons are \
    not duplicated
    :param ft_check: The feature used to check if exons are not duplicated
    :return:
    """
    df['id_gene'] = df['id_exon'].str.replace(r'_\d+', '')
    df_exon = df.loc[((df['ft_type'] == 'nt') & (df['ft'] == 'A')) |
                     ((df['ft_type'] == 'dnt') & (df['ft'] == 'AA')) |
                     ((df['ft_type'] == 'tnt') & (df['ft'] == 'AAA')) |
                     ((df['ft_type'] == 'codon') & (df['ft'] == 'AAA')) |
                     ((df['ft_type'] == 'aa') & (df['ft'] == 'A')) |
                     ((df['ft_type'] == 'properties') &
                      (df['ft'] == 'Flexibility_Vihinen')), :]
    df_exon_tmp = df.loc[((df['ft_type'] == 'nt') & (df['ft'] == 'A')) |
                         ((df['ft_type'] == 'codon') & (df['ft'] == 'AAA')), :]
    res = df_exon_tmp.groupby('id_exon').count()['length'].values
    if np.max(res) != np.min(res) != 1:
        msg = "One exon must be present only once !"
        logging.exception(msg)
        raise ValueError(msg)
    df_exon = df_exon[["id_gene", "ft_type", "length"]]
    gene_len = df_exon[['id_gene', 'length', 'ft_type']]\
        .groupby(['id_gene', 'ft_type']).sum()\
        .reset_index()
    print(gene_len.loc[gene_len["id_gene"] == '10006', :])
    df = pd.merge(df, gene_len, how='left', on=["id_gene", "ft_type"],
                  suffixes=['_exon', '_gene'])
    df['new_freq'] = df['frequency'] * (df['length_exon'] / df['length_gene'])
    df_final = df[['id_gene', 'ft_type', 'ft', 'new_freq']].\
        groupby(['id_gene', 'ft_type', 'ft']).sum(min_count=1).reset_index()
    df_final.rename(columns={"new_freq": 'frequency'}, inplace=True)
    return df_final


def create_or_load_freq_table(ps: int) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Create frequency tables for gene and exons.

    :param ps: The number of processes to use to compute the frequency \
    table
    """
    global HG19
    if not Config_freq.output_exon_file.is_file() \
            or not Config_freq.output_gene_file.is_file():
        logging.debug('loading hg19 genome')
        HG19 = load_hg19(Config_freq.hg19)
    if not Config_freq.output_exon_file.is_file():
        logging.debug('get frequency table for exon')
        df_exon = get_freq_table(ps, Config.bed_exon,
                                 multiprocess_freq, get_ft_type)
        logging.debug(df_exon.head())
        logging.debug('Get orf frequency for exon')
        df_orf_exon = get_freq_table(ps, Config_freq.exon_orf_bed,
                                     multiprocess_freq_orf, get_orf_ft_type)
        logging.debug(df_orf_exon.head())
        df_exon = pd.concat([df_exon, df_orf_exon], axis=0, ignore_index=True,
                            sort=False)
        df_exon.to_csv(Config_freq.output_exon_file, sep="\t", index=False,
                       compression='gzip')
    else:
        logging.debug('Loading exon_freq file ...')
        df_exon = pd.read_csv(Config_freq.output_exon_file, sep="\t",
                              compression='gzip')
        df_orf_exon = df_exon.loc[
                      df_exon['ft_type'].isin(['codon', 'aa', 'properties']),
                      :].copy()
    if not Config_freq.output_gene_file.is_file():
        logging.debug('get frequency table gene')
        df_gene_tmp = get_freq_table(ps, Config.bed_gene,
                                     multiprocess_freq, get_ft_type)
        df_gene_tmp.drop('length', inplace=True, axis=1)
        logging.debug(df_gene_tmp.head())
        df_nt_exon = df_exon.loc[
                     df_exon['ft_type'].isin(['nt', 'dnt', 'tnt']),
                     :].copy()
        logging.debug('Get mean exon frequency for gene')
        df_exon_nt_gene = compute_gene_orf_data(df_nt_exon.copy(),
                                                ft_type_check='nt',
                                                ft_check='A')
        logging.debug(df_exon_nt_gene.head())
        logging.debug('Get mean exon orf frequency for gene')
        df_exon_orf_gene = compute_gene_orf_data(df_orf_exon)
        logging.debug(df_exon_orf_gene.head())
        logging.debug('Get mean intron frequency for gene')
        df_intron = get_freq_table(ps, Config_freq.intron_bed,
                                   multiprocess_freq, get_ft_type)
        df_intron.rename({'id_intron': 'id_exon'}, axis=1, inplace=True)
        logging.debug(df_intron.head())
        df_intron_gene = compute_gene_orf_data(df_intron, ft_type_check='nt',
                                               ft_check='A')
        logging.debug(df_intron_gene.head())
        df_gene = pd.concat([df_gene_tmp, df_exon_nt_gene, df_exon_orf_gene,
                             df_intron_gene],
                            axis=0, ignore_index=True,
                            sort=False)
        df_gene['region'] = ['gene'] * df_gene_tmp.shape[0] \
                            + ['exon'] * df_exon_nt_gene.shape[0] \
                            + ['exon'] * df_exon_orf_gene.shape[0] \
                            + ['intron'] * df_intron_gene.shape[0]
        df_gene.to_csv(Config_freq.output_gene_file, sep="\t", index=False,
                       compression='gzip')
    else:
        df_gene = pd.read_csv(Config_freq.output_gene_file, sep="\t",
                              compression='gzip')
    HG19 = None
    df_exon['frequency'] = round(df_exon['frequency'], 5)
    df_gene['frequency'] = round(df_gene['frequency'], 5)
    return df_exon, df_gene


def fill_frequency_tables(ps: int = 1, logging_level: str = 'DISABLE'):
    """
    Fill the frequency tables of exon and gene.

    :param ps: the number of process to create
    :param logging_level: The level of information to display
    """
    Config_freq.output.mkdir(parents=True, exist_ok=True)
    logging_def(Config_freq.output, __file__, logging_level)
    df_exon, df_gene = create_or_load_freq_table(ps)
    df_exon.drop('length', inplace=True, axis=1)
    df_exon.reset_index(inplace=True)
    df_exon.rename(columns={'index': 'id'}, inplace=True)
    logging.debug('Filling cin_exon_frequency')
    populate_df(table='cin_exon_frequency', df=df_exon, clean='y')
    logging.debug('Filling cin_gene_frequency')
    df_gene.reset_index(inplace=True)
    df_gene.rename(columns={'index': 'id'}, inplace=True)
    populate_df(table='cin_gene_frequency', df=df_gene, clean='y')


if __name__ == "__main__":
    fill_frequency_tables(1, 'DEBUG')
