#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script contains function to handle ORF
"""


from typing import List, Dict, Tuple
from Bio.Seq import Seq
from .config_freq import full_defined, coding_full_defined
import numpy as np
import logging


def get_codon_freq(nt_seq: str, codon_list: List[str]) -> Dict[str, float]:
    """
    Get the frequency of every amino acid.

    :param nt_seq: an amino acid sequence
    :param codon_list: a list of codon
    :return: a dictionary of float value corresponding to condon frequency \
    in nt_seq.
    """
    if not full_defined(nt_seq):
        return {codon: np.nan for codon in codon_list}
    dic = {}
    codon_seq = [nt_seq[i:i+3] for i in range(0, len(nt_seq) - 2, 3)]
    for codon in codon_list:
        dic[codon] = round(codon_seq.count(codon) / len(codon_seq) * 100, 5)
    return dic


def get_aa_freq(aa_seq: str, aa_list: List[str]) -> Dict[str, float]:
    """
    Get the frequency of every amino acid.

    :param aa_seq: an amino acid sequence
    :param aa_list: a list of amino acid
    :return: Frequency of each amino acid
    """
    if not coding_full_defined(aa_seq):
        return {aa: np.nan for aa in aa_list}
    dic = {}
    for aa in aa_list:
        dic[aa] = round(aa_seq.count(aa) / len(aa_seq) * 100, 5)
    return dic


def get_mean_propensity_score(aa_seq: str,
                              property_scale: Dict[str, Dict[str, float]]
                              ) -> Dict[str, float]:
    """
    Get the mean score for each scale.

    :param aa_seq: an amino acid sequence
    :param property_scale: link each scale to it's property.
    :return: The mean property values for the sequence aa_seq
    """
    if not coding_full_defined(aa_seq):
        return {s: np.nan for s in property_scale.keys()}
    dic = {}
    for scale in property_scale.keys():
        count = 0
        for aa in aa_seq:
            count += property_scale[scale][aa]
        dic[scale] = round((count / len(aa_seq)), 5)
    return dic


def mytranslate(dic_seq: Dict[str, Seq], coordinates: List[str]
                ) -> Tuple[str, str]:
    """
    Translate the sequence given by coordinates.

    :param dic_seq: a dictionary containing chromosome.
    :param coordinates: chr, start, stop strand
    :return: the sequence and the sequence translated
    """
    ntseq = dic_seq[str(coordinates[0])][int(coordinates[1]):
                                         int(coordinates[2])]
    if coordinates[3] == "+":
        seq = ntseq.translate(table=1)
    else:
        seq = ntseq.reverse_complement().translate(table=1)
        ntseq = ntseq.reverse_complement()
    if seq[-1] == "*":
        seq = seq[:-1]
    return str(ntseq), str(seq)


def get_orf_ft_type(ft: str) -> str:
    """
    From a feature get the feature type of interest.

    :param ft: The name of the feature of interest
    :return: The feature type of interest
    """
    if len(ft) == 1:
        return 'aa'
    elif len(ft) == 3:
        return 'codon'
    elif len(ft) > 3:
        return 'properties'
    else:
        msg = f'The len of parameter feature should be 1, 3, or >3' \
              f' not {len(ft)} !'
        logging.exception(msg)
        raise NameError(msg)