#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: Configuration class
"""

from pathlib import Path
import multiprocessing as mp


class Config:
    """
    A class containing every parameters used in the submodule db_utils
    """
    cpu = mp.cpu_count()
    data = Path(__file__).parents[2] / 'data'
    results = Path(__file__).parents[2] / "results"
    db_file = results / 'chia_pet_database.db'
    bed_exon = data / 'bed' / 'exon.bed'
    bed_gene = data / 'bed' / 'gene.bed'
    hgnc_file = data / 'hgnc.txt'
    hgnc_bad = data / 'hgnc_bad.txt'
    ase_event_file = data / 'splicing_lore_data' / 'ase_event.txt'
    splicing_projects = data / 'splicing_lore_data' / \
        'splicing_lore_projects.txt'
    chia_pet_files = data / 'interactions_files' / 'chia_pet'
    tests_files = Path(__file__).parents[2] / 'tests' / "files"
    tf_metadata = data / "TF_data" / "Encode" / "ENCODE_shRNA_metadata.csv"
    tf_folder = data / "TF_data" / "Encode" / "TF_DE_data"
    ktf_metadata = data / "TF_data" / "KnockTF-Browse.csv"
    ktf_db = data / "TF_data" / "knockTF_database.txt.gz"
    tf_output_de = results / 'tf_tables' / "cin_de_event.gz"
    gene_lost_knockft = results / 'tf_tables' / "gene_lost_knockft.txt"
    tf_output_metadata = results / 'tf_tables' / "cin_project_tf.txt"