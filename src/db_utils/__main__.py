#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description: Populate the gin database
"""

from .config import Config
from ..logging_conf import logging_def
from .db_creation import main_create_db
from .fill_exon_n_gene_table import main_fill_exon_n_gene
from .fill_splicing_lore_tables import fill_splicing_lore_data
from .frequency_scripts.create_n_fill_exon_frequency_file import \
    fill_frequency_tables
import logging
from .projects_metadata.get_fill_metadata import fill_projects_table
import subprocess
from .fill_TF_tables import fill_tf_data


def launch_features_interactions(opt: str):
    """
    Allows to launch the script features_interactions.py:
    - to determine which couples of genomic regions interact, e.g. exons or
    genes, that for each ChIA-PET dataset
    - to fill the cin_gene_interaction and cin_exon_interaction tables

    :param opt: which genomic regions are we studying, --exon or --gene
    """
    subprocess.check_call(f"for file in `ls {Config.chia_pet_files}" f"/*` ; "
                          f"do " f"basename_file=$(basename -s .bed " f"$file)"
                          f" ; " f"python -m "
                          f"src.db_utils.interactions.features_interactions " 
                          "${basename_file} " f"{opt} ; done", shell=True,
                          stderr=subprocess.STDOUT)


def launcher(logging_level: str = "INFO"):
    """
    Create and fill the database
    """
    logging_def(Config.db_file.parent, __file__, logging_level)
    logging.info('Database creation')
    main_create_db('DISABLE')
    logging.info('Filling cin_gene and cin_exon')
    main_fill_exon_n_gene('DISABLE')
    logging.info('Filling splicing lore tables')
    fill_splicing_lore_data('DISABLE')
    logging.info('Filling transcription factor tables')
    fill_tf_data('DISABLE')
    logging.info('Filling frequency tables')
    fill_frequency_tables(Config.cpu, 'DISABLE')
    logging.info('Filling projects table')
    fill_projects_table('DISABLE')
    logging.info('Filling cin_exon_interaction table')
    launch_features_interactions("--exon")
    logging.info('Filling cin_gene_interaction table')
    launch_features_interactions("--gene")


launcher(logging_level="DEBUG")
