#!/usr/bin/env python

# -*- coding: UTF-8 -*-


"""
Description: Configuration variables for subfolder projects_metadata
"""

from ..config import Config


class Config_metadata:
    """
    Configuration variable for this subfolder
    """
    geo_id_file = Config.data / 'metadata_files' / 'chia_pet_list_GSM.txt'
    output = Config.results / 'projects_metadata'
    metadata_file = Config.data / 'metadata_files' / 'chia_pet_list.csv'
    outfile = 'datasets'
