#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to download hic files from \
4dnucleome.org
"""

import pandas as pd
from ..config_tad_hic import ConfigHiC
from doctest import testmod
from pathlib import Path
import subprocess as sp
import hashlib
import logging
from typing import Optional
from ...logging_conf import logging_def
from ...find_interaction_cluster.tad_to_communities.tad_to_communities \
    import targeted_tad_to_communities


def read_hic_url_content() -> pd.DataFrame:
    """
    Read the content of HiC url

    :return: A table containing data about each files

    >>> d = read_hic_url_content()
    >>> d.shape
    (32, 24)
    >>> d["Experiment Set Accession"].to_list()[0:4]
    ['4DNES1E3ET5M', '4DNES54GS5KI', '4DNES61ZK6S8', '4DNES7N27U89']
    """
    content = []
    with ConfigHiC.hic_files_url.open("r") as ifile:
        for line in ifile:
            if "#" not in line:
                content.append(line.strip().split("\t"))
    return pd.DataFrame(content[1:], columns=content[0])


def add_cell_column(exp_table: pd.DataFrame) -> pd.DataFrame:
    """
    Add a column cell in the exp_table.

    :param exp_table: A table containing the url of hic files to download
    :return: The exp_table with a column cell.

    >>> d = read_hic_url_content()
    >>> add_cell_column(d)["cell"].to_list()[0:5]
    ['K652', 'K652', 'HeLa', 'HeLa', 'HeLa']
    """
    cell_list = []
    cell_selected = ["K562", "HepG2", "HeLa"]
    for i in range(exp_table.shape[0]):
        row = exp_table.iloc[i, :]
        res = row["Dataset"].split(" ")
        if "cells" in res:
            cell_list.append(res[res.index("cells") - 1])
        else:
            tmp = [x for x in cell_selected if x in res]
            if len(tmp) == 1:
                cell_list.append(tmp[0])
            else:
                cell_list.append(row["Condition"])
    exp_table["cell"] = cell_list
    return exp_table


def md5(fname: Path) -> str:
    """
    Compute md5_sum of the resulting file.
    :param fname: The filename
    :return: The md5sum of the filename
    """
    hash_md5 = hashlib.md5()
    with fname.open("rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def download_experiment(exp_row: pd.Series) -> Path:
    """

    :param exp_row: A row containing the data about an experiment
    :return: The path of the hic file downloaded
    """
    ConfigHiC.output_hic.mkdir(exist_ok=True, parents=True)
    file_name = f"{exp_row['cell']}_{exp_row['Project']}_" \
                f"{exp_row['File Accession']}_grch38.hic"
    outfile = ConfigHiC.output_hic / file_name

    cmd = f"curl -L {exp_row['File Download URL']} -o {outfile} --silent " \
          f"--user K2WPPNW5:qcxz67wca7pac5tl"
    if outfile.is_file():
        return outfile
    sp.check_call(cmd, shell=True)
    if exp_row["md5sum"] != "N/A":
        my_md5 = md5(outfile)
        if my_md5 != exp_row["md5sum"]:
            logging.warning(f"File {exp_row['File Accession']}: \n"
                            f"expected md5sum: {exp_row['md5sum']} \n"
                            f"    real md5sum: {my_md5}")
    return outfile


def arrowhead_launch(hic_file: Path, resolution: int) -> Optional[Path]:
    """
    Launch arrowhead on an hic file.

    :param hic_file: An hic file
    :param resolution: The resolution of interest
    :return: Tad calling with arrowHead
    """
    ConfigHiC.hic_tad_caller.mkdir(exist_ok=True, parents=False)
    outfolder = ConfigHiC.hic_tad_caller / hic_file.name
    outfile = outfolder / f"{resolution}_blocks.bedpe"
    if not outfile.is_file():
        cmd = f"java -jar {ConfigHiC.juicer_prog} arrowhead -r {resolution} " \
              f"-k KR {hic_file} {outfolder}"
        sp.check_call(cmd, shell=True)
        if not outfile.is_file():
            return None
    return outfile


def check_n_save_bed(bed_file: Path, name_file: str, resolution: int) -> Path:
    """
    Check if the bed file is ok.

    :param bed_file: A bed files containing TADs produced by arrowhead.
    :param name_file: The file name of the hic file used to produced \
    the file containing TADs.
    :param resolution: The resolution of interest
    :return: The path containing the bed file updated.
    """
    data = pd.read_csv(bed_file, sep="\t")
    outfile = bed_file.parent / \
        f"{resolution}_{name_file.replace('.hic', '_overlapped.bed')}"
    if outfile.is_file():
        return outfile
    df = data.loc[(data["chr1"] == data["chr2"]) &
                  (data["x1"] == data["y1"]) &
                  (data["x2"] == data["y2"]), :]
    if df.shape[0] != data.shape[0]:
        raise ValueError(f"The 3 first column of the bed file produced from "
                         f"{name_file} should be equal the 3 next columns")
    df = data[["chr1", "x1", "x2"]].copy()
    new_name = name_file.replace("_grch38.hic", "")
    df.sort_values(["chr1", "x1"], ascending=True, inplace=True)
    df["chr1"] = "chr" + df["chr1"].astype(str)
    df["name"] = [f"{new_name}_{x + 1}" for x in range(df.shape[0])]
    df["score"] = ["."] * df.shape[0]
    df["strand"] = ["+"] * df.shape[0]
    df.to_csv(outfile, sep="\t", index=False, header=False)
    return outfile


def liftover(tad_file: Path) -> Path:
    """
    LiftOver tads.

    :param tad_file: A simplified TAD file produced from ArrowHead
    :return: The bed file containing TAD with hg19 coordinates
    """
    ConfigHiC.lost_folder.mkdir(exist_ok=True, parents=True)
    outfile = tad_file.parents[1] / tad_file.name.replace('grch38', 'hg19')
    if not outfile.is_file():
        lost_file = ConfigHiC.lost_folder / outfile.name.replace(".bed",
                                                                 "_lost.bed")
        cmd = f"{ConfigHiC.lift_over} {tad_file} {ConfigHiC.chain} " \
              f"{outfile} {lost_file}"
        sp.check_call(cmd, shell=True)
    return outfile


def merge_tads(tad_files: Path, name_file: str) -> Path:
    """

    :param tad_files: A simplified TAD file produced from ArrowHead
    :param name_file: The file name of the hic file used to produced \
    the file containing TADs.
    :return: The file containing merged TADs
    """
    merged_tad = tad_files.parent / tad_files.name.replace("_overlapped.bed",
                                                           ".bed")
    cmd = f"bedtools merge -i {tad_files} -d -1 > {merged_tad}"
    sp.check_call(cmd, shell=True)
    df = pd.read_csv(merged_tad, sep="\t", names=["chr", "start", "stop"])
    new_name = name_file.replace("_grch38.hic", "")
    df["name"] = [f"{new_name}_{x + 1}" for x in range(df.shape[0])]
    df["score"] = ["."] * df.shape[0]
    df["strand"] = ["+"] * df.shape[0]
    df.to_csv(merged_tad, sep="\t", index=False, header=False)
    return merged_tad


def process_hic_files(exp_table: pd.DataFrame,
                      resolution: int = 10000) -> None:
    """
    Produce the tads for an experiment.

    :param exp_table: An experiment table of hic file selected
    :param resolution: The resolution of interest
    """
    for i in range(exp_table.shape[0]):
        row = exp_table.iloc[i, :]
        if row['File Accession'] in ["4DNFIJBJ6QVH", "4DNFITUOMFUQ"]:
            logging.info(f"Working on {row['File Accession']}")
            logging.info("    Downloading ...")
            hic_file = download_experiment(row)
            logging.info("    Calling TADs ...")
            tad_file = arrowhead_launch(hic_file, resolution)
            if tad_file is None:
                logging.warning("    No TADs detected (hic data too sparse ?)")
                continue
            logging.info("    Reformating TADs")
            tad_file = check_n_save_bed(tad_file, hic_file.name, resolution)
            logging.info("Merging tad")
            tad_file = merge_tads(tad_file, hic_file.name)
            logging.info("    LiftOver TADs")
            liftover_file = liftover(tad_file)
            logging.info("    Creating communities")
            final_file = liftover_file.parent / \
                f"communities_{liftover_file.name}"
            targeted_tad_to_communities(str(liftover_file),  str(final_file),
                                        "gene")


def calling_tads(resolution: int = 10000):
    """
    Calling TAD for every experiments.

    :param resolution: The resolution of interest
    """
    ConfigHiC.output_hic.mkdir(exist_ok=True, parents=True)
    logging_def(ConfigHiC.output_hic, __file__, "INFO")
    df = read_hic_url_content()
    df = add_cell_column(df)
    process_hic_files(df, resolution)


if __name__ == "__main__":
    testmod()
