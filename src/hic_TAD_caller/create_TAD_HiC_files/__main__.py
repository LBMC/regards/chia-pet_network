#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch TAD caller
"""

from .calling_tads import calling_tads
import lazyparser as lp
from typing import List


@lp.parse(resolution=[1000, 2000, 5000, 10000, 25000, 50000, 100000, 250000,
                      500000, 1000000, 2500000, 5000000, 10000000])
def launcher(resolutions: List[int] = 10000):
    """
    Call TADs for a list of 4DN nucleome project/

    :param resolution: The list of resolutions wanted of the Hi-C file
    """
    for resolution in resolutions:
        calling_tads(resolution)


launcher()