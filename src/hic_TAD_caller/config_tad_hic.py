#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Contains all the variables needed in this submodule
"""

from ..db_utils.config import Config


class ConfigHiC:
    """
    Contains the variables used in this submodule
    """
    juicer_url = "http://hicfiles.tc4ga.com.s3.amazonaws.com/public/juicer/"
    juicer_file = "juicer_tools.1.8.9_jcuda.0.8.jar"
    result = Config.results
    juicer_prog = result / "juicer_tools.jar"
    # output hic files
    output_arrowhead = result / "TAD_arrowhead"
    output_hic = output_arrowhead / "TAD_hic_files"
    hic_tad_caller = output_hic / "Tad_calling"
    lost_folder = hic_tad_caller / "lost_lifover"
    # input hic_files
    hic_files_url = Config.data / "HiC" / "metadata_2021-02-05-08h-42m.tsv"
    # LiftOver tool
    lift_over = Config.data / "LiftOver" / "liftOver"
    chain = Config.data / "LiftOver" / "hg38ToHg19.over.chain"