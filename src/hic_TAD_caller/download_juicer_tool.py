#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script aims to download juicer tools
"""


import subprocess as sp
from .config_tad_hic import ConfigHiC
from doctest import testmod


def download_juicer_tools():
    """
    Download juicer tools

    >>> download_juicer_tools()
    >>> res = ConfigHiC.result / "juicer_tools.jar"
    >>> res.is_file()
    True
    """
    url = ConfigHiC.juicer_url + ConfigHiC.juicer_file
    cmd = f"curl -L {url} -o {ConfigHiC.juicer_prog} --silent"
    sp.check_call(cmd, shell=True)


if __name__ == "__main__":
    testmod()